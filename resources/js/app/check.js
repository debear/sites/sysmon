/* Log Detail actions */
class LogDetail {
    // Toggle open
    static show(ele) {
        ele.classList.add('inc-body');
        // Prevent the background from scrolling
        DOM.child('body').classList.add('modal');
    }

    // Toggle close
    static hide(ele) {
        ele.closest('.detail-open').classList.remove('inc-body');
        // Allow the background to scroll again
        DOM.child('body').classList.remove('modal');
    }
}

/* Click Handler */
get('dom:load').push(() => {
    Events.attach(DOM.child('.paged_table'), 'click', (e) => {
        // Is this a target element for toggling log detail?
        var ele = e.target;
        if (ele.classList.contains('detail-open')) {
            // Toggle Open
            LogDetail.show(ele);
        } else if (ele.classList.contains('detail-close')) {
            // Toggle Closed
            LogDetail.hide(ele);
        }
    });
});
