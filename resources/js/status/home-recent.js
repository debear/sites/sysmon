get('dom:load').push(() => {
    Events.attach(document, 'debear:dropdown-set', (e) => {
        var memo = e.detail;
        if (memo.id != 'recent_group') {
            return;
        }
        // Turning on all options?
        if (memo.value == '') {
            DOM.child('.subnav-recent').querySelectorAll('dl.box.hidden').forEach((ele) => { ele.classList.remove('hidden'); });
            return;
        }
        var boxPrefix = 'group-';
        // Disable the existing selections
        DOM.child('.subnav-recent').querySelectorAll(`dl.box:not(.${boxPrefix}${memo.value}):not(.hidden)`).forEach((ele) => { ele.classList.add('hidden'); });
        // Enable the new filter
        DOM.child('.subnav-recent').querySelectorAll(`dl.box.${boxPrefix}${memo.value}.hidden`).forEach((ele) => { ele.classList.remove('hidden'); });
    });
});
