get('dom:load').push(() => {
    set('pagination:ajax', new Ajax());

    Events.attach(DOM.child('content'), 'click', (e) => {
        // Stop the event, as we'll deal with it all manually
        Events.cancel(e);

        // Get the link to process
        var ele = e.target.closest('.onclick_target');
        if (!Input.isDefined(ele)
            || !Input.isDefined(ele.closest('ul.pagination'))
            || !Input.isDefined(ele.href)
        ) {
            return;
        }

        // Previously cached?
        var pageNum = ele.dataset.page;
        var cachedPage = get(`cache:page:${pageNum}`);
        if (Input.isDefined(cachedPage)) {
            DOM.child('.paged_table').innerHTML = cachedPage;
            return;
        }

        // Make the Ajax request and update the content
        get('pagination:ajax').request({
            'method': 'GET',
            'url': ele.href,
            'extra': {
                'pageNum': pageNum
            },
            'success': (retValue, config) => {
                DOM.child('.paged_table').innerHTML = retValue;
                set(`cache:page:${config.extra.pageNum}`, retValue);
                ScrollTable?.resizeSetup();
            }
        });
    });

    // Initial cache
    set('cache:page:1', DOM.child('.paged_table').innerHTML);
});
