CREATE TABLE `SYSMON_CHECKS` (
  `check_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `group_id` TINYINT(3) UNSIGNED DEFAULT NULL,
  `status_id` TINYINT(3) UNSIGNED DEFAULT NULL,
  `script` ENUM('usage','comms','lockfile','server','logs','http_request','http_status','age') COLLATE latin1_general_ci NOT NULL,
  `script_args` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `instance` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `frequency` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `status_warn` INT(11) UNSIGNED DEFAULT NULL,
  `status_critical` INT(11) UNSIGNED DEFAULT NULL,
  `active` TINYINT(1) UNSIGNED DEFAULT 0,
  `notifications_on` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `notifications_freq` TINYINT(1) UNSIGNED DEFAULT NULL,
  `notifications_offset` TINYINT(3) UNSIGNED DEFAULT NULL,
  `last_checked` DATETIME DEFAULT NULL,
  `last_status` ENUM('unknown','okay','recovered','warn','critical') COLLATE latin1_general_ci DEFAULT NULL,
  `last_summary` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`check_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_CHECKS_GROUPS` (
  `group_id` TINYINT(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `order` TINYINT(2) UNSIGNED NOT NULL DEFAULT 0,
  `internal` TINYINT(1) UNSIGNED DEFAULT 0,
  `state` ENUM('unknown','okay','warn','critical') COLLATE latin1_general_ci DEFAULT NULL,
  `num_okay` TINYINT(2) UNSIGNED DEFAULT NULL,
  `num_warn` TINYINT(2) UNSIGNED DEFAULT NULL,
  `num_critical` TINYINT(2) UNSIGNED DEFAULT NULL,
  `num_unknown` TINYINT(2) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_CHECKS_STATUS_OVERRIDE` (
  `check_id` TINYINT(3) UNSIGNED NOT NULL,
  `sub_key` VARCHAR(100) COLLATE latin1_general_ci NOT NULL,
  `status_id` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`check_id`,`sub_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_LOG` (
  `check_id` TINYINT(3) UNSIGNED NOT NULL,
  `log_date` DATETIME NOT NULL,
  `started` DATETIME DEFAULT NULL,
  `finished` DATETIME DEFAULT NULL,
  `status` ENUM('unknown','okay','recovered','warn','critical') COLLATE latin1_general_ci NOT NULL DEFAULT 'unknown',
  `notified` ENUM('yes','no','disabled','suppressed') COLLATE latin1_general_ci NOT NULL,
  `info` VARCHAR(500) COLLATE latin1_general_ci DEFAULT NULL,
  `processed` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`check_id`,`log_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_NOTIFICATION_SILENT` (
  `check_id` TINYINT(3) UNSIGNED NOT NULL,
  `start` DATETIME NOT NULL,
  `end` DATETIME DEFAULT NULL,
  `num_suppressed` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`check_id`,`start`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_CHECKS_ADDITIONAL` (
  `addtl_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `script` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `script_args` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `timing` ENUM('pre','post') COLLATE latin1_general_ci NOT NULL,
  `frequency` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `active` TINYINT(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`addtl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
