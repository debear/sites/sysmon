CREATE TABLE `SYSMON_USAGE` (
  `checked` DATE NOT NULL,
  `total_usage` BIGINT(20) UNSIGNED DEFAULT 0,
  `remaining` INT(10) UNSIGNED DEFAULT 0,
  `email_debear` INT(10) UNSIGNED DEFAULT 0,
  `email_padd` INT(10) UNSIGNED DEFAULT 0,
  `backup` INT(10) UNSIGNED DEFAULT 0,
  `logs` INT(10) UNSIGNED DEFAULT 0,
  `merges` INT(10) UNSIGNED DEFAULT NULL,
  `sites_cdn` INT(10) UNSIGNED DEFAULT 0,
  `sites_other` INT(10) UNSIGNED DEFAULT 0,
  `db_sports` INT(10) UNSIGNED DEFAULT 0,
  `db_fantasy` INT(10) UNSIGNED DEFAULT 0,
  `db_other` INT(10) UNSIGNED DEFAULT 0,
  `other` INT(10) UNSIGNED DEFAULT 0,
  `status` ENUM('unknown','okay','recovered','warn','critical') COLLATE latin1_general_ci NOT NULL DEFAULT 'unknown',
  `notified` ENUM('yes','no','disabled','suppressed') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_COMMS` (
  `type` ENUM('email','twitter','twitter_dm','instagram','facebook') COLLATE latin1_general_ci NOT NULL,
  `date` DATE NOT NULL,
  `num_queued` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_sent` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_failed` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_other` TINYINT(3) UNSIGNED DEFAULT 0,
  `status` ENUM('unknown','okay','recovered','warn','critical') COLLATE latin1_general_ci NOT NULL DEFAULT 'unknown',
  `notified` ENUM('yes','no','disabled','suppressed') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`type`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_LOCKFILE` (
  `app` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `checked` DATE NOT NULL,
  `state` ENUM('valid','failed','missing') COLLATE latin1_general_ci NOT NULL,
  `status` ENUM('unknown','okay','recovered','warn','critical') COLLATE latin1_general_ci NOT NULL DEFAULT 'unknown',
  `notified` ENUM('yes','no','disabled','suppressed') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`app`,`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_SERVER` (
  `server` ENUM('data') COLLATE latin1_general_ci NOT NULL,
  `checked` DATETIME NOT NULL,
  `checked_date` DATE DEFAULT NULL,
  `checked_time` TIME DEFAULT NULL,
  `ip` CHAR(15) COLLATE latin1_general_ci DEFAULT NULL,
  `state` ENUM('up','down') COLLATE latin1_general_ci NOT NULL,
  `status` ENUM('unknown','okay','recovered','warn','critical') COLLATE latin1_general_ci NOT NULL DEFAULT 'unknown',
  `notified` ENUM('yes','no','disabled','suppressed') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`server`,`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_LOGFILE` (
  `app` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `checked` DATETIME NOT NULL,
  `errors` TINYINT(2) UNSIGNED DEFAULT 0,
  `detail` VARCHAR(10240) COLLATE latin1_general_ci DEFAULT NULL,
  `status` ENUM('unknown','okay','recovered','warn','critical') COLLATE latin1_general_ci NOT NULL DEFAULT 'unknown',
  `notified` ENUM('yes','no','disabled','suppressed') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`app`,`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_LOGFILE_BREAKDOWN` (
  `app` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `checked` DATETIME NOT NULL,
  `section` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `errors` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`app`,`checked`,`section`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_HTTP_REQUEST` (
  `subdomain` VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
  `path` VARCHAR(255) COLLATE latin1_general_ci NOT NULL,
  `checked` DATETIME NOT NULL,
  `checked_date` SMALLINT(5) UNSIGNED NOT NULL,
  `checked_hour` TINYINT(3) UNSIGNED NOT NULL,
  `status_expected` SMALLINT(5) UNSIGNED DEFAULT 0,
  `status_returned` SMALLINT(5) UNSIGNED DEFAULT 0,
  `content_length` MEDIUMINT(8) UNSIGNED DEFAULT 0,
  `duration` DECIMAL(5,3) UNSIGNED DEFAULT 0.000,
  `state` ENUM('expected','mismatch','slow') COLLATE latin1_general_ci DEFAULT NULL,
  `status` ENUM('unknown','okay','recovered','warn','critical') COLLATE latin1_general_ci DEFAULT 'unknown',
  `notified` ENUM('yes','no','disabled','suppressed') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`subdomain`,`path`,`checked_date`,`checked_hour`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_HTTP_STATUS` (
  `subdomain` VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
  `date` DATE NOT NULL,
  `first_checked` DATETIME NOT NULL,
  `last_checked` DATETIME NOT NULL,
  `tot_requests` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `tot_1xx` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `tot_2xx` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_200` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_201` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_204` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `tot_3xx` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_301` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_302` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_304` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_307` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_308` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `tot_4xx` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_400` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_401` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_403` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_404` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_405` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `tot_5xx` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_500` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `num_501` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `tot_other` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `status` ENUM('unknown','okay','recovered','warn','critical') COLLATE latin1_general_ci DEFAULT 'unknown',
  `notified` ENUM('yes','no','disabled','suppressed') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`subdomain`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_AGE` (
  `ref` VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
  `type` ENUM('file','db') COLLATE latin1_general_ci NOT NULL,
  `path` VARCHAR(200) COLLATE latin1_general_ci NOT NULL,
  `checked` DATETIME NOT NULL,
  `ref_date` DATETIME DEFAULT NULL,
  `age` INT(10) UNSIGNED DEFAULT NULL,
  `status` ENUM('unknown','okay','recovered','warn','critical') COLLATE latin1_general_ci DEFAULT 'unknown',
  `notified` ENUM('yes','no','disabled','suppressed') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ref`,`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
