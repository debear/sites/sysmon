CREATE TABLE `SYSMON_STATUS_GROUPS` (
  `group_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `code` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `order` TINYINT(3) UNSIGNED NOT NULL,
  `status_user` ENUM('operational','issues','outage') COLLATE latin1_general_ci DEFAULT 'operational',
  `status_admin` ENUM('operational','issues','outage') COLLATE latin1_general_ci DEFAULT 'operational',
  `note` VARCHAR(250) COLLATE latin1_general_ci DEFAULT NULL,
  `last_outage` DATETIME DEFAULT NULL,
  `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_STATUS_ITEMS` (
  `status_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `icon` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `group_id` TINYINT(3) UNSIGNED NOT NULL,
  `order` TINYINT(3) UNSIGNED NOT NULL,
  `admin_only` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `status` ENUM('unknown','okay','recovered','warn','critical') COLLATE latin1_general_ci DEFAULT 'unknown',
  `note` VARCHAR(250) COLLATE latin1_general_ci DEFAULT NULL,
  `current_started` DATETIME DEFAULT NULL,
  `last_started` DATETIME DEFAULT NULL,
  `last_resolved` DATETIME DEFAULT NULL,
  `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `notifications_on` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_STATUS_GROUPS_METRICS` (
  `group_id` TINYINT(3) UNSIGNED NOT NULL,
  `when_parsed` DATE NOT NULL,
  `is_current` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `metrics_7num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `metrics_7avg` DECIMAL(5,2) UNSIGNED DEFAULT NULL,
  `metrics_14num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `metrics_14avg` DECIMAL(5,2) UNSIGNED DEFAULT NULL,
  `metrics_30num` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `metrics_30avg` DECIMAL(5,2) UNSIGNED DEFAULT NULL,
  `metrics_90num` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `metrics_90avg` DECIMAL(5,2) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`group_id`,`when_parsed`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_STATUS_ITEMS_METRICS` (
  `status_id` TINYINT(3) UNSIGNED NOT NULL,
  `when_parsed` DATE NOT NULL,
  `is_current` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `metrics_7num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `metrics_7avg` DECIMAL(5,2) UNSIGNED DEFAULT NULL,
  `metrics_14num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `metrics_14avg` DECIMAL(5,2) UNSIGNED DEFAULT NULL,
  `metrics_30num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `metrics_30avg` DECIMAL(5,2) UNSIGNED DEFAULT NULL,
  `metrics_90num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `metrics_90avg` DECIMAL(5,2) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`status_id`,`when_parsed`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
