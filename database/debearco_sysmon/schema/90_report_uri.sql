CREATE TABLE `REPORTS_INSTANCE` (
  `report_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` ENUM('csp','ct','js','php') COLLATE latin1_general_ci NOT NULL,
  `site` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `v_site` VARCHAR(10) COLLATE latin1_general_ci DEFAULT NULL,
  `rev_site` SMALLINT(4) UNSIGNED DEFAULT NULL,
  `rev_skel` SMALLINT(4) UNSIGNED DEFAULT NULL,
  `uri_location` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `uri_args` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `request_time` DATETIME DEFAULT NULL,
  `num_reports` TINYINT(3) UNSIGNED DEFAULT 0,
  `user_agent` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `browser` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `browser_version` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `os` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `geoip_country` CHAR(2) COLLATE latin1_general_ci DEFAULT NULL,
  `geoip_city` VARCHAR(200) COLLATE latin1_general_ci DEFAULT NULL,
  `user_logged_in` TINYINT(1) UNSIGNED DEFAULT 0,
  `filename` VARCHAR(120) COLLATE latin1_general_ci DEFAULT NULL,
  `imported` DATETIME DEFAULT NULL,
  `status` ENUM('reported','missing_detail','false_positive','resolved') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`report_id`),
  UNIQUE KEY `filename` (`filename`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `REPORTS_CSP` (
  `report_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `document_uri` VARCHAR(150) COLLATE latin1_general_ci DEFAULT NULL,
  `violated_directive` VARCHAR(500) COLLATE latin1_general_ci DEFAULT NULL,
  `blocked_uri` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `source_file` VARCHAR(200) COLLATE latin1_general_ci DEFAULT NULL,
  `line_number` SMALLINT(5) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `REPORTS_JS` (
  `report_id` INT(10) UNSIGNED NOT NULL,
  `instance_id` TINYINT(3) UNSIGNED NOT NULL,
  `error` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `trace` TEXT COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`report_id`,`instance_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `REPORTS_PHP` (
  `report_id` INT(10) UNSIGNED NOT NULL,
  `instance_id` TINYINT(3) UNSIGNED NOT NULL,
  `code` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `error` VARCHAR(2048) COLLATE latin1_general_ci DEFAULT NULL,
  `file` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `line` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `trace` TEXT COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`report_id`,`instance_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
