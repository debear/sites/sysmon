CREATE TABLE `SYSMON_INCIDENTS` (
  `group_id` TINYINT(3) UNSIGNED NOT NULL,
  `group_code` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `date_ref` SMALLINT(5) UNSIGNED NOT NULL,
  `incident_id` TINYINT(2) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `status_id` TINYINT(3) UNSIGNED DEFAULT NULL,
  `check_id` TINYINT(3) UNSIGNED DEFAULT NULL,
  `status` ENUM('unknown','warn','critical') COLLATE latin1_general_ci DEFAULT 'unknown',
  `title` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `detail` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `started` DATETIME DEFAULT NULL,
  `resolved` DATETIME DEFAULT NULL,
  `is_current` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`group_id`,`date_ref`,`incident_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SYSMON_INCIDENTS_LEDGER` (
  `group_id` TINYINT(3) UNSIGNED NOT NULL,
  `date_ref` SMALLINT(5) UNSIGNED NOT NULL,
  `incident_id` TINYINT(2) UNSIGNED ZEROFILL NOT NULL,
  `ledger_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status_id` TINYINT(3) UNSIGNED NOT NULL,
  `status_from` ENUM('unknown','okay','warn','critical') COLLATE latin1_general_ci NOT NULL,
  `status_to` ENUM('unknown','recovered','warn','critical') COLLATE latin1_general_ci NOT NULL,
  `status_changed` DATETIME NOT NULL,
  `note` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`group_id`,`date_ref`,`incident_id`,`ledger_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
