@extends('skeleton.layouts.html')

@section('content')

<h1>{!! $check->name !!} History</h1>

<div class="charts {!! $check->script !!} {!! $check->instance ?: '' !!}">
    @foreach ($charts as $chart)
        <div id="chart-{!! $chart['subnav-code'] !!}" class="subnav-chart-{!! $chart['subnav-code'] !!} {!! $loop->index ? 'hidden' : '' !!}">
            <div class="box section icon_loading_section">
                Please wait, loading...
            </div>
        </div>
    @endforeach
</div>

@if (count($subnav['list']) > 1)
    @include('skeleton.widgets.subnav')
@endif

<div class="paged_table">
    @include('sysmon.app.check.data')
</div>

<script {!! HTTP::buildCSPNonceTag() !!}>
    var charts = [];
    @foreach ($charts as $chart)
        charts['{!! $chart['subnav-code'] !!}'] = new Highcharts.Chart({!! Highcharts::decode($chart) !!});
    @endforeach
</script>

@endsection
