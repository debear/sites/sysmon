@extends('skeleton.layouts.html')

@section('content')

<h1>{!! $check->name !!} Logs</h1>

<div class="paged_table">
    @include('sysmon.app.logs.data')
</div>

@endsection
