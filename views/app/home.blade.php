@php
    $headline = $checks->where('internal', '=', 1)->first();
    $groups = $checks->where('internal', '=', 0);
@endphp
@extends('skeleton.layouts.html')

@section('content')

@include('sysmon.app.home.headline')

@foreach($groups as $group)
    @include('sysmon.app.home.group')
@endforeach

@endsection
