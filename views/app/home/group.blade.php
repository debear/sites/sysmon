<div class="box {!! $group->state !!} num-{!! $loop->iteration !!}-{!! $loop->count !!} cell grid-1-3 grid-t-1-2 grid-m-1-1">
    <ul class="inline_list group">
        <li class="title row_{!! $group->state !!}">{!! $group->name !!}</li>
        @foreach ($group->checks as $check)
            @php
                $row_top_css = 'row_' . ($check->last_status ?: ($loop->index % 2));
                $row_bot_css = 'arow_0';
                // Notification icon
                if ($check->notificationSnooze) {
                    $notif_info = 'Snoozed ' . ($check->notificationSnooze->end ? ' Until ' . $check->notificationSnooze->end->format('H:i, D jS M') : 'Indefinitely');
                    $notif_icon = 'snooze';
                } else {
                    $notif_info = ($check->notifications_on ? 'On' : 'Off');
                    $notif_icon = strtolower($notif_info);
                }
            @endphp
            <li class="name {!! $row_top_css !!} icon_script_{!! $check->script !!}">{!! $check->name !!}</li>
            <li class="type {!! $row_top_css !!}">
                {!! $check->scriptDisplay() !!}
            </li>
            <li class="status {!! $row_top_css !!}">
                <span {!! $check->last_status ? ' class="icon_status_' . $check->last_status . '"' : '' !!}>{!! $check->last_status ? ucfirst($check->last_status) : '&ndash;' !!}</span>
            </li>
            <li class="summary {!! $row_top_css !!}">
                {!! $check->formatSummary() !!}
            </li>
            <li class="freq {!! $row_bot_css !!}">
                {!! ucfirst($check->frequency_type) !!}
                {!! $check->frequency_type == 'daily' ? '(' . $check->frequency . 'h)' : '' !!}
            </li>
            <li class="notif {!! $row_bot_css !!} icon_notif_{!! $notif_icon !!}" title="Notifications {!! $notif_info !!}"></li>
            <li class="checked {!! $row_bot_css !!}">
                {!! isset($check->last_checked) ? '<span>Checked:</span>' . $check->last_checked->format('H:i, D jS M') : '&ndash;' !!}
            </li>
            <li class="actions {!! $row_bot_css !!}">
                [ <a href="{!! $check->linkDetail() !!}" title="View Detail" class="icon_history img"></a> ]
                [ <a href="{!! $check->linkLogs() !!}" title="View Logs" class="icon_logs img"></a> ]
            </li>
        @endforeach
    </ul>
</div>
