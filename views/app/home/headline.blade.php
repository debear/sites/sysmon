@php
    $num_okay = array_sum($groups->pluck('num_okay')->toArray());
    $num_warn = array_sum($groups->pluck('num_warn')->toArray());
    $num_critical = array_sum($groups->pluck('num_critical')->toArray());
    $num_unknown = array_sum($groups->pluck('num_unknown')->toArray());
@endphp
<ul class="inline_list headlines num-{!! $num_unknown ? 4 : 3 !!} grid-1-1 grid-t-1-1 grid-m-1-1">
    <li class="title">Headline Figures:</li>
    <li class="status row_okay">
        <span class="icon icon_status_okay">Okay: {!! $num_okay !!}</span>
    </li>
    <li class="status row_warn">
        <span class="icon icon_status_warn">Warn: {!! $num_warn !!}</span>
    </li>
    <li class="status row_critical">
        <span class="icon icon_status_critical">Critical: {!! $num_critical !!}</span>
    </li>
    @if ($num_unknown)
        <li class="status row_unknown">
            <span class="icon icon_status_unknown">Unknown: {!! $num_unknown !!}</span>
        </li>
    @endif
    <li class="actions">
        [ <a href="{!! $headline->checks->first()->linkDetail() !!}" title="View Detail" class="icon_history img"></a> ]
        [ <a href="{!! $headline->checks->first()->linkLogs() !!}" title="View Logs" class="icon_logs img"></a> ]
    </li>
</ul>
