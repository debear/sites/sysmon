@php
    $cols = $check->getColumns();
    $keys = array_keys($cols);
    $num_keys = sizeof($keys);
    $main = $keys[0];
    $default = $keys[1];
    $mobile_select = ($num_keys > 2);
@endphp

<scrolltable class="scroll-x with-summary type_{!! $check->script !!} {!! $check->instance ? "check_{$check->instance}" : '' !!}" data-current="{!! $default !!}">
    <mobile-select>
        <current class="row_head {!! !$mobile_select ? 'no-select' : '' !!}">{!! $cols[$default] !!}</current>
        @if ($mobile_select)
            <select>
                @foreach (array_slice($cols, 1) as $key => $label)
                    <option value="{!! $key !!}" {!! $key == $default ? ' selected="selected"' : '' !!}>{!! $label !!}</option>
                @endforeach
            </select>
        @endif
    </mobile-select>
    <main>
        {{-- Header --}}
        <top>
            <cell class="row_head main">{!! $cols[$main] !!}</cell>
            <cell class="row_head summary status">Status</cell>
            <cell class="row_head summary notified">Notified</cell>
        </top>
        {{-- Data --}}
        @foreach ($details as $row)
            @php
                $row_css = 'row_' . ($loop->iteration % 2);
            @endphp
            <row>
                <cell class="{!! $row_css !!} main">{!! $row->formatCol($main) !!}</cell>
                <cell class="row_{!! $row->status !!} summary status">
                    <span class="icon icon_status_{!! $row->status !!}">{!! ucfirst($row->status) !!}</span>
                </cell>
                <cell class="{!! $row_css !!} summary notified">
                    <span class="icon icon_notified_{!! $row->notified !!}">{!! ucfirst($row->notified) !!}</span>
                </cell>
            </row>
        @endforeach
    </main>
    <data>
        <datalist>
            {{-- Header --}}
            <top>
                @for ($i = 1; $i < $num_keys; $i++)
                    <cell class="row_head data {!! $keys[$i] !!}">{!! $cols[$keys[$i]] !!}</cell>
                @endfor
            </top>
            {{-- Entities --}}
            @foreach ($details as $row)
                @php
                    $row_css = 'row_' . ($loop->iteration % 2);
                @endphp
                <row>
                    @for ($i = 1; $i < $num_keys; $i++)
                        <cell class="{!! $row_css !!} data {!! $keys[$i] != $default ? 'hidden-m' : '' !!} {!! $keys[$i] !!}" data-field="{!! $keys[$i] !!}">{!! $row->formatCol($keys[$i]) !!}</cell>
                    @endfor
                </row>
            @endforeach
        </datalist>
    </data>
</scrolltable>

<div class="clearfix">
    {!! Pagination::render($totalRows, $perPage, $check->linkDetail() . '/page/', $page) !!}
</div>
