<ul class="inline_list table log_list">
    <li class="date row_head">Log Date</li>
    <li class="status row_head">Status</li>
    <li class="notif row_head">Notification</li>
    <li class="info row_head">Info</li>

    @foreach ($logs as $log)
        @php
            $row_css = 'row_' . ($loop->index % 2);
        @endphp
        <li class="date {!! $row_css !!}">
            {!! $log->logDateFormat() !!}
        </li>
        <li class="status row_{!! $log->status !!}">
            <span class="icon icon_status_{!! $log->status !!}">{!! ucfirst($log->status) !!}</span>
        </li>
        <li class="notif {!! $row_css !!}">
            <span class="icon icon_notified_{!! $log->notified !!}">{!! ucfirst($log->notified) !!}</span>
        </li>
        <li class="info {!! $row_css !!}" title="{!! $log->info !!}">
            {!! $log->info !!}
        </li>
    @endforeach
</ul>

<div class="clearfix">
    {!! Pagination::render(sizeof($check->logs), $perPage, $check->linkLogs() . '/page/', $page) !!}
</div>
