@extends('skeleton.layouts.html')

@section('content')

<h1>{!! FrameworkConfig::get('debear.names.site') !!}</h1>

{{-- Summary--}}
@include('sysmon.status.home.headline')

<div class="groups">
    @foreach($status_groups as $group)
        @include('sysmon.status.home.group')
    @endforeach
</div>

{{-- Incident Detail --}}
@isset($subnav)
    @include('sysmon.status.home.detail')
@endisset

@endsection
