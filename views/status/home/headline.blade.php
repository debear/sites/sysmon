@php
    // Determine based on incident counts
    $by_status = $incidents->pluck('status')->countBy()->toArray();
    if (!sizeof($by_status)) {
        // All okay
        $status = 'okay';
        $icon = 'valid';
        $message = 'All systems are fully operational.';
    } elseif (isset($by_status['critical'])) {
        // Major outage
        $status = 'critical';
        $icon = 'error';
        $message = 'We are aware of, and dealing with, ' . ($by_status['critical'] == 1 ? 'a major issue' : 'major issues') . '.';
    } else {
        // Minor issues
        $status = 'warn';
        $icon = 'warning';
        $message = 'We are aware of, and dealing with, ' . ($by_status['warn'] == 1 ? 'a minor issue' : 'minor issues') . '.';
    }
@endphp
{{-- Render --}}
<div class="box text {!! $status !!} icon icon_{!! $icon !!}">{!! $message !!}</div>
