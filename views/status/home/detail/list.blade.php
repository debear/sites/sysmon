{{-- TODO: Icon for open/closed incident --}}
<div class="incidents subnav-{!! $type !!} {!! $type != $subnav['default'] ? 'hidden' : '' !!}">
    @includeIf("sysmon.status.home.detail.list-$type")
    @foreach ($list as $incident)
        <dl class="box clearfix {!! $incident->status !!} group-{!! strtolower($incident->statusItem->group->code) !!}">
            <dt class="title row_{!! $incident->status !!} icon_right_{!! $incident->icon !!}">
                {!! $incident->ref_code !!}
                &ndash; {!! $incident->statusItem->group->name !!}
                &ndash; {!! $incident->title !!}
            </dt>
            <dt class="affected field">{!! $incident->is_current ? 'Affecting:' : 'Affected:' !!}</dt>
                <dd class="affected field">
                    {!! $incident->statusItem->group->name !!}
                    &raquo;
                    <span class="icon item_icon_{!! $incident->statusItem->icon !!}">{!! $incident->statusItem->name !!}</span>
                </dd>
            <dt class="severity field">Severity:</dt>
                <dd class="severity field">{!! $incident->severity !!}</dd>
            <dt class="started field">{!! $incident->is_current ? 'Since:' : 'Started:' !!}</dt>
                <dd class="started field">{!! $incident->started->format('D jS M Y \a\t G:i') !!} ({!! $incident->started->diffForHumans() !!})</dd>
            @if (!$incident->is_current)
                <dt class="resolved field">Resolved:</dt>
                    <dd class="resolved field">{!! $incident->resolved->format('D jS M Y \a\t G:i') !!} ({!! $incident->resolved->diffForHumans() !!})</dd>
                <dt class="duration field">Duration:</dt>
                    <dd class="duration field">{!! $incident->duration !!}</dd>
            @endif
            <dd class="detail">{!! $incident->detail !!}</dd>

            @if ($incident->renderLedgerChanges())
                <dt class="subtitle">Incident Log:</dt>

                @foreach ($incident->ledgerChanges() as $ledger)
                    <dt class="changed field">{!! $ledger->status_changed->format('jS M Y, G:i') !!}:</dt>
                        <dd class="ledger field ">{!! $ledger->detail !!}</dd>
                @endforeach
            @endif
        </dl>
    @endforeach
</div>
