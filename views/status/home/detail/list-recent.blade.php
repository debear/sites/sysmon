@php
    // Group
    $groups = $recent->groupBy('statusItem.group.code')->sortKeys()->map(function ($by_group, $code) {
        return ['id' => strtolower($code), 'label' => $by_group->first()->statusItem->group->name . " (" . $by_group->count() . ")"];
    })->values()->toArray();
    if (count($groups) > 1) {
        $filter = new Dropdown('recent_group', $groups, [
            'select' => 'All Groups',
        ]);
    }
@endphp
@isset ($filter)
    <div class="filter">
        <label>Filter by:</label>
        {!! $filter->render() !!}
    </div>
@endisset
