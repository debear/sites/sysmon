@php
    $inc_last = $group->isOkay() && isset($group->last_outage);
@endphp
<div class="group grid-1-4 grid-tl-1-3 grid-tp-1-2 grid-m-1-1">
    <dl class="box {!! $group->status_box !!} {!! $inc_last ? 'with-last' : '' !!}">
        <dt class="row_{!! $group->status_box !!} icon_right_{!! $group->icon !!}">{!! $group->name !!}</dt>
            <dd class="summary text {!! $group->status_box !!}">{!! $group->summary !!}</dd>

        {{-- Line items --}}
        @foreach($status_items->where('group_id', $group->group_id) as $item)
            <dd class="item row_pastel {!! $item->status_icon !!} icon_right_{!! $item->status_icon !!} item_icon_{!! $item->icon !!}">
                @if($item->admin_only)
                    <span class="icon_right_item_admin">
                @endif
                {!! $item->name !!}
                @if($item->admin_only)
                    </span>
                @endif
            </dd>

            {{-- Incidents --}}
            @foreach($incidents->where('status_id', $item->status_id) as $incident)
                <dd class="incident row_pastel {!! $item->status_icon !!}">
                    <span class="started relative_date" title="{!! $incident->started->format('D jS M Y \a\t G:i') !!}">{!! $incident->started->diffForHumans() !!}</span>
                    {!! $incident->ref_code !!}
                    @isset($incident->title)
                        &ndash; {!! $incident->title !!}
                    @endisset
                </dd>
            @endforeach
        @endforeach

        {{-- Last outage --}}
        @if($inc_last)
            <dd class="last text relative_date {!! $group->status_box !!}" title="{!! $group->last_outage->format('D jS M Y \a\t G:i') !!}">Last outage: {!! $group->last_outage->diffForHumans() !!}</dd>
        @endif
    </dl>
</div>
