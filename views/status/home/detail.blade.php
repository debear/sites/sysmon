{{-- Tabs --}}
@include('skeleton.widgets.subnav')

{{-- Item lists --}}
@if ($incidents->count())
    @include('sysmon.status.home.detail.list', ['type' => 'current', 'list' => $incidents])
@endif
@if ($recent->count())
    @include('sysmon.status.home.detail.list', ['type' => 'recent', 'list' => $recent])
@endif
