## Build v2.0.385 - 8703722 - 2025-02-22 - SoftDep: February 2025
* 8703722: Update PHP.ini locations to the new docker image location (_Thierry Draper_)
* 6f02034: Upgrade Highcharts to v12 (_Thierry Draper_)
* 783f836: Migrate config getting to the framework Config class (_Thierry Draper_)

## Build v2.0.382 - afc8e4a - 2025-01-17 - SoftDep: January 2025
* (Auto-commits only)

## Build v2.0.382 - 15de555 - 2024-12-15 - SoftDep: December 2024
* (Auto-commits only)

## Build v2.0.382 - 1fd0196 - 2024-11-16 - SoftDep: November 2024
* (Auto-commits only)

## Build v2.0.382 - ff666ca - 2024-10-21 - SoftDep: October 2024
* (Auto-commits only)

## Build v2.0.382 - 0963ff7 - 2024-09-30 - Feature: Odds Log Monitoring
* 0963ff7: Introduce a Sports Odds logging parser for monitoring the process (_Thierry Draper_)

## Build v2.0.381 - c1abf4a - 2024-09-15 - SoftDep: September 2024
* (Auto-commits only)

## Build v2.0.381 - 1386505 - 2024-09-15 - SysAdmin: CI Tweaks
* 1386505: Catch Phan run errors that could cause cyclical self-calling (_Thierry Draper_)
* 0b18972: Fix first run setup for downloading seed data (_Thierry Draper_)
* d17fdac: Apply some minor CI script tidying and improvements (_Thierry Draper_)

## Build v2.0.378 - 566bf19 - 2024-08-30 - Hotfix: MySQL 8 Datatypes
* 566bf19: Fix Perl handling of MySQL 8 return datatypes (_Thierry Draper_)

## Build v2.0.377 - 582355b - 2024-08-19 - SoftDep: August 2024
* (Auto-commits only)

## Build v2.0.377 - b233ec6 - 2024-08-13 - Improvement: MySQL Zero Dates
* b233ec6: Remove a possible MySQL zerodate instance when validating a remote server is up (_Thierry Draper_)

## Build v2.0.376 - c807013 - 2024-08-09 - SysAdmin: MariaDB to MySQL Migration
* c807013: Handle the way MySQL considers GROUPS as a keyword (_Thierry Draper_)
* 7791a56: Fix the GROUP BY clause in status propagation due to the ONLY_FULL_GROUP_BY sql_mode (_Thierry Draper_)
* fd02413: Migrate our CI from MariaDB to MySQL (_Thierry Draper_)

## Build v2.0.373 - 01dbbde - 2024-07-17 - Hotfix: SAST Reporting
* 01dbbde: Fix the SAST report download sort ordering (_Thierry Draper_)

## Build v2.0.372 - 9a3079f - 2024-07-17 - Feature: CI Server Sync Job
* 9a3079f: Include the server sync database to our CI job list (_Thierry Draper_)

## Build v2.0.371 - 51b520a - 2024-07-16 - SoftDep: July 2024
* (Auto-commits only)

## Build v2.0.371 - d7b4b9b - 2024-06-28 - Hotfix: Group Metric Col Widths
* d7b4b9b: Widen group metric status num cols to match usage (_Thierry Draper_)

## Build v2.0.370 - 9ac8fb7 - 2024-06-21 - Hotfix: Local CI Job Parsing
* 9ac8fb7: Fix the local CI job parser to exclude comments after the allow_failure flag (_Thierry Draper_)

## Build v2.0.369 - d604b91 - 2024-06-18 - SoftDep: June 2024
* (Auto-commits only)

## Build v2.0.369 - 74c9950 - 2024-06-17 - Hotfix: CI Downloads
* 74c9950: Fix the request sent to determine what CI data files need downloading (_Thierry Draper_)
* 8a71a99: Improve the location of the CI download trust store cert (_Thierry Draper_)

## Build v2.0.367 - 8536b89 - 2024-06-01 - Improvement: HTTP Status Parsing
* 8536b89: Parse the internal app logs as an access_log HTTP Status replacement (_Thierry Draper_)

## Build v2.0.366 - 95f64b8 - 2024-05-22 - Hotfix: Laravel Error Reporting
* 95f64b8: Correctly report the PHP file an error was detected, not the logfile it was reported in (_Thierry Draper_)

## Build v2.0.365 - 1a40d48 - 2024-05-18 - SoftDep: May 2024
* (Auto-commits only)

## Build v2.0.365 - 51dbc05 - 2024-05-16 - Improvement: Check Updates
* 51dbc05: Ensure the HTTP CookieJar directory exists before using it (_Thierry Draper_)
* 68ada31: Cap the representive daily comms counters used for charts, as the detail is stored in the notes (_Thierry Draper_)
* b581148: Emulate how the app processes GeoIP lookups with no city info in the daily sitrep tests (_Thierry Draper_)

## Build v2.0.362 - c4bddfa - 2024-05-13 - Security: CI Jobs
* c4bddfa: Switch our localci SAST exclusion to the new emulator (_Thierry Draper_)
* 7fd0bf6: Implement a local SAST emulator script using the GitLab docker images (_Thierry Draper_)
* 246c7b6: Implement an initial SAST reporting tool (_Thierry Draper_)
* 27c7759: Migrate our only/except job logic to the preferred rules format (_Thierry Draper_)
* d5b63fd: Incorporate the GitLab managed SAST pipeline job (_Thierry Draper_)
* 9751ef1: Add some native tool dependency vulnerability scanning within our CI (_Thierry Draper_)

## Build v2.0.356 - a54c41d - 2024-04-27 - Improvement: CI Downloads
* a54c41d: Switch CI downloads from wget to curl (_Thierry Draper_)

## Build v2.0.355 - c3837cb - 2024-04-24 - Improvement: JavaScript Formatting
* c3837cb: Apply some minor eslint hint tweaks to the JavaScript (_Thierry Draper_)
* 131b468: Disable the eslint unused disabled rule checked whilst linting, as it conflicts with the standards check (_Thierry Draper_)
* 2aa0f0d: Migrate to eslints flat config format (_Thierry Draper_)
* 0278266: Fix the script argument parsing to allow = within the RHS (_Thierry Draper_)

## Build v2.0.351 - a215517 - 2024-04-21 - SoftDep: April 2024
* (Auto-commits only)

## Build v2.0.351 - 184da47 - 2024-04-18 - Improvement: CI ECMAScript Version
* 184da47: Update the targeted version of ECMAScript in CI to v13 / 2022 (_Thierry Draper_)

## Build v2.0.350 - 1dd3f90 - 2024-04-04 - Hotfix: Script Localisation
* 1dd3f90: Ensure scripts are run in Europe/London following the move to a server set in UTC (_Thierry Draper_)

## Build v2.0.349 - ab1dd38 - 2024-03-31 - Hotfix: New Hosting Infrastructure
* ab1dd38: Re-work the error log parsing script, including a new Laravel log parser (_Thierry Draper_)
* 9b83169: Process the PHP error_log file in the new location, and also attempt to extract the file and line number (_Thierry Draper_)
* 7d60a0c: Render the script logging messages to screen in debug mode (_Thierry Draper_)
* f50f3fd: Do not process inactive checks within the app (_Thierry Draper_)
* c614b21: Hide du errors from appearing in the cron logs due to backup dir permissions (_Thierry Draper_)

## Build v2.0.344 - e05e3d0 - 2024-03-17 - SoftDep: Laravel 11 and March 2024 Update
* e05e3d0: Upgrade dependencies to PHPUnit 11 (_Thierry Draper_)
* 4f6d0f1: Switch use of the now-removed $dates property in Eloquent to its $casts equivalent (_Thierry Draper_)
* 8e0fa2f: Upgrade dependencies to PHPUnit 10 (_Thierry Draper_)

## Build v2.0.341 - a2ece85 - 2024-03-16 - SysAdmin: Hosting Fixes
* a2ece85: Use a better mechanism for determining dev/live run determination (_Thierry Draper_)
* c413151: Widen the logfile check detail column to account for now non-silent errors (_Thierry Draper_)

## Build v2.0.339 - 64971e3 - 2024-03-09 - SysAdmin: New Hosting Paths
* 64971e3: Apply minor path tweaks to the new hosted environment (_Thierry Draper_)

## Build v2.0.338 - c715dc2 - 2024-02-17 - SoftDep: February 2024
* (Auto-commits only)

## Build v2.0.338 - 91b1747 - 2024-01-27 - Hotfix: Check Paged ScrollTable
* 91b1747: Fix the scrolltable trigger on check pagination (_Thierry Draper_)

## Build v2.0.337 - b5680f5 - 2024-01-27 - Improvement: Refactoring JavaScript
* b5680f5: Apply the latest round of micro-optimisations and small improvements (_Thierry Draper_)
* 297e991: Revert PHPCS "linting" enforced JavaScript standards for curly brace positioning (_Thierry Draper_)
* 093d389: Replace the JavaScript linter with something less enforcing of (clunky) standards (_Thierry Draper_)
* 9dea431: Move to greater use of the arrow functions (_Thierry Draper_)
* 32eace5: Make better use of template literals in string expressions (_Thierry Draper_)
* 254ce39: Move away from the deprecated centralised DOM querySelector parent node argument (_Thierry Draper_)
* 3a1e190: Update unit tests following server-side to client-side JavaScript event changes in the skeleton (_Thierry Draper_)

## Build v2.0.330 - d75fd56 - 2024-01-19 - Improvement: JavaScript Null Operators
* d75fd56: Update eslints base ECMAScript version to allow for null-handling simplification (_Thierry Draper_)

## Build v2.0.329 - bc3469b - 2024-01-14 - SoftDep: January 2024
* (Auto-commits only)

## Build v2.0.329 - 48c03e8 - 2023-12-23 - Improvement: Fantasy Database Usage
* 48c03e8: Store the fantasy database size separately from the "other" database usage column (_Thierry Draper_)

## Build v2.0.328 - d1582e8 - 2023-12-17 - SoftDep: December 2023
* (Auto-commits only)

## Build v2.0.328 - 1f266fb - 2023-11-27 - CI: Standardised CSS Standards
* 1f266fb: Apply our standard 120-char width for CSS standards rather than the default 80 (_Thierry Draper_)

## Build v2.0.327 - 0f693aa - 2023-11-18 - Hotfix: Report URI Browser parsing
* 0f693aa: Extract the Report URI browser info from the new location (_Thierry Draper_)

## Build v2.0.326 - 1bf76c5 - 2023-10-14 - Composer: October 2023
* 1bf76c5: Backport a CI fix from earlier skeleton CDN update (_Thierry Draper_)

## Build v2.0.325 - dd22097 - 2023-09-18 - Composer: September 2023
* (Auto-commits only)

## Build v2.0.325 - d39feb6 - 2023-08-30 - Improvement: CI SymLink Conversion
* d39feb6: Switch to the new symlink-to-dir CI converter (_Thierry Draper_)

## Build v2.0.324 - 0f386b9 - 2023-08-05 - Composer: August 2023
* (Auto-commits only)

## Build v2.0.324 - 65acb04 - 2023-07-15 - Composer: July 2023
* (Auto-commits only)

## Build v2.0.324 - ff14337 - 2023-06-29 - Improvement: Suspicious Activity query
* ff14337: Update the suspicious activity query following the recent changes to activity log schema (_Thierry Draper_)

## Build v2.0.323 - 28857be - 2023-06-17 - Composer: June 2023
* (Auto-commits only)

## Build v2.0.323 - 85f9411 - 2023-05-14 - Improvement: Highcharts v11
* 85f9411: Switch Highcharts innvocation to the new v11 format (_Thierry Draper_)
* a8dead0: Switch from Highcharts v10 to v11 (_Thierry Draper_)

## Build v2.0.321 - 2de6d10 - 2023-05-14 - Composer: May 2023
* (Auto-commits only)

## Build v2.0.321 - 1796bcb - 2023-05-02 - SysAdmin: Deprecations
* 1796bcb: Replace use of fgrep with grep given it is being flagged overtly as obsolete (_Thierry Draper_)

## Build v2.0.320 - b74a144 - 2023-04-15 - Composer: April 2023
* (Auto-commits only)

## Build v2.0.320 - e389364 - 2023-03-11 - Composer: March 2023
* (Auto-commits only)

## Build v2.0.320 - 4c40c6e - 2023-03-01 - Improvement: CSS Standards to Prettier
* 4c40c6e: Fix CSS standards according to the new Prettier rules (_Thierry Draper_)
* ebf38e4: Switch the deprecated Stylelint CSS standards to Prettier (_Thierry Draper_)

## Build v2.0.318 - 9800a26 - 2023-02-12 - Composer: February 2023
* (Auto-commits only)

## Build v2.0.318 - c2dfdfb - 2023-01-15 - Composer: January 2023
* (Auto-commits only)

## Build v2.0.318 - 365d00a - 2023-01-08 - SysAdmin: CI Docker Images from Container Registry
* 365d00a: Switch the CI jobs to images from our Container Registry (_Thierry Draper_)

## Build v2.0.317 - 2d583f3 - 2023-01-05 - Improvement: MySQL Linting
* 2d583f3: Switch the CI MariaDB collation to latin1_general (_Thierry Draper_)
* 148306e: Include the CI job for linting setup scripts (_Thierry Draper_)

## Build v2.0.315 - 6187f7a - 2022-12-18 - Composer: December 2022
* (Auto-commits only)

## Build v2.0.315 - 5c8dbfd - 2022-11-19 - Composer: November 2022
* (Auto-commits only)

## Build v2.0.315 - def3f83 - 2022-10-16 - Composer: October 2022
* (Auto-commits only)

## Build v2.0.315 - 532d3bd - 2022-09-18 - Composer: September 2022
* (Auto-commits only)

## Build v2.0.315 - 206b06f - 2022-09-15 - CI: Environment dependency fix
* 206b06f: Install wget as a CI environment dependency (_Thierry Draper_)

## Build v2.0.314 - f342d61 - 2022-08-14 - Composer: August 2022
* (Auto-commits only)

## Build v2.0.314 - 2826867 - 2022-07-20 - Improvement: Report URI PHP and JS False Positives
* 2826867: Start auto-processing False Positive Report URIs for PHP and JS errors (_Thierry Draper_)

## Build v2.0.313 - 3df2e35 - 2022-07-19 - SysAdmin: Backport for PHP 8.0
* 3df2e35: Update the composer file to allow PHP 8.0, as well as the previous 8.1 (_Thierry Draper_)

## Build v2.0.312 - ba8b9bb - 2022-07-17 - Hotfix: GeoIP CI test data loading
* ba8b9bb: Use the new location for the test data in GeoIP CI tests (_Thierry Draper_)

## Build v2.0.311 - aef3981 - 2022-07-16 - Composer: July 2022
* (Auto-commits only)

## Build v2.0.311 - 0ab15ad - 2022-07-15 - Hotfix: PHPMD TooManyMethods whitelist
* 0ab15ad: Increase the scope of PHPMDs TooManyMethods whitelist (_Thierry Draper_)

## Build v2.0.310 - fe376f0 - 2022-06-19 - Composer: June 2022
* (Auto-commits only)

## Build v2.0.310 - 46d1e8e - 2022-06-09 - Improvement: Reduce number of Recently Resolved issues on the Status page
* 46d1e8e: Reduce (now via config) the number of recently resolved issues on the Status page (_Thierry Draper_)

## Build v2.0.309 - 1b9e3eb - 2022-06-08 - SysAdmin: Revert to native Blade data displaying
* 1b9e3eb: Re-factor our Blade template variable echoing to use the appropriate native Laravel mechanism (_Thierry Draper_)
* 993ceb3: Move common DeBear helper classes to the alias list for use in Blade templates (_Thierry Draper_)
* 37d9945: Revert use of @print and @config in Blade templates with the native Laravel method (_Thierry Draper_)
* a11a741: Restore Blade linting using the new dependency (_Thierry Draper_)

## Build v2.0.305 - 589965e - 2022-05-31 - Improvement: Policy checks in middleware
* 589965e: Move controller policy checking to the middleware (_Thierry Draper_)

## Build v2.0.304 - 3d0f439 - 2022-05-27 - Improvement: Highcharts v10
* 3d0f439: Fix minor issues with chart tooltips (_Thierry Draper_)
* 2bf61ee: Switch from HighCharts v9 to v10 (_Thierry Draper_)

## Build v2.0.302 - a2b14ba - 2022-05-27 - CI: PHP Code Coverage streamlining
* a2b14ba: Streamline the PHP Code Coverage output, and consider output less than 100% as a failure (_Thierry Draper_)

## Build v2.0.301 - a043a54 - 2022-05-26 - CI: Report PHP Unit code coverage on master
* a043a54: Propagate the PHP Unit code coverage report to the deploy stage for its result to be tagged against the master branch (_Thierry Draper_)

## Build v2.0.300 - 37ad4af - 2022-05-22 - CI: Switch Code Coverage Driver
* 37ad4af: Switch Code Coverage from using XDebug to PCOV (_Thierry Draper_)

## Build v2.0.299 - f2c3f66 - 2022-05-22 - Improvement: Unit Tests as part of Code Coverage
* f2c3f66: Add the GitLab code coverage regexp pattern, previously configured within the UI (_Thierry Draper_)
* f50a340: Refine our Unit Tests according to localci tweaks made to the skeleton (_Thierry Draper_)
* 75ee860: Fix the existing unit tests to achieve 100% code coverage (_Thierry Draper_)
* 9e0d15b: Include appropriate PHPUnit code in our code coverage tests (_Thierry Draper_)

## Build v2.0.295 - f61d47f - 2022-05-22 - Improvement: Laravel 9
* f61d47f: Make Phan tweaks following the upgrade to Laravel 9 (_Thierry Draper_)
* db7434a: Make fixes to issues identified by unit tests following the Laravel 9 upgrade (_Thierry Draper_)
* a63ecd3: Remove Blade linting that is no longer available following the Laravel 9 upgrade (_Thierry Draper_)

## Build v2.0.292 - 0b8879d - 2022-05-21 - SysAdmin: PHP 8
* 121ab70: Use a new stylelint rule modifier to prevent recent false negatives (_Thierry Draper_)
* bc96926: Add a new CI step to lint Blade templates (_Thierry Draper_)
* c5bd667: Move away from our Arrays::hasIndex wrapper (_Thierry Draper_)
* aebe242: Include a repo-specific version of the Phan PHP Standards CI job (_Thierry Draper_)
* fe9517a: Apply the final fixes suggested by Phan (_Thierry Draper_)
* c94376f: Apply several type-hinting related fixes (_Thierry Draper_)
* ec35e70: Remove unused imported classes (_Thierry Draper_)
* 851268d: Switch from Laravel's on-the-fly class facades at the root level to their actual full path (_Thierry Draper_)
* 7f60456: Fix code coverage fallout from PHP 8 migration (_Thierry Draper_)

## Build v2.0.283 - df52b52 - 2021-11-13 - Composer: November 2021
* (Auto-commits only)

## Build v2.0.283 - 000ef21 - 2021-11-12 - CI: CSS Property Standards
* 000ef21: Update CSS property rules to reflect the new alphabetical standards (_Thierry Draper_)
* a67b9bd: Add new stylelint standards test for alphabetical CSS property ordering (_Thierry Draper_)
* fd09370: Remove a recently deprecated stylelint rule (_Thierry Draper_)

## Build v2.0.280 - 5936eee - 2021-10-16 - Composer: October 2021
* (Auto-commits only)

## Build v2.0.280 - 184f781 - 2021-09-18 - Composer: September 2021
* (Auto-commits only)

## Build v2.0.280 - 1f57811 - 2021-08-28 - Hotfix: Total Usage database column width
* 1f57811: Extend the usage check's Total column width to cater for our capacity being greater than an unsigned INT (_Thierry Draper_)

## Build v2.0.279 - 7c06a6c - 2021-08-13 - Composer: August 2021
* 7c06a6c: Make necessary tooltip changes following upgrade to HighCharts v9.1.2 (_Thierry Draper_)

## Build v2.0.278 - 37986c0 - 2021-08-12 - Improvement: Carbonised Date Fixes
* 37986c0: Add missing .env values to the PHPUnit test XML spec (_Thierry Draper_)
* 1cb2171: Make necessary tweaks to recent skeleton timezone changes (_Thierry Draper_)

## Build v2.0.276 - 22cc51f - 2021-07-27 - Feature: Sitemap URL tester
* 22cc51f: Sitemap parser and processor for URL testing (_Thierry Draper_)

## Build v2.0.275 - 7aa176a - 2021-07-17 - Composer: July 2021
* (Auto-commits only)

## Build v2.0.275 - cc5cf28 - 2021-06-21 - Composer: June 2021
* (Auto-commits only)

## Build v2.0.275 - c924768 - 2021-05-15 - Composer: May 2021
* (Auto-commits only)

## Build v2.0.275 - f44adeb - 2021-04-30 - CI: Pipeline Tweaks
* f44adeb: Make use of the CI's needs: option to streamline job and stage links (_Thierry Draper_)

## Build v2.0.274 - 145f445 - 2021-04-17 - Composer: April 2021
* (Auto-commits only)

## Build v2.0.274 - c741cc2 - 2021-03-13 - Composer: March 2021
* c741cc2: Apply mobile pagination skeleton fixes to the CI (_Thierry Draper_)

## Build v2.0.273 - a4d93df - 2021-02-13 - Composer: February 2021
* (Auto-commits only)

## Build v2.0.273 - 8d210a0 - 2021-02-13 - Improvement: Highcharts v9
* 8d210a0: Upgrade to Highcharts v9 (_Thierry Draper_)

## Build v2.0.272 - 5641671 - 2021-02-13 - Hotfix: Catching Script Database Errors
* 5641671: Improve database error handling in the check scripts (_Thierry Draper_)
* 6a35b9b: Expand the check log info column width given the info we're now storing (_Thierry Draper_)

## Build v2.0.270 - 3e68e26 - 2021-02-07 - Improvement: Hourly Check Definition
* 3e68e26: Switch the check frequency to crontab style logic (_Thierry Draper_)

## Build v2.0.269 - 50e3074 - 2021-01-16 - Composer: January 2021
* (Auto-commits only)

## Build v2.0.269 - 1549398 - 2021-01-14 - Hotfix: HTTP Request error messages
* 1549398: Correct the wording of HTTP Request error messages (_Thierry Draper_)

## Build v2.0.268 - 02d6f05 - 2021-01-11 - Improvement: Password Policy Skeleton Changes
* 02d6f05: Rely on an appropriate version of guzzlehttp/guzzle after adding the password policy (_Thierry Draper_)
* 3f97f5e: Start using the new @config Blade directive for rending config values (_Thierry Draper_)

## Build v2.0.266 - 58236fc - 2021-01-03 - CI: Only deploy on master branch
* 58236fc: Only deploy during CI/AD of the master branch (_Thierry Draper_)

## Build v2.0.265 - 53142e1 - 2021-01-03 - Hotfix: Silenced Notification Counts
* 53142e1: Fix the SQL for counting silenced notifications (_Thierry Draper_)

## Build v2.0.264 - 1db8170 - 2020-12-20 - Composer: December 2020
* (Auto-commits only)

## Build v2.0.264 - 875808f - 2020-12-05 - Hotfix: MySQL Strictness
* 875808f: Fixes (and workarounds) for the switch from Xdebug 2 to 3 (_Thierry Draper_)
* a30ddf0: Fix SQL queries that require extra columns to satisfy strictness (_Thierry Draper_)

## Build v2.0.262 - e443609 - 2020-11-14 - Composer: November 2020
* (Auto-commits only)

## Build v2.0.262 - f75467e - 2020-10-16 - Improvement: Unit Test manifest.json
* f75467e: Add the manifest.json file to the suite of Unit Tests (_Thierry Draper_)

## Build v2.0.261 - 073a8dc - 2020-10-09 - Composer: Upgrade to Laravel 8
* 073a8dc: Update PHPUnit assertions used following the upgrade to PHPUnit 9 (_Thierry Draper_)
* bc72eb0: Update our CI rules following the Laravel 8 (+deps) upgrade (_Thierry Draper_)
* 7bb04a3: Update dependencies from upgrading from Laravel 7 to 8 (_Thierry Draper_)

## Build v2.0.258 - edf51bd - 2020-09-26 - Improvement: Migrations to Schema
* edf51bd: Switch from Migrations to Schemas updated via schema-sync (_Thierry Draper_)

## Build v2.0.257 - 1929dd7 - 2020-09-19 - Composer: September 2020
* (Auto-commits only)

## Build v2.0.257 - 16b2211 - 2020-09-19 - CI: Merge PHP Standards Jobs
* 16b2211: Merge the PHP Mess Detector and Standards tests into a single job (_Thierry Draper_)

## Build v2.0.256 - 8057dc3 - 2020-09-18 - Improvement: HTTP Status Wrapper
* 8057dc3: Start using generic (named) wrappers for sending particular HTTP status codes (_Thierry Draper_)

## Build v2.0.255 - b66c059 - 2020-09-18 - Improvement: Browscap Root Age Check
* b66c059: Start checking the Browscap age against its root online file (_Thierry Draper_)

## Build v2.0.254 - 69266a0 - 2020-09-17 - Improvement: External GeoIP Test Specs
* 69266a0: Remove the post-lockfile-data-check requirement from the Lockfile check (_Thierry Draper_)
* 6382857: Switch the GeoIP unit tests to the externally sourced specs (_Thierry Draper_)

## Build v2.0.252 - 6aa6e5b - 2020-08-15 - Composer: August 2020
* (Auto-commits only)

## Build v2.0.252 - 0535f1a - 2020-07-25 - Hotfix: Public Notifications
* 0535f1a: Use the correct hash for checking per-check notifications are enabled when making public announcements (_Thierry Draper_)

## Build v2.0.251 - 7ef32a1 - 2020-07-13 - Composer: July 2020
* (Auto-commits only)

## Build v2.0.251 - b1e4189 - 2020-06-28 - Feature: HTTP Status Incidents
* b1e4189: Add incident post-processing for HTTP Status codes (_Thierry Draper_)

## Build v2.0.250 - 3e79e7e - 2020-06-28 - Feature: Monitor HTTP Status Codes
* 3e79e7e: Add the page for the new HTTP Status logs (_Thierry Draper_)
* a3832cd: Add the script to scan HTTP status codes, re-jigging the previous "http" check as targettig individual HTTP requests (_Thierry Draper_)

## Build v2.0.248 - 48a7db2 - 2020-06-18 - Improvement: Usage Pie Chart Segment Percentage
* 48a7db2: Include pie-chart segment %age on the Disk Usage current usage chart (_Thierry Draper_)

## Build v2.0.247 - 75c4590 - 2020-06-18 - Hotfix: Report URI Filename RegEx
* 75c4590: Use the correct regex for parsing the Report URI report filename to glean appropriate details (_Thierry Draper_)

## Build v2.0.246 - 84c10a3 - 2020-06-17 - Hotfix: Report URI Log Parsing
* 84c10a3: Tweak the way we access Report URI logs for parsing to avoid new release dir based duplicates (_Thierry Draper_)

## Build v2.0.245 - a08e751 - 2020-06-17 - Hotfix: PHPUnit Test Locale
* a08e751: Fix PHPUnit tests on CI as the locale does not match dev/prod environments (_Thierry Draper_)

## Build v2.0.244 - 7ba6d36 - 2020-06-14 - Composer: June 2020
* (Auto-commits only)

## Build v2.0.244 - eb33e37 - 2020-06-13 - SysAdmin: Reduce Release Overlap
* eb33e37: Reflect the new patch structure in the Report URI site identification (_Thierry Draper_)
* 081ac53: Reduce the overlap time between releases when performing the release (_Thierry Draper_)

## Build v2.0.242 - c95a1c7 - 2020-06-09 - Feature: Incident Metrics
* c95a1c7: Link the Group/Item status models to their Metrics models (_Thierry Draper_)
* 6a74f6e: Add schema and calcs for incident metrics (_Thierry Draper_)

## Build v2.0.240 - 4df838f - 2020-06-09 - Feature: File Age Check
* 4df838f: Add a new File Age check (_Thierry Draper_)
* b3ce575: Adds a new "age" check (_Thierry Draper_)

## Build v2.0.238 - 76cf079 - 2020-06-06 - Improvement: PSR-4-like Standards Check
* 76cf079: Add PSR-4 like standards checks, given recent class and file name mis-matches (_Thierry Draper_)

## Build v2.0.237 - e024f28 - 2020-06-02 - CI: Local PHPUnit Dev Setup
* e024f28: Add scripts to setup a local dev environment for working on PHPUnit tests (_Thierry Draper_)

## Build v2.0.236 - 4b302d5 - 2020-05-18 - Composer: May 2020
* (Auto-commits only)

## Build v2.0.236 - 42e61ee - 2020-04-24 - Hotfix: Silenced Notification Recovery Calcs
* 42e61ee: Correct the way notification silencing calculates if it should send recoveries (_Thierry Draper_)

## Build v2.0.235 - 9fbc55c - 2020-04-21 - Composer: April 2020
* (Auto-commits only)

## Build v2.0.235 - cfae7b6 - 2020-04-12 - Composer: Laravel 7
* cfae7b6: Make some minor changes following the upgrade to Laravel 7 (_Thierry Draper_)

## Build v2.0.234 - 535b930 - 2020-04-11 - Feature: Notification Offset
* 535b930: Allow for an initial 'offset' of failed checks before a notification is raised (_Thierry Draper_)

## Build v2.0.233 - 6156cec - 2020-04-07 - Improvement: Codified Check Name in URLs
* 6156cec: Use a codified version of the check's name in its URLs (_Thierry Draper_)

## Build v2.0.232 - 29d648a - 2020-04-07 - Improvement: Dynamic PHPUnit Setup Components
* 29d648a: Standarise and split the PHPUnit setup script into dynamic components (_Thierry Draper_)

## Build v2.0.231 - 645b27d - 2020-04-06 - Hotfix: PHPUnit Error Handling
* 645b27d: Ensure the new PHPUnit run script errors on failure at the appropriate stage (_Thierry Draper_)

## Build v2.0.230 - 877350b - 2020-04-04 - Composer: Replacing PHP Linter
* 877350b: Replace a deprecated PHP Linting package with its replacement (_Thierry Draper_)

## Build v2.0.229 - c4581e7 - 2020-04-04 - Improvement: PHPUnit Efficiency
* c4581e7: Split the PHPUnit run in to a more efficient process (_Thierry Draper_)

## Build v2.0.228 - aaf3e75 - 2020-03-14 - Composer: March 2020
* (Auto-commits only)

## Build v2.0.228 - 0f45ce2 - 2020-02-19 - Improvement: HighCharts v8
* 0f45ce2: Switch from HighCharts v7 to v8 (_Thierry Draper_)

## Build v2.0.227 - 6cec0d7 - 2020-02-14 - Composer: February 2020
* (Auto-commits only)

## Build v2.0.227 - 86ae5c6 - 2020-02-10 - Improvement: Footer Links
* 86ae5c6: Remove the bespoke DeBearSupport social media footer link, which is now the default anyway (_Thierry Draper_)
* 65004a5: Remove the (self-circular) System Status link in the page footer (_Thierry Draper_)

## Build v2.0.225 - e08bb95 - 2020-02-07 - Hotfix: Notification Frequency Calcs
* e08bb95: Fix the notification frequency and silence period calculations calculations (_Thierry Draper_)

## Build v2.0.224 - b11881a - 2020-01-31 - Hotfix: Status Unit Test Recency
* b11881a: Status unit test 'recency' cannot be a dynamic date when using fixed test data (_Thierry Draper_)

## Build v2.0.223 - 62034b7 - 2020-01-31 - SysAdmin: .gitignore for env config
* 62034b7: Updated gitignore to hide a local env config file (_Thierry Draper_)

## Build v2.0.222 - 460a6d3 - 2020-01-27 - Improvement: HTTP Check Detail Optimisation
* 460a6d3: Major optimisations to the HTTP check detail calcs (_Thierry Draper_)

## Build v2.0.221 - 6526820 - 2020-01-26 - Hotfix: Twitter DM Sending
* 6526820: Minor logfile check summary notification formatting improvements (_Thierry Draper_)
* 56ffe48: Check notification via Twitter DM query missing neessary columns (_Thierry Draper_)

## Build v2.0.219 - bce4ead - 2020-01-24 - Hotfix: HTTP Incident Wording
* bce4ead: Fix the order of the HTTP incident mis-match log (expected and returned were the wrong way round) (_Thierry Draper_)

## Build v2.0.218 - 391e7b9 - 2020-01-24 - Feature: Check Notifications via Twitter DM
* 391e7b9: Somehow, missed a 500, from the list of HTTP Status Codes that could be returned in a http check (_Thierry Draper_)
* 1c06cd9: Individual check changes to summarise its findings in a one-liner for the new notifications (_Thierry Draper_)
* 484ecb8: Implement a new notification sending mechanism, which allows none, one or both of email (as previously) or Twitter DMs (which include a few more notifications than before) (_Thierry Draper_)

## Build v2.0.215 - e258db0 - 2020-01-21 - Hotfix: Comms Sending Script Errors
* e258db0: Comms sending script needs the expected arguments defined properly, and results storable (_Thierry Draper_)

## Build v2.0.214 - acf71b4 - 2020-01-20 - Improvement: Comms Instance Definition
* acf71b4: Move the 'comms' instance definition from config to command line args (_Thierry Draper_)

## Build v2.0.213 - 49ae710 - 2020-01-19 - Improvement: Incident Notification Toggle
* 49ae710: Per-Status Item (public) notification toggle (_Thierry Draper_)

## Build v2.0.212 - 76f7637 - 2020-01-17 - Feature: Public Incident Notification via Twitter
* 76f7637: Status OpenGraph tweaks, binding to our DeBearSupport Twitter user and removing domains from Status Groups/Items (which interfere with the card generation) (_Thierry Draper_)
* 123e0c7: Ability to send (public) tweets when an incident is raised or resolved (_Thierry Draper_)
* 775d805: Switch the check 'pre-processing' list to a possible pre- or post-processing list (_Thierry Draper_)

## Build v2.0.209 - 996fcf2 - 2020-01-14 - Feature: Status Incident Ledger
* 996fcf2: Build process (minus current database extract itself) for the SysMon unit tests (_Thierry Draper_)
* 6f0127d: Remove the specific duration from the Status incident detail within HTTP checks (to be consistent with other checks) (_Thierry Draper_)
* a1b6fa9: Render, as appropriate, the new Status incident ledger against an individual incident (_Thierry Draper_)
* 489c5e9: Merge Status incidents that flap between statuses (with corresponding ledger), rather than create a new incident each time (_Thierry Draper_)

## Build v2.0.205 - f65ac12 - 2020-01-13 - Improvement: Status Item Icons
* f65ac12: Give the individual Status Items an icon for prettier visuals (_Thierry Draper_)

## Build v2.0.204 - ec66879 - 2020-01-12 - Composer: January 2020
* (Auto-commits only)

## Build v2.0.204 - 19f0465 - 2020-01-11 - Improvement: Status Incident List
* 19f0465: Fix a Status unit test we had wrong and fixed when adding incidents (_Thierry Draper_)
* 42c7b78: Display a list of individual incidents (current and limited list of recently resolved) on the Status site (_Thierry Draper_)

## Build v2.0.202 - 05f2ec2 - 2020-01-07 - Improvement: Expanded Incident Details
* 05f2ec2: Move the status incident time to a trailing attribute, rather than leading (_Thierry Draper_)
* 7d1b2e4: Update the incident builder to include some end-user facing notes in the form of a title and detail (rather than just single notes field) (_Thierry Draper_)

## Build v2.0.200 - cfffc73 - 2020-01-06 - Hotfix: HTTP Check Error Reason
* cfffc73: Include a more informative reason for HTTP checks in the error message (_Thierry Draper_)

## Build v2.0.199 - af57728 - 2020-01-02 - Feature: System Status Sub-site
* af57728: System Status unit tests, with split Feature tests but merged in the existing Unit tests (to preserve 100% code coverage) (_Thierry Draper_)
* 169cfb1: New System Status subsite homepage, with corresponding filesystem grouping (_Thierry Draper_)

## Build v2.0.197 - c5f571a - 2019-12-31 - SysAdmin: Prune SCHEMA server-sync steps
* c5f571a: Prune the code/schema server-sync steps we've new mechanisms for (_Thierry Draper_)

## Build v2.0.196 - e7059f4 - 2019-12-29 - Feature: Incident Management
* e7059f4: Incident identification and grouping (both public and internal) based on the outcome of the existing checks (_Thierry Draper_)

## Build v2.0.195 - 08589cf - 2019-12-28 - Hotfix: HTTP Check Recovery
* 08589cf: Shorten the HTTP log 'info' column to fit in to the VARCHAR column (_Thierry Draper_)
* 7ff1faa: Correctly identify 'resolved' HTTP checks (_Thierry Draper_)

## Build v2.0.193 - fb35c99 - 2019-12-26 - Hotfix: PHPUnit CI Job Shell Errors
* fb35c99: Fix tput/clear errors during the PHPUnit CI job (_Thierry Draper_)

## Build v2.0.192 - fadf21f - 2019-12-21 - Hotfix: Detail Table Pagination
* fadf21f: Log detail JS handling was blocking table pagination (_Thierry Draper_)

## Build v2.0.191 - da6810b - 2019-12-21 - Hotfix: HTTP Check Unit Tests
* da6810b: HTTP check unit tests (_Thierry Draper_)

## Build v2.0.190 - c1be195 - 2019-12-21 - Feature: HTTP Check
* c1be195: Database migration standardisation of the *INT columns to use the unsigned*Int method instead of *int('col')->unsigned() longform (_Thierry Draper_)
* 88d43b6: Standardise the scripts to record dates/times as UTC (to prevent issues with changing in and out of summertime) (_Thierry Draper_)
* 66041fa: Changes in the interface for the new HTTP check (_Thierry Draper_)
* 0444417: The new HTTP check script and database changes (_Thierry Draper_)

## Build v2.0.186 - 6f5a01b - 2019-12-20 - Improvement: ScrollTable Shading
* 6f5a01b: ScrollTable shading changes specific to the SysMon checks (_Thierry Draper_)

## Build v2.0.185 - b11ec6d - 2019-12-17 - Hotfix: Logfile Detail Display
* b11ec6d: Internal Status check detail chart colour fix (_Thierry Draper_)
* b3423b4: Logfile detail page fix around the per-check detail text presentation (_Thierry Draper_)

## Build v2.0.183 - fcd092b - 2019-12-16 - Hotfix: Empty Log Detail Charts
* fcd092b: Unit test fixes for the empty log breakdown charts (_Thierry Draper_)
* 8856bf1: Fix empty "log" detail charts by adding a faux (empty) series (_Thierry Draper_)

## Build v2.0.181 - f9b6de7 - 2019-12-15 - Composer: December 2019
* (Auto-commits only)

## Build v2.0.181 - ef0fb99 - 2019-12-14 - Feature: Secondary Detail Charts
* ef0fb99: Add secondary charts to the check detail page (such as current usage as a pie chart) (_Thierry Draper_)

## Build v2.0.180 - abe7f9c - 2019-12-11 - Improvement: Check Detail Tables to ScrollTable
* abe7f9c: Convert the check detail tables to the new ScrollTable widget (_Thierry Draper_)

## Build v2.0.179 - e2f6c00 - 2019-12-07 - Improvement: Email Usage by Domain
* e2f6c00: Appropriate unit test changes for the new column (_Thierry Draper_)
* 362ab39: Corresponding interface changes to use our new column (though the table will be tidied up separately shortly) (_Thierry Draper_)
* 8d81559: Split out the email usage by domain (whilst also moving some parts to the 'other' pot) (_Thierry Draper_)
* 235d1f0: Compress the initial migrations into a grouped file, rather than one per table (_Thierry Draper_)

## Build v2.0.175 - 6ff2a1c - 2019-11-19 - CI: PHP Comments Standards
* 6ff2a1c: Fall-out from the new PHP comments standards being enforced (_Thierry Draper_)
* d99fd8c: Extend our PSR-12 standards check to include the format of comments (_Thierry Draper_)

## Build v2.0.173 - 7f4ed9b - 2019-11-16 - Composer: November 2019
* (Auto-commits only)

## Build v2.0.173 - c6bcc6e - 2019-11-14 - CI: YAML Fixes and Improvements
* c6bcc6e: CI YAML fixes and improvements, such as moving the PHP/MySQL versions used to GitLab (_Thierry Draper_)

## Build v2.0.172 - bc49c0a - 2019-11-05 - Composer: Laravel 6 Fallout
* bc49c0a: Explicitly state the non-integer primary key types within models (_Thierry Draper_)

## Build v2.0.171 - 5dbd77d - 2019-10-23 - Composer: October 2019
* 5dbd77d: PSR-12 whitespace fixes (_Thierry Draper_)

## Build v2.0.170 - d4d25e8 - 2019-10-19 - Improvement: CSS Linting
* d4d25e8: Fix the CSS Standards CI job so it runs in the correct group (_Thierry Draper_)
* ef9f169: CSS tweaks from new linting and standards checks (_Thierry Draper_)
* e2fe7ed: Switch the CSS Linting test to use stylelint, and split in to both linting and standards (_Thierry Draper_)

## Build v2.0.167 - 70b946e - 2019-10-12 - Improvement: Lockfile Instance as Prefix
* 70b946e: Switch the Lockfile check to look for all stamps that start with the instance, not just is the instance (_Thierry Draper_)

## Build v2.0.166 - e6e8ce3 - 2019-09-25 - Composer: September 2019
* (Auto-commits only)

## Build v2.0.166 - 920dcf0 - 2019-09-04 - CI: Skip Polish GeoIP Test
* 920dcf0: Skip the Polish GeoIP test, as it's too variable (_Thierry Draper_)

## Build v2.0.165 - 5e6b47d - 2019-09-02 - CI: Fix 'Clean' Local Runs
* 5e6b47d: Fix the 'clean' running of our CI locally (_Thierry Draper_)

## Build v2.0.164 - 59c578d - 2019-08-27 - Composer: August 2019
* (Auto-commits only)

## Build v2.0.164 - a77bc10 - 2019-08-16 - Hotfix: Eager Load the ORM queries
* a77bc10: Localise the use of a SysMon specific CSS class that is also defined globally (_Thierry Draper_)
* 207240e: Minor correction of <?php tag use within a Blade template (_Thierry Draper_)
* e94fe2f: Eager load the page queries for performance purposes (_Thierry Draper_)

## Build v2.0.161 - feb9269 - 2019-08-15 - CI: 100% Code Coverage
* feb9269: Changes to get our Code Coverage up to 100% (_Thierry Draper_)

## Build v2.0.160 - 1fe43b7 - 2019-08-14 - CI: Standardise GeoIP Unit Tests
* 1fe43b7: Switch the GeoIP unit test definitions to config from the skeleton, standardising between the two repos (_Thierry Draper_)

## Build v2.0.159 - 6157cf2 - 2019-08-09 - CD: Move Server Details to Env Vars
* 6157cf2: Minor GeoIP unit test tweak given latest database update (_Thierry Draper_)
* db76878: Move the deploy server access details to environment variables (_Thierry Draper_)

## Build v2.0.157 - ebc8746 - 2019-07-21 - Composer: July 2019
* (Auto-commits only)

## Build v2.0.157 - 16eaf40 - 2019-07-17 - Feature: Monitor PHP Code Coverage
* 16eaf40: Enable PHP code coverage report during CI (_Thierry Draper_)

## Build v2.0.156 - fbcf930 - 2019-07-10 - Hotfix: Skip Deploy in LocalCI
* fbcf930: The new deploy stage should not be included in our localci run script (which also requires the script: rule to be last) (_Thierry Draper_)

## Build v2.0.155 - 685d154 - 2019-07-05 - Feature: Enable Continuous Delivery
* 685d154: Envoy script for implementing our Continuous Delivery (_Thierry Draper_)

## Build v2.0.154 - a0e987d - 2019-07-04 - Feature: RSS Sync Check
* a0e987d: Whilst checking for errors in the RSS feed logs, also check for out-of-date feed syncs (_Thierry Draper_)

## Build v2.0.153 - f9ec4aa - 2019-06-18 - CI: PHPUnit vendor simplification
* f9ec4aa: Fix, and where appropriate merge, vendor folder creation within the CI (_Thierry Draper_)
* ee9e5ef: Installing the skeleton for CI is a simpler process (_Thierry Draper_)
* 69e7cbe: Ensure composer has a vendor folder to be installed into (_Thierry Draper_)
* e1b99fe: Propagate the CI_FAUX flag when running CI tests locally, but from clean (_Thierry Draper_)

## Build v2.0.149 - b3bd980 - 2019-06-11 - Hotfix: Report URI instance STH creation
* b3bd980: Report URI instance sth creation needs to be callable even if no "traditional" errors have been found (_Thierry Draper_)

## Build v2.0.148 - 659d3ff - 2019-06-09 - SysAdmin: Sprites to CDN
* 659d3ff: Move the sprites to the CDN (_Thierry Draper_)

## Build v2.0.147 - fc68d1f - 2019-06-08 - CI: Data Download Errors
* fc68d1f: When downloading test CI data return an error if the download fails, rather than assume it was a 204/skip (_Thierry Draper_)

## Build v2.0.146 - bbd6ecd - 2019-06-08 - CI: Handle Perl Upgrades
* bbd6ecd: May need to create the base CPAN CI dir (_Thierry Draper_)
* 778909c: Handle changing versions of perl in the CI (_Thierry Draper_)

## Build v2.0.144 - e4539e9 - 2019-05-24 - Feature: Process production's error_log
* e4539e9: New part of the Report URI pre-processor to scan for PHP errors from the error_log on production that didn't get caught via our expected method (_Thierry Draper_)

## Build v2.0.143 - 777c08f - 2019-05-22 - Hotfix: Highcharts CSP false-positives
* 777c08f: Individual check's chart needs to be CSP compatible (_Thierry Draper_)

## Build v2.0.142 - 06ef4fe - 2019-05-21 - CI: Fix PHPUnit skeleton setup
* 06ef4fe: rsync management of PHPUnit test setup fix (_Thierry Draper_)

## Build v2.0.141 - 01a0d06 - 2019-05-19 - Composer: May 2019 updates
* 01a0d06: Upgrade our Highcharts links to a more generic "v7" link, as we're not tied to specific minor version (_Thierry Draper_)

## Build v2.0.140 - 73611e6 - 2019-05-02 - Migrations: Merge w/Stored Procedures
* 73611e6: Re-organise the database migrations layouts to keep all sub-grouped migrations/stored procs together (_Thierry Draper_)

## Build v2.0.139 - 584cce9 - 2019-04-24 - Migrations: Connection-appropriate DROP TABLES
* 584cce9: Migration "drop tables" needs appropriate Laravel connection applied (_Thierry Draper_)

## Build v2.0.138 - 4785a97 - 2019-04-22 - CI: Testing Migrations
* 4785a97: Include running the database migrations properly as part of the MySQL Linting CI job (plus testing their rollbacks) (_Thierry Draper_)

## Build v2.0.137 - 825d33c - 2019-04-16 - Composer: April 2019 updates
* 825d33c: Some local 'clean' CI test fixes and code optimisations (_Thierry Draper_)

## Build v2.0.136 - fc03cb8 - 2019-04-09 - CI: Fix Logging
* fc03cb8: Automatically fix the logs when cloning the skeleton within CI (_Thierry Draper_)

## Build v2.0.135 - 4d3248f - 2019-04-08 - CI: Check Migrations
* 4d3248f: Add the migrations to our CI, including a PSR-12 workaround for Laravel's migrations (who do not follow PSR-4's fully qualified class name) (_Thierry Draper_)
* 85cc32a: Re-jig how we run Laravel's migrations script should we sub-filter our migrations (_Thierry Draper_)

## Build v2.0.133 - d77f4dd - 2019-03-29 - CI: Exclude Tags
* d77f4dd: Copy across the GeoIP IP ranges from the equivalent skeleton test (_Thierry Draper_)
* 99bf51f: Prevent CI when pushing tags (_Thierry Draper_)

## Build v2.0.131 - e321fd8 - 2019-03-26 - Feature: Modernise Server DNS Lookup
* e321fd8: Slightly modernise the server script's hostname -> IP lookup mechanism (which is even flagged as legacy in perldoc) (_Thierry Draper_)

## Build v2.0.130 - 0b8ee92 - 2019-03-26 - CI: Re-grouped Git remote
* 0b8ee92: CI perl lint DBI loading definition a bit loose? (_Thierry Draper_)
* 942f364: Reflect re-grouping of the git remotes in our CI scripts (_Thierry Draper_)

## Build v2.0.128 - 7d00854 - 2019-03-22 - CI: MySQL Linter
* 7d00854: Minor changes as part of importing a MySQL linter (_Thierry Draper_)

## Build v2.0.127 - 35d585c - 2019-03-10 - Hotfix: Post Launch
* 35d585c: Start using a singular composer cache to try and cut down on CI pipeline build time (_Thierry Draper_)
* e13b209: Revise the way test databases are created, and include some post-test cleanup of them (_Thierry Draper_)
* def609f: Fixes from running the Perl linter (_Thierry Draper_)
* 7a41116: CI refinements to the Composer vendor caching rules and include a new Perl-linting process (_Thierry Draper_)
* 1aaf17e: Make the unit tests compatible with some skeleton filesystem tweaks (_Thierry Draper_)
* 2c6a358: Test script tweaks to attempt repo-name parity between repo and skeleton (_Thierry Draper_)
* ccbde38: Root-level merged folder isn't actually required (_Thierry Draper_)
* 6240296: The 'scripts/run' folder isn't something to be preserved in git (_Thierry Draper_)
* 419ac1b: Tidy up the icon-and-text markup to prevent unwanted whitespace appearing between the icon and text (_Thierry Draper_)
* 9b85926: Previous attempt failed, reverting to stand-alone test using same (but hard-coded) details as the skeleton test (_Thierry Draper_)
* 97b33e2: Experimental unit test for the GeoIP report_uri script parser (_Thierry Draper_)
* 1c7a99b: Proper fix for the Report URI GeoIP calc (unit testing coming...!) (_Thierry Draper_)
* 8abd9fe: Fix (and PSR!) the pre-processing script to get GeoIP info of reports (_Thierry Draper_)
* f4e42c4: Comms check log table migration needs to have the TINYINTs as UNSIGNED (_Thierry Draper_)
* 3571a3e: Add symlinks required from live (_Thierry Draper_)

## Build v2.0.112 - fe6792b - 2019-02-17 - Launching Laravel - [![Version: 2.0](https://img.shields.io/badge/Version-2.0-brightgreen.svg)](https://gitlab.com/debear/sysmon/tree/v2.0)
* fe6792b: Git tidy pre merge of Laravel branch (_Thierry Draper_)
* 73646b3: Make the VERSION info part of the repo (_Thierry Draper_)
* 65e859c: Minor CI tidying (_Thierry Draper_)
* deda833: Fix the Unit unit tests... to actually run! (_Thierry Draper_)
* 3193a0d: Unit-level unit tests for the models, including a minor resulting fix (_Thierry Draper_)
* 1da5305: Cache pagination tables after load so re-loading doesn't require another server hit (_Thierry Draper_)
* d7d380d: Highchart charts for each of the check types (_Thierry Draper_)
* 6c22894: Unit tests for the individual check tables (_Thierry Draper_)
* 5703ccf: Basic page for a single check - tabular info with pagination, responded (_Thierry Draper_)
* 6b56af5: Unit tests for the logs page (_Thierry Draper_)
* 7e9a256: Individual logs page (_Thierry Draper_)
* b7f73f5: Remove the navigation bar (_Thierry Draper_)
* 3a7c659: CI test tweak (_Thierry Draper_)
* ef59f75: Minor mess detector fix from copy-from-legacy (_Thierry Draper_)
* d354141: Expand tests to include the new files, plus a basic test of the homepage and access rights (_Thierry Draper_)
* ba8f52b: New version of the homepage, listing all checks (_Thierry Draper_)
* 769ef73: Unit tests (correctly!) fail as homepage is now hidden behind a required password (_Thierry Draper_)
* 012f95c: Initial layout and URL setup (_Thierry Draper_)
* 4e85941: Migrations for the schema (_Thierry Draper_)
* 120afc7: Copy across latest test database scripts (_Thierry Draper_)
* 89ddd35: Regular Composer update (_Thierry Draper_)
* 230c15f: Minor CI tweaks copied from "fuller" WWW changes (_Thierry Draper_)
* 20eef0a: Implement a "feature" PHPUnit test, testing access to the homepage (_Thierry Draper_)
* 2772325: Refine the way sub-site routes are loaded (_Thierry Draper_)
* 6695d28: CI composer fix to only install missing/out-of-date packages rather than update outside of composer.lock context (_Thierry Draper_)
* 62f2e90: Disable JS Standards CI test until we have JS to test... (_Thierry Draper_)
* 5b217ff: Include extended JS standards checking in the CI (_Thierry Draper_)
* 59b642f: Script to run the CI tests locally (and accessing the composer libs directly, rather than through a git-runner) (_Thierry Draper_)
* 50f6b85: Add PSR and Pipeline Status flags to the README (_Thierry Draper_)
* 7d969af: Update the CONTRIBUTING.md file following confirmation of PSR use (_Thierry Draper_)
* 58c3a21: Add PSR-12 compliance testing to our CI (_Thierry Draper_)
* 93e3249: Minor PSR tidying following a phpcs run (_Thierry Draper_)
* e478cf0: Add some CSS and JS linting to our CI (_Thierry Draper_)
* 14cc99f: PHPMD definition tweaks (_Thierry Draper_)
* f4f4d2f: Switch PHP we are testing against from 7.3 to 7.2 (_Thierry Draper_)
* 06822f9: Initial project CI (_Thierry Draper_)
* 888cb6b: Minor LICENSE file tidying (_Thierry Draper_)
* e4c5136: Tweaked iconset CSS naming (_Thierry Draper_)
* f26846c: Fixed a symlink that needed to point a new location given the updated framework split (_Thierry Draper_)
* fc93030: Adding a .gitignore file (_Thierry Draper_)
* 47ef7fc: Base resources and config from www setup work (_Thierry Draper_)
* b683f77: Proof-of-concept route (_Thierry Draper_)
* 58c8396: Start of process of switching from Homebrew skeleton to Laravel (_Thierry Draper_)
* 9138fc3: Some missing SysMon script sites/(live|dev) -> sites updates (_Thierry Draper_)
* 55cdba2: Removal of the 'hosted dev' environment on production (_Thierry Draper_)

## Build v1.0.64 - 4150513 - 2018-06-10 - Homebrew
* _Changelog truncated for initial build_
