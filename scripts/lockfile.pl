#!/usr/bin/perl -w
# Check and report on a script lockfiles
# TD. 2017-03-15.
#     2019-10-10. Added argument-as-a-prefix expectation.

use strict;
our %config;

BEGIN {
  use File::Basename;
  my $core = dirname(__FILE__) . '/lib/core.pl';
  require $core;
}

##
## Getter
##

$config{'dbh_alt'}{'lockfile'} = dbi_connect($config{'db_lockfile'});
my $sql = 'SELECT * FROM LOCKFILE_STAMPS WHERE app LIKE CONCAT(?, "%") ORDER BY app;';
my $sth = $config{'dbh_alt'}{'lockfile'}->prepare($sql);
$sth->execute($config{'instance'})
  or log_error("Unable to execute dbh_alt->lockfile: " . $config{'dbh_alt'}{'lockfile'}->errstr);
my @locks = ();
while (my $lock = $sth->fetchrow_hashref) {
  push @locks, $lock;
}

##
## Analyse
##

my $state; my $status; my $info; my $summary_field; my $summary_msg;

# Check A: No info
if (!@locks) {
  log_msg('No instance(s) found. Silently ignoring.');
  $state = 'missing';
  $status = 'warn';
  $info = $summary_msg = 'No database row(s)';
  $summary_field = 2;

# Check B: started < finished
} else {
  $info = ''; my $failed = 0; my $date;
  foreach my $lock (@locks) {
    if (@locks > 1) {
      $info .= "$$lock{'app'}: ";
    }
    $info .= 'Started: ' . $$lock{'last_started'} . '; Finished: ' . $$lock{'last_finished'} . "\n";
    # Did it fail?
    $failed++
      if $$lock{'last_started'} gt $$lock{'last_finished'};
    # Checking last date
    my $dt = substr($$lock{'last_finished'}, 0, 10);
    $date = $dt
      if !defined($date) || $date < $dt;
  }

  # If we failed, say so...
  if ($failed) {
    $state = 'failed';
    $status = 'critical';
    $summary_field = 0;

  # Check C: Recent enough check? (i.e., it's not been run...?)
  } elsif (defined($config{'cmp-db'})) {
    log_msg("Dates valid. Checking recentness of data from '$date'.");

    $config{'dbh_alt'}{'date'} = dbi_connect($config{'cmp-db'});
    my $sql_where = (defined($config{'cmp-where'}) ? ' AND ' . $config{'cmp-where'} : '');
    $sql = "SELECT COUNT(*) AS num_gap FROM $config{'cmp-tbl'} WHERE $config{'cmp-col'} < CURDATE()$sql_where;";
    $sth = $config{'dbh_alt'}{'date'}->prepare($sql);
    $sth->execute
      or log_error("Unable to execute dbh_alt->date: " . $config{'dbh_alt'}{'date'}->errstr);
    my ($num_gap) = $sth->fetchrow_array;
    $info .= '- ' . int($num_gap) . ' row(s) since last finished';

    if ($num_gap) {
      $state = 'failed';
      $status = 'critical';
      $summary_field = 0;
    } else {
      $state = 'valid';
      $status = 'okay';
      $summary_field = 1;
    }

  # No secondary check, so all valid
  } else {
    $state = 'valid';
    $status = 'okay';
    $summary_field = 1;
  }
  $summary_msg = "Lockfile $state";

  # Let's check for a 'recovery' status, if we're (now) okay
  $status = determine_status_recovered()
    if $status eq 'okay';
}

##
## Store
##

# Notify & Log
my $notified = check_send_notif($info, $summary_msg, $status);
log_run($status, $summary_field, $notified, $info);

# Store our lockfile status check
$sql = "INSERT INTO SYSMON_LOCKFILE (app, checked, state, status, notified)
  VALUES (?, UTC_DATE(), ?, ?, ?)
ON DUPLICATE KEY UPDATE state = VALUES(state),
                        status = VALUES(status),
                        notified = VALUES(notified);";
$sth = $config{'dbh'}->prepare($sql);
$sth->execute($config{'instance'}, $state, $status, $notified)
  or log_error("Unable to execute sth->lockfile: " . $config{'dbh'}->errstr);
