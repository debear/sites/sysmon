#!/usr/bin/perl -w
# Check and report on logfile issues
# TD. 2017-12-18.

use strict;
use utf8;
use POSIX;
use Compress::Zlib;
our %config;

BEGIN {
  use File::Basename;
  my $core = dirname(__FILE__) . '/lib/core.pl';
  require $core;
}

# Load the detail code for this specific instance
dynamic_require(__DIR__ . '/detail/logs/' . $config{'instance'} . '.pl');

# Identify the file for the day before
our @yest = localtime(time() - 86400); # 86400 = 24 * 60 * 60
my $log_yest = strftime($config{'log_file'}{'fmt'}, @yest);
my $log_yest_db = strftime($config{'log_file'}{'fmt'} . ' ' . ($config{'check'}{'frequency_type'} eq 'hourly' ? '%H' : '00') . ':00:00', @yest);

# Process the logs
my ($warnings, $breakdown, $total_warnings, $summary_col) = parse_logs($log_yest);

# Summarise
my $detail = ''; my $info = ''; my $summary = '';
foreach my $t (sort keys %{$breakdown}) {
  my $section = "By $t:\n";
  foreach my $k (sort {$$breakdown{$t}{$b} cmp $$breakdown{$t}{$a}} keys %{$$breakdown{$t}}) {
    $section .= "- $k: " . $$breakdown{$t}{$k} . "\n";
  }
  $info = $section
    if $t eq $summary_col;
  $detail .= "$section\n";
}

# Append the individual instances
if ($total_warnings) {
  $detail .= "Raw List:\n- " . join("\n- ", @$warnings) . "\n";
} else {
  $detail  = 'No warnings found.';
}

# Determine status informatiom
my $status;
if (!$total_warnings) {
  $info = $summary = 'No warnings found';
  # Let's check for a 'recovery' status, if we're (now) okay
  $status = determine_status_recovered();
} else {
  $summary = $total_warnings . ' warning' . ($total_warnings > 1 ? 's' : '') . ' found';
  if (defined($config{'check'}{'status_critical'}) && ($total_warnings >= $config{'check'}{'status_critical'})) {
    $status = 'critical';
  } elsif (defined($config{'check'}{'status_warn'}) && ($total_warnings >= $config{'check'}{'status_warn'})) {
    $status = 'warn';
   } else {
    $status = 'critical';
  }
}
log_msg("$summary.");

# Notify & Log
my $notified = check_send_notif($detail, $summary, $status);
log_run($status, $total_warnings, $notified, $info, $log_yest_db);

# Store our lockfile status check
my $sql = "INSERT INTO SYSMON_LOGFILE (app, checked, errors, detail, status, notified)
  VALUES (?, ?, ?, ?, ?, ?)
ON DUPLICATE KEY UPDATE errors = VALUES(errors),
                        detail = VALUES(detail),
                        status = VALUES(status),
                        notified = VALUES(notified);";
my $sth = $config{'dbh'}->prepare($sql);
$sth->execute($config{'instance'}, $log_yest_db, $total_warnings, $detail, $status, $notified)
  or log_error("Unable to execute sth->log: " . $config{'dbh'}->errstr);

# And then our breakdown details
$sql = 'INSERT INTO SYSMON_LOGFILE_BREAKDOWN (app, checked, section, errors)
  VALUES (?, ?, ?, ?)
ON DUPLICATE KEY UPDATE errors = VALUES(errors);';
$sth = $config{'dbh'}->prepare($sql);
foreach my $k (sort keys %{$$breakdown{$summary_col}}) {
  $sth->execute($config{'instance'}, $log_yest_db, $k, $$breakdown{$summary_col}{$k})
    or log_error("Unable to execute sth->breakdown: " . $config{'dbh'}->errstr);
}
