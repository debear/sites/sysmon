#!/usr/bin/perl -w
use strict;
use Dir::Self;
use Cwd 'abs_path';
our %config = ();

# Environment related
our $home_dir;
$config{'is_dev'} = (substr($home_dir, 0, 5) eq '/var/');

# Load config / libs
foreach my $file ('../config/worker.pl', '../lib/dbi.pl', '../lib/debug.pl', 'notify.pl', 'notify/twitter.pl') {
  my $config = abs_path(__DIR__ . "/$file");
  require $config;
}

# Main config
our $script;

# Timestamp that we started...
$config{'started'} = strftime('%Y-%m-%d %H:%M:%S', localtime);
$config{'started_gmt'} = strftime('%Y-%m-%d %H:%M:%S', gmtime);

# Logging related
$config{'debug'} = grep(/^--debug$/, @ARGV);
$config{'verbose'} = grep(/^--verbose$/, @ARGV);
$config{'logs'} = [ ];
$config{'logs_dir'} = abs_path(__DIR__ . '/../../logs/scripts-post-process');
$config{'log_inc_time'} = '%H%M';

# List of DBI statements we'll be preparing
$config{'sth'} = { };

# List of sub-modules we've loaded
$config{'script_processing'} = { };

# Generic notification settings
$config{'notif'} = {
  'enabled' => !grep(/^--skip-notify$/, @ARGV),
  'max_age' => 60, # Max time (in minutes) after a log is generated that a notification could be sent
};

# "Dynamic" (single-line) require, since `require __DIR__.'/script.pl'` is syntactically invalid
sub dynamic_require {
  my ($path) = @_;
  require $path
    if -e $path;
}

# Prepare the SQL statements
sub prepare_sth {
  return if defined($config{'sth'}{'get'});

  my $sql = 'SELECT LOG.check_id, SGROUPS.group_id, SGROUPS.name AS group_name, SGROUPS.code AS group_code,
       ITEM.name AS item_name, ITEM.admin_only, ITEM.notifications_on,
       CHECKS.script, CHECKS.name, CHECKS.instance, CHECKS.frequency, CHECKS.status_id,
       LOG.log_date, LOG.started, LOG.status, LOG.notified, LOG.info AS log_info,
       LOG.started >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL ? MINUTE) AS incident_notify
FROM SYSMON_LOG AS LOG
LEFT JOIN SYSMON_CHECKS AS CHECKS
  ON (CHECKS.check_id = LOG.check_id)
LEFT JOIN SYSMON_STATUS_ITEMS AS ITEM
  ON (ITEM.status_id = CHECKS.status_id)
LEFT JOIN SYSMON_STATUS_GROUPS AS SGROUPS
  ON (SGROUPS.group_id = ITEM.group_id)
WHERE LOG.processed = 0
ORDER BY LOG.started, LOG.finished, LOG.check_id;';
  $config{'sth'}{'get'} = $config{'dbh'}->prepare($sql);

  $sql = 'SELECT status_id FROM SYSMON_CHECKS_STATUS_OVERRIDE WHERE check_id = ? AND sub_key = ?;';
  $config{'sth'}{'get_override'} = $config{'dbh'}->prepare($sql);

  $sql = 'UPDATE SYSMON_LOG SET processed = 1 WHERE check_id = ? AND log_date = ?;';
  $config{'sth'}{'processed'} = $config{'dbh'}->prepare($sql);

  $sql = 'SELECT * FROM SYSMON_INCIDENTS WHERE group_id = ? AND check_id = ? AND is_current = 1;';
  $config{'sth'}{'current_incident'} = $config{'dbh'}->prepare($sql);

  $sql = 'UPDATE SYSMON_INCIDENTS SET resolved = ?, is_current = 0 WHERE group_id = ? AND date_ref = ? AND incident_id = ?;';
  $config{'sth'}{'resolve_incident'} = $config{'dbh'}->prepare($sql);

  $sql = 'UPDATE SYSMON_INCIDENTS SET status = ? WHERE group_id = ? AND date_ref = ? AND incident_id = ?;';
  $config{'sth'}{'update_incident'} = $config{'dbh'}->prepare($sql);

  $sql = 'INSERT INTO SYSMON_INCIDENTS (group_id, group_code, date_ref, incident_id, status_id, check_id, status, title, detail, started, resolved, is_current)
  VALUES (?, ?, DATE_FORMAT(?, "%y%j"), NULL, ?, ?, ?, ?, ?, ?, NULL, 1);';
  $config{'sth'}{'raise_incident'} = $config{'dbh'}->prepare($sql);

  $sql = 'INSERT INTO SYSMON_INCIDENTS_LEDGER (group_id, date_ref, incident_id, ledger_id, status_id, status_from, status_to, status_changed)
  VALUES (?, ?, ?, NULL, ?, ?, ?, ?);';
  $config{'sth'}{'incident_ledger'} = $config{'dbh'}->prepare($sql);
}

# Post-processing incident data (such as statuses and metrics)
sub post_process {
  my $sql;
  # Bubbling statuses to Items
  $sql = 'INSERT INTO SYSMON_STATUS_ITEMS (status_id, name, group_id, `order`, status, current_started, last_started, last_resolved)
  SELECT status_id, "IGNORED" AS name, 0 AS group_id, 0 AS `order`,
         IF(SUM(is_current AND status = "critical") > 0, "critical",
            IF(SUM(is_current AND status = "warn") > 0, "warn",
               IF(SUM(is_current AND status = "recovered") > 0, "recovered",
                  IF(SUM(is_current = 0) > 0, "okay", "unknown")))) AS status,
         MAX(IF(is_current, started, NULL)) AS current_started,
         SUBSTRING(MAX(IF(is_current = 0, CONCAT(date_ref, incident_id, ":", resolved, ":", started), NULL)), 29, 19) AS last_started,
         SUBSTRING(MAX(IF(is_current = 0, CONCAT(date_ref, incident_id, ":", resolved, ":", started), NULL)), 9, 19) AS last_resolved
  FROM SYSMON_INCIDENTS
  GROUP BY status_id
  ORDER BY status_id
ON DUPLICATE KEY UPDATE status = VALUES(status),
                        current_started = VALUES(current_started),
                        last_started = VALUES(last_started),
                        last_resolved = VALUES(last_resolved);';
  $config{'dbh'}->do($sql);

  # Bubbling statuses to Groups
  $sql = 'INSERT INTO SYSMON_STATUS_GROUPS (group_id, name, code, `order`, status_user, status_admin, last_outage)
  SELECT group_id, "IGNORED" AS name, "IGN" AS code, 0 AS `order`,
         IF(SUM(admin_only = 0 AND status = "critical") > 0, "outage",
            IF(SUM(admin_only = 0 AND status = "warn") > 0, "issues", "operational")) AS status_user,
         IF(SUM(status = "critical") > 0, "outage",
            IF(SUM(status = "warn") > 0, "issues", "operational")) AS status_admin,
         MAX(IFNULL(current_started, last_resolved)) AS last_outage
  FROM SYSMON_STATUS_ITEMS
  GROUP BY group_id
  ORDER BY group_id
ON DUPLICATE KEY UPDATE status_user = VALUES(status_user),
                        status_admin = VALUES(status_admin),
                        last_outage = VALUES(last_outage);';
  $config{'dbh'}->do($sql);

  # The base query for our Metrics calcs
  my $metrics_sql_ins = 'INSERT INTO SYSMON_STATUS_{TBL}_METRICS ({COL}, when_parsed, is_current, metrics_7num, metrics_7avg, metrics_14num, metrics_14avg, metrics_30num, metrics_30avg, metrics_90num, metrics_90avg)
  SELECT {COL}, UTC_DATE() AS when_parsed, 1 AS is_current,
         SUM(started > DATE_SUB(UTC_DATE(), INTERVAL 6 DAY)) AS metrics_7num,
         AVG(IF(started > DATE_SUB(UTC_DATE(), INTERVAL 6 DAY), (UNIX_TIMESTAMP(IFNULL(resolved, NOW())) - UNIX_TIMESTAMP(started)) / 3600, NULL)) AS metrics_7avg,
         SUM(started > DATE_SUB(UTC_DATE(), INTERVAL 13 DAY)) AS metrics_14num,
         AVG(IF(started > DATE_SUB(UTC_DATE(), INTERVAL 13 DAY), (UNIX_TIMESTAMP(IFNULL(resolved, NOW())) - UNIX_TIMESTAMP(started)) / 3600, NULL)) AS metrics_14avg,
         SUM(started > DATE_SUB(UTC_DATE(), INTERVAL 29 DAY)) AS metrics_30num,
         AVG(IF(started > DATE_SUB(UTC_DATE(), INTERVAL 29 DAY), (UNIX_TIMESTAMP(IFNULL(resolved, NOW())) - UNIX_TIMESTAMP(started)) / 3600, NULL)) AS metrics_30avg,
         SUM(started > DATE_SUB(UTC_DATE(), INTERVAL 89 DAY)) AS metrics_90num,
         AVG(IF(started > DATE_SUB(UTC_DATE(), INTERVAL 89 DAY), (UNIX_TIMESTAMP(IFNULL(resolved, NOW())) - UNIX_TIMESTAMP(started)) / 3600, NULL)) AS metrics_90avg
  FROM SYSMON_INCIDENTS
  WHERE started > DATE_SUB(UTC_DATE(), INTERVAL 89 DAY)
  GROUP BY {COL}
  ORDER BY {COL}
ON DUPLICATE KEY UPDATE is_current = VALUES(is_current),
                        metrics_7num = VALUES(metrics_7num),
                        metrics_7avg = VALUES(metrics_7avg),
                        metrics_14num = VALUES(metrics_14num),
                        metrics_14avg = VALUES(metrics_14avg),
                        metrics_30num = VALUES(metrics_30num),
                        metrics_30avg = VALUES(metrics_30avg),
                        metrics_90num = VALUES(metrics_90num),
                        metrics_90avg = VALUES(metrics_90avg);';
  my $metrics_sql_upd = 'UPDATE SYSMON_STATUS_{TBL}_METRICS SET is_current = 0 WHERE when_parsed < UTC_DATE() AND is_current = 1;';;

  # Metrics for Items
  $sql = $metrics_sql_ins; $sql =~ s/{TBL}/ITEMS/g; $sql =~ s/{COL}/status_id/g;
  $config{'dbh'}->do($sql);
  $sql = $metrics_sql_upd; $sql =~ s/{TBL}/ITEMS/g;
  $config{'dbh'}->do($sql);

  # Metrics for Groups
  $sql = $metrics_sql_ins; $sql =~ s/{TBL}/GROUPS/g; $sql =~ s/{COL}/group_id/g;
  $config{'dbh'}->do($sql);
  $sql = $metrics_sql_upd; $sql =~ s/{TBL}/GROUPS/g;
  $config{'dbh'}->do($sql);
}

# Given a log item, get the individual checks
sub get_checks {
  my ($log) = @_;

  # Load the appropriate module
  if (!defined($config{'script_processing'}{$$log{'script'}})) {
    dynamic_require(__DIR__ . "/incidents/$$log{'script'}.pl");
  }
  # Make the appropriate call
  $config{'script_processing'}{$$log{'script'}}($log);
}

# Return true to pacify the compiler
1;
