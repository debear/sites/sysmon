#!/usr/bin/perl -w
use strict;
use Dir::Self;
our %config;

# Load our Twitter-specific config
dynamic_require(__DIR__ . '/../../config/twitter.pl');

# Make a public notification via a Tweet
sub notify_public_tweet {
  my ($incident, $check, $tweet_type) = @_;
  log_msg("- Sending '$tweet_type' notification tweet.");

  # Ensure we are setup fully
  prepare_sth_twitter();

  # Get a Tweet template to run
  $config{'sth'}{'tweet_template'}->execute($config{'notif_twitter'}{'our_app'}, $tweet_type)
    or log_error("Unable to execute sth->tweet_template: " . $config{'dbh_alt'}{'twitter'}->errstr);
  my ($template_id, $tweet) = $config{'sth'}{'tweet_template'}->fetchrow_array;
  if (!defined($template_id)) {
    log_msg("  - ERROR: Unable to find suitable tweet template. Aborting.");
    return;
  }
  log_msg("  - Template $template_id selected.");

  # Interpolate some variables
  my %map = (
    'ref_code' => "$$incident{'group_code'}-$$incident{'date_ref'}$$incident{'incident_id'}",
    'title' => $$incident{'title'},
    'group' => $$check{'group_name'},
    'item' => $$check{'item_name'},
    'check' => $$check{'name'},
    'severity' => $$incident{'status'} eq 'warn' ? 'Warning' : 'Critical',
    'severity_emoji' => $$incident{'status'} eq 'warn' ? '&#x26A0;' : '&#x26D4;', # Close matches to the icon sprite
    'domain' => $config{'notif_twitter'}{'domain'},
  );
  foreach my $key (keys %map) {
    $tweet =~ s/\{$key\}/$map{$key}/g;
  }

  # Store for sending
  $config{'sth'}{'tweet_notify'}->execute(
    $config{'notif_twitter'}{'our_app'},
    $tweet_type,
    $template_id,
    $tweet,
    $config{'notif_twitter'}{'send_delay'},
    $config{'notif_twitter'}{'twitter_app'},
    $config{'notif_twitter'}{'twitter_account'}
  ) or log_error("Unable to execute sth->tweet_notify: " . $config{'dbh_alt'}{'twitter'}->errstr);
  my $tweet_id = $config{'dbh_alt'}{'twitter'}->last_insert_id(undef, undef, undef, undef);
  log_msg("  - Tweet queued with ID '$tweet_id'.");
}

# Ensure the database statements are prepared properly
sub prepare_sth_twitter {
  # Only needs running once
  return if defined($config{'dbh_alt'}{'twitter'});
  log_msg("  - First send, preparing database handles / queries.");
  my $sql;

  # Open a database connection to the appropriate database
  $config{'dbh_alt'}{'twitter'} = dbi_connect($config{'db_comms'});

  # Prepare the statement for our Tweet template getter
  $sql = 'SELECT template_id, template_body FROM COMMS_TWITTER_TEMPLATE WHERE app = ? AND tweet_type = ? ORDER BY RAND() LIMIT 1;';
  $config{'sth'}{'tweet_template'} = $config{'dbh_alt'}{'twitter'}->prepare($sql);

  # Then prepare our insert statement
  $sql = 'INSERT INTO COMMS_TWITTER (app, tweet_id, tweet_type, template_id, retweet_id, tweet_body, tweet_queued, tweet_send, tweet_status, twitter_app, twitter_account)
  VALUES (?, NULL, ?, ?, NULL, ?, NOW(), DATE_ADD(NOW(), INTERVAL ? MINUTE), "queued", ?, ?);';
  $config{'sth'}{'tweet_notify'} = $config{'dbh_alt'}{'twitter'}->prepare($sql);
}

# Return true to pacify the compiler
1;
