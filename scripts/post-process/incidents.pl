#!/usr/bin/perl -w
# Incident Management, post-checks.
# TD. 2019-12-24.

use strict;

#
# BEGIN: Update @INC on the Xilo server
#
BEGIN {
  use File::Basename;

  # Mask the filename as we're on a shared server
  our $script_raw = $0;
  our $script = basename($0, '.pl');
  $0 = basename $0;

  # Some path customisation needs to be made if running on the prod server
  our $home_dir;
  our $xilo_home = '/var/www/f6a0812f-78d0-45a6-bec0-c060de245116';
  my $xilo_custom_path = 'debear/sites/common/perl5'; # Relative to ~
  our $base_dir = dirname $script_raw;
  if ($base_dir =~ /^$xilo_home/) {
    # Okay, now link to the other perl modules
    unshift(@INC, $xilo_home.'/'.$xilo_custom_path);
    $home_dir = $xilo_home;
  } else {
    $home_dir = '/var/www';
  }

  my $config = dirname(__FILE__) . '/config.pl';
  require $config;
}

#
# END: Tie-up database connections and logs
#
END {
  dbi_disconnect() if defined(&dbi_disconnect);
  save_logs();
}

use Dir::Self;
use JSON;

our %config;

# Say we're going...
log_msg('===============');
log_msg('Run starting...');

# Prepare all queries
prepare_sth();

# Loop through the logs and process
$config{'sth'}{'get'}->execute($config{'notif'}{'max_age'})
  or log_error("Unable to execute sth->get: " . $config{'dbh'}->errstr);
while (my $log = $config{'sth'}{'get'}->fetchrow_hashref) {
  # Get the appropriate check detail for this log
  my @checks = get_checks($log);

  # Then process each individually
  foreach my $check (@checks) {
    my $check_line = "Check $$check{'check_id'}: $$check{'name'} - $$check{'log_date'} ($$check{'status'})";

    # If we've an 'okay' (or Unknown) returned, we've nothing to do.
    if (!defined($$check{'status_id'}) || $$check{'status'} =~ /^(okay|unknown)$/) {
      my $reason = (!defined($$check{'status_id'}) ? 'check' : 'status');
      if ($config{'verbose'}) {
        log_msg($check_line);
        log_msg("-> Not an appropriate log item ($reason), skipping.");
      }
      $config{'sth'}{'processed'}->execute($$check{'check_id'}, $$check{'log_date'})
        or log_error("Unable to execute sth->processed: " . $config{'dbh'}->errstr);
      next;
    }
    # If we're processing, log in all instances
    log_msg($check_line);

    # Determine if we have an existing incident open for this check
    $config{'sth'}{'current_incident'}->execute($$check{'group_id'}, $$check{'check_id'})
      or log_error("Unable to execute sth->current_incident: " . $config{'dbh'}->errstr);
    my $incident = $config{'sth'}{'current_incident'}->fetchrow_hashref;

    # If we've recovered, resolve the incident
    my $no_change = 0; my $raised = 0;
    if ($$check{'status'} eq 'recovered') {
      # If no existing incident... err, error?
      if (!defined($incident)) {
        log_msg('-> ERROR: Incident resolved, but no current incident found?');
        $config{'sth'}{'processed'}->execute($$check{'check_id'}, $$check{'log_date'})
          or log_error("Unable to execute sth->processed: " . $config{'dbh'}->errstr);
        next;
      }
      log_msg("-> Resolving incident $$incident{'group_code'}-$$incident{'date_ref'}$$incident{'incident_id'}.");
      $config{'sth'}{'resolve_incident'}->execute($$check{'started'}, $$incident{'group_id'}, $$incident{'date_ref'}, $$incident{'incident_id'})
        or log_error("Unable to execute sth->resolve_incident: " . $config{'dbh'}->errstr);

      # Pass to the public notification handler
      notify_public($incident, $check, $$incident{'status'}, $$check{'status'});

    # If we've no open incident, create one
    } elsif (!defined($incident)) {
      $config{'sth'}{'raise_incident'}->execute(
        $$check{'group_id'},
        $$check{'group_code'},
        $$check{'started'},
        $$check{'status_id'},
        $$check{'check_id'},
        $$check{'status'},
        defined($$check{'incident_title'}) ? $$check{'incident_title'} : undef,
        defined($$check{'incident_detail'}) ? $$check{'incident_detail'} : undef,
        $$check{'started'}
      ) or log_error("Unable to execute sth->raise_incident: " . $config{'dbh'}->errstr);
      $config{'sth'}{'current_incident'}->execute($$check{'group_id'}, $$check{'check_id'})
        or log_error("Unable to execute sth->current_incident: " . $config{'dbh'}->errstr);
      $incident = $config{'sth'}{'current_incident'}->fetchrow_hashref;
      $raised = 1;
      log_msg("-> Raised a new incident: $$incident{'group_code'}-$$incident{'date_ref'}$$incident{'incident_id'}.");

      # Pass to the public notification handler
      notify_public($incident, $check, 'okay', $$check{'status'});

    # Otherwise, do we need to do anything to the open incident?
    } elsif ($$incident{'status'} ne $$check{'status'}) {
      # Yes - a change in state was detected
      log_msg("-> Updating incident $$incident{'group_code'}-$$incident{'date_ref'}$$incident{'incident_id'} to status '$$check{'status'}.");
      $config{'sth'}{'update_incident'}->execute($$check{'status'}, $$incident{'group_id'}, $$incident{'date_ref'}, $$incident{'incident_id'})
        or log_error("Unable to execute sth->update_incident: " . $config{'dbh'}->errstr);

      # Pass to the public notification handler
      notify_public($incident, $check, $$incident{'status'}, $$check{'status'});

    } else {
      log_msg("-> Extends existing incident $$incident{'group_code'}-$$incident{'date_ref'}$$incident{'incident_id'}.");
      $no_change = 1;
    }

    # If we've a change, record it
    if (!$no_change) {
      my $status_from = (!$raised ? $$incident{'status'} : 'okay'); # If raising, we can't rely on $$incident (but can guess!)
      log_msg("-> Recording status change for incident $$incident{'group_code'}-$$incident{'date_ref'}$$incident{'incident_id'} from '$status_from' to '$$check{'status'}'.");
      $config{'sth'}{'incident_ledger'}->execute(
        $$incident{'group_id'},
        $$incident{'date_ref'},
        $$incident{'incident_id'},
        $$check{'status_id'},
        $status_from, # From
        $$check{'status'}, # To
        $$check{'started'}
      ) or log_error("Unable to execute sth->incident_ledger: " . $config{'dbh'}->errstr);
    }

    # Stamp that we've processed the log
    $config{'sth'}{'processed'}->execute($$check{'check_id'}, $$check{'log_date'})
      or log_error("Unable to execute sth->processed: " . $config{'dbh'}->errstr);
  }
}

# End by bubbling up the incidents and performing the metrics calcs
post_process();
