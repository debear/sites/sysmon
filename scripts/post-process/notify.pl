#!/usr/bin/perl -w
use strict;
our %config;

# Consider public notification of the incident
sub notify_public {
  my ($incident, $check, $status_from, $status_to) = @_;

  # Skip (silently) if notifications are disabled
  return if !$config{'notif'}{'enabled'} || !$$check{'notifications_on'};

  # If the check did not occur in the last x-hours, skip
  if (!$$check{'incident_notify'}) {
    log_msg("- Skipping notification as '$$check{'started'}' is deemed to be an old update.");
    return;
  # We only want public messages for public items
  } elsif ($$check{'admin_only'}) {
    log_msg("- Skipping notification on an admin-only check.");
    return;
  }

  # Raised
  if ($status_from eq 'okay') {
    notify_public_tweet($incident, $check, 'incident_raised');
  # Resolved
  } elsif ($status_to eq 'recovered') {
    notify_public_tweet($incident, $check, 'incident_resolved');
  # Changes
  } else {
    # Skip notifications at this stage (type: incident_update)
    log_msg("- Skipping notifications of status change.");
  }
}

# Return true to pacify the compiler
1;
