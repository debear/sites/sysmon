#!/usr/bin/perl -w
# HTTP Request Log to Incident breakdown
use strict;
our %config;

# Some additional database statements need defining
my $sql = 'SELECT path, status_expected, status_returned, duration, state, status, notified
FROM SYSMON_HTTP_REQUEST
WHERE subdomain = ?
AND   checked_date = DATE_FORMAT(?, "%y%j")
AND   checked_hour = HOUR(?)
ORDER BY path;';
$config{'sth'}{'log_http'} = $config{'dbh'}->prepare($sql);

# Our worker method
$config{'script_processing'}{'http_request'} = sub {
  my ($log) = @_;
  my @checks = ( );
  my %codes = build_response_codes();

  # Determine which checks were run
  $config{'sth'}{'log_http'}->execute($$log{'instance'}, $$log{'started'}, $$log{'started'})
    or log_error("Unable to execute sth->log_http: " . $config{'dbh'}->errstr);
  while (my $http = $config{'sth'}{'log_http'}->fetchrow_hashref) {
    # Make a copy of the log that we'll override
    my $check = { %$log };
    $$check{'state'} = $$http{'state'};
    $$check{'status_expected'} = $$http{'status_expected'};
    $$check{'status_returned'} = $$http{'status_returned'};
    $$check{'duration'} = $$http{'duration'};
    $$check{'status'} = $$http{'status'};
    $$check{'notified'} = $$http{'notified'};

    # Add to the name to make it instance-specific
    $$check{'name'} .= " - $$http{'path'}";

    # Determine if there is a status_id override
    $config{'sth'}{'get_override'}->execute($$check{'check_id'}, $$http{'path'})
      or log_error("Unable to execute sth->get_override: " . $config{'dbh'}->errstr);
    my $override = $config{'sth'}{'get_override'}->fetchrow_hashref;
    $$check{'status_id'} = $$override{'status_id'}
      if defined($override);

    # Summarise the log for its note field
    if ($$check{'state'} eq 'mismatch') {
      $$check{'incident_title'} = 'Site Error';
      $$check{'incident_detail'} = "The server returned a HTTP response code of $$check{'status_returned'} ('$codes{$$check{'status_returned'}}') rather than the expected $$check{'status_expected'} ('$codes{$$check{'status_expected'}}').";
    } elsif ($$check{'state'} eq 'slow') {
      $$check{'incident_title'} = 'Site Slow';
      $$check{'incident_detail'} = "The request took longer to complete than our " . ($$check{'status'} eq 'warn' ? 'threshold for concern.' : 'acceptable response time policy.');
    }

    # Add to the list of checks
    push @checks, $check;
  }

  return @checks;
};

# HTTP Status Code => Descrip mapping
sub build_response_codes {
  return (
    100 => 'Continue',
    101 => 'Switching Protocols',
    102 => 'Processing',
    103 => 'Early Hints',
    200 => 'OK',
    201 => 'Created',
    202 => 'Accepted',
    203 => 'Non-Authoritative Information',
    204 => 'No Content',
    205 => 'Reset Content',
    206 => 'Partial Content',
    207 => 'Multi-Status',
    208 => 'Already Reported',
    226 => 'IM Used',
    301 => 'Moved Permanently',
    302 => 'Found',
    303 => 'See Other',
    304 => 'Not Modified',
    305 => 'Use Proxy',
    306 => 'Switch Proxy',
    307 => 'Temporary Redirect',
    308 => 'Permanent Redirect',
    400 => 'Bad Request',
    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    406 => 'Not Acceptable',
    407 => 'Proxy Authentication Required',
    408 => 'Request Timeout',
    409 => 'Conflict',
    410 => 'Gone',
    411 => 'Length Required',
    412 => 'Precondition Failed',
    413 => 'Payload Too Large',
    414 => 'URI Too Long',
    415 => 'Unsupported Media Type',
    416 => 'Range Not Satisfiable',
    417 => 'Expectation Failed',
    418 => 'I\'m a teapot',
    421 => 'Misdirected Request',
    422 => 'Unprocessable Entity',
    423 => 'Locked',
    424 => 'Failed Dependency',
    425 => 'Too Early',
    426 => 'Upgrade Required',
    428 => 'Precondition Required',
    429 => 'Too Many Requests',
    431 => 'Request Header Fields Too Large',
    451 => 'Unavailable For Legal Reasons',
    500 => 'Internal Server Error',
    501 => 'Not Implemented',
    502 => 'Bad Gateway',
    503 => 'Service Unavailable',
    504 => 'Gateway Timeout',
    505 => 'HTTP Version Not Supported',
    506 => 'Variant Also Negotiates',
    507 => 'Insufficient Storage',
    508 => 'Loop Detected',
    510 => 'Not Extended',
    511 => 'Network Authentication Required',
  );
}

# Return true to pacify the compiler
1;
