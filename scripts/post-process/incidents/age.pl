#!/usr/bin/perl -w
# Lockfile Log to Incident breakdown
use strict;
our %config;

$config{'script_processing'}{'age'} = sub {
  my ($log) = @_;

  # Make a copy of the log that we'll override
  my $check = { %$log };

  # Summarise the log for its note field
  if ($$check{'status'} =~ /^warn|critical$/) {
    $$check{'incident_title'} = "$$check{'name'} Out-of-Date";
    $$check{'incident_detail'} = "It has been identified that a recent update of the $$check{'name'} has not been completed.";
  }

  # Return the modified log as a list
  return ( $check );
};

# Return true to pacify the compiler
1;
