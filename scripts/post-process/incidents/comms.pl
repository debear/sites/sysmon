#!/usr/bin/perl -w
# Comms Log to Incident breakdown
use strict;
our %config;

$config{'script_processing'}{'comms'} = sub {
  my ($log) = @_;

  # Make a copy of the log that we'll override
  my $check = { %$log };

  # Summarise the log for its note field
  if ($$check{'status'} =~ /^warn|critical$/) {
    my $type = lc $$check{'name'}; $type =~ s/s? Sent$//;
    my ($num) = ($$check{'log_info'} =~ /; Failed: (\d+);/);
    $$check{'incident_title'} = 'Sending Failures';
    $$check{'incident_detail'} = "It was identified that $num $type" . ($num > 1 ? 's' : '') . ' failed to send.';
  }

  # Return the modified log as a list
  return ( $check );
};

# Return true to pacify the compiler
1;
