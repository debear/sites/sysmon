#!/usr/bin/perl -w
# Logs Check Log to Incident breakdown
use strict;
our %config;

$config{'script_processing'}{'logs'} = sub {
  my ($log) = @_;

  # Make a copy of the log that we'll override
  my $check = { %$log };

  # Summarise the log for its note field
  if ($$check{'status'} =~ /^warn|critical$/) {
    # Get the total number of warnings
    my $num_warnings = 0; my $num_by = 0;
    foreach my $line (split "\n", $$check{'log_info'}) {
      # We only care about the first section
      my $is_by = ($line =~ /^By /);
      $num_by++ if $is_by;
      next if $is_by || ($num_by > 1);
      # Add the number in this instance
      my ($num_sect) = ($line =~ /: (\d+)$/);
      $num_warnings += $num_sect;
    }
    # Produce our incident details
    $$check{'incident_title'} = ($$check{'status'} eq 'warn' ? 'Issues Discovered' : 'Problems Reported');
    $$check{'incident_detail'} = "During the last run, $num_warnings warning" . ($num_warnings == 1 ? ' was' : 's were') . ' reported.';
  }

  # Return the modified log as a list
  return ( $check );
};

# Return true to pacify the compiler
1;
