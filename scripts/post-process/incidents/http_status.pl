#!/usr/bin/perl -w
# HTTP Status Log to Incident breakdown
use strict;
our %config;

$config{'script_processing'}{'http_status'} = sub {
  my ($log) = @_;

  # Make a copy of the log that we'll override
  my $check = { %$log };

  # Summarise the log for its note field
  if ($$check{'status'} =~ /^warn|critical$/) {
    my $is_warn = ($$check{'status'} eq 'warn');
    $$check{'incident_title'} = 'HTTP Error Codes ' . ($is_warn ? 'Too High' : 'Unacceptable');
    $$check{'incident_detail'} = 'Recent usage indicated ' . ($is_warn ? 'a high number of errors' : 'too many errors') . ' were returned.';
  }

  # Return the modified log as a list
  return ( $check );
};

# Return true to pacify the compiler
1;
