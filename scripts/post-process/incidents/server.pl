#!/usr/bin/perl -w
# Server Status Log to Incident breakdown
use strict;
our %config;

$config{'script_processing'}{'server'} = sub {
  my ($log) = @_;

  # Make a copy of the log that we'll override
  my $check = { %$log };

  # Summarise the log for its note field
  my ($state) = ($$check{'log_info'} =~ /Result: (.+)$/);
  if ($state eq 'Down') {
    $$check{'incident_title'} = "$$check{'name'} Down";
    $$check{'incident_detail'} = 'The server did not respond to all our status checks.';
  }

  # Return the modified log as a list
  return ( $check );
};

# Return true to pacify the compiler
1;
