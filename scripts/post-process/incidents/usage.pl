#!/usr/bin/perl -w
# Disk Usage Log to Incident breakdown
use strict;
our %config;

$config{'script_processing'}{'usage'} = sub {
  my ($log) = @_;

  # Make a copy of the log that we'll override
  my $check = { %$log };

  # Summarise the log for its note field
  if ($$check{'status'} =~ /^warn|critical$/) {
    my $is_warn = ($$check{'status'} eq 'warn');
    $$check{'incident_title'} = 'Disk Space ' . ($is_warn ? 'Concerns' : 'Low');
    $$check{'incident_detail'} = 'The current disk usage has exceeded our ' . ($is_warn ? 'threshold for concern.' : 'high-risk threshold.');
  }

  # Return the modified log as a list
  return ( $check );
};

# Return true to pacify the compiler
1;
