#!/usr/bin/perl -w
# Lockfile Log to Incident breakdown
use strict;
our %config;

$config{'script_processing'}{'lockfile'} = sub {
  my ($log) = @_;

  # Make a copy of the log that we'll override
  my $check = { %$log };

  # Summarise the log for its note field
  if ($$check{'status'} =~ /^warn|critical$/) {
    my $name_noun = $$check{'name'}; $name_noun =~ s/^.+ (\S+)$/$1/;
    my $name_noun_lc = lc $name_noun;
    $$check{'incident_title'} = "$name_noun Missed";
    $$check{'incident_detail'} = "The expected notification that the latest $name_noun_lc run had been completed was not received.";
  }

  # Return the modified log as a list
  return ( $check );
};

# Return true to pacify the compiler
1;
