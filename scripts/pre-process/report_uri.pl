#!/usr/bin/perl -w
# Report URI log file parsing.
# TD. 2017-12-21.

use strict;

#
# BEGIN: Update @INC on the Xilo server
#
BEGIN {
  use File::Basename;

  # Mask the filename as we're on a shared server
  our $script_raw = $0;
  our $script = basename($0, '.pl');
  $0 = basename $0;

  # Some path customisation needs to be made if running on the prod server
  our $home_dir;
  our $xilo_home = '/var/www/f6a0812f-78d0-45a6-bec0-c060de245116';
  our $xilo_is = 0;
  my $xilo_custom_path = 'debear/sites/common/perl5'; # Relative to ~
  our $base_dir = dirname $script_raw;
  if ($base_dir =~ /^$xilo_home/) {
    # Okay, now link to the other perl modules
    unshift(@INC, $xilo_home.'/'.$xilo_custom_path);
    $home_dir = $xilo_home;
    $xilo_is = 1;
  } else {
    $home_dir = '/var/www';
  }

  my $config = dirname(__FILE__) . '/config.pl';
  require $config;
}

#
# END: Tie-up database connections and logs
#
END {
  dbi_disconnect() if defined(&dbi_disconnect);
  save_logs();
}

use Dir::Self;

our %config;

# Load the log parsing checks
foreach my $checks (glob(__DIR__ . '/report_uri/checks/*.pl')) {
  dynamic_require($checks);
}

# Say we're going...
log_msg('===============');
log_msg('Run starting...');
log_msg(' ** Debug Mode **')
  if $config{'debug'};

# Prepare the database statement
prepare_sth();

# Run the individual checks
foreach my $checks (@{$config{'checks'}}) {
  $checks->();
}
