#!/usr/bin/perl -w
use strict;
use Dir::Self;
use Cwd 'abs_path';
use Compress::Zlib;
use JSON;

# Load libs
foreach my $file ('../lib/dbi.pl', '../lib/debug.pl') {
  my $config = abs_path(__DIR__ . "/$file");
  require $config;
}

# Main config
our %config;
our $script;

# Timestamp that we started...
$config{'started'} = strftime('%Y-%m-%d %H:%M:%S', localtime);
$config{'started_gmt'} = strftime('%Y-%m-%d %H:%M:%S', gmtime);

# Logging related
$config{'debug'} = grep(/^--debug$/, @ARGV);
$config{'logs'} = [ ];
$config{'logs_dir'} = abs_path(__DIR__ . '/../../logs/scripts-pre-process');
$config{'log_inc_time'} = '%H%M';

# List of DBI statements we'll be preparing
$config{'sth'} = { };

# Where we'll get our files
$config{'parse'} = {
  'base_dir' => abs_path(__DIR__ . '/../../../../logs'),
  'glob' => '*/reports_{csp,ct,js,php}/*.json.gz',
};

# GeoIP info
$config{'geoip'} = {
  'data_file_city' => $config{'parse'}{'base_dir'} . '/common/geoip/GeoLite2-City.mmdb',
  'data_file_country' => $config{'parse'}{'base_dir'} . '/common/geoip/GeoLite2-Country.mmdb',
  'local_dns' => 'data.debear.co.uk',
};
# Check to see if city exists, and fall back to country if not
$config{'geoip'}{'city_mode'} = (-e $config{'geoip'}{'data_file_city'});
$config{'geoip'}{'data_file'} = $config{'geoip'}{'city_mode'} ? $config{'geoip'}{'data_file_city'} : $config{'geoip'}{'data_file_country'};

# PHP error_log parsing
our $xilo_is; our $xilo_home;
my $base_dir = (!$xilo_is ? '/var/www' : $xilo_home);
$config{'php'} = {
  'error_log' => "$base_dir/debear/logs/skeleton/php-error.log",
  'date_lookup' => {
    'Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06',
    'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12',
  },
};

# Laravel log parsing
$config{'laravel'} = {
  'mode' => 'do-not-run', # By default we do not run, we need to determine explicitly we should run this check
  'base_dir' => "$base_dir/debear/logs/skeleton/laravel",
  'lookback_days' => 3, # Look back at logs within the last 3 days
};

# Individual check processing logic
$config{'checks'} = [ ];

# "Dynamic" (single-line) require, since `require __DIR__.'/script.pl'` is syntactically invalid
sub dynamic_require {
  my ($path) = @_;
  require $path
    if -e $path;
}

# Load a file
sub load_file {
  my ($file) = @_;

  my $contents = '';
  my $buffer;
  my $gzh = gzopen($config{'parse'}{'base_dir'} . '/' . $file, 'rb');
  $contents .= $buffer while $gzh->gzread($buffer) > 0;
  $gzh->gzclose();

  my $report;
  $report = eval { decode_json($contents); };
  if ($@) {
    # Invalid JSON saved
    $report = { };
    log_msg("WARNING - Unable to decode JSON in file '$file'");
  }
  return $report;
}

# Prepare the SQL statements
sub prepare_sth {
  return if defined($config{'sth'}{'instance'});

  my $sql = 'INSERT IGNORE INTO REPORTS_INSTANCE (report_id, type, site, v_site, rev_site, rev_skel, uri_location, uri_args, request_time, num_reports, user_agent, browser, browser_version, os, geoip_country, geoip_city, user_logged_in, filename, imported, status)
    VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP(), NULL);';
  $config{'sth'}{'instance'} = $config{'dbh'}->prepare($sql);

  $sql = 'SELECT report_id FROM REPORTS_INSTANCE WHERE filename = ?;';
  $config{'sth'}{'record_id'} = $config{'dbh'}->prepare($sql);

  $sql = 'UPDATE REPORTS_INSTANCE SET status = ? WHERE report_id = ?;';
  $config{'sth'}{'status'} = $config{'dbh'}->prepare($sql);

  $sql = 'DELETE FROM REPORTS_INSTANCE WHERE report_id = ?;';
  $config{'sth'}{'prune'} = $config{'dbh'}->prepare($sql);

  # Now some statements specific to the PHP table used across multiple parsing blocks
  $sql = 'INSERT IGNORE INTO REPORTS_PHP (report_id, instance_id, code, error, file, line, trace)
    VALUES (?, ?, ?, ?, ?, ?, ?);';
  $config{'sth'}{'php'} = $config{'dbh'}->prepare($sql);
}

# Return true to pacify the compiler
1;
