#!/usr/bin/perl -w
# PHP specific report_uri report parsing
use strict;
use HTML::Entities;
use JSON;
our %config;

sub parse_php {
  my ($record) = @_;
  my $valid = 0;

  my $instance_id = 1;
  foreach my $detail (@{$$record{'detail'}}) {

    # Skip if no value to consider
    next
      if !defined($detail) || ref($detail) ne 'HASH';
    $valid = 1;

    # Prepare the database statement
    prepare_sth_php();

    # Run
    $config{'sth'}{'php'}->execute($$record{'id'}, $instance_id++, $$detail{'code'}, $$detail{'error'}, $$detail{'file'}, $$detail{'line'}, encode_json($$detail{'trace'}))
      or log_error("Unable to execute sth->php: " . $config{'dbh'}->errstr)
        if !$config{'debug'};
  }
  $$record{'status'} = ($valid ? 'reported' : 'missing_detail');
}

# Return true to pacify the compiler
1;
