#!/usr/bin/perl -w
# CSP specific report_uri report parsing
use strict;
use HTML::Entities;
use JSON;
our %config;

sub parse_csp {
  my ($record) = @_;
  my $detail = $$record{'detail'}[0];
  my $is_hash = (ref($detail) eq 'HASH');
  $detail = decode_entities($detail)
    if !$is_hash;

  # Skip if no value to consider
  if (!defined($detail) || $detail eq '') {
    $$record{'status'} = 'missing_detail';
    return;
  }

  # Prepare the database statement
  if (!defined($config{'sth'}{'csp'})) {
    my $sql = 'INSERT IGNORE INTO REPORTS_CSP (report_id, document_uri, violated_directive, blocked_uri, source_file, line_number)
  VALUES (?, ?, ?, ?, ?, ?);';
    $config{'sth'}{'csp'} = $config{'dbh'}->prepare($sql)
  }

  # Parse the directive
  my %detail = !$is_hash ? %{decode_json($detail)} : %$detail;
  $detail{'csp-report'} = { } if !defined($detail{'csp-report'}{'document-uri'});

  # Run
  $config{'sth'}{'csp'}->execute($$record{'id'}, $detail{'csp-report'}{'document-uri'}, $detail{'csp-report'}{'violated-directive'}, $detail{'csp-report'}{'blocked-uri'}, $detail{'csp-report'}{'source-file'}, $detail{'csp-report'}{'line-number'})
    or log_error("Unable to execute sth->csp: " . $config{'dbh'}->errstr)
      if !$config{'debug'};

  # Determine Status
  # - Known to generate false positives
  if ($$record{'user_agent'} =~ m/BingPreview/i) {
    $$record{'status'} = 'false_positive';
  } else {
    $$record{'status'} = 'reported';
  }
}

# Return true to pacify the compiler
1;
