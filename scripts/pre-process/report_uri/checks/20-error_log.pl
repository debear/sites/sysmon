#!/usr/bin/perl -w
# Parsing of the web server's error log for Report URI logging
use strict;
our %config;

push @{$config{'checks'}}, sub {
  # Only parse if the file exists
  log_msg("Processing error_log on production '$config{'php'}{'error_log'}'");
  if (! -e $config{'php'}{'error_log'}) {
    log_msg('-> Skipped, file not found.');
    return;
  }

  # Load the file and loop through
  open FILE, "<$config{'php'}{'error_log'}";
  my @errs = <FILE>;
  close FILE;
  my $errs = join('', @errs) . "\n["; # Final [ is for our splitting regex
  foreach my $e ($errs =~ m/([0-3]\d\-[A-Z][a-z]{2}\-20\d{2}.*?)\n\[/gsi) {
    # Break in to component parts
    my ($date, $time, $error) = ($e =~ m/^([0-3]\d\-[A-Z][a-z]{2}\-20\d{2}) (\d{2}:\d{2}:\d{2}).*?\] (.*?):?\s*$/gs);

    # Convert the time
    my @date = split('-', $date);
    my $datetime = "$date[2]-$config{'php'}{'date_lookup'}{$date[1]}-$date[0] $time";

    # Determine a level
    my ($level) = ($error =~ m/PHP ([a-z]+)/gsi);
    $level = uc $level if defined($level);
    $level = 'INTERNAL' if !defined($level);

    # Split into component parts
    my ($trace) = ($error =~ m/Stack trace:\n(.+)$/gsi);
    $error =~ s/\nStack trace:.+$//gsi if defined($trace);
    $trace = undef if defined($trace) && $trace =~ /^\s*$/;

    # File location
    my ($file, $line) = ($error =~ /in (.+)(?:\:| on line )(\d+)$/gsi);

    # Create the main instance
    $config{'sth'}{'instance'}->execute(
      'php',     # type
      undef, undef, undef, undef, undef, undef,
      $datetime, # request_time
      1,         # num_reports
      undef, undef, undef, undef, undef, undef, undef, undef,
    ) or log_error("Unable to execute sth->instance: " . $config{'dbh'}->errstr)
      if !$config{'debug'};
    my $record_id = !$config{'debug'} ? $config{'sth'}{'instance'}->{mysql_insertid} : -1;

    # Then the PHP-specific details
    $config{'sth'}{'php'}->execute($record_id, 1, $level, $error, $file, $line, $trace)
      or log_error("Unable to execute sth->php: " . $config{'dbh'}->errstr)
        if !$config{'debug'};

    # Some errors may be treated as false positive
    my $status = ($error =~ /^PHP Fatal error:  Out of memory/ ? 'false_positive' : 'reported');

    # Update the instance record to say if we were successfuly or not
    $config{'sth'}{'status'}->execute($status, $record_id)
      or log_error("Unable to execute sth->status: " . $config{'dbh'}->errstr)
        if !$config{'debug'};

    log_msg("-> ID: $record_id; Reports: 1; Date/Time: $datetime; Status: $status");
  }

  # Remove the file
  my $rmv = unlink $config{'php'}{'error_log'}
    if !$config{'debug'};
  log_msg('-> Removing error_log ' . (!$config{'debug'} ? ($rmv ? 'succeeded' : 'failed') : 'not attempted'));
};

# Return true to pacify the compiler
1;
