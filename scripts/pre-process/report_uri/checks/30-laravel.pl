#!/usr/bin/perl -w
# Parsing of the Laravel error log for Report URI logging
use strict;
use Digest::MD5 qw(md5_hex);
use POSIX qw(tzset);
our %config;
$ENV{TZ} = 'Europe/London'; # Ensure we run on Europe/London time

# Determine whether we should run this check or not
# - Runs every day at midnight
if (strftime('%H', localtime) eq '00') {
  $config{'laravel'}{'mode'} = 'daily';
# - Runs hourly on final day of month, but only that day's file
} elsif (`date +'%m' -d 'tomorrow'` ne `date +'%m'`) {
  $config{'laravel'}{'mode'} = 'last-of-month';
  $config{'laravel'}{'lookback_days'} = 1; # Update our debug process
# - Run in debug mode
} elsif ($config{'debug'}) {
  $config{'laravel'}{'mode'} = 'debug';
}

# Do not proceed if we have not determined this is an appropriate time to run this check
return 1 if $config{'laravel'}{'mode'} eq 'do-not-run';

push @{$config{'checks'}}, sub {
  # Only parse if any files are found
  log_msg("Processing Laravel log on production within the last $config{'laravel'}{'lookback_days'} day(s) in '$config{'laravel'}{'base_dir'}'");
  my $logs = `find $config{'laravel'}{'base_dir'} -name \*.log -type f -mtime -$config{'laravel'}{'lookback_days'} 2>/dev/null | sort -n`;
  chomp($logs);
  my @logs = split("\n", $logs);
    # In last-of-month mode, we only want to process that day's file
  if ($config{'laravel'}{'mode'} eq 'last-of-month') {
    my $today = strftime('%Y-%m-%d', localtime);
    @logs = grep {/\/$today\.log$/} @logs;
  }
  # No need to continue if no files were found to process
  if (! @logs) {
    log_msg('-> Skipped, no files found.');
    return;
  }

  # Prepare the database query for determining if an error has already been imported
  $config{'sth'}{'laravel_check'} = $config{'dbh'}->prepare('SELECT COUNT(*) AS is_imported
  FROM REPORTS_INSTANCE AS RI
  JOIN REPORTS_PHP AS RP USING (report_id)
  WHERE RI.type = "php"
  AND   RI.filename LIKE CONCAT(?, "-%")
  AND   RI.request_time = ?
  AND   RP.code = ?
  AND   MD5(RP.error) = ?;');

  foreach my $file (@logs) {
    my $filename = basename $file;
    my $filename_db = "laravel/$filename";
    # If we are only processing the final day of the month's files, we only process that day's file
    log_msg("- Processing $filename");

    open FILE, "<$file";
    my @errs = <FILE>;
    close FILE;
    my $errs = join('', @errs);
    my %counts = ( 'proc' => 0, 'skip' => 0, 'err' => 0 );
    my $i = 0;
    foreach my $e ($errs =~ m/(\[20\d{2}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\] [^\.]+\.[^:]+:.*?"})/gsi) {
      $i++;
      # Break in to component parts
      my ($datetime, $level, $error, $trace) = ($e =~ m/^\[(20\d{2}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})\] [^\.]+\.([^:]+):\s*([^\n]+)\n\[stacktrace\]\n(.*?)\n\#\d+\s*\{main\}/gsi);

      # Try and determine an offending filename and line number
      my ($err_file, $line) = ($error =~ /(\/var\/www\/[^:\s]+):(\d+)/gsi);

      # Check to see if this entry has already been imported
      my $sth_err = undef;
      $config{'sth'}{'laravel_check'}->execute($filename_db, $datetime, $level, md5_hex($error))
        or ($sth_err = $config{'sth'}->errstr);
      if (defined($sth_err) && $sth_err ne '') {
        log_msg("  - ERROR: Unable to execute sth->laravel_check for '$datetime' $level: $sth_err");
        $counts{'err'}++;
        next;
      }
      my $check = $config{'sth'}{'laravel_check'}->fetchrow_hashref;
      if ($$check{'is_imported'}) {
        $counts{'skip'}++;
        next;
      }

      # Create the main instance
      $sth_err = undef;
      $config{'sth'}{'instance'}->execute(
        'php',        # type
        undef, undef, undef, undef, undef, undef,
        $datetime,    # request_time
        1,            # num_reports
        undef, undef, undef, undef, undef, undef, undef,
        "$filename_db-$i", # filename
      ) or ($sth_err = $config{'sth'}->errstr)
        if !$config{'debug'};
      my $record_id = !$config{'debug'} ? $config{'sth'}{'instance'}->last_insert_id() : -1;
      if (defined($sth_err) && $sth_err ne '') {
        log_msg("  - ERROR: Unable to execute sth->instance for '$datetime' $level: $sth_err");
        $counts{'err'}++;
        next;
      }

      # Then the PHP-specific details
      $sth_err = undef;
      $config{'sth'}{'php'}->execute($record_id, 1, $level, $error, $err_file, $line, $trace)
        or ($sth_err = $config{'sth'}->errstr)
          if !$config{'debug'};
      if (defined($sth_err) && $sth_err ne '') {
        log_msg("  - ERROR: Unable to execute sth->php for instance $record_id: $sth_err");
        # We should roll back the instance row too, as hopefully the next run will re-attempt the import
        $config{'sth'}{'prune'}->execute($record_id)
          or log_msg("    - ERROR: Unable to execute sth->prune for instance $record_id: " . $config{'dbh'}->errstr)
            if !$config{'debug'};
        $counts{'err'}++;
        next;
      }

      # In future, some errors may be treated as false positive
      my $status = 'reported';

      # Update the instance record to say if we were successfuly or not
      $sth_err = undef;
      $config{'sth'}{'status'}->execute($status, $record_id)
        or ($sth_err = $config{'sth'}->errstr)
          if !$config{'debug'};
      if (defined($sth_err) && $sth_err ne '') {
        log_msg("  - ERROR: Unable to execute sth->status for instance $record_id: $sth_err");
        # This probably isn't the worst error - not ideal, but no rollback is really necessary
        $counts{'err'}++;
        next;
      }

      $counts{'proc'}++;
    }

    # Output what we did
    log_msg("  - $counts{'proc'} entry/ies processed, $counts{'skip'} skipped, $counts{'err'} errored.");
  }
};

# Return true to pacify the compiler
1;
