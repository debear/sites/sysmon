#!/usr/bin/perl -w
# Parsing of the DeBear reported files for Report URI logging
use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;
our %config;

push @{$config{'checks'}}, sub {
  # Let's find some files to process
  my @files = glob($config{'parse'}{'base_dir'} . '/' . $config{'parse'}{'glob'});
  # Sort by timestamp
  @files = sort {(stat($a))[9] <=> (stat($b))[9]} @files;
  log_msg("Searching for '$config{'parse'}{'glob'} in '$config{'parse'}{'base_dir'}'");
  log_msg(@files . ' file' . (@files == 1 ? '' : 's') . ' found for processing.');

  # If nothing found, no need to continue
  if (!@files) {
    log_msg('-> No need to continue.');
  } else {
    # Determine which we've already processed
    @files = map $_ =~ /^$config{'parse'}{'base_dir'}\/(.+)$/, @files; # Strip base_dir
    my @imported_files = map $_->[0], @{$config{'dbh'}->selectall_arrayref('SELECT filename FROM REPORTS_INSTANCE WHERE status IS NOT NULL AND filename IS NOT NULL;')};

    my %diff = ( );
    @diff{@files} = undef;
    delete @diff{@imported_files};
    my @import = sort keys %diff;
    undef %diff; # Garbage collect

    log_msg(@import . ' file' . (@import == 1 ? '' : 's') . ' to be processed, ' . (@files - @import) . ' previously imported.');

    # Loop through the files to import and process
    foreach my $file (@import) {
      # Break it down into its component parts
      my ($site, $type, $date, $time, $uniq) = ($file =~ m/^([^\/]+)\/reports_([^\/]+)\/(20\d{2}-\d{2}-\d{2})\.(\d{2}-\d{2}-\d{2})\.(\d+)\./);
      $site =~ s/_[0-9a-f]{40}$//;
      $time =~ s/-/:/g;
      log_msg("- Site: $site; Type: $type; Date: $date; Time: $time; Unique: $uniq");

      # Load the file
      my $report = load_file($file);
      if (!defined($$report{'_SERVER'})) {
        log_msg("  - Empty file or invalid JSON. Skipping...");
        next;
      }

      # The URL being reported on
      my $uri = defined($$report{'_REPORT'}) ? $$report{'_SERVER'}{'REQUEST_URI'} : $$report{'_SERVER'}{'HTTP_REFERER'};
      my $qmark_pos = -1;
      if (defined($uri)) {
        $uri =~ s/^https?\:\/\/[^\/]+\//\//;
        $qmark_pos = index($uri, '?');
      }

      # Some pre-processing
      my $detail = defined($$report{'_REPORT'}) ? $$report{'_REPORT'} : $$report{'php://input'};
      my @detail = ref($detail) eq 'ARRAY' ? @$detail : ( $detail );

      # GeoIP info
      my $lookup_ip = $$report{'_SERVER'}{'REMOTE_ADDR'};
      if ($lookup_ip eq '127.0.0.1') {
        if (!defined($config{'geoip'}{'local_dns_ip'})) {
          $config{'geoip'}{'local_dns_ip'} = `dig +short $config{'geoip'}{'local_dns'} | tail -1`;
          chomp($config{'geoip'}{'local_dns_ip'});
        }
        $lookup_ip = $config{'geoip'}{'local_dns_ip'};
      }
      my $geoip_script = abs_path(__DIR__ . '/../_geoip.php');
      my $geoip_info = `php $geoip_script $lookup_ip`;
      chomp($geoip_info);
      $geoip_info =~ s/^([^\{]+)(\{)/$2/msi; # Strip headers
      my $geoip_record = decode_json($geoip_info);

      # Some additional data fixes
      foreach my $key ('_REV', '_SESSION', '_BROWSER') {
        $$report{$key} = { } if !defined($$report{$key}) || (ref $$report{$key} ne 'HASH');
      }

      # Parse generic details
      my %record = (
        'type' => $type,
        'site' => $site,
        'v_site' => defined($$report{'_REV'}{'v'}) && $$report{'_REV'}{'v'} ? $$report{'_REV'}{'v'} : undef,
        'rev_site' => defined($$report{'_REV'}{'rev_site'}) && $$report{'_REV'}{'rev_site'} ? $$report{'_REV'}{'rev_site'} : undef,
        'rev_skel' => defined($$report{'_REV'}{'rev_skel'}) && $$report{'_REV'}{'rev_skel'} ? $$report{'_REV'}{'rev_skel'} : undef,
        'uri_location' => $qmark_pos == -1 ? $uri : substr($uri, 0, $qmark_pos),
        'uri_args' => $qmark_pos == -1 ? undef : substr($uri, $qmark_pos + 1),
        'request_time' => "$date $time",
        'num_reports' => scalar @detail,
        'user_agent' => $$report{'_SERVER'}{'HTTP_USER_AGENT'},
        'browser' => $$report{'_BROWSER'}{'browser'},
        'browser_version' => $$report{'_BROWSER'}{'version'},
        'os' => $$report{'_BROWSER'}{'os'},
        'geoip_country' => defined($$geoip_record{'country'}{'iso_code'}) && $$geoip_record{'country'}{'iso_code'} ne '' ? $$geoip_record{'country'}{'iso_code'} : undef,
        'geoip_city' => defined($$geoip_record{'city'}{'names'}{'en'}) && $$geoip_record{'city'}{'names'}{'en'} ne '' ? $$geoip_record{'city'}{'names'}{'en'} : undef,
        'user_logged_in' => int(defined($$report{'_SESSION'}{'logged_in'}) && $$report{'_SESSION'}{'logged_in'}),
        'filename' => $file,
        'detail' => \@detail,
      );
      $record{'browser'} = undef
        if defined($record{'browser'}) && (!$record{'browser'} || $record{'browser'} eq 'Default Browser');
      $record{'browser_version'} = undef
        if defined($record{'browser_version'}) && (!$record{'browser_version'} || $record{'browser_version'} eq '0.0');
      $record{'os'} = undef
        if defined($record{'os'}) && (!$record{'os'} || $record{'os'} eq 'unknown');

      # Store main instance record
      $config{'sth'}{'instance'}->execute(
        $record{'type'},
        $record{'site'},
        $record{'v_site'},
        $record{'rev_site'},
        $record{'rev_skel'},
        $record{'uri_location'},
        $record{'uri_args'},
        $record{'request_time'},
        $record{'num_reports'},
        $record{'user_agent'},
        $record{'browser'},
        $record{'browser_version'},
        $record{'os'},
        $record{'geoip_country'},
        $record{'geoip_city'},
        $record{'user_logged_in'},
        $record{'filename'},
      ) or log_error("Unable to execute sth->instance: " . $config{'dbh'}->errstr)
        if !$config{'debug'};
      $record{'id'} = !$config{'debug'} ? $config{'sth'}{'instance'}->{mysql_insertid} : -1;
      if (!$record{'id'}) {
        $config{'sth'}{'record_id'}->execute($record{'filename'})
          or log_error("Unable to execute sth->record_id: " . $config{'dbh'}->errstr);
        ($record{'id'}) = $config{'sth'}{'record_id'}->fetchrow_array;
      }

      # Parse type-specific info
      my $fn = "parse_$type";
      dynamic_require(abs_path(__DIR__ . "/../$type.pl")) if !exists &$fn;
      {
        no strict "refs";
        &$fn(\%record);
      }

      # Update the instance record to say if we were successfuly or not
      my $status_defined = defined($record{'status'});
      $config{'sth'}{'status'}->execute($record{'status'}, $record{'id'})
        or log_error("Unable to execute sth->status: " . $config{'dbh'}->errstr)
          if !$config{'debug'} && $status_defined;

      log_msg("  -> ID: $record{'id'}; Reports: $record{'num_reports'}; Status: " . ($status_defined ? $record{'status'} : 'undef'));
    }
  }
};

# Return true to pacify the compiler
1;
