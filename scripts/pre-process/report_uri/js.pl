#!/usr/bin/perl -w
# JS specific report_uri report parsing
use strict;
use HTML::Entities;
use JSON;
use List::Util qw(max);
our %config;

sub parse_js {
  my ($record) = @_;
  my $valid = 0;

  my $instance_id = 1;
  foreach my $detail (@{$$record{'detail'}}) {
    # Skip if no value to consider
    next
      if !defined($detail);

    # Prepare the database statement
    if (!defined($config{'sth'}{'js'})) {
      my $sql = 'INSERT IGNORE INTO REPORTS_JS (report_id, instance_id, error, trace)
    VALUES (?, ?, ?, ?);';
      $config{'sth'}{'js'} = $config{'dbh'}->prepare($sql)
    }

    # Run
    my $trace = encode_json($$detail{'trace'});
    $config{'sth'}{'js'}->execute($$record{'id'}, $instance_id++, $$detail{'err'}, $trace)
      or log_error("Unable to execute sth->js: " . $config{'dbh'}->errstr)
        if !$config{'debug'};
    # Determine validity based on the possibility of some false positives
    $valid = max($valid, (($$detail{'err'} =~ /^\d{3} error during AJAX request to/) || ($trace eq '["unknown:??"]') ? 2 : 1));
  }

  # Determine Status
  # - Known to generate false positives
  if ($valid == 2) {
    $$record{'status'} = 'false_positive';
  } else {
    $$record{'status'} = ($valid ? 'reported' : 'missing_detail');
  }
}

# Return true to pacify the compiler
1;
