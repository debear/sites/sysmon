<?php

/*
 * This script is intended to be a simple, raw, wrapper around the GeoIP database lookup.
 */

use MaxMind\Db\Reader;

$ret = [];
$dirs = [
    'common' => __DIR__ . '/../../../../common',
    'common-test' => __DIR__ . '/../../../../_debear/vendor/debear/setup',
    'libraries' => __DIR__ . '/../../../../_debear',
];
require_once $dirs['libraries'] . '/vendor/maxmind-db/reader/autoload.php';

// Validate the argument.
$ip = isset($argv[1]) ? $argv[1] : '';
if (!preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$$/', $ip)) {
    fwrite(STDERR, "Invalid IP address '$ip'");
    print json_encode($ret, JSON_FORCE_OBJECT);
    exit(1);
}

// Load the library.
$mmdb_subdir = '/geoip';
$mmdb_path = file_exists($dirs['common'] . $mmdb_subdir)
    ? $dirs['common'] . $mmdb_subdir
    : $dirs['common-test'] . $mmdb_subdir;
$mmdb_file = file_exists($mmdb_path . '/GeoLite2-City.mmdb')
    ? $mmdb_path . '/GeoLite2-City.mmdb'
    : $mmdb_path . '/GeoLite2-Country.mmdb';
$geoip = new Reader($mmdb_file);

// Parse the IP.
try {
    $ret = $geoip->get($ip);
} catch (Exception $e) {
    fwrite(STDERR, "Unable to GeoIP the address '$ip'");
}

// Return as a json object.
print json_encode($ret, JSON_FORCE_OBJECT);
