#!/usr/bin/perl -w
# CT specific report_uri report parsing
use strict;

sub parse_ct {
  my ($record) = @_;

  # For now, let's just save the file and break it down when we actually have a report...
  $$record{'status'} = 'reported';
}

# Return true to pacify the compiler
1;
