#!/usr/bin/perl -w
# Check and report on the statuses of all HTTP requests
# TD. 2020-06-25.

use strict;
our %config;

BEGIN {
  use File::Basename;
  my $core = dirname(__FILE__) . '/lib/core.pl';
  require $core;
}

# Determine the dates we will be processing - today, plus early on the first of the month (pre-removal) the day before
$ENV{TZ} = 'Europe/London'; # Ensure we run on Europe/London time
my @dates = ( strftime('%Y-%m-%d', localtime) );
unshift @dates, strftime('%Y-%m-%d', localtime(time() - 86400)) # Sufficient accuracy for final day of the month
  if strftime('%d-%H', localtime) eq '01-00';

# Determine and loop through all the files for this subdomain on the dates being processed
my @domains = defined($config{'paths'}{'domain_map'}{$config{'instance'}})
  ? @{$config{'paths'}{'domain_map'}{$config{'instance'}}}
  : ( $config{'instance'} );
my %counts = ();
foreach my $domain (@domains) {
  log_msg("Processing logs from domain: $domain");
  foreach my $date (@dates) {
    log_msg("- $date");
    my $log = $config{'paths'}{'log_base'} . "/$date.log.gz"; $log =~ s/\${domain}/$domain/g;
    if (! -e $log) {
      log_msg("  - File '$log' not found, skipping.");
      next;
    }
    # Get the content for the file, excluding our own internal checks
    open FILE, "gunzip -c $log |";
    my @lines = <FILE>;
    close FILE;
    @lines = grep(/HTTP Code: [0-9]{3}/, @lines);
    @lines = grep(!/HTTP (Status|Request) Checker\/1\.0/, @lines);
    my $list = join('', @lines);
    my %statuses = ( );
    $statuses{$_}++ for ($list =~ m/HTTP Code: [0-9]{3}/gsi);
    # Process the individual status-count lines
    foreach my $code (sort keys %statuses) {
      next if $code =~ /^\s*$/;
      my $count = $statuses{$code};
      $code =~ s/^.+(\d{3})$/$1/;
      log_msg("  - $code: $count");
      # Update the counters
      $counts{$date} = {'tot_requests' => 0} if !defined($counts{$date});
      $counts{$date}{'tot_requests'} += $count;
      my $code_group = substr($code, 0, 1);
      if (($code_group >= 1) || ($code_group <= 5)) {
        # Ensure we have a counter for this date and status code
        $counts{$date}{"tot_${code_group}xx"} = 0 if !defined($counts{$date}{"tot_${code_group}xx"});
        $counts{$date}{"tot_${code_group}xx"} += $count;
        if (defined($config{'paths'}{'individual_codes'}{$code}) && defined($config{'paths'}{'individual_codes'}{$code})) {
          $counts{$date}{"num_$code"} = 0 if !defined($counts{$date}{"num_$code"});
          $counts{$date}{"num_$code"} += $count;
        }
      } else {
        # Other
        $counts{$date}{'tot_other'} = 0 if !defined($counts{$date}{'tot_other'});
        $counts{$date}{'tot_other'} += $count;
      }
    }
  }
}
log_msg("Log file processing complete");

# Prepare our query to determine the last
my $sql = 'SELECT status FROM SYSMON_HTTP_STATUS WHERE subdomain = ? AND `date` < ? ORDER BY `date` DESC LIMIT 1;';
$config{'sth'}{'http_status_last'} = $config{'dbh'}->prepare($sql);

# Now write the SQL
my $status; my $info; my $summary_msg;
my $last_date; my $pct;
foreach my $date (sort keys %counts) {
  log_msg("Storing $date:");
  # We're only interested in 2xx and 5xx statuses for status calcs, so get these numbers
  my $tot_2xx = (defined($counts{$date}{"tot_2xx"}) ? $counts{$date}{"tot_2xx"} : 0);
  log_msg("- 2xx: $tot_2xx");
  my $tot_5xx = (defined($counts{$date}{"tot_5xx"}) ? $counts{$date}{"tot_5xx"} : 0);
  log_msg("- 5xx: $tot_5xx");
  my $tot = ($tot_2xx + $tot_5xx);
  $pct = ($tot ? 100 * ($tot_2xx / $tot) : undef);
  my $pct_fmt = (defined($pct) ? sprintf('%.02f%%', $pct) : 'n/a');
  log_msg("- 2xx %age: $pct_fmt");
  $info = "2xx: $tot_2xx, 5xx: $tot_5xx, 2xx %age: $pct_fmt";
  $summary_msg = "$pct_fmt requests returned a 2xx status";
  # Determine the status (based on %age 2xx and 5xx only)
  if (defined($config{'critical'}) && defined($pct) && ($pct < $config{'critical'})) {
    log_msg("  - Deemed a 'critical'");
    $status = 'critical';
  } elsif (defined($config{'warn'}) && defined($pct) && ($pct < $config{'warn'})) {
    log_msg("  - Deemed a 'warn'");
    $status = 'warn';
  } else {
    # Was this recovering from a previous instance?
    $config{'sth'}{'http_status_last'}->execute($config{'instance'}, $date)
      or log_error("Unable to execute sth->http_status_last: " . $config{'dbh'}->errstr);
    my ($last_status) = $config{'sth'}{'http_status_last'}->fetchrow_array;
    if ($last_status =~ /^(warn|critical)$/) {
      log_msg("  - Recovered from a previous problem");
      $status = 'recovered';
    } else {
      log_msg("  - Deemed 'okay'");
      $status = 'okay';
    }
  }
  # Prepare the query components
  my $cols = ''; my $args = ''; my $odku = '';
  my @bind = ($config{'instance'}, $date, $status);
  # Add the data
  foreach my $col (keys %{$counts{$date}}) {
    $cols .= ", `$col`";
    $args .= ', ?';
    $odku .= ", `$col` = VALUES(`$col`)";
    push @bind, $counts{$date}{$col};
  }
  # And then build and run the SQL
  my $sql = "INSERT INTO SYSMON_HTTP_STATUS (`subdomain`, `date`, `first_checked`, `last_checked`, `status`, `notified`$cols)
  VALUES (?, ?, UTC_TIMESTAMP(), UTC_TIMESTAMP(), ?, 'no'$args)
ON DUPLICATE KEY UPDATE `last_checked` = VALUES(`last_checked`), `status` = VALUES(`status`)$odku;";
  $config{'sth'}{'http_status'} = $config{'dbh'}->prepare($sql);
  $config{'sth'}{'http_status'}->execute(@bind)
    or log_error("Unable to execute sth->http_status: " . $config{'dbh'}->errstr);
  # Flag the last date processed for future use
  $last_date = $date;
}

# If we didn't have any data for today, log and consider it an extension of the previous status
if ($last_date ne $config{'today'}) {
  log_msg("Back-filling a missing entry for today, $config{'today'}");
  $config{'sth'}{'http_status_last'}->execute($config{'instance'}, $config{'today'})
    or log_error("Unable to execute sth->http_status_last: " . $config{'dbh'}->errstr);
  ($status) = $config{'sth'}{'http_status_last'}->fetchrow_array;
  log_msg("- Considering this a continuation of the previous status, '$status'");
  $info = $summary_msg = 'No available requests to process';
  $pct = undef;
  # Build and run the SQL
  my $sql = "INSERT INTO SYSMON_HTTP_STATUS (`subdomain`, `date`, `first_checked`, `last_checked`, `status`, `notified`)
  VALUES (?, ?, UTC_TIMESTAMP(), UTC_TIMESTAMP(), ?, 'no')
ON DUPLICATE KEY UPDATE `last_checked` = VALUES(`last_checked`), `status` = VALUES(`status`);";
  $config{'sth'}{'http_status'} = $config{'dbh'}->prepare($sql);
  $config{'sth'}{'http_status'}->execute($config{'instance'}, $config{'today'}, $status)
    or log_error("Unable to execute sth->http_status: " . $config{'dbh'}->errstr);
}

# Overall status is based off the final processed date, so notify & Log
my $summary_field = (defined($pct) ? int($pct + 0.5) : undef);
my $notified = check_send_notif($info, $summary_msg, $status);
log_run($status, $summary_field, $notified, $info);

# Backreference the individual checks to set notification of failed checks
if ($notified ne 'no') {
  $sql = 'UPDATE SYSMON_HTTP_STATUS
SET notified = ?
WHERE subdomain = ?
AND   `date` = ?;';
  my $sth = $config{'dbh'}->prepare($sql);
  $sth->execute($notified, $config{'instance'}, $config{'today'})
    or log_error("Unable to execute sth->status: " . $config{'dbh'}->errstr);
}
