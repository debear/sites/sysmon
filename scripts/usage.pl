#!/usr/bin/perl -w
# Check and report on disk usage, broken down by area
# TD. 2017-03-13.

use strict;
our %config; our $home_dir;

BEGIN {
  use File::Basename;
  my $core = dirname(__FILE__) . '/lib/core.pl';
  require $core;
}

##
## Disk Usage
##

# First, get the total disk space
my $fs_total = fs_usage($home_dir.($config{'is_dev'} ? '/debear' : '')); # When on dev, we don't want all of /var/www sized

# Then, broken down by other folders
# Mail (by domain) - No longer available
my $fs_mail_debear = 0; #fs_usage($home_dir.'/mail/debear.uk') + fs_usage($home_dir.'/mail/debear.co.uk');
my $fs_mail_padd = 0; #fs_usage($home_dir.'/mail/pa-dd.uk');
my $fs_mail_total = $fs_mail_debear + $fs_mail_padd;
# Backups
my $fs_backups = fs_usage($home_dir.'/debear/backups');
# Logs
my $fs_logs = fs_usage($home_dir.'/debear/logs');
# Merges
my $fs_merges = fs_usage($home_dir.'/debear/merges');
# Sites: CDN & Other ("Other" == Total - CDN)
my $fs_sites_all = fs_usage($home_dir.'/debear/sites');
my $fs_sites_cdn = fs_usage($home_dir.'/debear/sites/cdn');
my $fs_sites_other = $fs_sites_all - $fs_sites_cdn;
# Other (Total - SUM(above))
my $fs_other = $fs_total - $fs_mail_debear - $fs_mail_padd - $fs_backups - $fs_logs - $fs_merges - $fs_sites_all;

##
## Database Usage
##

# DB: Sports & Others
my $sql = 'SELECT SUM(DATA_LENGTH + INDEX_LENGTH) AS total_size,
  SUM(IF(TABLE_SCHEMA = "debearco_sports", DATA_LENGTH + INDEX_LENGTH, 0)) AS sports_size,
  SUM(IF(TABLE_SCHEMA = "debearco_fantasy", DATA_LENGTH + INDEX_LENGTH, 0)) AS fantasy_size,
  SUM(IF(TABLE_SCHEMA NOT IN ("debearco_sports", "debearco_fantasy"), DATA_LENGTH + INDEX_LENGTH, 0)) AS other_size
FROM information_schema.TABLES
WHERE TABLE_SCHEMA LIKE "debearco%";';
my $sth = $config{'dbh'}->prepare($sql);
$sth->execute
  or log_error("Unable to execute dbh->db: " . $config{'dbh'}->errstr);
my ($db_total, $db_sports, $db_fantasy, $db_other) = $sth->fetchrow_array;

##
## Analyse
##
my $usage = $fs_total + $db_total;
my $usage_pct = 100 * ($usage / $config{'usage_max'});
my $remaining = $config{'usage_max'} - $usage;

# Summarise (Note: 1048576 bytes = 1 meg)
my $info = sprintf('Usage: %.01f MiB; Remaining: %.01f MiB (Filesystem: %.01f MiB; Database: %.01f MiB)',
                   $usage / 1048576,
                   $remaining / 1048576,
                   $fs_total / 1048576,
                   $db_total / 1048576);

# Get the status based on our total usage
my $status = determine_status($usage);

##
## Store
##

# Notify & Log (Again: 1048576 bytes = 1 meg)
my $msg_detail = sprintf(
  "Usage: %.01f MiB (%.02f%%)\n- Filesystem: %.01f MiB\n  - Mail: %.01f MiB\n  - DeBear: %.01f MiB\n  - pa-dd: %.01f MiB\n  - Backups: %.01f MiB\n  - Logs: %.01f MiB\n  - Merges: %.01f MiB\n  - Sites: %.01f MiB\n    - CDN: %.01f MiB\n    - Other: %.01f MiB\n  - Other: %.01f MiB\n- Database: %.01f MiB\n  - Sports: %.01f MiB\n  - Fantasy: %.01f MiB\n  - Other: %.01f MiB",
  $usage / 1048576,
  $usage_pct,
  $fs_total / 1048576,
  $fs_mail_total / 1048576,
  $fs_mail_debear / 1048576,
  $fs_mail_padd / 1048576,
  $fs_backups / 1048576,
  $fs_logs / 1048576,
  $fs_merges / 1048576,
  $fs_sites_all / 1048576,
  $fs_sites_cdn / 1048576,
  $fs_sites_other / 1048576,
  $fs_other / 1048576,
  $db_total / 1048576,
  $db_sports / 1048576,
  $db_fantasy / 1048576,
  $db_other / 1048576
);
my $msg_summary = sprintf('Usage is at %.02f%%', $usage_pct);
my $notified = check_send_notif($msg_detail, $msg_summary, $status);
log_run($status, int($usage_pct), $notified, $info);

# Store our usage stats
$sql = "INSERT INTO SYSMON_USAGE (checked, total_usage, remaining, email_debear, email_padd, backup, logs, merges, sites_cdn, sites_other, db_sports, db_fantasy, db_other, other, status, notified)
  VALUES (UTC_DATE(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
ON DUPLICATE KEY UPDATE total_usage = VALUES(total_usage),
                        remaining = VALUES(remaining),
                        email_debear = VALUES(email_debear),
                        email_padd = VALUES(email_padd),
                        backup = VALUES(backup),
                        logs = VALUES(logs),
                        merges = VALUES(merges),
                        sites_cdn = VALUES(sites_cdn),
                        sites_other = VALUES(sites_other),
                        db_sports = VALUES(db_sports),
                        db_fantasy = VALUES(db_fantasy),
                        db_other = VALUES(db_other),
                        other = VALUES(other),
                        status = VALUES(status),
                        notified = VALUES(notified);";
$sth = $config{'dbh'}->prepare($sql);
$sth->execute($usage, $remaining, $fs_mail_debear, $fs_mail_padd, $fs_backups, $fs_logs, $fs_merges, $fs_sites_cdn, $fs_sites_other, $db_sports, $db_fantasy, $db_other, $fs_other, $status, $notified)
  or log_error("Unable to execute dbh->insert: " . $config{'dbh'}->errstr);

##
## Internal Methods
##

# Determine file system usage for a particular directory
sub fs_usage {
  my ($dir) = @_;

  return 0 if ! -e $dir; # Skip "silently" if dir not found
  my $du = `du -s $dir 2>/dev/null | awk '{ print \$1 }'`;
  chomp($du);
  $du *= 1024; # Convert kb to b
  return $du;
}
