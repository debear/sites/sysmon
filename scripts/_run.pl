#!/usr/bin/perl -w
# System Monitor script controller.
# TD. 2017-03-19.

use strict;
our %config;

BEGIN {
  use File::Basename;
  my $core = dirname(__FILE__) . '/lib/core.pl';
  require $core;
}

# Load the schedule file so we know what to process
my @schedule = load_schedule();

# Run each, capturing output
foreach my $check (@schedule) {
  my $rc = command($$check{'script'});
  log_msg("Script: '$$check{'comment'}'; Script: '$$check{'script'}'; Result: '$rc'");
}

#
# Internal Methods
#
sub command {
  system __DIR__ . '/' . $_[0];
  # Get return code
  return ($? >> 8);
}
