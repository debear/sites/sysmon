#!/usr/bin/perl -w
# Check and report on the age of a path
# TD. 2020-06-07.

use strict;
our %config;

BEGIN {
  use File::Basename;
  my $core = dirname(__FILE__) . '/lib/core.pl';
  require $core;
}

use POSIX qw(floor);

# Tweak the path specified when testing
$config{'path'} =~ s@/f6a0812f\-78d0\-45a6\-bec0\-c060de245116/@/@
  if $config{'is_dev'};

##
## Analyse
##

my $ref_date; my $age; my $status; my $info; my $summary_field; my $summary_msg;

log_msg("Test: $config{'path'}");
if (! -e $config{'path'}) {
  # File is missing. We are treating this as bad.
  $info = $summary_msg = 'File not found';
  $summary_field = $ref_date = $age = undef;
  $status = 'critical';
  log_msg(" - $info. Considering this an automatic Critical.");

} else {
  # Use stat to get the file's last modified time
  my @stat = stat $config{'path'};
  $ref_date = $stat[9];
  $age = time() - $ref_date;
  $summary_field = floor($age / 86400); # 86400 = 24 * 60 * 60 (Crude # days calc)
  # Format the timestamp for debug/display purposes
  my @mtime = localtime($ref_date);
  my $ref_date_disp = sprintf(
    '%4d-%02d-%02d %02d:%02d:%02d',
    $mtime[5] + 1900, $mtime[4] + 1, $mtime[3],
    $mtime[2], $mtime[1], $mtime[0]
  );
  $info = "Last modified: $ref_date_disp ($summary_field day/s)";
  log_msg(" - $info");
  # Summary: Number of days since last updated. 255 actually stands for "255+"
  if ($summary_field > 255) {
    log_msg(" - Capping summary field to 255 days");
    $summary_field = 255;
  }

  # Determine the status
  my $state;
  if ($age >= $config{'critical'}) {
    $status = 'critical';
    $state = 'too old';
  } elsif (defined($config{'warn'}) && $age >= $config{'warn'}) {
    $status = 'warn';
    $state = 'concerning';
  } else {
    $status = $state = 'okay';
  }

  # Could there be extenuating circumstances?
  if ($state ne 'okay') {
    # Load the detail code for this specific instance
    my $detail = __DIR__ . '/detail/age/' . $config{'instance'} . '.pl';
    if (-e $detail) {
      dynamic_require($detail);
      if (parse_detail(substr($ref_date_disp, 0, 10))) {
        log_msg("   - Post-parsing confirms age okay, despite initial '$status'");
        $status = $state = 'okay';
      }
    }
  }

  $summary_msg = "File age $state";
}

# Let's check for a 'recovery' status, if we're (now) okay
$status = determine_status_recovered()
  if $status eq 'okay';

##
## Store
##

# Notify & Log
my $notified = check_send_notif($info, $summary_msg, $status);
log_run($status, $summary_field, $notified, $info);

# Store our age status check
# - For now, all checks are file-based. Could be modified to be DB at a later date.
my $sql = 'INSERT INTO SYSMON_AGE (ref, type, path, checked, ref_date, age, status, notified)
  VALUES (?, "file", ?, UTC_TIMESTAMP(), FROM_UNIXTIME(?), ?, ?, ?)
ON DUPLICATE KEY UPDATE ref_date = VALUES(ref_date),
                        age = VALUES(age),
                        status = VALUES(status),
                        notified = VALUES(notified);';
my $sth = $config{'dbh'}->prepare($sql);
$sth->execute($config{'instance'}, $config{'path'}, $ref_date, $age, $status, $notified)
  or log_error("Unable to execute sth->age: " . $config{'dbh'}->errstr);
