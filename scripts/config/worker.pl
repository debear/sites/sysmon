#!/usr/bin/perl -w
# Worker-script configuration settings
use strict;
our %config;

# Possible parameters (boolean flag indicates whether next argument is read)
$config{'args'} = {
  'debug' => 0,
  'warn' => 1,
  'critical' => 1,
};

# Hosting config
$config{'usage_max'} = 5000000000; # 5 GB package
$config{'domain'} = 'debear.' . ($config{'is_dev'} ? 'dev' : 'uk');

# Notification details
$config{'notif_email'} = {
  'on' => grep(/^--notify-email$/, @ARGV) ? 1 : 0,
  'from' => "DeBear System Monitor <noreply\@$config{'domain'}>",
  'to' => "DeBear Support <support\@$config{'domain'}>",
};
$config{'notif_twitter_dm'} = {
  'on' => grep(/^--notify-twitter-dm$/, @ARGV) ? 1 : 0,
  'recipients' => [
    89576503, # @thierrydraper
  ],
  'twitter_app' => 'support',
  'twitter_account' => 'DeBearSupport',
};

# Return true to pacify the compiler
1;
