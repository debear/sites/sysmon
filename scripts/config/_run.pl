#!/usr/bin/perl -w
# Script controller-specific configuration
use strict;
use Cwd 'abs_path';
use Dir::Self;
our %config;

# Define where the schedule file is
$config{'schedule'} = abs_path(__DIR__ . '/../run/schedule');

# Load our specific additional libraries
dynamic_require(abs_path(__DIR__ . '/../lib/schedule.pl'));

# Return true to pacify the compiler
1;
