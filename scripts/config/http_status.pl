#!/usr/bin/perl -w
# HTTP request check specific configuration settings
use strict;
use POSIX;
our %config;

# Arguments we need to have received
$config{'args'}{'instance'} = 1;
$config{'args_required'} = [ 'instance' ];

$config{'today'} = strftime('%Y-%m-%d', localtime);

# Define the mapping from instance (subdomain) to file(s)
our $home_dir;
$config{'paths'} = {
    'log_base' => ($config{'is_dev'} ? '/sshfs' : $home_dir) . '/debear/logs/${domain}/app',
    'domain_map' => {
        'misc' => ['cdn'], # If we get native access logs back, this should include static
    },
    'individual_codes' => {
        200 => 1, 201 => 1, 204 => 1,
        301 => 1, 302 => 1, 304 => 1, 307 => 1, 308 => 1,
        400 => 1, 401 => 1, 403 => 1, 404 => 1, 405 => 1,
        500 => 1, 501 => 1,
    },
};
my @now = localtime();

# Return true to pacify the compiler
1;
