#!/usr/bin/perl -w
# Lockfile check specific configuration settings
use Cwd 'abs_path';
use Dir::Self;

# Arguments we need to have received
$config{'args'}{'instance'} = 1;
$config{'args_required'} = [ 'instance' ];

# Logging definition
$config{'log_file'} = { 'dir' => abs_path(__DIR__ . '/../../../../logs/common'),
                        'fmt' => '%Y-%m-%d',
                        'ext' => 'log.gz' };

# Common method for loading the list to parse via a logfile
sub load_from_file {
  my ($log_yest) = @_;
  my $log_file = $config{'log_file'}{'dir'}. '/' . $config{'instance'} . '/' . $log_yest . '.' . $config{'log_file'}{'ext'};

  # Load the file
  my $buffer;
  my $contents = '';
  my $gzh = gzopen($log_file, 'rb');
  $contents .= $buffer while $gzh->gzread($buffer) > 0;
  $gzh->gzclose();

  # Get the warning list
  return grep / -\s+(WARNING|ERROR) - /, split("\n", $contents);
}

# Return true to pacify the compiler
1;
