#!/usr/bin/perl -w
# Communications specific configuration settings
use strict;
our %config;

# Arguments we need to have received
$config{'args'}{'instance'} = 1;
$config{'args'}{'table'} = 1;
$config{'args'}{'date-col'} = 1;
$config{'args'}{'status-col'} = 1;
$config{'args_required'} = [ 'instance', 'critical', 'table', 'date-col', 'status-col' ];

# Return true to pacify the compiler
1;
