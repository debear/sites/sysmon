#!/usr/bin/perl -w
# Age check specific configuration settings
use strict;
use Dir::Self;
our %config;

# Arguments we need to have received
$config{'args'}{'instance'} = 1;
$config{'args'}{'path'} = 1;
$config{'args_required'} = [ 'instance', 'critical', 'path' ];

# Return true to pacify the compiler
1;
