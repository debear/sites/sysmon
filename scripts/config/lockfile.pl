#!/usr/bin/perl -w
# Lockfile check specific configuration settings
use strict;
our %config;

# Arguments we need to have received
$config{'args'}{'instance'} = 1;
$config{'args'}{'cmp-db'} = 1;
$config{'args'}{'cmp-tbl'} = 1;
$config{'args'}{'cmp-col'} = 1;
$config{'args'}{'cmp-where'} = 1;
$config{'args_required'} = [ 'instance' ];

# Data is in our admin database
$config{'db_lockfile'} = 'debearco_admin';

# Return true to pacify the compiler
1;
