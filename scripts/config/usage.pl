#!/usr/bin/perl -w
# Usage check specific configuration settings
use strict;
our %config;

# Arguments we need to have received
$config{'args_required'} = [ 'critical', 'warn' ];

# Return true to pacify the compiler
1;
