#!/usr/bin/perl -w
# Global database configuration settings
use strict;
our %config;

# Notification app
$config{'notif_twitter'} = {
  'our_app' => 'sysmon_status',
  'twitter_app' => 'support',
  'twitter_account' => 'DeBearSupport',
  'send_delay' => 10, # Number of minutes we should delay (allowing recall) sending of the Tweet
  'domain' => 'https://status.' . $config{'domain'},
};

# Return true to pacify the compiler
1;
