#!/usr/bin/perl -w
# General / Global configuration settings
use strict;
use POSIX;
use Cwd 'abs_path';
use Dir::Self;
our %config;
our $script;

# Timestamp that we started...
$config{'started'} = strftime('%Y-%m-%d %H:%M:%S', localtime);
$config{'started_gmt'} = strftime('%Y-%m-%d %H:%M:%S', gmtime);

# Environment related
our $home_dir;
$config{'is_dev'} = ($home_dir =~ /\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\//i);

# Script info
$config{'script'} = $script;
$config{'is_worker'} = ($config{'script'} ne '_run');

# Logging related
$config{'debug'} = grep(/^--debug$/, @ARGV);
$config{'logs'} = [ ];
$config{'logs_dir'} = abs_path(__DIR__ . '/../../logs/scripts' . ($config{'is_worker'} ? '-' . $config{'script'} : ''));

# Error codes
$config{'exit_codes'} = {
  'codes' => {
    'args' => 10,
    'check-details' => 20,
    'schedule' => 80,
    'unknown' => 99,
  },
};
%{$config{'exit_codes'}{'numbers'}} = map { $config{'exit_codes'}{'codes'}{$_} => $_ } keys(%{$config{'exit_codes'}{'codes'}});

#
# Specific config
#
dynamic_require(__DIR__ . '/worker.pl') if $config{'is_worker'};
dynamic_require(__DIR__ . '/' . $0);

# "Dynamic" (single-line) require, since `require __DIR__.'/script.pl'` is syntactically invalid
sub dynamic_require {
  my ($path) = @_;
  require $path
    if -e $path;
}

# Return true to pacify the compiler
1;
