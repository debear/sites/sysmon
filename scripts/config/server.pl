#!/usr/bin/perl -w
# Server check specific configuration settings
use strict;
our %config;

# Arguments we need to have received
$config{'args'}{'instance'} = 1;
$config{'args'}{'host'} = 1;
$config{'args'}{'port'} = 1;
$config{'args_required'} = [ 'instance', 'critical', 'warn', 'host', 'port' ];

# Return true to pacify the compiler
1;
