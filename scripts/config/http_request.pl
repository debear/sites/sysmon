#!/usr/bin/perl -w
# HTTP request check specific configuration settings
use strict;
use Dir::Self;
our %config;

# Arguments we need to have received
$config{'args'}{'instance'} = 1;
$config{'args'}{'path'} = 1;
$config{'args_required'} = [ 'instance', 'critical', 'warn', 'path' ];

# Define where the cookies should be saved between calls
$config{'cookies'} = abs_path(__DIR__ . '/..') . '/run/http.cookies';

# Return true to pacify the compiler
1;
