#!/usr/bin/perl -w
# Global database configuration settings
use strict;
use Dir::Self;
use MIME::Base64;
our %config;

$config{'db_name'} = 'debearco_sysmon';
$config{'db_comms'} = 'debearco_common';
$config{'db_sports'} = 'debearco_sports';
$config{'db_fantasy'} = 'debearco_fantasy';
$config{'db_admin'} = 'debearco_admin';
$config{'db_user'} = 'debearco_sysadmn';
# Password
if (defined($ENV{'PERL_LINT'}) && $ENV{'PERL_LINT'} && !defined($config{'db_pass'})) {
  $config{'db_pass'} = $ENV{'MYSQL_ROOT_PASSWORD'};
} else {
  my $db_pass_file = __DIR__;
  $db_pass_file =~ s@/sites/.+$@/etc/passwd/web/db@;
  $config{'db_pass'} = do { local(@ARGV, $/) = $db_pass_file; <> };
  chomp($config{'db_pass'});
  $config{'db_pass'} = decode_base64($config{'db_pass'});
}

# Return true to pacify the compiler
1;
