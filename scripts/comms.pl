#!/usr/bin/perl -w
# Check and report on whether communnication messages are being sent successfully
# TD. 2017-03-17.

use strict;
our %config;

BEGIN {
  use File::Basename;
  my $core = dirname(__FILE__) . '/lib/core.pl';
  require $core;
}

use POSIX;

$config{'dbh_alt'}{'comms'} = dbi_connect($config{'db_comms'});

##
## Date checks
##

# We run a day behind, so check sends from the date of the last check up to (but not including) today
my $last_check;
if ($config{'check'}{'last_checked'}) {
  $last_check = substr($config{'check'}{'last_checked'}, 0, 10);
} else {
  # Assuming mathemtical subtraction is DST-switch safe for first run (only expected to be an issue if run ~1am)
  #  Note: 7776000 = 90 * 86400 = 90 days
  $last_check = strftime('%Y-%m-%d', localtime(time() - 7776000));
}
my $cur_date = strftime('%Y-%m-%d', localtime);
log_msg("Capturing '$config{'instance'}' messages from '$last_check' up to (but not including) '$cur_date' (today)");

##
## Run (on a per-day basis)
##
my $check_date = $last_check;
while ($check_date lt $cur_date) {

  ##
  ## Getter
  ##
  my $sql = "SELECT IFNULL(SUM($config{'status-col'} = 'queued'), 0) AS num_queued,
                    IFNULL(SUM($config{'status-col'} = 'sent'), 0) AS num_sent,
                    IFNULL(SUM($config{'status-col'} = 'failed'), 0) AS num_failed,
                    IFNULL(SUM($config{'status-col'} NOT IN ('queued','sent','failed')), 0) AS num_other,
                    DATE_ADD(?, INTERVAL 1 DAY) AS next_date
             FROM $config{'table'}
             WHERE DATE($config{'date-col'}) = ?;";
  my $sth = $config{'dbh_alt'}{'comms'}->prepare($sql);
  $sth->execute($check_date, $check_date)
    or log_error("Unable to execute dbh_alt->comms: " . $config{'dbh_alt'}{'comms'}->errstr);
  my ($num_queued, $num_sent, $num_failed, $num_other, $next_date) = $sth->fetchrow_array;
  my $info = "Date: $check_date; Queued: $num_queued; Sent: $num_sent; Failed: $num_failed; Other: $num_other";
  log_msg($info);

  ##
  ## Analyse
  ##
  my $status;

  # No failed sends implies all okay
  if (!$num_failed) {
    $status = determine_status_recovered();

  # If we have a certain number of failures, consider them to be a critical
  } elsif (($config{'critical'} !~ /^\d+$/) || ($num_failed >= $config{'critical'})) {
    $status = 'critical';

  # Otherwise, we'll just consider it a warning
  } else {
    $status = 'warn';
  }

  ##
  ## Summarise
  ##
  # Worst-case
  my $summary_field;
  if ($num_failed) {
    $summary_field = 0;
  } elsif ($num_other) {
    $summary_field = 3;
  } elsif ($num_queued) {
    $summary_field = 2;
  } elsif ($num_sent) {
    $summary_field = 1;
  } else {
    $summary_field = 99;
  }

  # Individual counts
  my @summary_msg = ();
  push @summary_msg, sprintf('%d failed', $num_failed)
    if $num_failed;
  push @summary_msg, sprintf('%d other', $num_other)
    if $num_other;
  push @summary_msg, sprintf('%d queued', $num_queued)
    if $num_queued;
  push @summary_msg, sprintf('%d sent', $num_sent)
    if $num_sent;
  my $summary_msg = (@summary_msg
    ? join(', ', @summary_msg)
    : 'Unknown message statuses');


  ##
  ## Store
  ##

  # Notify & Log
  my $msg_detail = "- $info"; $msg_detail =~ s/;/\n- /g;
  my $notified = check_send_notif($msg_detail, $summary_msg, $status);
  log_run($status, $summary_field, $notified, $info, $check_date);

  # Store our lockfile status check (with values capped to a TINYINT limit, as that is sufficiently illustrative)
  $sql = "INSERT INTO SYSMON_COMMS (type, date, num_queued, num_sent, num_failed, num_other, status, notified)
    VALUES (?, ?, LEAST(?, 255), LEAST(?, 255), LEAST(?, 255), LEAST(?, 255), ?, ?)
  ON DUPLICATE KEY UPDATE num_queued = VALUES(num_queued),
                          num_sent = VALUES(num_sent),
                          num_failed = VALUES(num_failed),
                          num_other = VALUES(num_other),
                          status = VALUES(status),
                          notified = VALUES(notified);";
  $sth = $config{'dbh'}->prepare($sql);
  $sth->execute($config{'instance'}, $check_date, int($num_queued), int($num_sent), int($num_failed), int($num_other), $status, $notified)
    or log_error("Unable to execute sth->comms: " . $config{'dbh'}->errstr);

  # Updates for the next iteration
  $check_date = $next_date;
  $config{'check'}{'last_status'} = $status;
}
