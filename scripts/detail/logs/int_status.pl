#!/usr/bin/perl -w
# Internal status updater
sub parse_logs {
  our %config;
  # Load from the database
  my $sql = 'SELECT group_id, name, num_okay, num_warn, num_critical, num_unknown
FROM SYSMON_CHECKS_GROUPS
WHERE internal = 0
ORDER BY `order`;';
  my $sth = $config{'dbh'}->prepare($sql);
  $sth->execute()
    or log_error("Unable to execute dbh->int: " . $config{'dbh'}->errstr);

  # Then parse
  my @warnings = ( ); my $total_warnings = 0;
  my %breakdown = ( 'status' => { 'okay' => 0, 'warn' => 0, 'critical' => 0, 'unknown' => 0 } );
  while (my $w = $sth->fetchrow_hashref) {
    $$w{'num_warnings'} = $$w{'num_warn'} + $$w{'num_critical'};
    $total_warnings += $$w{'num_warnings'};

    $breakdown{'status'}{'okay'} += $$w{'num_okay'};
    $breakdown{'status'}{'warn'} += $$w{'num_warn'};
    $breakdown{'status'}{'critical'} += $$w{'num_critical'};
    $breakdown{'status'}{'unknown'} += $$w{'num_unknown'};

    push @warnings, "$$w{'name'} (ID: $$w{'group_id'}) - Okay: $$w{'num_okay'}; Warn: $$w{'num_warn'}; Critical: $$w{'num_critical'}; Unknown: $$w{'num_unknown'}"
      if $$w{'num_warnings'};
  }
  return (\@warnings, \%breakdown, $total_warnings, 'status'); # Run summary is of the "status" breakdown
}

# Return true to pacify the compiler
1;
