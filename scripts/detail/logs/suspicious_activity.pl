#!/usr/bin/perl -w
# Suspicious activity log file loading and parsing
sub parse_logs {
  our %config;
  # Connect to the appropriate database
  $config{'dbh_alt'}{'admin'} = dbi_connect($config{'db_admin'});

  # Load from the database
  my $sql = 'SELECT activity_time, type, summary
FROM USER_ACTIVITY_LOG
WHERE DATE(activity_time) = ?
AND   JSON_EXTRACT(detail, "$.blocked") = true
ORDER BY activity_time, type;';
  my $sth = $config{'dbh_alt'}{'admin'}->prepare($sql);
  $sth->execute($_[0])
    or log_error("Unable to execute dbh_alt->log: " . $config{'dbh_alt'}{'admin'}->errstr);

  my @warnings = ( );
  my %breakdown = ( 'type' => { } );
  while (my $w = $sth->fetchrow_hashref) {
    # Add to lists
    $breakdown{'type'}{$$w{'type'}} = 0 if !defined($breakdown{'type'}{$$w{'type'}});
    $breakdown{'type'}{$$w{'type'}}++;

    push @warnings, "$$w{'activity_time'} - Type $$w{'type'} - $$w{'summary'}";
  }

  return (\@warnings, \%breakdown, scalar @warnings, 'type'); # Run summary is of the "type" breakdown
}

# Return true to pacify the compiler
1;
