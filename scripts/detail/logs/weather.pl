#!/usr/bin/perl -w
# Weather sync specific log file loading and parsing
sub parse_logs {
  my @w = load_from_file(@_);
  my @warnings = ( );
  my %breakdown = ( 'app' => { }, 'error' => { } );
  foreach my $w (@w) {
    # Break in to component parts
    my ($time, $err, $app, $key, $id, $loc) = ($w =~ m/^.*? (\d{2}:\d{2}:\d{2}) - WARNING - (.*?) from .*?\((.*?) \/\/ (.*?) \/\/ (.*?) \/\/ (.*?)\)$/);

    # Add to lists
    my $app_combi = $app . '-' . $key;
    $breakdown{'app'}{$app_combi} = 0 if !defined($breakdown{'app'}{$app_combi});
    $breakdown{'app'}{$app_combi}++;

    $breakdown{'error'}{$err} = 0 if !defined($breakdown{'error'}{$err});
    $breakdown{'error'}{$err}++;

    push @warnings, "$time - $app - $key - $id - $loc - $err";
  }

  return (\@warnings, \%breakdown, scalar @warnings, 'app'); # Run summary is of the "by app" breakdown
}

# Return true to pacify the compiler
1;
