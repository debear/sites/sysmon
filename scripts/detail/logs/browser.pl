#!/usr/bin/perl -w
# Browser report specific log file loading and parsing
sub parse_logs {
  our %config;
  # Load from the database
  my $sql = 'SELECT request_time, IFNULL(site, "(unknown)") AS site, IFNULL(uri_location, "") AS uri_location, type, num_reports
FROM REPORTS_INSTANCE
WHERE DATE(request_time) = ?
AND   status IN ("reported", "resolved")
ORDER BY request_time, site, type;';
  my $sth = $config{'dbh'}->prepare($sql);
  $sth->execute($_[0])
    or log_error("Unable to execute dbh->reports: " . $config{'dbh'}->errstr);

  # Then parse
  my @warnings = ( ); my $total_warnings = 0;
  my %breakdown = ( 'type' => { }, 'site' => { } );
  while (my $w = $sth->fetchrow_hashref) {
    # Add to lists
    foreach my $col ('type','site') {
      $breakdown{$col}{$$w{$col}} = 0 if !defined($breakdown{$col}{$$w{$col}});
      $breakdown{$col}{$$w{$col}} += $$w{'num_reports'};
    }
    $total_warnings += $$w{'num_reports'};

    push @warnings, "$$w{'request_time'} - $$w{'site'} - $$w{'uri_location'} - $$w{'type'} - $$w{'num_reports'}";
  }

  return (\@warnings, \%breakdown, $total_warnings, 'type'); # Run summary is of the "type" breakdown
}

# Return true to pacify the compiler
1;
