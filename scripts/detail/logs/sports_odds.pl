#!/usr/bin/perl -w
# Sports odds sync specific log file loading and parsing
sub parse_logs {
  my @w = load_from_file(@_);
  my @warnings = ( );
  my %breakdown = ( 'sport' => { }, 'error' => { } );
  foreach my $w (@w) {
    # Break in to component parts
    my ($time, $level, $err, $sport, $key) = ($w =~ m/^.*? (\d{2}:\d{2}:\d{2}) -\s+(WARNING|ERROR) - (.*?) in (\S+) game (\S+)/);

    # Add to lists
    $breakdown{'sport'}{$sport} = 0 if !defined($breakdown{'sport'}{$sport});
    $breakdown{'sport'}{$sport}++;

    $breakdown{'error'}{$err} = 0 if !defined($breakdown{'error'}{$err});
    $breakdown{'error'}{$err}++;

    push @warnings, "$time - $sport - $key - $err";
  }

  return (\@warnings, \%breakdown, scalar @warnings, 'sport'); # Run summary is of the "by sport" breakdown
}

# Return true to pacify the compiler
1;
