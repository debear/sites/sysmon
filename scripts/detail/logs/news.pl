#!/usr/bin/perl -w
# News sync specific log file loading and parsing
sub parse_logs {
  # First check: errors in the log files
  my @w = load_from_file(@_);
  my @warnings = ( );
  my %breakdown = ( 'app' => { }, 'source' => { }, 'error' => { } );
  foreach my $w (@w) {
    # Break in to component parts
    my ($time, $err, $app, $source, $msg) = ($w =~ m/^.*? (\d{2}:\d{2}:\d{2}) - WARNING - (.*?) for ([^\/ ]+)(\/\/\d+)? .+: (.*?)$/);

    # Add to lists
    $breakdown{'app'}{$app} = 0 if !defined($breakdown{'app'}{$app});
    $breakdown{'app'}{$app}++;

    if (defined($source)) {
      my $app_source = "$app$source";
      $breakdown{'source'}{$app_source} = 0 if !defined($breakdown{'source'}{$app_source});
      $breakdown{'source'}{$app_source}++;
    } else {
      $source = '-';
    }

    $breakdown{'error'}{$err} = 0 if !defined($breakdown{'error'}{$err});
    $breakdown{'error'}{$err}++;

    my $source_id = $source; $source_id =~ s/^\/\///;
    push @warnings, "$time - $app - $source_id - $err";
  }

  # Secondary check: feeds that haven't been synced recently?
  our %config;
  # Connect to the appropriate database
  $config{'dbh_alt'}{'common'} = dbi_connect($config{'db_comms'});
  # Load from the database
  my $sql = 'SELECT app, source_id, name, last_synced
FROM NEWS_SOURCES
WHERE active = 1
AND   last_synced < DATE_SUB(NOW(), INTERVAL 6 HOUR)
ORDER BY last_synced;';
  my $sth = $config{'dbh_alt'}{'common'}->prepare($sql);
  $sth->execute
    or log_error("Unable to execute dbh_alt->news: " . $config{'dbh_alt'}{'common'}->errstr);
  while (my $w = $sth->fetchrow_hashref) {
    $$w{'err'} = "RSS feed last parsed successfully on $$w{'last_synced'}";

    # Add to lists
    $breakdown{'app'}{$$w{'app'}} = 0 if !defined($breakdown{'app'}{$$w{'app'}});
    $breakdown{'app'}{$$w{'app'}}++;

    if (defined($$w{'source'})) {
      my $app_source = "$$w{'app'}//$$w{'source'}";
      $breakdown{'source'}{$app_source} = 0 if !defined($breakdown{'source'}{$app_source});
      $breakdown{'source'}{$app_source}++;
    } else {
      $$w{'source'} = '-';
    }

    push @warnings, "$$w{'app'} - $$w{'source_id'} - $$w{'err'}";
  }

  return (\@warnings, \%breakdown, scalar @warnings, 'app'); # Run summary is of the "by app" breakdown
}

# Return true to pacify the compiler
1;
