#!/usr/bin/perl -w
# Unknown NHL Game Events specific log file loading and parsing
sub parse_logs {
  # Connect to the appropriate database
  $config{'dbh_alt'}{'sports'} = dbi_connect($config{'db_sports'});

  # Load from the database
  my $sql = 'SELECT GAME_EVENT.season, GAME_EVENT.game_type, GAME_EVENT.game_id, SCHED.home AS game_home, SCHED.visitor AS game_visitor, GAME_EVENT.event_id, GAME_EVENT.period, GAME_EVENT.event_time, GAME_EVENT.event_type, GAME_EVENT.play
FROM SPORTS_NHL_SCHEDULE AS SCHED
JOIN SPORTS_NHL_GAME_EVENT AS GAME_EVENT
  ON (GAME_EVENT.season = SCHED.season
  AND GAME_EVENT.game_type = SCHED.game_type
  AND GAME_EVENT.game_id = SCHED.game_id
  AND GAME_EVENT.event_type = "?")
WHERE SCHED.game_date = ?
ORDER BY game_type, game_id, event_id;';
  my $sth = $config{'dbh_alt'}{'sports'}->prepare($sql);
  $sth->execute($_[0])
    or log_error("Unable to execute dbh_alt->sports: " . $config{'dbh_alt'}{'sports'}->errstr);

  # Then parse
  my @warnings = ( ); my $total_warnings = 0;
  my %breakdown = ( 'type' => { } );
  while (my $w = $sth->fetchrow_hashref) {
    $breakdown{'type'}{$$w{'event_type'}} = 0 if !defined($breakdown{'type'}{$$w{'event_type'}});
    $breakdown{'type'}{$$w{'event_type'}}++;
    $total_warnings++;

    push @warnings, "$$w{'season'}//$$w{'game_type'}//$$w{'game_id'}, $$w{'game_visitor'} \@ $$w{'game_home'} - ID: $$w{'event_id'}, P: $$w{'period'}, T: $$w{'event_time'} - $$w{'event_type'} - $$w{'play'}";
  }

  return (\@warnings, \%breakdown, $total_warnings, 'type'); # Run summary is of the "type" breakdown
}

# Return true to pacify the compiler
1;
