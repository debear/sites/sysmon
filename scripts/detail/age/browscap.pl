#!/usr/bin/perl -w
use LWP::UserAgent;
use HTTP::Request;

# Specific post-parsing checks when we found the age is excessive
sub parse_detail {
  my ($ref_date) = @_;

  # Load the website which lists the latest file date
  my $ua = LWP::UserAgent->new();
  $ua->timeout(10);
  $ua->agent('HTTP Request Checker/1.0');
  my $request = HTTP::Request->new(GET => 'http://browscap.org/');
  my $response = $ua->request($request);

  my ($raw_d, $raw_m, $raw_y) = ($response->content =~ m/<p>The latest version is <strong>\d+<\/strong> \((\d+)(?:st|nd|rd|th) ([a-z]{3}) (20\d{2})\)<\/p>/gsi);
  my $raw_date = sprintf('%04d-%02d-%02d', $raw_y, parse_month($raw_m), $raw_d);

  # If the file date is on or after this raw date, the file is still valid
  log_msg(" - Raw Date: $raw_date");
  return $raw_date le $ref_date;
}

sub parse_month {
  my ($in) = @_;
  if ($in eq 'Jan') {
    return 1;
  } elsif ($in eq 'Feb') {
    return 2;
  } elsif ($in eq 'Mar') {
    return 3;
  } elsif ($in eq 'Apr') {
    return 4;
  } elsif ($in eq 'May') {
    return 5;
  } elsif ($in eq 'Jun') {
    return 6;
  } elsif ($in eq 'Jul') {
    return 7;
  } elsif ($in eq 'Aug') {
    return 8;
  } elsif ($in eq 'Sep') {
    return 9;
  } elsif ($in eq 'Oct') {
    return 10;
  } elsif ($in eq 'Nov') {
    return 11;
  } elsif ($in eq 'Dec') {
    return 12;
  }
}

# Return true to pacify the compiler
1;
