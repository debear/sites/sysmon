#!/usr/bin/perl -w
# Check and report on whether a server is up
# TD. 2017-03-16.

use strict;
our %config;

BEGIN {
  use File::Basename;
  my $core = dirname(__FILE__) . '/lib/core.pl';
  require $core;
}

use Socket;
use IO::Socket::PortState qw(check_ports);

##
## Getter
##

log_msg("Testing host '$config{'host'}' against port '$config{'port'}'");

# Current IP address?
my $ip;
if ($config{'host'} =~ m/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/) {
  # The arg we have is an IP, so no need to convert
  $ip = $config{'host'};
} else {
  # Convert a hostname into an IP
  my ($d_name, $d_aliases, $d_addrtype, $d_length, @d_addrs) = gethostbyname($config{'host'});
  $ip = join('.', unpack('C4', $d_addrs[0]))
    if @d_addrs;
  $ip = '*unknown*'
    if !defined($ip);
#} else {
#  # Original version of host->IP using functions marked in perldoc with "maintained for legacy support"
#  my $aton = inet_aton($config{'host'});
#  $ip = (defined($aton) ? inet_ntoa($aton) : '*unknown*');
}
log_msg("IP address (for information) recorded as '$ip'");

# Port open or not?
my %port = ( 'tcp' => { $config{'port'} => { 'name' => 'Known Port Test' } } );
check_ports($config{'host'}, 1, \%port); # 1 = timeout
# Alternate version: my $test_result = `(echo > /dev/tcp/$config{'host'}/$config{'port'}) >/dev/null 2>&1; echo $?`;

##
## Analyse
##

my $is_up = $port{'tcp'}{$config{'port'}}{'open'};
# Alternate version: my $is_up = !$test_result;
my $state = ($is_up ? 'up' : 'down');
my $info = "Host: $config{'host'}; IP: $ip; Test Port: $config{'port'}; Result: " . ucfirst($state);

# Status depends on if we're up or not, and if not for how long
my $status;
if ($is_up) {
  # We're up, so determine our okay/recovered status
  $status = determine_status_recovered();
} else {
  # How long have we been down? Decide based on last state and length of run
  # - On first run (where we expect a NULL state), consider this "down" a state switch, so consider the last state as "up"
  my $last_state;
  if (defined($config{'check'}{'last_status'})) {
    $last_state = ($config{'check'}{'last_status'} eq 'okay' || $config{'check'}{'last_status'} eq 'recovered') ? 'up' : 'down';
  } else {
    $last_state = 'down';
  }
  # Length of run
  my $length;
  if ($last_state eq 'up') {
    # If we were previously up, this is the first in the run
    $length = 1;
  } else {
    # How many runs since we were last up?
    # First, determine when this happened
    my $sql = 'SELECT MAX(checked) AS last_up FROM SYSMON_SERVER WHERE server = ? AND state = "up";';
    my $sth = $config{'dbh'}->prepare($sql);
    $sth->execute($config{'instance'})
      or log_error("Unable to execute sth->up: " . $config{'dbh'}->errstr);
    my ($last_up) = $sth->fetchrow_array;
    $last_up = '1970-01-01 00:00:00'
      if !defined($last_up);
    # Then the number of runs
    $sql = 'SELECT COUNT(*) FROM SYSMON_SERVER WHERE server = ? AND checked > ?;';
    $sth = $config{'dbh'}->prepare($sql);
    $sth->execute($config{'instance'}, $last_up)
      or log_error("Unable to execute sth->num: " . $config{'dbh'}->errstr);
    ($length) = $sth->fetchrow_array;
    $length = 0 if !defined($length);
    $length++; # This is the n+1th instance
  }
  log_msg("Down detected; Last state: '$last_state'; Length: '$length'");
  # Now do our warn/critical status check using this length
  $status = determine_status_error($length);
  $status = 'warn' if !defined($status); # Assume warning if misconfigured
}

##
## Store
##

# Notify & Log
my $msg_detail = "- $info"; $msg_detail =~ s/;/\n- /g;
my $msg_summary = "Host is $state";
my $notified = check_send_notif($msg_detail, $msg_summary, $status);
log_run($status, $is_up, $notified, $info);

# Store our lockfile status check
my $sql = "INSERT INTO SYSMON_SERVER (server, checked, checked_date, checked_time, ip, state, status, notified)
  VALUES (?, UTC_TIMESTAMP(), UTC_DATE(), UTC_TIME(), ?, ?, ?, ?)
ON DUPLICATE KEY UPDATE ip = VALUES(ip),
                        state = VALUES(state),
                        status = VALUES(status),
                        notified = VALUES(notified);";
my $sth = $config{'dbh'}->prepare($sql);
$sth->execute($config{'instance'}, $ip, $state, $status, $notified)
  or log_error("Unable to execute sth->server: " . $config{'dbh'}->errstr);
