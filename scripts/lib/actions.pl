#!/usr/bin/perl -w
use strict;
use Dir::Self;
our %config;

dynamic_require(__DIR__ . '/notification.pl');

# Determine info about the check script being run
sub determine_check_details {
  # Determine the script and supporting (instance) args
  our $script;
  my $instance = (defined($config{'instance'}) ? $config{'instance'} : '');
  log_msg("Identifying check based on script '$script'" . ($instance ? " and instance '$instance'" : ''));

  # Run
  my $sql = "SELECT * FROM SYSMON_CHECKS WHERE script = ? AND IFNULL(instance, '') = ?;";
  my $sth = $config{'dbh'}->prepare($sql);
  $sth->execute($script, $instance)
    or log_error("Unable to execute dbh->checks: " . $config{'dbh'}->errstr);
  $config{'check'} = $sth->fetchrow_hashref;
  $config{'check'}{'frequency_type'} = ($config{'check'}{'frequency'} =~ /^\d+$/ ? 'daily' : 'hourly');

  # Skip if no match
  log_error('Unknown script initiated. Aborting.', 'check-details')
    if !defined($config{'check'}{'check_id'});
  log_error('Inactive script initiated. Aborting.', 'check-details')
    if !$config{'check'}{'active'};

  log_msg("Check Identified: $config{'check'}{'name'} (ID: $config{'check'}{'check_id'})");
}

# Status of the check
sub determine_status {
  my ($comparison) = @_;
  log_msg("Determining status based on comparison value '$comparison'");

  # Above a threshold?
  my $error_status = determine_status_error($comparison);
  return $error_status
    if defined($error_status);

  # No, so everything's okay. So, have we just recovered from a previous error?
  return determine_status_recovered();
}

sub determine_status_error {
  my ($comparison) = @_;
  foreach my $c ('critical','warn') {
    if (defined($config{$c}) && ($config{$c} =~ m/^\d+$/) && ($comparison >= $config{$c})) {
      log_msg("Returning '$c' based on check against value '$config{$c}'");
      return $c;
    }
  }
  return undef;
}

# Determine if an 'okay' status is actually recovered
sub determine_status_recovered {
  my $last_status = $config{'check'}{'last_status'};
  my $ret = (!defined($last_status) || $last_status eq 'okay' || $last_status eq 'recovered') ? 'okay' : 'recovered';
  log_msg("Returning '$ret' status based on a last status of '$last_status'");
  return $ret;
}

# Notify user(s) of appropriate events
sub check_send_notif {
  my ($msg_detail, $msg_summary, $status) = @_;

  # In all scenarios, no send is necessary if the status is 'okay'
  if ($status eq 'okay') {
    log_msg('No notification is appropriate when the status is \'okay\'');
    return 'no';
  }
  # Catching the rare (undesired?) scenario that all mechanisms are tured off
  if (!$config{'notif_email'}{'on'} && !$config{'notif_twitter_dm'}{'on'}) {
    log_msg('All notification mechanisms have been disabled');
    return 'disabled';
  }

  # Flag if we're dealing with a status change
  my $status_changed = ($config{'check'}{'last_status'} ne $status);

  # If we are only sending email notification, we only send if the new status is 'critical'
  if (($status ne 'critical') && $config{'notif_email'}{'on'} && !$config{'notif_twitter_dm'}{'on'}) {
    log_msg("No email-only notification relevant due to run status '$status'");
    return 'no';
  }

  # Skip if no notifications required
  if (!$config{'check'}{'notifications_on'}) {
    log_msg('Notification disabled within the check configuration');
    return 'disabled';
  }

  # Are we within an offset period?
  if (defined($config{'check'}{'notifications_offset'}) && ($config{'check'}{'notifications_offset'} > 0)) {
    my $num_offset = $config{'check'}{'notifications_offset'};
    log_msg("Determining if we are within a '$num_offset' check notification offset");
    # When was the last okay received?
    my $last_sql = 'SELECT MAX(started) AS last_okay FROM SYSMON_LOG WHERE check_id = ? AND status IN ("okay", "recovered") ORDER BY started DESC LIMIT 1;';
    my $last_sth = $config{'dbh'}->prepare($last_sql);
    $last_sth->execute($config{'check'}{'check_id'})
      or log_error("Unable to execute dbh->okay: " . $config{'dbh'}->errstr);
    my ($last_okay) = $last_sth->fetchrow_array;
    log_msg("- Last 'okay' returned on '$last_okay'");
    # How many checks since?
    my $num_sql = 'SELECT COUNT(*) AS num_since, SUM(status IN ("warn", "critical") AND notified = "yes") AS num_notified FROM SYSMON_LOG WHERE check_id = ? AND started > ?;';
    my $num_sth = $config{'dbh'}->prepare($num_sql);
    $num_sth->execute($config{'check'}{'check_id'}, $last_okay)
      or log_error("Unable to execute dbh->num: " . $config{'dbh'}->errstr);
    my ($num_since, $num_notified) = $num_sth->fetchrow_array;
    log_msg("- There ha(s|ve) been '$num_since' check(s) completed since the last 'okay', with '$num_notified' notifications sent.");
    # If we're recovered, have we had any warn/critical notifications?
    if (($status eq 'recovered') && !$num_notified) {
      log_msg(' - Skipping: Check recovered before making a warn/critical notification.');
      return 'disabled';
    # What does this mean?
    } elsif ($num_since <= $config{'check'}{'notifications_offset'}) {
      log_msg(' ** This is within an notification offset **');
      return 'disabled';
    }
  }

  # Next, check if sent "recently" if there's no significant status change
  if (!$status_changed) {
    my $notif_freq = $config{'check'}{'notifications_freq'};
    $notif_freq  =  0 if !defined($notif_freq); # Ensure _a_ value is set (that will result in a notification)
    $notif_freq *= 24 if ($config{'check'}{'frequency_type'} eq 'daily'); # Our checks look back over x hours, so convert a daily frequency value

    my $freq_sql = 'SELECT IFNULL(SUM(notified = "yes"), 0) AS num_notif FROM SYSMON_LOG WHERE check_id = ? AND started > DATE_SUB(UTC_TIMESTAMP(), INTERVAL ? HOUR);';
    my $freq_sth = $config{'dbh'}->prepare($freq_sql);
    $freq_sth->execute($config{'check'}{'check_id'}, $notif_freq)
      or log_error("Unable to execute dbh->sent: " . $config{'dbh'}->errstr);
    my ($notif_sent) = $freq_sth->fetchrow_array;

    my $notif_freq_type = ($config{'check'}{'frequency_type'} eq 'daily' ? 'day' : 'hour') . '(s)';
    log_msg("Notifications sent in last $notif_freq $notif_freq_type: $notif_sent");

    # Skip if at least one send found
    if ($notif_sent) {
      log_msg(' - No notification appropriate');
      return 'no';
    }
  }

  # Skip if notifications are being suppressed (but we'll also record this as such)
  my $suppress_sql = 'SELECT (check_id IS NOT NULL) AS suppress, start
FROM SYSMON_NOTIFICATION_SILENT
WHERE check_id = ?
AND   UTC_TIMESTAMP() BETWEEN start AND IFNULL(end, DATE_ADD(UTC_TIMESTAMP(), INTERVAL 1 SECOND));';
  my $suppress_sth = $config{'dbh'}->prepare($suppress_sql);
  $suppress_sth->execute($config{'check'}{'check_id'})
    or log_error("Unable to execute dbh->suppress: " . $config{'dbh'}->errstr);
  my ($suppress, $suppress_start) = $suppress_sth->fetchrow_array;
  if ($suppress) {
    # We'll also update the counter
    $suppress_sql = 'UPDATE SYSMON_NOTIFICATION_SILENT
SET num_suppressed = num_suppressed + 1
WHERE check_id = ?
AND   start = ?;';
    $suppress_sth = $config{'dbh'}->prepare($suppress_sql);
    $suppress_sth->execute($config{'check'}{'check_id'}, $suppress_start)
      or log_error("Unable to execute dbh->counter: " . $config{'dbh'}->errstr);
    return 'suppressed';
  }

  # Perform the send (on live...)
  if (!$config{'is_dev'}) {
    # Email?
    notification_email($msg_detail)
      if $config{'notif_email'}{'on'};
    # Twitter DM?
    notification_twitter_dm($status, $status_changed, $msg_summary)
      if $config{'notif_twitter_dm'}{'on'};
  } else {
    log_msg(" - Skipping actual send as on dev server");
  }
  return 'yes';
}

# Log a check run
sub log_run {
  my ($status, $summary, $notified, $info, $log_date) = @_;

  # Default $log_date needed?
  $log_date = ($config{'check'}{'frequency_type'} eq 'daily' ? 'UTC_DATE()' : 'UTC_TIMESTAMP()')
    if !defined($log_date);

  # First in our row-by-row log table
  my $sql = 'INSERT INTO SYSMON_LOG (check_id, log_date, started, finished, status, notified, info) VALUES (?, CASE ? WHEN "UTC_DATE()" THEN UTC_DATE() WHEN "UTC_TIMESTAMP()" THEN UTC_TIMESTAMP() ELSE ? END, ?, UTC_TIMESTAMP(), ?, ?, ?);';
  my $sth = $config{'dbh'}->prepare($sql);
  $sth->execute($config{'check'}{'check_id'}, $log_date, $log_date, $config{'started_gmt'}, $status, $notified, $info)
    or log_error("Unable to execute dbh->log: " . $config{'dbh'}->errstr);
  log_msg("Logging run: '$config{'check'}{'check_id'}', '$config{'started_gmt'}', '$status', '$notified', " . (defined($info) ? "'$info'" : 'NULL'));

  # But also within our check definition table
  $sql = 'UPDATE SYSMON_CHECKS SET last_checked = UTC_TIMESTAMP(), last_status = ?, last_summary = ? WHERE check_id = ?;';
  $sth = $config{'dbh'}->prepare($sql);
  $sth->execute($status, $summary, $config{'check'}{'check_id'})
    or log_error("Unable to execute dbh->update: " . $config{'dbh'}->errstr);
  log_msg("Updating checks definition table to say we've run");

  # And finally bubble up to the group
  $sql = 'INSERT INTO SYSMON_CHECKS_GROUPS (group_id, name, state, num_okay, num_warn, num_critical, num_unknown)
  SELECT group_id, name,
         IF(SUM(last_status = "critical"), "critical",
            IF(SUM(last_status = "warn"), "warn",
               IF(SUM(last_status IN ("okay", "recovered")), "okay",
                  "unknown"))) AS state,
         SUM(last_status IN ("okay", "recovered")) AS num_okay,
         SUM(last_status = "warn") AS num_warn,
         SUM(last_status = "critical") AS num_critical,
         SUM(IFNULL(last_status, "unknown") = "unknown") AS num_unknown
  FROM SYSMON_CHECKS
  WHERE group_id = ?
  GROUP BY group_id, name
ON DUPLICATE KEY UPDATE state = VALUES(state),
                        num_okay = VALUES(num_okay),
                        num_warn = VALUES(num_warn),
                        num_critical = VALUES(num_critical),
                        num_unknown = VALUES(num_unknown);';
  $sth = $config{'dbh'}->prepare($sql);
  $sth->execute($config{'check'}{'group_id'})
    or log_error("Unable to execute dbh->group: " . $config{'dbh'}->errstr);
  log_msg("Updating groups table to include our latest info");
}

# Return true to pacify the compiler
1;
