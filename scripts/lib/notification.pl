#!/usr/bin/perl -w
use strict;
our %config;

# Send as an email
sub notification_email {
  my ($msg_detail) = @_;
  our $script_raw;

  # Connect to the database, unless we've already done so in the script
  $config{'dbh_alt'}{'comms'} = dbi_connect($config{'db_comms'})
    if !defined($config{'dbh_alt'}{'comms'});

  # Build our message
  my $msg = 'During the just completed run of the ' . $config{'check'}{'name'} . ' script, the following was raised:' . "\n\n" .
    $msg_detail . "\n\n" .
    'System Monitor Script info: ' . $script_raw . ($config{'args-orig'} ? ' ' . $config{'args-orig'} : '');

  # And store to be sent
  my $sql = 'INSERT INTO COMMS_EMAIL (app, email_id, email_reason, email_from, email_to, email_subject, email_body, email_queued, email_send, email_status)
  VALUES (\'sysmon\', NULL, CONCAT(\'notification_\', ?), ?, ?, ?, ?, UTC_TIMESTAMP(), UTC_TIMESTAMP(), \'queued\');';
  my $sth = $config{'dbh_alt'}{'comms'}->prepare($sql);
  $sth->execute(
    $config{'check'}{'script'}, # email_reason (script part of arg)
    $config{'notif_email'}{'from'},
    $config{'notif_email'}{'to'},
    'System Monitor \'' . $config{'check'}{'name'} . '\' problem identified',
    $msg
  ) or log_error("Unable to execute dbh_alt->comms: " . $config{'dbh_alt'}{'comms'}->errstr);
  log_msg(" - Sending email to '$config{'notif_email'}{'to'}'");
}

# Send as a direct message through twitter
sub notification_twitter_dm {
  my ($status, $status_changed, $msg_summary) = @_;

  # Connect to the database, unless we've already done so in the script
  $config{'dbh_alt'}{'comms'} = dbi_connect($config{'db_comms'})
    if !defined($config{'dbh_alt'}{'comms'});

  # Build our message
  my $msg = "$config{'check'}{'name'}: " . ucfirst($config{'check'}{'last_status'});
  if ($status_changed) {
    # Status change, so include the note
    $msg .= " &raquo; " . ucfirst($status) . ($msg_summary ? "\n$msg_summary." : '');
  } else {
    # Continuation of (critical|warn) status (assumed on input)
    $msg .= " status continues\nThis is a reminder that the check remains in a " . ucfirst($config{'check'}{'last_status'}) . " state.";
  }

  # And store to be sent
  my $sql = 'INSERT INTO COMMS_TWITTER_DM (app, dm_id, dm_type, recipient_id, dm_body, dm_queued, dm_send, dm_status, twitter_app, twitter_account)
  VALUES (\'sysmon\', NULL, CONCAT(\'notification_\', ?), ?, ?, UTC_TIMESTAMP(), UTC_TIMESTAMP(), \'queued\', ?, ?);';
  my $sth = $config{'dbh_alt'}{'comms'}->prepare($sql);
  foreach my $recipient (@{$config{'notif_twitter_dm'}{'recipients'}}) {
    $sth->execute(
      $config{'check'}{'script'}, # dm_type (script part of arg)
      $recipient,
      $msg,
      $config{'notif_twitter_dm'}{'twitter_app'},
      $config{'notif_twitter_dm'}{'twitter_account'}
    ) or log_error("Unable to execute dbh_alt->comms: " . $config{'dbh_alt'}{'comms'}->errstr);
  }

  log_msg(' - Sending Twitter direct message to ' . join(', ', @{$config{'notif_twitter_dm'}{'recipients'}}));
}

# Return true to pacify the compiler
1;
