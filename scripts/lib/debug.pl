#!/usr/bin/perl -w
# Debug logging methods
use strict;
use POSIX;
our %config;

# Record the log
sub log_msg {
  my ($msg) = @_;
  my $log_msg = '[' . strftime('%Y-%m-%d %H:%M:%S', localtime) . (defined($config{'script'}) ? " // S: $config{'script'}" : '') . " // P: $$] $msg";
  push @{$config{'logs'}}, $log_msg;
  print "$log_msg\n" if $config{'debug'};
}

# Record as a warning (non-fatal error)
sub log_warn {
  my ($msg) = @_;
  log_msg("WARN: $msg");
}

# Record as an error (fatal error)
sub log_error {
  my ($msg, $exit_code) = @_;
  $exit_code = 'unknown' if (!defined($exit_code) || !defined($config{'exit_codes'}{'codes'}{$exit_code}));
  my $exit_code_num = $config{'exit_codes'}{'codes'}{$exit_code};
  log_msg("ERROR: $msg (Exit Code: '$exit_code' // '$exit_code_num')");
  print STDERR "$msg\n";
  exit $exit_code_num;
}

# Save to disk
sub save_logs {
  # If no logs, no need to save anything
  return if !@{$config{'logs'}};

  # Does the log directory exist?
  mkdir $config{'logs_dir'} if !-e $config{'logs_dir'};

  # Determine the filename (based on current day)
  my $fmt = '%Y-%m-%d' . (defined($config{'log_inc_time'}) ? '_' . $config{'log_inc_time'} : '');
  my $file = $config{'logs_dir'} . '/' . strftime($fmt, localtime) . '.log.gz';

  # Any exit code seems to be re-set by the call to | gzip in the logging, so capture and restore
  my $exit_code = $?;

  # Now save
  open FILE, "| gzip >>$file";
  binmode FILE, ":encoding(utf8)";
  print FILE join("\n", @{$config{'logs'}}) . "\n";
  close FILE;

  # Restore exit code
  $? = $exit_code;
}

# Return true to pacify the compiler
1;
