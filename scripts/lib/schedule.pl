#!/usr/bin/perl -w
# Schedule parsing libraries
use strict;
use POSIX qw(tzset);
our %config;
$ENV{TZ} = 'Europe/London'; # Ensure we run on Europe/London time

sub load_schedule {
  # Determine the schedule from the database
  return load_schedule_db();
}

sub load_schedule_db {
  my @sched = ( );

  # A single query for pre-/post-processing
  my $sql = 'SELECT addtl_id, name, frequency, script, script_args
FROM SYSMON_CHECKS_ADDITIONAL
WHERE timing = ?
AND   active = 1;';
  my $addtl_sth = $config{'dbh'}->prepare($sql);

  # Pre-processing
  $addtl_sth->execute('pre')
    or log_error("Unable to execute dbh->addtl: " . $config{'dbh'}->errstr);
  while (my $row = $addtl_sth->fetchrow_hashref) {
    # Determine suitability from frequency
    next if !validate_frequency($$row{'frequency'});
    # Arguments to append to the script?
    my @args = ( );
    push @args, $$row{'script_args'} if defined($$row{'script_args'}) && $$row{'script_args'} ne '';
    $$row{'script'} .= ' ' . join(' ', @args) if @args;
    # Add to the list
    push @sched, { 'comment' => $$row{'name'}, 'script' => $$row{'script'} };
  }

  # Regular checks
  $sql = 'SELECT check_id, name, frequency, script, script_args, instance, status_warn, status_critical
FROM SYSMON_CHECKS
WHERE active = 1;';
  my $sth = $config{'dbh'}->prepare($sql);
  $sth->execute
    or log_error("Unable to execute dbh->checks: " . $config{'dbh'}->errstr);
  while (my $row = $sth->fetchrow_hashref) {
    # Determine suitability from frequency
    next if !validate_frequency($$row{'frequency'});
    # Arguments to append to the script?
    my @args = ( );
    push @args, $$row{'script_args'} if defined($$row{'script_args'}) && $$row{'script_args'} ne '';
    push @args, "--instance=$$row{'instance'}" if defined($$row{'instance'}) && $$row{'instance'} ne '';
    push @args, "--warn=$$row{'status_warn'}" if defined($$row{'status_warn'}) && $$row{'status_warn'} ne '';
    push @args, "--critical=$$row{'status_critical'}" if defined($$row{'status_critical'}) && $$row{'status_critical'} ne '';
    push @args, '--notify-email' if grep(/^--notify-email$/, @ARGV);
    push @args, '--notify-twitter-dm' if grep(/^--notify-twitter-dm$/, @ARGV);
    $$row{'script'} .= '.pl';
    $$row{'script'} .= ' ' . join(' ', @args) if @args;
    # Add to the list
    push @sched, { 'comment' => $$row{'name'}, 'script' => $$row{'script'} };
  }

  # Post-processing
  $addtl_sth->execute('post')
    or log_error("Unable to execute dbh->addtl: " . $config{'dbh'}->errstr);
  while (my $row = $addtl_sth->fetchrow_hashref) {
    # Arguments to append to the script?
    my @args = ( );
    push @args, $$row{'script_args'} if defined($$row{'script_args'}) && $$row{'script_args'} ne '';
    $$row{'script'} .= ' ' . join(' ', @args) if @args;
    # Add to the list
    push @sched, { 'comment' => $$row{'name'}, 'script' => $$row{'script'} };
  }

  log_msg("Schedule loaded: " . scalar(@sched) . " returned");
  return @sched;
}

sub load_schedule_file {
  # Missing schedule?
  if (!-e $config{'schedule'}) {
    log_error("Schedule file not found in '$config{'schedule'}'", 'schedule');
  }

  # Load the file
  open FILE, "<$config{'schedule'}";
  my @schedule = <FILE>;
  close FILE;
  my $schedule = join('', @schedule);

  # Scan and check for suitable matches
  my @lt = localtime();
  my $cur_hour = $lt[2];
  my @matches = ( );
  my @full_list = ($schedule =~ /\n\#\s+([^\n]+\n(?:[0-9]+|\*) [^\n]+)/g);
  foreach my $line (@full_list) {
    my ($comment, $hour, $script) = ($line =~ m/^([^\n]+)\n([0-9]+|\*) ([^\n]+)$/);
    push @matches, { 'comment' => $comment, 'script' => $script }
      if ($hour == $cur_hour) || ($hour eq '*');
  }
  log_msg("Schedule parsed: " . scalar(@full_list) . " row(s) found; " . scalar(@matches) . " matched");

  return @matches;
}

# Determine if the current hour matches the crontab-like frequency
sub validate_frequency {
  my ($def) = @_;
  my @lt = localtime();
  my $h = $lt[2];

  foreach my $d (split(/,/, $def)) {
    # - Every hour
    return 1
      if $d eq '*';
    # - A specific hour
    return 1
      if $d eq $h;
    # - Range
    if ($d =~ /^\d+\-\d+$/) {
      my ($d_l, $d_h) = split('\s*-\s*', );
      return 1
        if ($d_l <= $h) && ($h <= $d_h);
    }
    # - Specific hourly period (* / 4)
    if ($d =~ /^\*\s*\/\s*\d+$/) {
      my ($h_div) = ($d =~ /\/\s*(\d+)$/);
      return 1
        if ($h % $h_div) == 0;
    }
    # - Unknown rule, silently ignore
  }

  # If we reached this part, no definition matched
  return 0;
}

# Return true to pacify the compiler
1;
