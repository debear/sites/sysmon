#!/usr/bin/perl -w
# DBI-related libraries and methods
use strict;
use DBI;
use Dir::Self;
our %config;

sub dbi_connect {
  my ($db_name) = @_;
  # CI lint test needs to be have a bit differently...
  if (defined($ENV{'PERL_LINT'}) && $ENV{'PERL_LINT'}) {
    return bless {}, 'FauxDBI';
  }

  my $connect_str = 'dbi:mysql:';
  $connect_str .= $db_name if defined($db_name);
  my $dbh = DBI->connect($connect_str, $config{'db_user'}, $config{'db_pass'});
  $dbh->do('SET time_zone = "Europe/London";'); # Ensure our connection is in the correct timezone
  return $dbh;
}

sub dbi_disconnect {
  # Any statement handles to close?
  if (defined($config{'sth'})) {
    foreach my $key (%{$config{'sth'}}) {
      $config{'sth'}{$key}->finish
        if defined($config{'sth'}{$key});
    }
  }
  # Then close the connection
  $config{'dbh'}->disconnect if defined($config{'dbh'});
  foreach my $k (keys %{$config{'dbh_alt'}}) {
    my $dbh = $config{'dbh_alt'}{$k};
    $dbh->disconnect if defined($dbh);
  }
}

# Common requires
dynamic_require(__DIR__ . '/../config/dbi.pl');

# Automatically connect
$config{'dbh'} = dbi_connect($config{'db_name'});
$config{'dbh_alt'} = { };

# Return true to pacify the compiler
1;
