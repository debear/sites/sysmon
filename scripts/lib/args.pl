#!/usr/bin/perl -w
# Command line argument parsing
use strict;
our %config;

sub parse_args {
  $config{'args-orig'} = join(' ', @ARGV);
  # Run through the command-line args
  my @unknown = ( );
  for (my $i = 0; $i < @ARGV; $i++) {
    my $v = $ARGV[$i];

    # Are we expecting this argument?
    my $known_arg = 0;
    if (substr($v, 0, 9) eq '--notify-') {
      # Notification settings will be dealt with separately
      $known_arg = 1;
    } elsif (substr($v, 0, 2) eq '--') {
      # Convert --X in to X for our check
      my $v_strip = substr($v, 2) if substr($v, 0, 2) eq '--';
      # If we have a value part in $v (e.g., a=b), correct
      my $extra;
      if (index($v_strip, '=') != -1) {
        my ($lhs, $rhs) = ($v_strip =~ m/^([^=]+)=(.*?)$/gsi);
        $v_strip = $lhs;
        $extra = $rhs;
      }

      # Check
      if (defined($config{'args'}{$v_strip})) {
        splice(@ARGV, $i+1, 0, $extra) if defined($extra);
        $known_arg = 1;
        # Do we take the next argument as the value, or treat like a boolean flag?
        if ($config{'args'}{$v_strip}) {
          $config{$v_strip} = $ARGV[++$i];
        } else {
          $config{$v_strip} = 1;
        }
      }
    }
    push @unknown, $v
      if !$known_arg;
  }

  # Any unknown args?
  log_error(sprintf "Unknown argument%s: '%s'",
              @unknown > 1 ? 's' : '',
              join('\', \'', @unknown),
            'args')
    if @unknown;

  # Required argument check... if we're not linting!
  if (defined($config{'args_required'})) {
    my @missing = ( );
    foreach my $arg (@{$config{'args_required'}}) {
      push @missing, $arg
        if !defined($config{$arg});
    }

    log_error(sprintf "Required argument%s missing: '--%s'",
                @missing > 1 ? 's' : '',
                join('\', \'--', @missing),
              'args')
      if @missing;
  }
}

# Return true to pacify the compiler
1;
