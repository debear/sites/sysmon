#!/usr/bin/perl -w
use strict;

#
# BEGIN: Update @INC on the Xilo server
#
BEGIN {
  use File::Basename;
  use Cwd 'abs_path';

  # Supress warnings from STDERR
  $SIG{'__WARN__'} = sub { };

  # Mask the filename as we're on a shared server
  our $script_raw = $0;
  our $script = basename($0, '.pl');
  $0 = basename $0;

  # Some path customisation needs to be made if running on the prod server
  our $home_dir;
  our $xilo_home = '/var/www/f6a0812f-78d0-45a6-bec0-c060de245116';
  my $xilo_custom_path = 'debear/sites/common/perl5'; # Relative to ~
  our $base_dir = abs_path(dirname(__FILE__) . '/..');
  if ($base_dir =~ /^$xilo_home/) {
    # Okay, now link to the other perl modules
    unshift(@INC, $xilo_home.'/'.$xilo_custom_path);
    $home_dir = $xilo_home;
  } else {
    $home_dir = '/var/www';
  }
}

#
# END: Tie-up database connections and logs
#
END {
  dbi_disconnect() if defined(&dbi_disconnect);
  save_logs();
}

#
# Skip if linting, as we're called within a BEGIN that is triggered...
#
return 1
  if (defined($ENV{'PERL_LINT'}) && $ENV{'PERL_LINT'});

#
# Start of the script
#
use Dir::Self;

# Load config
our %config; our $base_dir; our $home_dir;
my $config = $base_dir . '/config/core.pl';
require $config;

# Common requires
dynamic_require(__DIR__ . '/debug.pl');
dynamic_require(__DIR__ . '/dbi.pl');

# Say we're going...
log_msg('===============');
log_msg('Run starting...');

# Additional requires for worker scripts
dynamic_require(__DIR__ . '/worker.pl') if $config{'is_worker'};

# Return true to pacify the compiler
1;
