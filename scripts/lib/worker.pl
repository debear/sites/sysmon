#!/usr/bin/perl -w
use strict;
use Dir::Self;

# Load additional arguments
dynamic_require(__DIR__ . '/args.pl');
dynamic_require(__DIR__ . '/actions.pl');

# Parse arguments
parse_args();

# Determine check info
determine_check_details();

# Return true to pacify the compiler
1;
