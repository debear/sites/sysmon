#!/usr/bin/perl -w
# Check and report on individual HTTP requests
# TD. 2019-12-16.

use strict;
our %config;

BEGIN {
  use File::Basename;
  my $core = dirname(__FILE__) . '/lib/core.pl';
  require $core;
}

use Compress::Zlib;
use LWP::UserAgent;
use HTTP::Request;
use Time::HiRes qw(gettimeofday);
use HTTP::CookieJar::LWP;

## Crtical/Warn durations are passed in in milliseconds, so convert to seconds
$config{'critical'} /= 1000;
$config{'warn'} /= 1000;

##
## Prepare The Checks
##
my $url_subdomain = ($config{'instance'} eq 'www' ? '' : "$config{'instance'}."); # www is stripped
my $url_base = "https://$url_subdomain$config{'domain'}";
log_msg("Base domain: $url_base");
my @checks = split(',', $config{'path'});

# Load the cookies from previous checks
my $jar = HTTP::CookieJar::LWP->new;
if (-e $config{'cookies'}) {
  log_msg("Loading cookies from $config{'cookies'}");
  my $buffer;
  my $cookies = '';
  my $gzh = gzopen($config{'cookies'}, 'rb');
  $cookies .= $buffer while $gzh->gzread($buffer) > 0;
  $gzh->gzclose();
  my @cookies = split("\n", $cookies);
  $jar = $jar->load_cookies(@cookies);
} elsif (!-e dirname($config{'cookies'})) {
  mkdir dirname($config{'cookies'});
}

# Configure the User Agent object, with any previously loaded cookied
my $ua = LWP::UserAgent->new( cookie_jar => $jar );
$ua->timeout(10);
$ua->agent('HTTP Request Checker/1.0');
$ua->max_redirect(0); # Return redirects as redirects

##
## Prepare our per-path query
##
my $sql = 'INSERT INTO SYSMON_HTTP_REQUEST (subdomain, path, checked, checked_date, checked_hour, status_expected, status_returned, content_length, duration, state, status, notified)
  VALUES (?, ?, ?, DATE_FORMAT(?, "%y%j"), HOUR(?), ?, ?, ?, ?, ?, ?, "no")
ON DUPLICATE KEY UPDATE status_returned = VALUES(status_returned),
                        content_length = VALUES(content_length),
                        duration = VALUES(duration),
                        state = VALUES(state),
                        status = VALUES(status),
                        notified = VALUES(notified);';
$config{'sth'}{'path_sth'} = $config{'dbh'}->prepare($sql);

$sql = 'SELECT status
FROM SYSMON_HTTP_REQUEST
WHERE subdomain = ?
AND   path = ?
ORDER BY checked DESC
LIMIT 1;';
$config{'sth'}{'last_state_sth'} = $config{'dbh'}->prepare($sql);

##
## Run per-path
##
my $info = ''; my %totals = ( 'critical' => 0, 'warn' => 0, 'okay' => 0 );
my %counts = ( 'expected' => 0, 'mismatch' => 0, 'slow' => 0 );
foreach my $path (@checks) {
  # By default expect a 200, but this can be customisable
  my $expected = 200;
  ($path, $expected) = split('!', $path)
    if $path =~ m/!/;
  my $url = "$url_base$path";
  log_msg("Test: $path");

  # Build the request
  my $request = HTTP::Request->new(GET => $url);
  $request->header('Accept-Encoding' => 'gzip');

  # What does it return (HTTP code-wise) and in how long?
  my $start = gettimeofday();
  my $response = $ua->request($request);
  my $stop = gettimeofday();
  # Process the returned response
  my $run_time = $stop - $start; my $run_time_disp = sprintf('%0.03fs', $run_time);
  my $content_length = length($response->content);
  log_msg(" - Expecting: $expected; Returned: " . $response->code . "; Length: $content_length; Time: $run_time_disp");
  # What does this mean status-wise?
  my $status = 'okay'; my $status_info = $status; my $state = 'expected';
  if ($response->code != $expected) {
    $status = 'critical';
    $state = $status_info = 'mismatch';
  } elsif ($run_time >= $config{'critical'}) {
    $status = 'critical';
    $state = 'slow';
    $status_info = "$state ($status)";
  } elsif ($run_time >= $config{'warn'}) {
    $status = 'warn';
    $state = 'slow';
    $status_info = "$state ($status)";
  }
  $counts{$state}++;
  # Let's check for a 'recovery' status, if we're (now) okay
  $status = determine_http_recovered($path)
    if $status eq 'okay';
  $totals{$status}++;
  log_msg(" - Resulting status: $status");
  # Store the result
  $config{'sth'}{'path_sth'}->execute($config{'instance'}, $path, $config{'started_gmt'}, $config{'started_gmt'}, $config{'started_gmt'}, $expected, $response->code, $content_length, $run_time, $state, $status)
    or log_error("Unable to execute sth->path_sth: " . $config{'dbh'}->errstr);
  # Prepare our other return objects
  $info .= "$path: $status_info\n";
}

# Now we're done, write back all the cookies
log_msg("Writing cookies back to disk");
open FILE, "| gzip >$config{'cookies'}";
binmode FILE, ":encoding(utf8)";
print FILE ( join "\n", $jar->dump_cookies );
close FILE;

##
## Determine global status
##
my $status = 'okay';
if ($totals{'critical'}) {
  $status = 'critical';
} elsif ($totals{'warn'}) {
  $status = 'warn';
}
my $summary_field = $totals{'critical'} + $totals{'warn'};
$info =~ s/\n$//; # Trim trailing newline

# Let's check for a 'recovery' status, if we're (now) okay
$status = determine_status_recovered()
  if $status eq 'okay';

# Build our summary one-liner
my @summary_msg = ();
push @summary_msg, sprintf('%d check%s mis-matched', $counts{'mismatch'}, $counts{'mismatch'} > 1 ? 's' : '')
  if $counts{'mismatch'};
push @summary_msg, sprintf('%d check%s slow', $counts{'slow'}, $counts{'slow'} > 1 ? 's' : '')
  if $counts{'slow'};
my $summary_msg = (@summary_msg
  ? join(', ', @summary_msg)
  : sprintf('%d check%s as expected', $counts{'expected'}, $counts{'expected'} > 1 ? 's' : ''));

# Notify & Log
my $notified = check_send_notif($info, $summary_msg, $status);
log_run($status, $summary_field, $notified, $info);

# Backreference the individual checks to set notification of failed checks
if ($notified ne 'no') {
  $sql = 'UPDATE SYSMON_HTTP_REQUEST
SET notified = ?
WHERE subdomain = ?
AND   checked_date = DATE_FORMAT(?, "%y%j")
AND   checked_hour = HOUR(?)
AND   status = "critical";';
  $config{'sth'}{'http_notif'} = $config{'dbh'}->prepare($sql);
  $config{'sth'}{'http_notif'}->execute($notified, $config{'instance'}, $config{'started_gmt'}, $config{'started_gmt'})
    or log_error("Unable to execute sth->http_notif: " . $config{'dbh'}->errstr);
}

##
## Internal Methods
##

# Determine file system usage for a particular directory
sub determine_http_recovered {
  my ($path) = @_;
  # Get the state of the last check for this path
  $config{'sth'}{'last_state_sth'}->execute($config{'instance'}, $path)
    or log_error("Unable to execute sth->last_state_sth: " . $config{'dbh'}->errstr);
  my $last = $config{'sth'}{'last_state_sth'}->fetchrow_hashref;
  # Process to determine an appropriate state for 'okay'
  my $ret = (!defined($$last{'status'}) || $$last{'status'} eq 'okay' || $$last{'status'} eq 'recovered') ? 'okay' : 'recovered';
  log_msg(" - Returning '$ret' status based on a last status of '$$last{'status'}'");
  return $ret;
}
