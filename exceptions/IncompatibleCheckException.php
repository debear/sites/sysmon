<?php

namespace DeBear\Exceptions\SysMon;

use DeBear\Exceptions\BaseException;

class IncompatibleCheckException extends BaseException
{
}
