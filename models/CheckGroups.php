<?php

namespace DeBear\Models\SysMon;

use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Implementations\Model;

class CheckGroups extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_CHECKS_GROUPS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'group_id';
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Relationship: 1:M checks within this group
     * @return HasMany
     */
    public function checks(): HasMany
    {
        return $this->hasMany(Checks::class, 'group_id', 'group_id')->where('active', 1)->orderBy('name');
    }
}
