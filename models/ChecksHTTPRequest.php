<?php

namespace DeBear\Models\SysMon;

use DeBear\Implementations\Model;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Format;
use DeBear\Helpers\Highcharts;

class ChecksHTTPRequest extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_HTTP_REQUEST';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'subdomain',
        'path',
        'checked_date',
        'checked_hour',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'checked' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Get the list of columns to use in the detailed history table
     * @return array The col/label pairs of appropriate detail table columns for this check
     */
    public static function getColumns(): array
    {
        return [
            'checked' => 'Date',
            'path' => 'Path',
            'status_expected' => 'Expected',
            'status_returned' => 'Returned',
            'content_length' => 'Length',
            'duration' => 'Duration',
            'state' => 'State',
        ];
    }

    /**
     * Build the Highcharts JSON object
     * @param Checks $check  The base check object we are running against.
     * @param string $dom_id ID of the DOM element the chart is to be added to.
     * @return array Array of JSON info for the Highcharts module
     */
    public static function buildHighchartsObject(Checks $check, string $dom_id): array
    {
        // Build the base object.
        $chart = Highcharts::new($dom_id);
        // Get and parse the data (using native functions appears to be more efficient than Collection equivalents).
        $start_point = $check->details()->map(function ($detail) {
            return $detail->checked->toDateTimeString();
        })->toArray();
        $start_point = array_reverse(array_unique($start_point));
        $start_point = array_slice($start_point, 0, FrameworkConfig::get('debear.setup.chart-points'));
        $start_point = array_pop($start_point);
        // For some reason, Phan can determine the ->where($key, $operator, $value) signature.
        /* @phan-suppress-next-line PhanUndeclaredFunctionInCallable */
        $data = $check->details()->where('checked', '>=', $start_point)->reverse();
        $parsed = [];
        foreach ($data as $row) {
            $date = $row->formatCol('checked');
            if (!isset($parsed[$date]) || !is_array($parsed[$date])) {
                $parsed[$date] = ['expected' => 0, 'slow' => 0, 'mismatch' => 0];
            }
            $parsed[$date][$row->state]++;
        }
        // Customise - type.
        $chart['chart']['type'] = 'column';
        // Customise - series.
        $chart['series'] = [];
        $chart['series'][0] = ['name' => 'Expected', 'data' => array_column($parsed, 'expected')];
        $chart['series'][1] = ['name' => 'Slow', 'data' => array_column($parsed, 'slow')];
        $chart['series'][2] = ['name' => 'Mismatch', 'data' => array_column($parsed, 'mismatch')];
        // Customise - xAxis.
        $chart['xAxis'] = ['categories' => array_keys($parsed)];
        // Customise - yAxis.
        $chart['yAxis'] = [
            'title' => ['text' => 'State'],
            'labels' => ['enabled' => false],
            'min' => 0,
        ];
        // Customise - plotOptions.
        $chart['plotOptions'] = [
            'column' => ['stacking' => 'number'],
            'series' => [
                'pointPadding' => 0,
                'groupPadding' => 0,
                'borderWidth' => 0,
            ],
        ];
        // Customise - tooltips.
        $chart['tooltip'] = [
            'shared' => true,
            'useHTML' => true,
            'formatter' => 'function() { var ret = \'<strong>\' + this.category + \'</strong>:\'; '
                . 'for (var i = 0; i < this.points.length; i++) if (this.points[i].y) { '
                . 'ret += \'<br/>\' + this.points[i].series.name; } return \'<tooltip>\' + ret + \'</tooltip>\'; }',
        ];
        return $chart;
    }

    /**
     * Build the additional Highcharts JSON objects
     * @param Checks $check The base check object we are running against.d to.
     * @return array An array of zero, one or more JSON info objects for the Highcharts module
     */
    public static function buildHighchartsExtraObjects(Checks $check): array
    {
        // By status as a pie chart.
        $status = Highcharts::new('faux'); // Appropriate $dom_id will be interpolated later.
        $status['subnav-code'] = 'status';
        $status['subnav-name'] = 'By Status';
        // Get the data.
        $data = $check->details()->forPage(1, FrameworkConfig::get('debear.setup.chart-points'));
        $by_state = $data->pluck('state')->countBy()->toArray();
        // Customise - type.
        $status['chart']['type'] = 'pie';
        // Customise - series.
        $status['series'] = [];
        $status['series'][0] = ['name' => $status['subnav-name'], 'colorByPoint' => true, 'data' => []];
        // Expected.
        $status['series'][0]['data'][0] = [
            'name' => 'Expected',
            'y' => $by_state['expected'] ?? 0,
        ];
        // Slow.
        $status['series'][0]['data'][1] = [
            'name' => 'Slow',
            'y' => $by_state['slow'] ?? 0,
        ];
        // Mismatch.
        $status['series'][0]['data'][2] = [
            'name' => 'Mismatch',
            'y' => $by_state['mismatch'] ?? 0,
        ];
        // Customise - tooltips.
        $status['tooltip'] = [
            'shared' => false,
            'useHTML' => true,
            'formatter' => 'function() { return \'<tooltip><strong>\' + this.key + \'</strong>: \''
                . ' + this.percentage.toFixed(2) + \'%</tooltip>\'; }',
        ];
        return [$status];
    }

    /**
     * Return a formatted script representing a particular column
     * @param string $col The detail column to format.
     * @return string The formatted column string
     */
    public function formatCol(string $col): string
    {
        switch ($col) {
            case 'checked':
                return $this->$col->format('jS M Y, H:i');

            case 'path':
            case 'status_expected':
            case 'status_returned':
                return "<code>{$this->$col}</code>";

            case 'content_length':
                return Format::bytes($this->$col);

            case 'duration':
                return sprintf('%.03fs', $this->$col);
        }
        // State.
        return '<span class="icon icon_http_' . $this->$col . '">' . ucfirst($this->$col) . '</span>';
    }
}
