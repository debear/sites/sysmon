<?php

namespace DeBear\Models\SysMon;

use Illuminate\Database\Eloquent\Relations\HasOne;
use DeBear\Implementations\Model;

class ChecksLogfileBreakdown extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_LOGFILE_BREAKDOWN';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'app',
        'checked',
        'section',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'checked' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Relationship: 1:1 core logfile instance
     * @return HasOne
     */
    public function logfile(): HasOne
    {
        return $this->hasOne(ChecksLogfile::class, ['app', 'checked'], ['app', 'checked']);
    }

    /**
     * Return a formatted script representing a particular column
     * @param string $col The detail column to format.
     * @return string The formatted column string
     */
    public function formatCol(string $col): string
    {
        switch ($col) {
            case 'checked':
                $fmt = $this->$col->format('jS M Y');
                break;

            case 'errors':
                $fmt = (int)$this->$col;
                break;
        }
        return $fmt ?? 'Unknown';
    }
}
