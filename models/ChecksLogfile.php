<?php

namespace DeBear\Models\SysMon;

use DeBear\Implementations\Model;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Highcharts;

class ChecksLogfile extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_LOGFILE';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'app',
        'checked',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'checked' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Get the list of columns to use in the detailed history table
     * @return array The col/label pairs of appropriate detail table columns for this check
     */
    public static function getColumns(): array
    {
        return [
            'checked' => 'Date',
            'errors' => 'Errors',
            'detail' => 'Detail',
        ];
    }

    /**
     * Build the Highcharts JSON object
     * @param Checks $check  The base check object we are running against.
     * @param string $dom_id ID of the DOM element the chart is to be added to.
     * @return array Array of JSON info for the Highcharts module
     */
    public static function buildHighchartsObject(Checks $check, string $dom_id): array
    {
        // Build the base object.
        $chart = Highcharts::new($dom_id);
        // Get the data.
        $data = $check->details()->forPage(1, FrameworkConfig::get('debear.setup.chart-points'))->reverse();
        $minRow = $data->first()->checked;
        $logs = $check->logs->where('log_date', '>=', $minRow->toDateTimeString());
        $breakdowns = $check->detailsBreakdown($minRow)->reverse();
        // Customse - type.
        $chart['chart']['type'] = 'column';
        // Customise - series.
        $sections = array_unique($breakdowns->pluck('section')->toArray());
        static::sortLogsSection($check->instance, $sections);
        $dates = $logs->pluck('log_date')->toArray();
        $datesDisp = [];
        foreach ($logs as $row) {
            $datesDisp[] = $row->logDateFormat();
        }
        // Base.
        $series = [];
        foreach ($sections as $s) {
            $name = static::formatBreakdownSection($check->instance, $s);
            $series[$s] = ['name' => $name, 'data' => array_fill_keys($dates, null)];
        }
        // Add values and calc daily max.
        $daily_totals = array_fill_keys($dates, 0);
        foreach ($breakdowns as $row) {
            $checked = $row->checked->toDateTimeString();
            $series[$row->section]['data'][$checked] = $row->errors;
            $daily_totals[$checked] += $row->errors;
        }
        // Ensure we have a series to display.
        if (!sizeof($series)) {
            $series[] = ['name' => $check->name, 'data' => array_fill_keys($dates, null)];
        }
        // Convert associative arrays into indexed.
        $chart['series'] = array_values($series);
        foreach ($chart['series'] as &$s) {
            $s['data'] = array_values($s['data']);
        }
        // Customise - xAxis.
        $chart['xAxis'] = ['categories' => $datesDisp];
        // Customise - yAxis.
        $chart['yAxis'] = [
            'title' => ['text' => 'Errors'],
            'labels' => ['enabled' => true],
            'allowDecimals' => false,
            'min' => 0,
        ];
        // Customise - plotOptions.
        $chart['plotOptions'] = [
            'column' => ['stacking' => 'normal'],
            'series' => [
                'pointPadding' => 0,
                'groupPadding' => 0,
            ],
        ];
        // Customise - tooltips.
        $chart['tooltip'] = [
            'shared' => true,
            'useHTML' => true,
            'formatter' => 'function() { var str = \'\'; var total = 0; for (var i = 0; i < this.points.length; i++) '
                . '{ str += \'<br/><em>\' + this.points[i].series.name + \'</em>: \' + this.points[i].y; '
                . 'total += this.points[i].y; } return \'<tooltip><strong>\' + this.category + \'</strong>:\' + '
                . '(this.points.length > 1 ? \'<br/><em>Total:</em> \' + total : \'\') + str + \'</tooltip>\'; }',
        ];
        return $chart;
    }

    /**
     * Build the additional Highcharts JSON objects
     * @param Checks $check The base check object we are running against.d to.
     * @return array An array of zero, one or more JSON info objects for the Highcharts module
     */
    public static function buildHighchartsExtraObjects(Checks $check): array
    {
        // By type as a pie chart.
        $type = Highcharts::new('faux'); // Appropriate $dom_id will be interpolated later.
        $type['subnav-code'] = 'type';
        $type['subnav-name'] = 'By Type';
        // Get the data.
        $data = $check->details()->forPage(1, FrameworkConfig::get('debear.setup.chart-points'))->reverse();
        $minRow = $data->first()->checked;
        $breakdowns = $check->detailsBreakdown($minRow);
        // Customise - type.
        $type['chart']['type'] = 'pie';
        // Customise - series.
        $sections = array_unique($breakdowns->pluck('section')->toArray());
        static::sortLogsSection($check->instance, $sections);
        $type['series'] = [['name' => $type['subnav-name'], 'colorByPoint' => true, 'data' => []]];
        foreach ($sections as $s) {
            $type['series'][0]['data'][] = [
                'name' => static::formatBreakdownSection($check->instance, $s),
                /* @phan-suppress-next-line PhanUndeclaredFunctionInCallable */
                'y' => $breakdowns->where('section', $s)->sum('errors'),
            ];
        }
        // Ensure we have a series to display.
        if (!sizeof($type['series'][0]['data'])) {
            $type['series'][0]['data'][] = [
                'name' => $check->name,
                'y' => 0,
            ];
        }
        // Customise - tooltips.
        $type['tooltip'] = [
            'shared' => false,
            'useHTML' => true,
            'formatter' => 'function() { return \'<tooltip><strong>\' + this.key + \'</strong>: \''
                . ' + this.y + \' (\' + this.percentage.toFixed(2) + \'%)</tooltip>\'; }',
        ];
        return [$type];
    }

    /**
     * Format the breakdown section title
     * @param string $instance The check instance we are processing.
     * @param string $title    The title to be formatted.
     * @return string The formatted title
     */
    protected static function formatBreakdownSection(string $instance, string $title): string
    {
        switch ($instance) {
            case 'news':
            case 'weather':
                if (strpos($title, '_') === false) {
                    return $title;
                }
                list($a, $b) = explode('_', $title);
                return ucfirst($a) . ' ' . strtoupper($b);

            case 'browser':
                return strtoupper($title);

            case 'int_status':
                return ucfirst($title);

            case 'suspicious_activity':
                return ucwords(str_replace('_', ' ', $title));
        }
        return $title;
    }

    /**
     * Sort the list of sections we have retrieved from the database
     * @param string $instance Instance of the section list being modified.
     * @param array  $sections List of sections to sort. (Pass-by-reference).
     * @return void
     */
    protected static function sortLogsSection(string $instance, array &$sections): void
    {
        if ($instance == 'int_status') {
            $sortOrder = [
                'okay' => 0,
                'warn' => 1,
                'critical' => 2,
                'unknown' => 3,
            ];
            usort($sections, function ($a, $b) use ($sortOrder) {
                return $sortOrder[$a] <=> $sortOrder[$b];
            });
        } else {
            sort($sections);
        }
    }


    /**
     * Return a formatted script representing a particular column
     * @param string $col The detail column to format.
     * @return string The formatted column string
     */
    public function formatCol(string $col): string
    {
        switch ($col) {
            case 'checked':
                $fmt = 'jS M Y';
                if ($this->app == 'int_status') {
                    $fmt .= ' H:i:s';
                }
                return $this->$col->format($fmt);

            case 'errors':
                return (string)intval($this->$col);
        }
        // Split in to 1st Line and the Rest.
        $ret = trim($this->$col);
        $by_lines = array_filter(explode("\n", $ret));
        if (sizeof($by_lines) > 1) {
            $ret = '<div class="detail detail-open icon_right_info">'
                . '<span class="head">' . $by_lines[0] . '</span>'
                . '<div class="body box status">'
                . '<span class="close">[ <a class="detail-close">Close</a> ]</span>'
                . '<strong class="date">' . $this->formatCol('checked') . '</strong>'
                . "<pre>$ret</pre>"
                . '</div></div>';
        }
        return $ret;
    }
}
