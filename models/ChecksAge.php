<?php

namespace DeBear\Models\SysMon;

use DeBear\Implementations\Model;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Format;
use DeBear\Helpers\Highcharts;

class ChecksAge extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_AGE';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'ref',
        'checked',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'checked' => 'datetime:Y-m-d',
        'ref_date' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Get the list of columns to use in the detailed history table
     * @return array The col/label pairs of appropriate detail table columns for this check
     */
    public static function getColumns(): array
    {
        return [
            'checked' => 'Checked',
            'ref_date' => 'File Date',
            'age' => 'Age',
        ];
    }

    /**
     * Build the Highcharts JSON object
     * @param Checks $check  The base check object we are running against.
     * @param string $dom_id ID of the DOM element the chart is to be added to.
     * @return array Array of JSON info for the Highcharts module
     */
    public static function buildHighchartsObject(Checks $check, string $dom_id): array
    {
        // Build the base object.
        $chart = Highcharts::new($dom_id);
        // Get the data.
        $data = $check->details()->forPage(1, FrameworkConfig::get('debear.setup.chart-points'))->reverse();
        // Customise - type.
        $chart['chart']['type'] = 'area';
        // Customise - series.
        $chart['series'] = [
            ['name' => 'Age', 'data' => []],
        ];
        foreach ($data as $row) {
            $chart['series'][0]['data'][] = $row->age;
        }
        // Customise - xAxis.
        $chart['xAxis'] = [
            'categories' => [],
        ];
        foreach ($data as $row) {
            $chart['xAxis']['categories'][] = $row->formatCol('checked');
        }
        // Customise - yAxis.
        $chart['yAxis'] = [
            'title' => ['text' => 'Age'],
            'labels' => ['formatter' => 'function() { return Math.floor(this.value / 86400) + "d"; }'],
        ];
        // Code coverage workaround.
        $chart['yAxis']['min'] = 0;
        $chart['yAxis']['max'] = ceil(max($chart['series'][0]['data']) / 5) * 5;
        // Customise - tooltips.
        $chart['tooltip'] = [
            'shared' => true,
            'useHTML' => true,
            'formatter' => 'function() { return \'<tooltip><strong>\' + this.category + \'</strong>:<br/>\' + '
                . 'Numbers.toAge(this.y) + \'</tooltip>\'; }',
        ];
        return $chart;
    }

    /**
     * Build the additional Highcharts JSON objects
     * @param Checks $check The base check object we are running against.d to.
     * @return array An array of zero, one or more JSON info objects for the Highcharts module
     */
    public static function buildHighchartsExtraObjects(Checks $check): array
    {
        return [];
    }

    /**
     * Return a formatted script representing a particular column
     * @param string $col The detail column to format.
     * @return string The formatted column string
     */
    public function formatCol(string $col): string
    {
        if ($col == 'checked') {
            return $this->$col->format('jS M Y');
        } elseif ($col == 'ref_date') {
            return $this->$col->format('jS M Y H:i:s');
        }
        return Format::age($this->$col);
    }
}
