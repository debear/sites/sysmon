<?php

namespace DeBear\Models\SysMon;

use DeBear\Implementations\Model;
use DeBear\Helpers\Highcharts;

class ChecksHTTPStatus extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_HTTP_STATUS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'subdomain',
        'date',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'date' => 'datetime:Y-m-d',
        'first_checked' => 'datetime',
        'last_checked' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Get the list of columns to use in the detailed history table
     * @return array The col/label pairs of appropriate detail table columns for this check
     */
    public static function getColumns(): array
    {
        return [
            'date' => 'Date',
            'tot_requests' => 'Requests',
            'tot_1xx' => '1xx',
            'tot_2xx' => '2xx',
            'num_200' => '200',
            'num_201' => '201',
            'num_204' => '204',
            'tot_3xx' => '3xx',
            'num_301' => '301',
            'num_302' => '302',
            'num_304' => '304',
            'num_307' => '307',
            'num_308' => '308',
            'tot_4xx' => '4xx',
            'num_400' => '400',
            'num_401' => '401',
            'num_403' => '403',
            'num_404' => '404',
            'num_405' => '405',
            'tot_5xx' => '5xx',
            'num_500' => '500',
            'num_501' => '501',
            'tot_other' => 'Other',
        ];
    }

    /**
     * Build the Highcharts JSON object
     * @param Checks $check  The base check object we are running against.
     * @param string $dom_id ID of the DOM element the chart is to be added to.
     * @return array Array of JSON info for the Highcharts module
     */
    public static function buildHighchartsObject(Checks $check, string $dom_id): array
    {
        // Build the base object.
        $chart = Highcharts::new($dom_id);
        // Get the data over the last 14 days.
        $data = $check->details()->forPage(1, 14);
        // Customise - type.
        $chart['chart']['type'] = 'pie';
        // Customise - series.
        $chart['series'] = [
            [
                'name' => 'Status Code',
                'colorByPoint' => true,
                'data' => [
                    // Default, a failsafe.
                    [
                        'name' => 'Requests',
                        'y' => 0,
                    ],
                ],
            ],
        ];
        // Determine the columns we'll display.
        $groups = array_filter(array_keys($data->random()->toArray()), function ($key) {
            return substr((string)$key, 0, 4) == 'num_';
        });
        $groups[] = 'tot_other';
        $inc_failsafe = true;
        foreach ($groups as $col) {
            $sum = $data->sum($col);
            if ($sum) {
                // Remove the failsafe.
                if ($inc_failsafe) {
                    $chart['series'][0]['data'] = [];
                    $inc_failsafe = false;
                }
                // Add the item.
                $chart['series'][0]['data'][] = [
                    'name' => ucfirst(substr($col, 4)),
                    'y' => $sum,
                ];
            }
        }
        // Customise - tooltips.
        $chart['tooltip'] = [
            'shared' => false,
            'useHTML' => true,
            'formatter' => 'function() { return \'<tooltip><strong>\' + this.key + \'</strong>: \''
                . ' + this.percentage.toFixed(2) + \'% (\' + this.y + \')</tooltip>\'; }',
        ];
        return $chart;
    }

    /**
     * Build the additional Highcharts JSON objects
     * @param Checks $check The base check object we are running against.d to.
     * @return array An array of zero, one or more JSON info objects for the Highcharts module
     */
    public static function buildHighchartsExtraObjects(Checks $check): array
    {
        // By status as a pie chart.
        $status = Highcharts::new('faux'); // Appropriate $dom_id will be interpolated later.
        $status['subnav-code'] = 'group';
        $status['subnav-name'] = 'By Group';
        // Get the data over the last 14 days.
        $data = $check->details()->forPage(1, 14);
        // Customise - type.
        $status['chart']['type'] = 'pie';
        // Customise - series.
        $status['series'] = [
            [
                'name' => 'Status Group',
                'colorByPoint' => true,
                'data' => [
                    // Default, a failsafe.
                    [
                        'name' => 'Requests',
                        'y' => 0,
                    ],
                ],
            ],
        ];
        $groups = ['tot_1xx', 'tot_2xx', 'tot_3xx', 'tot_4xx', 'tot_5xx', 'tot_other'];
        $inc_failsafe = true;
        foreach ($groups as $col) {
            $sum = $data->sum($col);
            if ($sum > 0) {
                // Remove the failsafe.
                if ($inc_failsafe) {
                    $status['series'][0]['data'] = [];
                    $inc_failsafe = false;
                }
                // Add the item.
                $status['series'][0]['data'][] = [
                    'name' => ucfirst(substr($col, 4)),
                    'y' => $sum,
                ];
            }
        }
        // Customise - tooltips.
        $status['tooltip'] = [
            'shared' => false,
            'useHTML' => true,
            'formatter' => 'function() { return \'<tooltip><strong>\' + this.key + \'</strong>: \''
                . ' + this.percentage.toFixed(2) + \'% (\' + this.y + \')</tooltip>\'; }',
        ];
        return [$status];
    }

    /**
     * Return a formatted script representing a particular column
     * @param string $col The detail column to format.
     * @return string The formatted column string
     */
    public function formatCol(string $col): string
    {
        // Date.
        if ($col == 'date') {
            return $this->$col->format('jS M Y');
        }
        // Counts.
        return $this->$col;
    }
}
