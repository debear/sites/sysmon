<?php

namespace DeBear\Models\SysMon;

use Illuminate\Database\Eloquent\Relations\HasOne;
use DeBear\Implementations\Model;

class IncidentsLedger extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_INCIDENTS_LEDGER';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'group_id',
        'date_ref',
        'incident_id',
        'ledger_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'status_changed' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Relationship: 1:1 original incident
     * @return HasOne
     */
    public function incident(): HasOne
    {
        return $this->hasOne(
            Incidents::class,
            ['group_id', 'date_ref', 'incident_id'],
            ['group_id', 'date_ref', 'incident_id']
        );
    }
    /**
     * Relationship: 1:1 status item
     * @return HasOne
     */
    public function statusItem(): HasOne
    {
        return $this->hasOne(StatusItem::class, 'status_id', 'status_id');
    }

    /**
     * Format the note to explain the ledger
     * @return string The ledger explanation
     */
    public function getDetailAttribute(): string
    {
        if ($this->status_from == 'okay') {
            // Incident raised.
            $status_disp = ($this->status_to == 'warn' ? 'Warning' : 'Critical');
            return "Incident raised with severity <em>$status_disp</em>.";
        } elseif ($this->status_to == 'recovered') {
            // Incident resolved.
            return 'Incident resolved.';
        } elseif ($this->status_from == 'warn') {
            // Upgraded from Warn to Critical.
            return 'Severity upgraded from <em>Warning</em> to <em>Critical</em>.';
        } elseif ($this->status_from == 'critical') {
            // Downgraded from Critical to Warn.
            return 'Severity downgraded from <em>Critical</em> to <em>Warning</em>.';
        }
        // Unknown.
        return 'Unknown reason.';
    }
}
