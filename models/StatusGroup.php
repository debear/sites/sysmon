<?php

namespace DeBear\Models\SysMon;

use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Implementations\Model;
use DeBear\Helpers\Policies;

class StatusGroup extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_STATUS_GROUPS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'group_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'last_outage' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Relationship: 1:M status item metrics
     * @return HasMany
     */
    public function metrics(): HasMany
    {
        return $this->hasMany(StatusGroupMetrics::class, 'group_id', 'group_id');
    }

    /**
     * Get the current metrics for this item
     * @return StatusGroupMetrics The current metrics
     */
    public function currentMetrics(): StatusGroupMetrics
    {
        return $this->metrics()->where('is_current', 1)->first();
    }

    /**
     * Determine which status field applies to our user
     * @return string The status, either user or admin, depending on policies
     */
    public function getStatusAttribute(): string
    {
        return Policies::match('user:admin') ? $this->status_admin : $this->status_user;
    }

    /**
     * Convert the group status to its check equivalent
     * @return string The appropriate CSS status
     */
    public function getStatusBoxAttribute(): string
    {
        if ($this->status == 'operational') {
            return 'okay';
        } elseif ($this->status == 'issues') {
            return 'warn';
        }
        // Status == 'outage'.
        return 'critical';
    }

    /**
     * Convert the group's status to an icon
     * @return string The appropriate status icon
     */
    public function getIconAttribute(): string
    {
        return "status_{$this->status_box}";
    }

    /**
     * Summarise the group
     * @return string The status summarised
     */
    public function getSummaryAttribute(): string
    {
        if ($this->status == 'operational') {
            return 'All components are fully operational.';
        } elseif ($this->status == 'issues') {
            return 'There are currently some issues.';
        }
        // Status == 'outage'.
        return 'There is currently an outage.';
    }

    /**
     * The group is fully operational
     * @return boolean Current status is 'operational'
     */
    public function isOkay(): bool
    {
        return ($this->status == 'operational');
    }

    /**
     * The group has minor issues
     * @return boolean Current status is 'issues'
     */
    public function isWarn(): bool
    {
        return ($this->status == 'issues');
    }

    /**
     * The group has an outage
     * @return boolean Current status is 'outage'
     */
    public function isCritical(): bool
    {
        return ($this->status == 'outage');
    }
}
