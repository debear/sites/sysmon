<?php

namespace DeBear\Models\SysMon;

use DeBear\Implementations\Model;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Highcharts;

class ChecksComms extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_COMMS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'type',
        'date',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'date' => 'datetime:Y-m-d',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Get the list of columns to use in the detailed history table
     * @return array The col/label pairs of appropriate detail table columns for this check
     */
    public static function getColumns(): array
    {
        return [
            'date' => 'Date',
            'num_queued' => 'Queued',
            'num_sent' => 'Sent',
            'num_failed' => 'Failed',
            'num_other' => 'Other',
        ];
    }

    /**
     * Build the Highcharts JSON object
     * @param Checks $check  The base check object we are running against.
     * @param string $dom_id ID of the DOM element the chart is to be added to.
     * @return array Array of JSON info for the Highcharts module
     */
    public static function buildHighchartsObject(Checks $check, string $dom_id): array
    {
        // Build the base object.
        $chart = Highcharts::new($dom_id);
        // Get the data.
        $data = $check->details()->forPage(1, FrameworkConfig::get('debear.setup.chart-points'))->reverse();
        // Customise - type.
        $chart['chart']['type'] = 'column';
        // Customise - series.
        $cols = array_diff_key(static::getColumns(), ['date' => true]);
        $chart['series'] = [];
        foreach ($cols as $col => $label) {
            $chart['series'][] = [
                'name' => $label,
                'data' => $data->pluck($col),
            ];
        }
        // Customise - xAxis.
        $chart['xAxis'] = [
            'categories' => [],
        ];
        foreach ($data as $row) {
            $chart['xAxis']['categories'][] = $row->formatCol('date');
        }
        // Customise - yAxis.
        $chart['yAxis'] = [
            'title' => ['text' => 'Messages'],
            'allowDecimals' => false,
            'min' => 0,
        ];
        // Customise - plotOptions.
        $chart['plotOptions'] = [
            'column' => ['stacking' => 'normal'],
            'series' => ['pointPadding' => 0, 'groupPadding' => 0],
        ];
        // Customise - tooltips.
        $chart['tooltip'] = [
            'shared' => true,
            'useHTML' => true,
            'formatter' => 'function() { var ret = \'<strong>\' + this.category + \'</strong>:<br/>\'; for (var i = 0;'
                . ' i < this.points.length; i++) { ret += this.points[i].series.name + \': \' + this.points[i].y + '
                . '\'<br/>\'; } return \'<tooltip>\' + ret + \'</tooltip>\'; }',
        ];
        return $chart;
    }

    /**
     * Build the additional Highcharts JSON objects
     * @param Checks $check The base check object we are running against.d to.
     * @return array An array of zero, one or more JSON info objects for the Highcharts module
     */
    public static function buildHighchartsExtraObjects(Checks $check): array
    {
        // By status as a pie chart.
        $status = Highcharts::new('faux'); // Appropriate $dom_id will be interpolated later.
        $status['subnav-code'] = 'status';
        $status['subnav-name'] = 'By Status';
        // Get the data.
        $data = $check->details()->forPage(1, FrameworkConfig::get('debear.setup.chart-points'));
        // Customise - type.
        $status['chart']['type'] = 'pie';
        // Customise - series.
        $cols = array_diff_key(static::getColumns(), ['date' => true]);
        $status['series'] = [['name' => $status['subnav-name'], 'colorByPoint' => true, 'data' => []]];
        foreach ($cols as $col => $label) {
            $status['series'][0]['data'][] = [
                'name' => $label,
                'y' => $data->sum($col),
            ];
        }
        // Customise - tooltips.
        $status['tooltip'] = [
            'shared' => false,
            'useHTML' => true,
            'formatter' => 'function() { return \'<tooltip><strong>\' + this.key + \'</strong>: \''
                . ' + this.percentage.toFixed(2) + \'%</tooltip>\'; }',
        ];
        return [$status];
    }

    /**
     * Return a formatted script representing a particular column
     * @param string $col The detail column to format.
     * @return string The formatted column string
     */
    public function formatCol(string $col): string
    {
        if ($col == 'date') {
            return $this->$col->format('jS M Y');
        }
        return number_format($this->$col);
    }
}
