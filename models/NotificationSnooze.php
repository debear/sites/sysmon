<?php

namespace DeBear\Models\SysMon;

use DeBear\Implementations\Model;

class NotificationSnooze extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_NOTIFICATION_SILENT';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'check_id',
        'start',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'start' => 'datetime',
        'end' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
}
