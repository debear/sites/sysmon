<?php

namespace DeBear\Models\SysMon;

use DeBear\Implementations\Model;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Highcharts;

class ChecksLockfile extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_LOCKFILE';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'app',
        'checked',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'checked' => 'datetime:Y-m-d',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Get the list of columns to use in the detailed history table
     * @return array The col/label pairs of appropriate detail table columns for this check
     */
    public static function getColumns(): array
    {
        return [
            'checked' => 'Date',
            'state' => 'State',
        ];
    }

    /**
     * Build the Highcharts JSON object
     * @param Checks $check  The base check object we are running against.
     * @param string $dom_id ID of the DOM element the chart is to be added to.
     * @return array Array of JSON info for the Highcharts module
     */
    public static function buildHighchartsObject(Checks $check, string $dom_id): array
    {
        // Build the base object.
        $chart = Highcharts::new($dom_id);
        // Get the data.
        $data = $check->details()->forPage(1, FrameworkConfig::get('debear.setup.chart-points'))->reverse();
        // Customise - type.
        $chart['chart']['type'] = 'column';
        // Customise - series.
        $chart['series'] = [
            ['name' => 'Valid', 'data' => []],
            ['name' => 'Missing', 'data' => []],
            ['name' => 'Failed', 'data' => []],
        ];
        foreach ($data as $row) {
            switch ($row['state']) {
                case 'valid':
                    $chart['series'][0]['data'][] = 3; // Is Valid.
                    $chart['series'][1]['data'][] = 0; // Not Missing.
                    $chart['series'][2]['data'][] = 0; // Not Failed.
                    break;
                case 'missing':
                    $chart['series'][0]['data'][] = 0; // Not Valid.
                    $chart['series'][1]['data'][] = 2; // Is Missing.
                    $chart['series'][2]['data'][] = 0; // Not Failed.
                    break;
                case 'failed':
                    $chart['series'][0]['data'][] = 0; // Not Valid.
                    $chart['series'][1]['data'][] = 0; // Not Missing.
                    $chart['series'][2]['data'][] = 1; // Is Failed.
                    break;
            }
        }
        // Customise - xAxis.
        $chart['xAxis'] = [
            'categories' => [],
        ];
        foreach ($data as $row) {
            $chart['xAxis']['categories'][] = $row->formatCol('checked');
        }
        // Customise - yAxis.
        $chart['yAxis'] = [
            'title' => ['text' => 'State'],
            'labels' => ['enabled' => false],
            'min' => 0,
            'max' => 3,
        ];
        // Customise - plotOptions.
        $chart['plotOptions'] = [
            'column' => ['stacking' => 'normal'],
            'series' => [
                'pointPadding' => 0,
                'groupPadding' => 0,
                'borderWidth' => 0,
            ],
        ];
        // Customise - tooltips.
        $chart['tooltip'] = [
            'shared' => true,
            'useHTML' => true,
            'formatter' => 'function() { for (var i = 0; i < this.points.length; i++) if (this.points[i].y) '
                . 'return \'<tooltip><strong>\' + this.category + \'</strong>: \' + this.points[i].series.name + '
                . '\'</tooltip>\'; }',
        ];
        return $chart;
    }

    /**
     * Build the additional Highcharts JSON objects
     * @param Checks $check The base check object we are running against.d to.
     * @return array An array of zero, one or more JSON info objects for the Highcharts module
     */
    public static function buildHighchartsExtraObjects(Checks $check): array
    {
        // By status as a pie chart.
        $status = Highcharts::new('faux'); // Appropriate $dom_id will be interpolated later.
        $status['subnav-code'] = 'status';
        $status['subnav-name'] = 'By Status';
        // Get the data.
        $data = $check->details()->forPage(1, FrameworkConfig::get('debear.setup.chart-points'));
        $by_state = $data->pluck('state')->countBy()->toArray();
        // Customise - type.
        $status['chart']['type'] = 'pie';
        // Customise - series.
        $status['series'] = [];
        $status['series'][0] = ['name' => $status['subnav-name'], 'colorByPoint' => true, 'data' => []];
        // Valid.
        $status['series'][0]['data'][0] = [
            'name' => 'Valid',
            'y' => $by_state['valid'] ?? 0,
        ];
        // Missing.
        $status['series'][0]['data'][1] = [
            'name' => 'Missing',
            'y' => $by_state['missing'] ?? 0,
        ];
        // Failed.
        $status['series'][0]['data'][2] = [
            'name' => 'Failed',
            'y' => $by_state['failed'] ?? 0,
        ];
        // Customise - tooltips.
        $status['tooltip'] = [
            'shared' => false,
            'useHTML' => true,
            'formatter' => 'function() { return \'<tooltip><strong>\' + this.key + \'</strong>: \''
                . ' + this.percentage.toFixed(2) + \'%</tooltip>\'; }',
        ];
        return [$status];
    }

    /**
     * Return a formatted script representing a particular column
     * @param string $col The detail column to format.
     * @return string The formatted column string
     */
    public function formatCol(string $col): string
    {
        if ($col == 'checked') {
            return $this->$col->format('jS M Y');
        }
        return '<span class="icon icon_lockfile_' . $this->$col . '">' . ucfirst($this->$col) . '</span>';
    }
}
