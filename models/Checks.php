<?php

namespace DeBear\Models\SysMon;

use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Carbon\Carbon;
use DeBear\Exceptions\SysMon\IncompatibleCheckException;
use DeBear\Implementations\Model;
use DeBear\Helpers\Format;
use DeBear\Helpers\Strings;
use DeBear\Repositories\Time;

class Checks extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_CHECKS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'check_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'last_checked' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Cached check history instances
     * @var ?Collection
     */
    protected $checkDetails;

    /**
     * Relationship: 1:1 group details
     * @return HasOne
     */
    public function groupDetails(): HasOne
    {
        return $this->hasOne(CheckGroups::class, 'group_id', 'group_id');
    }
    /**
     * Relationship: 1:1 notification settings
     * @return HasOne
     */
    public function notificationSnooze(): HasOne
    {
        return $this->hasOne(NotificationSnooze::class, 'check_id', 'check_id')
            ->where('start', '<=', App::make(Time::class)->formatServer())
            ->where(function ($q) {
                $q->where('end', '>=', App::make(Time::class)->formatServer())
                    ->orWhereNull('end');
            });
    }
    /**
     * Relationship: 1:M logs
     * @return HasMany
     */
    public function logs(): HasMany
    {
        return $this->hasMany(Logs::class, 'check_id', 'check_id');
    }

    /**
     * The base link for this check
     * @return string The base URL
     */
    protected function linkBase(): string
    {
        return '/check/' . Strings::codifyURL($this->name) . '-' . $this->check_id;
    }

    /**
     * Generate the detail URL for this check
     * @return string The appropriate 'detail' URL for this check
     */
    public function linkDetail(): string
    {
        return $this->linkBase();
    }

    /**
     * Generate the logs URL for this check
     * @return string The appropriate 'logs' URL for this check
     */
    public function linkLogs(): string
    {
        return $this->linkBase() . '/logs';
    }

    /**
     * Format the script type for display
     * @return string The formatted script type
     */
    public function scriptDisplay(): string
    {
        switch ($this->script) {
            case 'http_request':
                $title = 'Request';
                break;
            case 'http_status':
                $title = 'Status';
                break;
            default:
                $title = ucfirst($this->script);
                break;
        }
        return $title;
    }

    /**
     * Convert the frequency crontab hour in to a daily/hourly split
     * @return string The script frequency as daily/hourly
     */
    public function getFrequencyTypeAttribute(): string
    {
        return (preg_match('/^\d+$/', $this->frequency) ? 'daily' : 'hourly');
    }

    /**
     * Dynamic detailed history getter
     * @return Collection Appropriate collection of detailed history for this check
     */
    public function details() /* : Collection */
    {
        // If we've built it already, return it.
        if (isset($this->checkDetails)) {
            return $this->checkDetails;
        }

        // Otherwise, build, cache and return!
        switch ($this->script) {
            case 'usage':
                $this->checkDetails = ChecksUsage::orderByDesc('checked')
                    ->get();
                break;
            case 'comms':
                $this->checkDetails = ChecksComms::where('type', '=', $this->instance)
                    ->orderByDesc('date')
                    ->get();
                break;
            case 'lockfile':
                $this->checkDetails = ChecksLockfile::where('app', '=', $this->instance)
                    ->orderByDesc('checked')
                    ->get();
                break;
            case 'server':
                $this->checkDetails = ChecksServer::where('server', '=', $this->instance)
                    ->orderByDesc('checked')
                    ->get();
                break;
            case 'logs':
                $this->checkDetails = ChecksLogfile::where('app', '=', $this->instance)
                    ->orderByDesc('checked')
                    ->get();
                break;
            case 'http_request':
                $this->checkDetails = ChecksHTTPRequest::where('subdomain', '=', $this->instance)
                    ->orderByDesc('checked')
                    ->orderBy('path')
                    ->get();
                break;
            case 'http_status':
                $this->checkDetails = ChecksHTTPStatus::where('subdomain', '=', $this->instance)
                    ->orderByDesc('date')
                    ->get();
                break;
            case 'age':
                $this->checkDetails = ChecksAge::where('ref', '=', $this->instance)
                    ->orderByDesc('checked')
                    ->get();
                break;
        }
        return $this->checkDetails;
    }

    /**
     * Dynamic detailed history getter broken down by (arbitrary) section
     * @param Carbon $min Minimum timestamp for which the breakdowns should be returned.
     * @return Collection Appropriate collection of detailed history broken down by section for this check
     * @throws IncompatibleCheckException When requesting for a check that is incompatible with breakdowns.
     */
    public function detailsBreakdown(Carbon $min) /* : Collection */
    {
        if ($this->script != 'logs') {
            throw new IncompatibleCheckException('Details breakdown unavailable for this check.');
        }
        return ChecksLogfileBreakdown::where('app', '=', $this->instance)
            ->where('checked', '>=', $min->toDateTimeString())
            ->orderByDesc('checked')
            ->get();
    }

    /**
     * Get the name of the class containing the detailed info for this script
     * @return string The appropriate class name
     */
    protected function getCheckClassName(): string
    {
        $script = str_replace('_', '', $this->script);
        return __NAMESPACE__ . '\Checks' . ucfirst($script == 'logs' ? 'logfile' : $script);
    }

    /**
     * Get the list of columns to use in the detailed history table
     * @return array The col/label pairs of appropriate detail table columns for this check
     */
    public function getColumns(): array
    {
        return call_user_func([$this->getCheckClassName(), 'getColumns']);
    }

    /**
     * Build the main Highcharts JSON object
     * @param string $dom_id ID of the DOM element the chart is to be added to.
     * @return array Array of JSON info for the Highcharts module
     */
    public function buildHighchartsObject(string $dom_id): array
    {
        return call_user_func([$this->getCheckClassName(), 'buildHighchartsObject'], $this, $dom_id);
    }

    /**
     * Build the secondary Highcharts JSON object(s)
     * @return array Array of JSON info for the Highcharts module
     */
    public function buildHighchartsExtraObjects(): array
    {
        // Note: The 'faux' $dom_id will be interpolated later.
        return call_user_func([$this->getCheckClassName(), 'buildHighchartsExtraObjects'], $this);
    }

    /***********
     * Helpers *
     ***********/
    /**
     * Render the last summary value in a meaningful way
     * @return string Descriptive version of the summary value
     */
    public function formatSummary(): string
    {
        $fmt = '<span>Unknown</span>'; // A default value.
        if (!isset($this->last_summary)) {
            return $fmt;
        }

        switch ($this->script) {
            case 'usage':
                $fmt = $this->last_summary . '%';
                break;

            case 'comms':
                switch ($this->last_summary) {
                    case 99:
                        $fmt = '<span>None</span>';
                        break;
                    case 3:
                        $fmt = 'Other';
                        break;
                    case 2:
                        $fmt = 'Queued';
                        break;
                    case 1:
                        $fmt = 'Sent';
                        break;
                    case 0:
                        $fmt = 'Failed';
                        break;
                }
                break;

            case 'lockfile':
                switch ($this->last_summary) {
                    case 2:
                        $fmt = 'Missing';
                        break;
                    case 1:
                        $fmt = 'Valid';
                        break;
                    case 0:
                        $fmt = 'Failed';
                        break;
                }
                break;

            case 'server':
                $fmt = ($this->last_summary ? 'Up' : 'Down');
                break;

            case 'logs':
            case 'http_request':
                $fmt = Format::pluralise($this->last_summary, ' error');
                break;

            case 'http_status':
                $fmt = $this->last_summary . '% OK';
                break;

            case 'age':
                $fmt = ($this->last_summary < 255 ? Format::pluralise($this->last_summary, ' Day') : '255+ Days');
                break;
        }
        return $fmt;
    }
}
