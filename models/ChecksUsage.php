<?php

namespace DeBear\Models\SysMon;

use DeBear\Implementations\Model;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Format;
use DeBear\Helpers\Highcharts;

class ChecksUsage extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_USAGE';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'checked';
    /**
     * Our non-integer primary key column data type
     * @var string
     */
    protected $keyType = 'string';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'checked' => 'datetime:Y-m-d',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Get the list of columns to use in the detailed history table
     * @return array The col/label pairs of appropriate detail table columns for this check
     */
    public static function getColumns(): array
    {
        return array_filter([
            'checked' => 'Date',
            'total_usage' => 'Total',
            'remaining' => 'Remaining',
            'email_debear' => false, // 'Email: DeBear', - removed after move away from juliet.
            'email_padd' => false, // 'Email: pa-dd', - removed after move away from juliet.
            'backup' => 'Backups',
            'logs' => 'Logs',
            'merges' => 'Merges',
            'sites_other' => 'Sites',
            'sites_cdn' => 'CDN',
            'db_sports' => 'DB: Sports',
            'db_fantasy' => 'DB: Fantasy',
            'db_other' => 'DB: Rest',
            'other' => 'Other',
        ]);
    }

    /**
     * Build the Highcharts JSON object
     * @param Checks $check  The base check object we are running against.
     * @param string $dom_id ID of the DOM element the chart is to be added to.
     * @return array Array of JSON info for the Highcharts module
     */
    public static function buildHighchartsObject(Checks $check, string $dom_id): array
    {
        // Build the base object.
        $chart = Highcharts::new($dom_id);
        // Get the data.
        $data = $check->details()->forPage(1, FrameworkConfig::get('debear.setup.chart-points'))->reverse();
        // Customise - type.
        $chart['chart']['type'] = 'area';
        // Customise - series.
        $cols = array_diff_key(static::getColumns(), ['checked' => true, 'total_usage' => true, 'remaining' => true]);
        $chart['series'] = [];
        foreach ($cols as $col => $label) {
            $chart['series'][] = [
                'name' => $label,
                'data' => $data->pluck($col),

            ];
        }
        // Customise - xAxis.
        $chart['xAxis'] = [
            'categories' => [],
        ];
        foreach ($data as $row) {
            $chart['xAxis']['categories'][] = $row->formatCol('checked');
        }
        // Customise - yAxis.
        $chart['yAxis'] = [
            'title' => ['text' => 'Disk Usage'],
            'labels' => [
                // Always displayed in a consistent unit... 1073741824 bytes = 1 gig.
                'formatter' => 'function() { return (this.value / 1073741824).toFixed(2) + \' GiB\'; }',
            ],
        ];
        // Customise - plotOptions.
        $chart['plotOptions'] = [
            'area' => [
                'stacking' => 'normal',
                'marker' => [
                    'enabled' => false,
                ],
            ],
        ];
        // Customise - tooltips.
        $chart['tooltip'] = [
            'shared' => true,
            'useHTML' => true,
            'formatter' => 'function() { var str = \'\'; var total = 0; for (var i = 0; i < this.points.length; i++) '
                . '{ str += \'<br/><em>\' + this.points[i].series.name + \'</em>: \' + '
                . 'Numbers.toBytes(this.points[i].y); total += this.points[i].y; } return \'<tooltip><strong>\' + '
                . 'this.category + \'</strong>\' + (this.points.length > 1 ? \':<br/><em>Total:</em> \' + '
                . 'Numbers.toBytes(total) : \'\') + str; }',
        ];
        return $chart;
    }

    /**
     * Build the additional Highcharts JSON objects
     * @param Checks $check The base check object we are running against.d to.
     * @return array An array of zero, one or more JSON info objects for the Highcharts module
     */
    public static function buildHighchartsExtraObjects(Checks $check): array
    {
        // Current usage as a pie chart.
        $usage = Highcharts::new('faux'); // Appropriate $dom_id will be interpolated later.
        $usage['subnav-code'] = 'usage';
        $usage['subnav-name'] = 'Current Usage';
        // Get the data.
        $data = $check->details()->first();
        // The list of columns (with remaining bumped to the end of the list).
        $cols = array_merge(
            array_diff_key(static::getColumns(), ['checked' => true, 'total_usage' => true, 'remaining' => true]),
            array_intersect_key(static::getColumns(), ['remaining' => true])
        );
        // Customise - type.
        $usage['chart']['type'] = 'pie';
        // Customise - series.
        $usage['series'] = [['name' => $usage['subnav-name'], 'colorByPoint' => true, 'data' => []]];
        foreach ($cols as $col => $label) {
            $usage['series'][0]['data'][] = [
                'name' => $label,
                'y' => $data->$col,
            ];
        }
        // Customise - tooltips.
        $usage['tooltip'] = [
            'shared' => false,
            'useHTML' => true,
            'formatter' => 'function() { return \'<tooltip><strong>\' + this.key + \'</strong>: \' + '
                . 'Numbers.toBytes(this.y) + \' (\' + Numbers.toXdp(this.percentage) + \'%)</tooltip>\'; }',
        ];
        return [$usage];
    }

    /**
     * Return a formatted script representing a particular column
     * @param string $col The detail column to format.
     * @return string The formatted column string
     */
    public function formatCol(string $col): string
    {
        if ($col == 'checked') {
            return $this->$col->format('jS M Y');
        }
        return Format::bytes($this->$col);
    }
}
