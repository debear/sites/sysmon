<?php

namespace DeBear\Models\SysMon;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Implementations\Model;

class StatusItem extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_STATUS_ITEMS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'status_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'current_started' => 'datetime',
        'last_started' => 'datetime',
        'last_resolved' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Relationship: 1:1 status item
     * @return HasOne
     */
    public function group(): HasOne
    {
        return $this->hasOne(StatusGroup::class, 'group_id', 'group_id');
    }

    /**
     * Relationship: 1:M status item metrics
     * @return HasMany
     */
    public function metrics(): HasMany
    {
        return $this->hasMany(StatusItemMetrics::class, 'status_id', 'status_id');
    }

    /**
     * Get the current metrics for this item
     * @return StatusItemMetrics The current metrics
     */
    public function currentMetrics(): StatusItemMetrics
    {
        return $this->metrics()->where('is_current', 1)->first();
    }

    /**
     * Convert the item's status to an icon
     * @return string The appropriate status icon
     */
    public function getStatusIconAttribute(): string
    {
        switch ($this->status) {
            case 'okay':
            case 'recovered':
                return 'status_okay';
            case 'warn':
                return 'status_warn';
            case 'critical':
                return 'status_critical';
        }
        // Default: unknown.
        return 'status_unknown';
    }
}
