<?php

namespace DeBear\Models\SysMon;

use Illuminate\Database\Eloquent\Relations\HasOne;
use DeBear\Implementations\Model;

class Logs extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_LOG';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'check_id',
        'log_date',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'log_date' => 'datetime',
        'started' => 'datetime',
        'finished' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Relationship: 1:1 core check
     * @return HasOne
     */
    public function check(): HasOne
    {
        return $this->hasOne(Checks::class, 'check_id', 'check_id');
    }

    /**
     * Convert the log time in to an appropriate display version
     * @return string Formatted log time
     */
    public function logDateFormat(): string
    {
        $fmt = 'jS M Y';
        if ($this->check->frequency_type == 'hourly') {
            $fmt .= ', H:i';
        }
        return $this->log_date->format($fmt);
    }
}
