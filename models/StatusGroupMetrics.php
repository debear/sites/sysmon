<?php

namespace DeBear\Models\SysMon;

use DeBear\Implementations\Model;

class StatusGroupMetrics extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_STATUS_GROUPS_METRICS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'group_id',
        'when_parsed',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'when_parsed' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
}
