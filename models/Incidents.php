<?php

namespace DeBear\Models\SysMon;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Collection;
use DeBear\Implementations\Model;
use Carbon\CarbonInterface;

class Incidents extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sysmon';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SYSMON_INCIDENTS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'group_id',
        'date_ref',
        'incident_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'started' => 'datetime',
        'resolved' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Relationship: 1:1 status item
     * @return HasOne
     */
    public function statusItem(): HasOne
    {
        return $this->hasOne(StatusItem::class, 'status_id', 'status_id');
    }
    /**
     * Relationship: 1:1 raw check
     * @return HasOne
     */
    public function check(): HasOne
    {
        return $this->hasOne(Checks::class, 'check_id', 'check_id');
    }
    /**
     * Relationship: 1:M ledger
     * @return HasMany
     */
    public function ledger(): HasMany
    {
        return $this->hasMany(
            IncidentsLedger::class,
            ['group_id', 'date_ref', 'incident_id'],
            ['group_id', 'date_ref', 'incident_id']
        );
    }

    /**
     * Build the composite reference code from the info we have
     * @return string The built indicent reference code
     */
    public function getRefCodeAttribute(): string
    {
        return sprintf('%s-%d%02d', $this->group_code, $this->date_ref, $this->incident_id);
    }

    /**
     * Convert the incident's status to an icon
     * @return string The appropriate status icon
     */
    public function getIconAttribute(): string
    {
        switch ($this->status) {
            case 'warn':
                return 'status_warn';
            case 'critical':
                return 'status_critical';
        }
        // Default: unknown.
        return 'status_unknown';
    }

    /**
     * Determine (for display) the duration of the incident
     * @return string A displayable incident duration
     */
    public function getDurationAttribute(): string
    {
        // If it's not resolved, it must be ongoing.
        if (!isset($this->resolved)) {
            return 'Ongoing';
        }
        // Calculate via date difference.
        return $this->resolved->startOfHour()->diffForHumans(
            $this->started->startOfHour(),
            CarbonInterface::DIFF_ABSOLUTE
        );
    }

    /**
     * Render the status as a severity attribute
     * @return string The status explained as a severity
     */
    public function getSeverityAttribute(): string
    {
        return '<span class="icon_right icon_right_status_' . $this->status . '">'
            . ($this->status == 'warn' ? 'Warning' : 'Critical')
            . '</span>';
    }

    /**
     * Determine if we should render the ledger changes
     * @return boolean If there are non-start/finish ledger items to render
     */
    public function renderLedgerChanges(): bool
    {
        return $this->ledger
            // Ignore the original raising of the incident.
            ->where('status_from', '!=', 'okay')
            // As well as the resolution.
            ->where('status_to', '!=', 'recovered')
            // Must return one or more ledgers.
            ->count() > 0;
    }

    /**
     * Get the list of changes that have occurred to this incident
     * @return Collection The chronological list of zero, one or more changes
     */
    public function ledgerChanges(): Collection
    {
        return $this->ledger->sortBy('status_changed');
    }
}
