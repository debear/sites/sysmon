#!/bin/bash
# Include the subsite alias symlinks

if [ ! -z $CI ] || [ ! -z $CI_FAUX ]
then
    dir_base=`dirname $0`
    cd $dir_base/../../..
    echo "Creating 'status' symlink in `pwd`:"
    ln -s sysmon status
    ls -l
fi
