#!/bin/bash
# Script to run the CI components locally. TD. 2019-01.21.

# Load the common functions
basedir=$(dirname $(realpath $0))
. $basedir/../_scripts/helpers.sh

# Welcome!
root=$(realpath $basedir/../..)
repo=$(basename $root)
display_title "Local CI Testing for repo '$repo'"
# Add the root dir to our PATH (from which our tests usually descend)
cd $root
PATH=$root:$PATH

# Some internal tracking vars
all_passed=1

# Get the file
ci=.ci.yml
yml="$root/$ci"
if [ ! -e $yml ]
then
    display_error "Unable to find CI YAML file '$ci'?"
    exit 10
fi

# Parsing only certain tests?
filters="$@"
if [ "x$filters" = 'x' ]
then
    filtered=0
else
    filtered=1
fi

# Determine the appropriate test rules
IFS=$'\n'
rules=$($basedir/parse.pl)
nj=0;
for rule in ${rules[@]}
do
    # Break down to the component parts (0,3,etc because of the length of the separator)
    IFS='£££' read -r -a test <<< "$rule"
    rule_code="${test[0]}"
    rule_stage="${test[6]}"
    if [[ "$rule_stage" =~ ^(setup|deploy)$ ]]
    then
        # Skip...
        continue
    elif [ $filtered -eq 0 ] || [ ! -z $(echo "$filters" | grep -P "\b$rule_code\b") ]
    then
        nj=$(($nj + 1))
    fi
done
ij=1
for rule in ${rules[@]}
do
    # Break down to the component parts (0,3,etc because of the length of the separator)
    IFS='£££' read -r -a test <<< "$rule"
    rule_code="${test[0]}"
    rule_name="${test[3]}"
    rule_stage="${test[6]}"
    rule_alwfail="${test[9]}"
    rule_scripts="${test[12]}"

    # Filtered?
    if [ $filtered -eq 1 ] && [ -z $(echo "$filters" | grep -P "\b$rule_code\b") ]
    then
        # Not an argument we've been passed
        continue
    fi
    # Skip certain rule stages
    if [[ "$rule_stage" =~ ^(setup|deploy)$ ]]
    then
        continue
    fi

    # Title
    title_alwfail=
    if [ "x$rule_alwfail" = 'xtrue' ]
    then
        title_alwfail=" // ** Allowed to Fail **"
    fi
    echo
    display_section "Job $ij of $nj: ($rule_stage) $rule_code: $rule_name$title_alwfail"
    ij=$(($ij +1))

    # Break down the scripts
    IFS='@@@' read -r -a scripts <<< "$rule_scripts"
    nt=$(((${#scripts[@]} + 2) / 3))
    it=1
    for s in ${scripts[@]}
    do
        # Header
        if [ $it -gt 1 ]; then echo; fi # Pretty spacing
        display_info "Script $it of $nt: $s"
        it=$(($it +1))

        # Run
        eval "$s"

        # Status
        err=$?
        if [ $err -eq 0 ]
        then
            # Passed
            display_success "Passed"
        elif [ "x$rule_alwfail" = 'xtrue' ]
        then
            # Failed, but we are allowed to
            all_passed=0
            display_warning "Failed (Status Code: $err), but continuing as this is considered acceptable"
        else
            # Failed...
            display_error "Failed (Status Code: $err)"
            exit 20
        fi
    done
done

echo
display_title "Final Status:"
display_success "CI Tests Passing"
if [ $all_passed -eq 0 ]
then
    display_warning "The 'allow_failure' clause was triggered."
fi
