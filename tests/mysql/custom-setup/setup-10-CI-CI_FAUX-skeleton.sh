#!/bin/bash
# Clone the skeleton in the parent directory
echo -e "\e[1mOn CI server, so cloning _debear to parent directory:\e[0m"

dir_scripts=$(realpath $(dirname $0)/../../_scripts)
$dir_scripts/git-clone.sh skeleton _debear
