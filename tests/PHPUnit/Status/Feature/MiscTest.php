<?php

namespace Tests\PHPUnit\Status\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;

class MiscTest extends FeatureTestCase
{
    /**
     * A test of the manifest.json file
     * @return void
     */
    public function testManifest(): void
    {
        $response = $this->get('/manifest.json');
        $response->assertStatus(200);
        $response->assertJson([
            'name' => 'DeBear.uk System Status',
            'short_name' => 'DeBear.uk System Status',
            'description' => 'The site-wide system status of DeBear.uk',
            'start_url' => '/?utm_source=homescreen',
            'icons' => [[
                'src' => 'https://static.debear.test/skel/images/meta/debear.png',
                'sizes' => '512x512',
                'type' => 'image/png',
            ]],
        ]);
    }
}
