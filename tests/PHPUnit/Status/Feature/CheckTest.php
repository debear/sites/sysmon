<?php

namespace Tests\PHPUnit\Status\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;

class CheckTest extends FeatureTestCase
{
    /**
     * A test for 'guest' users.
     * @return void
     */
    public function testGuest(): void
    {
        $response = $this->get('/');
        $response->assertStatus(200);

        // Overall.
        $response->assertSee('<div class="box text critical icon icon_error">We are aware of, and dealing with,'
            . ' a major issue.</div>');
        // DeBear.uk.
        $response->assertSee('<dt class="row_warn icon_right_status_warn">DeBear</dt>
            <dd class="summary text warn">There are currently some issues.</dd>');
        $response->assertSee('<dd class="item row_pastel status_warn icon_right_status_warn item_icon_home">
                                Home
                            </dd>');
        $response->assertSee('DBR-1936306');
        $response->assertDontSee('<dd class="item row_pastel status_critical icon_right_status_critical'
            . ' item_icon_sysmon">
                                    <span class="icon_right_item_admin">
                                SysMon                                    </span>
                            </dd>');
        $response->assertDontSee('DBR-1935705');
        // Sports.
        $response->assertSee('<dt class="row_critical icon_right_status_critical">Sports</dt>
            <dd class="summary text critical">There is currently an outage.</dd>');
        $response->assertSee('<dd class="item row_pastel status_critical icon_right_status_critical item_icon_sports">
                                Home
                            </dd>');
        $response->assertSee('SP-1936202');
        $response->assertSee('<dd class="item row_pastel status_okay icon_right_status_okay item_icon_sports_nhl">
                                NHL
                            </dd>');
        // Fantasy.
        $response->assertSee('<dt class="row_okay icon_right_status_okay">Fantasy</dt>
            <dd class="summary text okay">All components are fully operational.</dd>');
        $response->assertSee('<dd class="item row_pastel status_okay icon_right_status_okay item_icon_fantasy">
                                Home
                            </dd>');
        $this->assertMatchesRegularExpression(
            '/<dd class="last text relative_date okay" title="[^"]+">Last outage:/',
            $response->getContent()
        );
        // Systems.
        $response->assertSee('<dt class="row_okay icon_right_status_okay">Systems</dt>
            <dd class="summary text okay">All components are fully operational.</dd>');
        $response->assertSee('<dd class="item row_pastel status_okay icon_right_status_okay item_icon_servers">
                                Servers
                            </dd>');
        $response->assertDontSee('<dd class="item row_pastel status_okay icon_right_status_okay item_icon_tweets">
                                    <span class="icon_right_item_admin">
                                Tweets                                    </span>
                            </dd>');
    }

    /**
     * A test of for 'admin' users.
     * @return void
     */
    public function testAdmin(): void
    {
        $post = [
            'username' => 'sysmon_admin',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);
        $response = $this->get('/');
        $response->assertStatus(200);

        // Overall.
        $response->assertSee('<div class="box text critical icon icon_error">We are aware of, and dealing with,'
            . ' major issues.</div>');
        // DeBear.uk.
        $response->assertSee('<dt class="row_critical icon_right_status_critical">DeBear</dt>
            <dd class="summary text critical">There is currently an outage.</dd>');
        $response->assertSee('<dd class="item row_pastel status_okay icon_right_status_okay item_icon_fantasy">
                                Home
                            </dd>');
        $response->assertSee('DBR-1936306');
        $response->assertSee('<dd class="item row_pastel status_critical icon_right_status_critical item_icon_sysmon">
                                    <span class="icon_right_item_admin">
                                SysMon
                                    </span>
                            </dd>');
        $response->assertSee('DBR-1935705');
        // Sports.
        $response->assertSee('<dt class="row_critical icon_right_status_critical">Sports</dt>
            <dd class="summary text critical">There is currently an outage.</dd>');
        $response->assertSee('<dd class="item row_pastel status_critical icon_right_status_critical item_icon_sports">
                                Home
                            </dd>');
        $response->assertSee('SP-1936202');
        $response->assertSee('<dd class="item row_pastel status_okay icon_right_status_okay item_icon_sports_nhl">
                                NHL
                            </dd>');
        // Fantasy.
        $response->assertSee('<dt class="row_okay icon_right_status_okay">Fantasy</dt>
            <dd class="summary text okay">All components are fully operational.</dd>');
        $response->assertSee('<dd class="item row_pastel status_okay icon_right_status_okay item_icon_fantasy">
                                Home
                            </dd>');
        $this->assertMatchesRegularExpression(
            '/<dd class="last text relative_date okay" title="[^"]+">Last outage:/',
            $response->getContent()
        );
        // Systems.
        $response->assertSee('<dt class="row_okay icon_right_status_okay">Systems</dt>
            <dd class="summary text okay">All components are fully operational.</dd>');
        $response->assertSee('<dd class="item row_pastel status_okay icon_right_status_okay item_icon_servers">
                                Servers
                            </dd>');
        $response->assertSee('<dd class="item row_pastel status_okay icon_right_status_okay item_icon_tweets">
                                    <span class="icon_right_item_admin">
                                Tweets
                                    </span>
                            </dd>');
    }
}
