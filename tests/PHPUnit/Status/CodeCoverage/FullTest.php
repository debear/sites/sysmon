<?php

namespace Tests\PHPUnit\Status\CodeCoverage;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use Carbon\Carbon;
use DeBear\Helpers\Config;
use DeBear\Http\Controllers\SysMon\Status;
use DeBear\Models\SysMon\StatusGroup;
use DeBear\Models\SysMon\StatusGroupMetrics;
use DeBear\Models\SysMon\StatusItem;
use DeBear\Models\SysMon\StatusItemMetrics;
use DeBear\Models\SysMon\Incidents;
use DeBear\Models\SysMon\IncidentsLedger;

class FullTest extends UnitTestCase
{
    /**
     * The original SysMon config array
     * @var array
     */
    protected $config_sysmon;
    /**
     * The new Stats config array
     * @var array
     */
    protected $config_stats;
    /**
     * The original SysMon $_SERVER array
     * @var array
     */
    protected $server_sysmon;
    /**
     * The new Stats $_SERVER array
     * @var array
     */
    protected $server_stats;

    /**
     * Convert the default SysMon config to Status
     * @return void
     */
    protected function setUp(): void
    {
        // Run any default behaviour.
        parent::setUp();

        // Update our $_SERVER setting.
        // Grab the default config.
        $this->server_sysmon = $_SERVER;
        // Tweak accordingly.
        $_SERVER['REQUEST_URI'] = '/';
        $this->server_stats = $_SERVER;

        // Then build our Status config cache.
        // Grab the default config.
        $this->config_sysmon = FrameworkConfig::get('debear');
        // Tweak accordingly.
        FrameworkConfig::set(['debear.url.sub' => 'status']);
        Config::loadSite();
        $this->config_stats = FrameworkConfig::get('debear');
    }

    /**
     * Once we've finished, restore the SysMon-configured config
     * @return void
     */
    protected function tearDown(): void
    {
        // Restore the config.
        FrameworkConfig::set(['debear' => $this->config_sysmon]);
        $_SERVER = $this->server_sysmon;
        // Run any default behaviour.
        parent::tearDown();
    }

    /**
     * Test the Status controller
     * @return void
     */
    public function testController(): void
    {
        $controller = new Status();
        $response = $controller->index();
        $this->assertInstanceOf(Response::class, $response);
        $this->assertNotEmpty($response->getContent());
    }

    /**
     * Test the StatusGroup model
     * @return void
     */
    public function testModelStatusGroup(): void
    {
        $all = StatusGroup::all();

        // DeBear.uk - Issues.
        $test = $all->firstWhere('group_id', 1);
        $this->assertEquals('DeBear', $test->name);
        $this->assertEquals('DBR', $test->code);
        $this->assertEquals(10, $test->order);
        $this->assertEquals('2019-12-29 22:10:02', $test->last_outage);
        $this->assertEquals('issues', $test->status_user);
        $this->assertEquals('outage', $test->status_admin);
        $this->assertEquals('issues', $test->status);
        $this->assertEquals('warn', $test->status_box);
        $this->assertEquals('status_warn', $test->icon);
        $this->assertEquals('There are currently some issues.', $test->summary);
        $this->assertFalse($test->isOkay());
        $this->assertTrue($test->isWarn());
        $this->assertFalse($test->isCritical());

        // Sports - Outage.
        $test = $all->firstWhere('group_id', 2);
        $this->assertEquals('Sports', $test->name);
        $this->assertEquals('SP', $test->code);
        $this->assertEquals(20, $test->order);
        $this->assertEquals('2019-12-28 17:10:04', $test->last_outage);
        $this->assertEquals('outage', $test->status_user);
        $this->assertEquals('outage', $test->status_admin);
        $this->assertEquals('outage', $test->status);
        $this->assertEquals('critical', $test->status_box);
        $this->assertEquals('status_critical', $test->icon);
        $this->assertEquals('There is currently an outage.', $test->summary);
        $this->assertFalse($test->isOkay());
        $this->assertFalse($test->isWarn());
        $this->assertTrue($test->isCritical());

        // Systems - Operational.
        $test = $all->firstWhere('group_id', 4);
        $this->assertEquals('Systems', $test->name);
        $this->assertEquals('SYS', $test->code);
        $this->assertEquals(40, $test->order);
        $this->assertEquals('2019-12-25 08:10:02', $test->last_outage);
        $this->assertEquals('operational', $test->status_user);
        $this->assertEquals('operational', $test->status_admin);
        $this->assertEquals('operational', $test->status);
        $this->assertEquals('okay', $test->status_box);
        $this->assertEquals('status_okay', $test->icon);
        $this->assertEquals('All components are fully operational.', $test->summary);
        $this->assertTrue($test->isOkay());
        $this->assertFalse($test->isWarn());
        $this->assertFalse($test->isCritical());

        // Metrics.
        $metrics = $all->firstWhere('group_id', 3)->currentMetrics();
        $this->assertInstanceOf(StatusGroupMetrics::class, $metrics);
        $this->assertInstanceOf(Carbon::class, $metrics->when_parsed);
        $this->assertEquals('2018-12-10', $metrics->when_parsed->toDateString());
        $this->assertEquals(0, $metrics->metrics_7num);
        $this->assertNull($metrics->metrics_7avg);
        $this->assertEquals(3, $metrics->metrics_14num);
        $this->assertEquals(14.67, $metrics->metrics_14avg);
        $this->assertEquals(10, $metrics->metrics_30num);
        $this->assertEquals(5.10, $metrics->metrics_30avg);
        $this->assertEquals(39, $metrics->metrics_90num);
        $this->assertEquals(2.05, $metrics->metrics_90avg);
    }

    /**
     * Test the StatusItem model
     * @return void
     */
    public function testModelStatusItem(): void
    {
        $test = StatusItem::with('group')->where('status_id', '=', 3)->get()->first();

        $this->assertEquals('2019-12-23 16:10:06', $test->current_started);
        $this->assertEquals('2019-12-18 22:10:10', $test->last_started);
        $this->assertEquals('2019-12-18 23:10:10', $test->last_resolved);
        $this->assertEquals(1, $test->admin_only);
        $this->assertEquals(30, $test->order);

        $this->assertEquals(1, $test->group_id);
        $this->assertEquals('DeBear', $test->group->name);
        $this->assertEquals('DBR', $test->group->code);
        $this->assertEquals('issues', $test->group->status_user);
        $this->assertEquals('outage', $test->group->status_admin);
        $this->assertEquals('sysmon', $test->icon);

        // Now push it through the various statuses.
        // Initial: Critical.
        $this->assertEquals('critical', $test->status);
        $this->assertEquals('status_critical', $test->status_icon);
        // Warn.
        $test->update([
            'status' => 'warn',
        ]);
        $this->assertEquals('warn', $test->status);
        $this->assertEquals('status_warn', $test->status_icon);
        // Okay.
        $test->update([
            'status' => 'okay',
        ]);
        $this->assertEquals('okay', $test->status);
        $this->assertEquals('status_okay', $test->status_icon);
        // Recovered.
        $test->update([
            'status' => 'recovered',
        ]);
        $this->assertEquals('recovered', $test->status);
        $this->assertEquals('status_okay', $test->status_icon);
        // Unknown.
        $test->update([
            'status' => 'unknown',
        ]);
        $this->assertEquals('unknown', $test->status);
        $this->assertEquals('status_unknown', $test->status_icon);

        // Metrics.
        $metrics = $test->currentMetrics();
        $this->assertInstanceOf(StatusItemMetrics::class, $metrics);
        $this->assertInstanceOf(Carbon::class, $metrics->when_parsed);
        $this->assertEquals('2018-12-10', $metrics->when_parsed->toDateString());
        $this->assertEquals(0, $metrics->metrics_7num);
        $this->assertNull($metrics->metrics_7avg);
        $this->assertEquals(1, $metrics->metrics_14num);
        $this->assertEquals(42.00, $metrics->metrics_14avg);
        $this->assertEquals(6, $metrics->metrics_30num);
        $this->assertEquals(7.83, $metrics->metrics_30avg);
        $this->assertEquals(25, $metrics->metrics_90num);
        $this->assertEquals(2.68, $metrics->metrics_90avg);
    }

    /**
     * Test the Incidents model
     * @return void
     */
    public function testModelIncidents(): void
    {
        /* Current */
        $test = Incidents::with(['statusItem', 'check', 'ledger'])->where([
            ['group_id', '=', 2],
            ['date_ref', '=', 19362],
            ['incident_id', '=', 2],
        ])->get()->first();

        $this->assertEquals('critical', $test->status);
        $this->assertEquals('2019-12-28 04:10:07', $test->started);
        $this->assertEquals('Site Slow', $test->title);
        $this->assertEquals(
            'The request took longer to complete than our threshold for concern.',
            $test->detail
        );
        $this->assertNull($test->resolved);
        $this->assertEquals(1, $test->is_current);
        $this->assertEquals('SP-1936202', $test->ref_code);
        $this->assertEquals('<span class="icon_right icon_right_status_critical">Critical</span>', $test->severity);

        $this->assertEquals(5, $test->status_id);
        $this->assertEquals('Home', $test->statusItem->name);
        $this->assertEquals('critical', $test->statusItem->status);

        $this->assertEquals(19, $test->check_id);
        $this->assertEquals('Sports Requests', $test->check->name);
        $this->assertEquals(1, $test->check->last_summary);

        $this->assertEquals('Ongoing', $test->duration);

        $this->assertTrue($test->renderLedgerChanges());
        $this->assertEquals(3, $test->ledgerChanges()->count());

        /* Old */
        $test = Incidents::where([
            ['group_id', '=', 1],
            ['date_ref', '=', 19352],
            ['incident_id', '=', 5],
        ])->get()->first();

        $this->assertEquals('critical', $test->status);
        $this->assertEquals('2019-12-18 22:10:04', $test->started);
        $this->assertEquals('2019-12-18 23:10:03', $test->resolved);
        $this->assertEquals('Site Slow', $test->title);
        $this->assertEquals(
            'The request took longer to complete than our acceptable response time policy.',
            $test->detail
        );
        $this->assertEquals(0, $test->is_current);
        $this->assertEquals('DBR-1935205', $test->ref_code);
        $this->assertEquals('<span class="icon_right icon_right_status_critical">Critical</span>', $test->severity);

        $this->assertNotEquals('Ongoing', $test->duration);

        $this->assertFalse($test->renderLedgerChanges());
        $this->assertEquals(2, $test->ledgerChanges()->count());

        /* Empty (in-memory) */
        $test = new Incidents();
        // All the "unknowns".
        $this->assertEquals('status_unknown', $test->icon);
    }

    /**
     * Test the IncidentsLedger model
     * @return void
     */
    public function testModelIncidentsLedger(): void
    {
        /* Resolved */
        $test_all = IncidentsLedger::with(['incident', 'statusItem'])->where([
            ['group_id', '=', 2],
            ['date_ref', '=', 19362],
            ['incident_id', '=', 1],
        ])->get();

        // 1: Okay -> Warn.
        $test = $test_all->firstWhere('ledger_id', 1);
        $this->assertEquals('SP-1936201', $test->incident->ref_code);
        $this->assertEquals('Home', $test->statusItem->name);
        $this->assertEquals('okay', $test->status_from);
        $this->assertEquals('warn', $test->status_to);
        $this->assertEquals('2019-12-28 01:10:03', $test->status_changed);
        $this->assertEquals('Incident raised with severity <em>Warning</em>.', $test->detail);

        // 2: Warn -> Recovered.
        $test = $test_all->firstWhere('ledger_id', 2);
        $this->assertEquals('SP-1936201', $test->incident->ref_code);
        $this->assertEquals('Home', $test->statusItem->name);
        $this->assertEquals('warn', $test->status_from);
        $this->assertEquals('recovered', $test->status_to);
        $this->assertEquals('2019-12-28 03:10:04', $test->status_changed);
        $this->assertEquals('Incident resolved.', $test->detail);

        /* Still Open */
        $test_all = IncidentsLedger::with(['incident', 'statusItem'])->where([
            ['group_id', '=', 2],
            ['date_ref', '=', 19362],
            ['incident_id', '=', 2],
        ])->get();

        // 1: Okay -> Warn.
        $test = $test_all->firstWhere('ledger_id', 1);
        $this->assertEquals('SP-1936202', $test->incident->ref_code);
        $this->assertEquals('Home', $test->statusItem->name);
        $this->assertEquals('okay', $test->status_from);
        $this->assertEquals('warn', $test->status_to);
        $this->assertEquals('2019-12-28 04:10:07', $test->status_changed);
        $this->assertEquals('Incident raised with severity <em>Warning</em>.', $test->detail);

        // 2: Warn -> Critical.
        $test = $test_all->firstWhere('ledger_id', 2);
        $this->assertEquals('SP-1936202', $test->incident->ref_code);
        $this->assertEquals('Home', $test->statusItem->name);
        $this->assertEquals('warn', $test->status_from);
        $this->assertEquals('critical', $test->status_to);
        $this->assertEquals('2019-12-28 16:10:03', $test->status_changed);
        $this->assertEquals('Severity upgraded from <em>Warning</em> to <em>Critical</em>.', $test->detail);

        // 3: Critical -> Warn.
        $test = $test_all->firstWhere('ledger_id', 3);
        $this->assertEquals('SP-1936202', $test->incident->ref_code);
        $this->assertEquals('Home', $test->statusItem->name);
        $this->assertEquals('critical', $test->status_from);
        $this->assertEquals('warn', $test->status_to);
        $this->assertEquals('2019-12-28 17:10:04', $test->status_changed);
        $this->assertEquals('Severity downgraded from <em>Critical</em> to <em>Warning</em>.', $test->detail);

        /* Empty (in-memory) */
        $test = new IncidentsLedger();
        // All the "unknowns".
        $this->assertEquals('Unknown reason.', $test->detail);
    }
}
