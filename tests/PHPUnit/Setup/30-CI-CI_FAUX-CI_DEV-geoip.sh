#!/bin/bash
# Download the latest GeoIP test data from the data server
dir_root=$(realpath $(dirname $0)/../..)
dir_scripts=$(realpath $dir_root/_scripts)
dir_vendor="$(realpath $dir_root/..)/vendor" # Do not realpath a potential vendor symlink
dir_setup="$dir_vendor/debear/setup"

echo -n "Downloading GeoIP Unit Tests: "
tests_dir="$dir_setup/geoip"
tests_flag="$tests_dir/.cache.tests"
tests_file="$tests_dir/tests.json.gz"

# Where does the final config get saved to?
ci_config_subdir=
git_repo=$($dir_scripts/git.sh repo)
if [ "x$git_repo" = 'xskeleton' ]
then
    ci_config_subdir='debear/'
fi
ci_config="$dir_root/../config/${ci_config_subdir}_ci.php"

if [ ! -e $tests_dir ]
then
    mkdir $tests_dir
fi

# Pass 1: Check via the MD5 cache if we actually need to download the file?
if [ -e $tests_flag ]
then
    tests_md5=$(cat $tests_flag)
fi
tests_HEAD=$($dir_scripts/download.sh geoip_tests - - --header "X-Cache:$tests_md5" --head --output /dev/null --write-out %{response_code}:%header{x-cache} 2>&1)
tests_status=$(echo "$tests_HEAD" | awk -F':' '{ print $1 }')
tests_md5_new=$(echo "$tests_HEAD" | awk -F':' '{ print $2 }')

# Pass 2: If it returns a 200, we need to download
if [ "x$tests_status" = 'x200' ]
then
    # Pass 2: Download
    rm -rf $tests_file
    $dir_scripts/download.sh geoip_tests - - --header "X-Cache:$tests_md5" --output $tests_file
    if [ ! -s $tests_file ]
    then
        echo "[ Failed ]"
        exit 1
    fi

    # Process
    echo -n "$tests_md5_new" >$tests_flag
    echo "[ Done ]"
# If a 204 was returned, we don't need to download the file
elif [ "x$tests_status" = 'x204' ]
then
    echo "[ Up-to-date ($tests_status) ]"
# Some kind of error occurred that we should consider fatal
else
    echo "[ Failed ($tests_status) ]"
    exit 1
fi

# Now we have the file, write it as our config/_env.php file
cat <<EOF >$ci_config
<?php

return [
    'tests' => [
        'geoip' => $(gzip -dc $tests_file)
    ],
];
EOF
