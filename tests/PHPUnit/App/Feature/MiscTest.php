<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;

class MiscTest extends FeatureTestCase
{
    /**
     * A test of the manifest.json file
     * @return void
     */
    public function testManifest(): void
    {
        $response = $this->get('/manifest.json');
        $response->assertStatus(200);
        $response->assertJson([
            'name' => 'DeBear System Monitor',
            'short_name' => 'DeBear System Monitor',
            'description' => 'The latest status of DeBear.uk',
            'start_url' => '/?utm_source=homescreen',
            'icons' => [[
                'src' => 'https://static.debear.test/skel/images/meta/debear.png',
                'sizes' => '512x512',
                'type' => 'image/png',
            ]],
        ]);
    }
}
