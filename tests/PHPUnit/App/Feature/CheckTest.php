<?php

namespace Tests\PHPUnit\App\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;

class CheckTest extends FeatureTestCase
{
    /**
     * A test of the access rights.
     * @return void
     */
    public function testAccess(): void
    {
        // Hidden behind passworded page, so 401 on default load.
        $response = $this->get('/');
        $response->assertStatus(401);

        // Invalid Check.
        $response = $this->get('/check/disk-usage-1');
        $response->assertStatus(401);

        // Log in as standard (non-admin) user.
        $post = [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        $response = $this->get('/');
        $response->assertStatus(403);

        // Then log in as the special admin user.
        $response = $this->post('/logout', []);
        $response->assertStatus(200);

        $post = [
            'username' => 'sysmon_admin',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        $response = $this->get('/');
        $response->assertStatus(200);

        // Unknown Check.
        $response = $this->get('/check/disk-usage-0');
        $response->assertStatus(404);
        $response = $this->get('/check/disk-usage-2');
        $response->assertStatus(404);
        $response = $this->get('/check/unknown-102');
        $response->assertStatus(404);

        // Unknown Check page.
        $response = $this->get('/check/disk-usage-1/page/89');
        $response->assertStatus(404);
    }

    /**
     * A test of the homepage
     * @return void
     */
    public function testHome(): void
    {
        $post = [
            'username' => 'sysmon_admin',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        $response = $this->get('/');
        $response->assertStatus(200);

        // Some tests of test status.
        $response->assertSee('<span class="icon icon_status_okay">Okay: 25</span>');
        $response->assertSee('<span class="icon icon_status_warn">Warn: 2</span>');
        $response->assertSee('<span class="icon icon_status_critical">Critical: 2</span>');
        $response->assertSee('<li class="title row_critical">Syncs</li>');
        $response->assertSee('<li class="name row_critical icon_script_logs">Browser Reports');
        $response->assertSee('<li class="name row_okay icon_script_logs">News Warnings');
    }

    /**
     * A test of a 'server' check page
     * @return void
     */
    public function testCheckServer(): void
    {
        $post = [
            'username' => 'sysmon_admin',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        /* First Log Page */
        $response = $this->get('/check/data-server-4');
        $response->assertStatus(200);
        $response->assertSee(
            '<ul class="inline_list breadcrumbs"><li>DeBear System Monitor</li><li>Data Server</li></ul>'
        );
        $response->assertSee(
            '<top>
            <cell class="row_head main">Date</cell>
            <cell class="row_head summary status">Status</cell>
            <cell class="row_head summary notified">Notified</cell>
        </top>'
        );
        $response->assertSee(
            '<top>
                                    <cell class="row_head data ip">IP</cell>
                                    <cell class="row_head data state">State</cell>
                            </top>'
        );
        // First / Last rows.
        $response->assertDontSee('main">10th Dec 2018, 15:10</cell>');
        $response->assertSee('main">10th Dec 2018, 14:10</cell>');
        $response->assertSee('main">9th Dec 2018, 14:10</cell>');
        $response->assertDontSee('main">9th Dec 2018, 13:10</cell>');
        // Final page.
        $response->assertSee('<a href="/check/data-server-4/page/88" class="onclick_target" data-page="88">'
            . '<em>88</em></a>');
        // Next page.
        $response->assertSee('<a href="/check/data-server-4/page/2" class="onclick_target" data-page="2">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>');

        /* Last Log Page */
        $response = $this->get('/check/data-server-4/page/88');
        $response->assertStatus(200);
        // First / Last rows.
        $response->assertDontSee('main">11th Sep 2018, 01:10</cell>');
        $response->assertSee('main">11th Sep 2018, 00:10</cell>');
        $response->assertDontSee('main">10th Sep 2018, 23:10</cell>');
        // Previous page.
        $response->assertSee('<a href="/check/data-server-4/page/87" class="onclick_target" data-page="87">
                &laquo; Prev<span class="hidden-m">ious Page</span>
            </a>');
        $response->assertSee('<a href="/check/data-server-4/page/87" class="onclick_target" data-page="87">'
            . '87</a>');
        $response->assertDontSee('<a href="/check/data-server-4/page/89" class="onclick_target" data-page="89">'
            . '89</a>');
        // First page.
        $response->assertSee('<a href="/check/data-server-4/page/1" class="onclick_target" data-page="1">'
            . '<em>1</em></a>');
    }

    /**
     * A test of a 'usage' check page
     * @return void
     */
    public function testCheckUsage(): void
    {
        $post = [
            'username' => 'sysmon_admin',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        /* First Log Page */
        $response = $this->get('/check/disk-usage-1');
        $response->assertStatus(200);
        $response->assertSee(
            '<ul class="inline_list breadcrumbs"><li>DeBear System Monitor</li><li>Disk Usage</li></ul>'
        );
        $response->assertSee(
            '<top>
            <cell class="row_head main">Date</cell>
            <cell class="row_head summary status">Status</cell>
            <cell class="row_head summary notified">Notified</cell>
        </top>'
        );
        $response->assertSee(
            '<top>
                                    <cell class="row_head data total_usage">Total</cell>
                                    <cell class="row_head data remaining">Remaining</cell>
                                    <cell class="row_head data backup">Backups</cell>
                                    <cell class="row_head data logs">Logs</cell>
                                    <cell class="row_head data merges">Merges</cell>
                                    <cell class="row_head data sites_other">Sites</cell>
                                    <cell class="row_head data sites_cdn">CDN</cell>
                                    <cell class="row_head data db_sports">DB: Sports</cell>
                                    <cell class="row_head data db_fantasy">DB: Fantasy</cell>
                                    <cell class="row_head data db_other">DB: Rest</cell>
                                    <cell class="row_head data other">Other</cell>
                            </top>'
        );
        $response->assertDontSee('<cell class="row_head data email_debear">Email: DeBear</cell>');
        $response->assertDontSee('<cell class="row_head data email_padd">Email: pa-dd</cell>');
        // First / Last rows.
        $response->assertDontSee('main">11th Dec 2018</cell>');
        $response->assertSee('main">10th Dec 2018</cell>');
        $response->assertSee('main">16th Nov 2018</cell>');
        $response->assertDontSee('main">15th Nov 2018</cell>');
        // Final page.
        $response->assertSee('<a href="/check/disk-usage-1/page/4" class="onclick_target" data-page="4">4</a>');
        $response->assertDontSee('<a href="/check/6/page/5" class="onclick_target" data-page="5">5</a>');
        // Next page.
        $response->assertSee('<a href="/check/disk-usage-1/page/2" class="onclick_target" data-page="2">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>');

        /* Last Log Page */
        $response = $this->get('/check/disk-usage-1/page/4');
        $response->assertStatus(200);
        // First / Last rows.
        $response->assertDontSee('main">27th Sep 2018</cell>');
        $response->assertSee('main">26th Sep 2018</cell>');
        $response->assertSee('main">11th Sep 2018</cell>');
        $response->assertDontSee('main">10th Sep 2018</cell>');
        // Previous page.
        $response->assertSee('<a href="/check/disk-usage-1/page/3" class="onclick_target" data-page="3">
                &laquo; Prev<span class="hidden-m">ious Page</span>
            </a>');
        $response->assertSee('<a href="/check/disk-usage-1/page/3" class="onclick_target" data-page="3">3</a>');
        $response->assertDontSee('<a href="/check/disk-usage-1/page/5" class="onclick_target" data-page="5">5</a>');
        // First page.
        $response->assertSee('<a href="/check/disk-usage-1/page/1" class="onclick_target" data-page="1">1</a>');
    }

    /**
     * A test of a 'comms' check page
     * @return void
     */
    public function testCheckComms(): void
    {
        $post = [
            'username' => 'sysmon_admin',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        /* First Log Page */
        $response = $this->get('/check/emails-sent-2');
        $response->assertStatus(200);
        $response->assertSee(
            '<ul class="inline_list breadcrumbs"><li>DeBear System Monitor</li><li>Emails Sent</li></ul>'
        );
        $response->assertSee(
            '<top>
            <cell class="row_head main">Date</cell>
            <cell class="row_head summary status">Status</cell>
            <cell class="row_head summary notified">Notified</cell>
        </top>'
        );
        $response->assertSee(
            '<top>
                                    <cell class="row_head data num_queued">Queued</cell>
                                    <cell class="row_head data num_sent">Sent</cell>
                                    <cell class="row_head data num_failed">Failed</cell>
                                    <cell class="row_head data num_other">Other</cell>
                            </top>'
        );
        // First / Last rows.
        $response->assertDontSee('main">10th Dec 2018</cell>');
        $response->assertSee('main">9th Dec 2018</cell>');
        $response->assertSee('main">15th Nov 2018</cell>');
        $response->assertDontSee('main">14th Nov 2018</cell>');
        // Final page.
        $response->assertSee('<a href="/check/emails-sent-2/page/4" class="onclick_target" data-page="4">4</a>');
        $response->assertDontSee('<a href="/check/6/page/5" class="onclick_target" data-page="5">5</a>');
        // Next page.
        $response->assertSee('<a href="/check/emails-sent-2/page/2" class="onclick_target" data-page="2">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>');

        /* Last Log Page */
        $response = $this->get('/check/emails-sent-2/page/4');
        $response->assertStatus(200);
        // First / Last rows.
        $response->assertDontSee('main">26th Sep 2018</cell>');
        $response->assertSee('main">25th Sep 2018</cell>');
        $response->assertSee('main">11th Sep 2018</cell>');
        $response->assertDontSee('main">10th Sep 2018</cell>');
        // Previous page.
        $response->assertSee('<a href="/check/emails-sent-2/page/3" class="onclick_target" data-page="3">
                &laquo; Prev<span class="hidden-m">ious Page</span>
            </a>');
        $response->assertSee('<a href="/check/emails-sent-2/page/3" class="onclick_target" data-page="3">3</a>');
        $response->assertDontSee('<a href="/check/emails-sent-2/page/5" class="onclick_target" data-page="5">5</a>');
        // First page.
        $response->assertSee('<a href="/check/emails-sent-2/page/1" class="onclick_target" data-page="1">1</a>');
    }

    /**
     * A test of a 'lockfile' check page
     * @return void
     */
    public function testCheckLockfile(): void
    {
        $post = [
            'username' => 'sysmon_admin',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        /* First Log Page */
        $response = $this->get('/check/nfl-lockfile-6');
        $response->assertStatus(200);
        $response->assertSee(
            '<ul class="inline_list breadcrumbs"><li>DeBear System Monitor</li><li>NFL Lockfile</li></ul>'
        );
        $response->assertSee(
            '<top>
            <cell class="row_head main">Date</cell>
            <cell class="row_head summary status">Status</cell>
            <cell class="row_head summary notified">Notified</cell>
        </top>'
        );
        $response->assertSee(
            '<top>
                                    <cell class="row_head data state">State</cell>
                            </top>'
        );
        // First / Last rows.
        $response->assertDontSee('main">11th Dec 2018</cell>');
        $response->assertSee('main">10th Dec 2018</cell>');
        $response->assertSee('main">16th Nov 2018</cell>');
        $response->assertDontSee('main">15th Nov 2018</cell>');
        // Final page.
        $response->assertSee('<a href="/check/nfl-lockfile-6/page/4" class="onclick_target" data-page="4">4</a>');
        $response->assertDontSee('<a href="/check/nfl-lockfile-6/page/5" class="onclick_target" data-page="5">5</a>');
        // Next page.
        $response->assertSee('<a href="/check/nfl-lockfile-6/page/2" class="onclick_target" data-page="2">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>');

        /* Last Log Page */
        $response = $this->get('/check/nfl-lockfile-6/page/4');
        $response->assertStatus(200);
        // First / Last rows.
        $response->assertDontSee('main">27th Sep 2018</cell>');
        $response->assertSee('main">26th Sep 2018</cell>');
        $response->assertSee('main">11th Sep 2018</cell>');
        $response->assertDontSee('main">10th Sep 2018</cell>');
        // Previous page.
        $response->assertSee('<a href="/check/nfl-lockfile-6/page/3" class="onclick_target" data-page="3">
                &laquo; Prev<span class="hidden-m">ious Page</span>
            </a>');
        $response->assertSee('<a href="/check/nfl-lockfile-6/page/3" class="onclick_target" data-page="3">3</a>');
        $response->assertDontSee('<a href="/check/nfl-lockfile-6/page/5" class="onclick_target" data-page="5">5</a>');
        // First page.
        $response->assertSee('<a href="/check/nfl-lockfile-6/page/1" class="onclick_target" data-page="1">1</a>');
    }

    /**
     * A test of a 'logfile' check page
     * @return void
     */
    public function testCheckLogfile(): void
    {
        $post = [
            'username' => 'sysmon_admin',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        /* Browser Reports: First Log Page */
        $response = $this->get('/check/browser-reports-12');
        $response->assertStatus(200);
        $response->assertSee(
            '<ul class="inline_list breadcrumbs"><li>DeBear System Monitor</li><li>Browser Reports</li></ul>'
        );
        $response->assertSee(
            '<top>
            <cell class="row_head main">Date</cell>
            <cell class="row_head summary status">Status</cell>
            <cell class="row_head summary notified">Notified</cell>
        </top>'
        );
        $response->assertSee(
            '<top>
                                    <cell class="row_head data errors">Errors</cell>
                                    <cell class="row_head data detail">Detail</cell>
                            </top>'
        );
        // First / Last rows.
        $response->assertDontSee('main">10th Dec 2018</cell>');
        $response->assertSee('main">9th Dec 2018</cell>');
        $response->assertSee('main">15th Nov 2018</cell>');
        $response->assertDontSee('main">14th Nov 2018</cell>');
        // Final page.
        $response->assertSee('<a href="/check/browser-reports-12/page/4" class="onclick_target" data-page="4">4</a>');
        $response->assertDontSee('<a href="/check/browser-reports-12/page/5" class="onclick_target" data-page="5">'
            . '5</a>');
        // Next page.
        $response->assertSee('<a href="/check/browser-reports-12/page/2" class="onclick_target" data-page="2">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>');

        /* Browser Reports: Last Log Page */
        $response = $this->get('/check/browser-reports-12/page/4');
        $response->assertStatus(200);
        // First / Last rows.
        $response->assertDontSee('main">26th Sep 2018</cell>');
        $response->assertSee('main">25th Sep 2018</cell>');
        $response->assertSee('main">11th Sep 2018</cell>');
        $response->assertDontSee('main">10th Sep 2018</cell>');
        // Previous page.
        $response->assertSee('<a href="/check/browser-reports-12/page/3" class="onclick_target" data-page="3">
                &laquo; Prev<span class="hidden-m">ious Page</span>
            </a>');
        $response->assertSee('<a href="/check/browser-reports-12/page/3" class="onclick_target" data-page="3">3</a>');
        $response->assertDontSee('<a href="/check/browser-reports-12/page/5" class="onclick_target" data-page="5">'
            . '5</a>');
        // First page.
        $response->assertSee('<a href="/check/browser-reports-12/page/1" class="onclick_target" data-page="1">1</a>');

        /* Other Types */
        // News.
        $response = $this->get('/check/news-warnings-10');
        $response->assertStatus(200);
        // Weather.
        $response = $this->get('/check/weather-warnings-11');
        $response->assertStatus(200);
        // This has an added wrinkle of empty charts.
        $response->assertSee('"series":[{"name":"Weather Warnings","data":[null');
        $response->assertSee('"data":[{"name":"Weather Warnings","y":0}]}]');
        // Internal Status.
        $response = $this->get('/check/internal-status-100');
        $response->assertStatus(200);
        // Suspicious Activity.
        $response = $this->get('/check/suspicious-activity-14');
        $response->assertStatus(200);
        // Other (Sports: NHL Events).
        $response = $this->get('/check/nhl-game-events-13');
        $response->assertStatus(200);
    }

    /**
     * A test of an 'age' check page
     * @return void
     */
    public function testCheckAge(): void
    {
        $post = [
            'username' => 'sysmon_admin',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        /* Only Log Page */
        $response = $this->get('/check/geoip-database-23');
        $response->assertStatus(200);
        $response->assertSee(
            '<ul class="inline_list breadcrumbs"><li>DeBear System Monitor</li><li>GeoIP Database</li></ul>'
        );
        $response->assertSee(
            '<top>
            <cell class="row_head main">Checked</cell>
            <cell class="row_head summary status">Status</cell>
            <cell class="row_head summary notified">Notified</cell>
        </top>'
        );
        $response->assertSee(
            '<top>
                                    <cell class="row_head data ref_date">File Date</cell>
                                    <cell class="row_head data age">Age</cell>
                            </top>'
        );
        // First / Last rows.
        $response->assertDontSee('main">11th Dec 2018</cell>');
        $response->assertSee('main">10th Dec 2018</cell>');
        $response->assertSee('main">26th Nov 2018</cell>');
        $response->assertDontSee('main">25th Nov 2018</cell>');
        // Final page.
        $response->assertSee('<li class="page_title unshaded_row">
        Pages:
    </li>

            <li class="page_box unshaded_row ">
            <strong>1</strong>
        </li>
    ' . '
    </ul>');
    }

    /**
     * A test of a 'http_request' check page
     * @return void
     */
    public function testCheckHTTPRequest(): void
    {
        $post = [
            'username' => 'sysmon_admin',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        /* First Log Page */
        $response = $this->get('/check/sports-requests-19');
        $response->assertStatus(200);
        $response->assertSee(
            '<ul class="inline_list breadcrumbs"><li>DeBear System Monitor</li><li>Sports Requests</li></ul>'
        );
        $response->assertSee(
            '<top>
            <cell class="row_head main">Date</cell>
            <cell class="row_head summary status">Status</cell>
            <cell class="row_head summary notified">Notified</cell>
        </top>'
        );
        $response->assertSee(
            '<top>
                                    <cell class="row_head data path">Path</cell>
                                    <cell class="row_head data status_expected">Expected</cell>
                                    <cell class="row_head data status_returned">Returned</cell>
                                    <cell class="row_head data content_length">Length</cell>
                                    <cell class="row_head data duration">Duration</cell>
                                    <cell class="row_head data state">State</cell>
                            </top>'
        );
        // First / Last rows.
        $response->assertDontSee('main">10th Dec 2018, 07:10</cell>');
        $response->assertSee('main">10th Dec 2018, 06:10</cell>');
        $response->assertSee('main">10th Dec 2018, 04:10</cell>');
        $response->assertDontSee('main">10th Dec 2018, 03:10</cell>');
        // Final page.
        $response->assertSee('<a href="/check/sports-requests-19/page/10" class="onclick_target" data-page="10">'
            . '10</a>');
        // Next page.
        $response->assertSee('<a href="/check/sports-requests-19/page/2" class="onclick_target" data-page="2">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>');

        /* Last Log Page */
        $response = $this->get('/check/sports-requests-19/page/10');
        $response->assertStatus(200);
        // First / Last rows.
        $response->assertDontSee('main">9th Dec 2018, 11:10</cell>');
        $response->assertSee('main">9th Dec 2018, 10:10</cell>');
        $response->assertDontSee('main">9th Dec 2018, 09:10</cell>');
        // Previous page.
        $response->assertSee('<a href="/check/sports-requests-19/page/9" class="onclick_target" data-page="9">
                &laquo; Prev<span class="hidden-m">ious Page</span>
            </a>');
        $response->assertSee('<a href="/check/sports-requests-19/page/9" class="onclick_target" data-page="9">9</a>');
        $response->assertDontSee('<a href="/check/sports-requests-19/page/11" class="onclick_target" data-page="11">'
            . '11</a>');
        // First page.
        $response->assertSee('<a href="/check/sports-requests-19/page/1" class="onclick_target" data-page="1">1</a>');
    }

    /**
     * A test of a 'http_status' check page
     * @return void
     */
    public function testCheckHTTPStatus(): void
    {
        $post = [
            'username' => 'sysmon_admin',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        /* First Log Page */
        $response = $this->get('/check/my-http-status-28');
        $response->assertStatus(200);
        $response->assertSee(
            '<ul class="inline_list breadcrumbs"><li>DeBear System Monitor</li><li>My HTTP Status</li></ul>'
        );
        $response->assertSee(
            '<top>
            <cell class="row_head main">Date</cell>
            <cell class="row_head summary status">Status</cell>
            <cell class="row_head summary notified">Notified</cell>
        </top>'
        );
        $response->assertSee(
            '<top>
                                    <cell class="row_head data tot_requests">Requests</cell>
                                    <cell class="row_head data tot_1xx">1xx</cell>
                                    <cell class="row_head data tot_2xx">2xx</cell>
                                    <cell class="row_head data num_200">200</cell>
                                    <cell class="row_head data num_201">201</cell>
                                    <cell class="row_head data num_204">204</cell>
                                    <cell class="row_head data tot_3xx">3xx</cell>
                                    <cell class="row_head data num_301">301</cell>
                                    <cell class="row_head data num_302">302</cell>
                                    <cell class="row_head data num_304">304</cell>
                                    <cell class="row_head data num_307">307</cell>
                                    <cell class="row_head data num_308">308</cell>
                                    <cell class="row_head data tot_4xx">4xx</cell>
                                    <cell class="row_head data num_400">400</cell>
                                    <cell class="row_head data num_401">401</cell>
                                    <cell class="row_head data num_403">403</cell>
                                    <cell class="row_head data num_404">404</cell>
                                    <cell class="row_head data num_405">405</cell>
                                    <cell class="row_head data tot_5xx">5xx</cell>
                                    <cell class="row_head data num_500">500</cell>
                                    <cell class="row_head data num_501">501</cell>
                                    <cell class="row_head data tot_other">Other</cell>
                            </top>'
        );
        // First / Last rows.
        $response->assertDontSee('main">11th Dec 2018</cell>');
        $response->assertSee('main">10th Dec 2018</cell>');
        $response->assertSee('main">16th Nov 2018</cell>');
        $response->assertDontSee('main">15th Nov 2018</cell>');
        // Final page.
        $response->assertSee('<a href="/check/my-http-status-28/page/2" class="onclick_target" data-page="2">'
            . '2</a>');
        // Final/Next page.
        $response->assertSee('<a href="/check/my-http-status-28/page/2" class="onclick_target" data-page="2">
                Last<span class="hidden-m"> Page</span> &raquo;
            </a>');

        /* Last Log Page */
        $response = $this->get('/check/my-http-status-28/page/2');
        $response->assertStatus(200);
        // First / Last rows.
        $response->assertDontSee('main">16th Nov 2018</cell>');
        $response->assertSee('main">15th Nov 2018</cell>');
        $response->assertSee('main">14th Nov 2018</cell>');
        $response->assertDontSee('main">13th Nov 2018</cell>');
        // Previous page.
        $response->assertSee('<a href="/check/my-http-status-28/page/1" class="onclick_target" data-page="1">
                &laquo; First<span class="hidden-m"> Page</span>
            </a>');
    }

    /**
     * A test of an individual logs page
     * @return void
     */
    public function testLogs(): void
    {
        /* Valid User */
        $post = [
            'username' => 'sysmon_admin',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        /* First Log Page */
        $response = $this->get('/check/internal-status-100/logs');
        $response->assertStatus(200);
        $response->assertSee(
            '<ul class="inline_list breadcrumbs"><li>DeBear System Monitor</li>'
            . '<li>Internal Status</li><li>Logs</li></ul>'
        );
        $response->assertSee(
            '<li class="date row_head">Log Date</li>
    <li class="status row_head">Status</li>
    <li class="notif row_head">Notification</li>
    <li class="info row_head">Info</li>'
        );
        // First / Last rows.
        $response->assertDontSee('9th Dec 2018, 15:00');
        $response->assertSee('9th Dec 2018, 14:00');
        $response->assertSee('7th Dec 2018, 13:00');
        $response->assertDontSee('7th Dec 2018, 12:00');
        // Final page.
        $response->assertSee('<a href="/check/internal-status-100/logs/page/44" class="onclick_target" data-page="44">'
            . '<em>44</em></a>');
        // Next page.
        $response->assertSee('<a href="/check/internal-status-100/logs/page/2" class="onclick_target" data-page="2">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>');

        /* Last Log Page */
        $response = $this->get('/check/internal-status-100/logs/page/44');
        $response->assertStatus(200);
        // First / Last rows.
        $response->assertDontSee('11th Sep 2018, 01:00');
        $response->assertSee('11th Sep 2018, 00:00');
        $response->assertDontSee('10th Sep 2018, 23:00');
        // Previous page.
        $response->assertSee('<a href="/check/internal-status-100/logs/page/43" class="onclick_target" data-page="43">
                &laquo; Prev<span class="hidden-m">ious Page</span>
            </a>');
        $response->assertSee('<a href="/check/internal-status-100/logs/page/43" class="onclick_target" data-page="43">'
            . '43</a>');
        $response->assertDontSee('<a href="/check/internal-status-100/logs/page/45" class="onclick_target" '
            . 'data-page="45">45</a>');
        // First page.
        $response->assertSee('<a href="/check/internal-status-100/logs/page/1" class="onclick_target" data-page="1">'
            . '<em>1</em></a>');

        // Beyond the last page.
        $response = $this->get('/check/internal-status-100/logs/page/45');
        $response->assertStatus(404);

        /* Invalid Check Passed */
        $response = $this->get('/check/internal-status-102/logs');
        $response->assertStatus(404);
        $response = $this->get('/check/unknown-100/logs');
        $response->assertStatus(404);
        $response = $this->get('/check/unknown-102/logs');
        $response->assertStatus(404);

        /* No User */
        $response = $this->post('/logout', []);
        $response->assertStatus(200);

        $response = $this->get('/check/internal-status-100/logs');
        $response->assertStatus(401);

        /* Invalid User */
        $post = [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);
        $response->assertStatus(200);

        $response = $this->get('/check/internal-status-100/logs');
        $response->assertStatus(403);
    }
}
