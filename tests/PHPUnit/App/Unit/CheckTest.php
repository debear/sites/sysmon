<?php

namespace Tests\PHPUnit\App\Unit;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use DeBear\Models\SysMon\CheckGroups;
use DeBear\Models\SysMon\Checks;
use DeBear\Models\SysMon\ChecksComms;
use DeBear\Models\SysMon\ChecksHTTPRequest;
use DeBear\Models\SysMon\ChecksHTTPStatus;
use DeBear\Models\SysMon\ChecksLockfile;
use DeBear\Models\SysMon\ChecksLogfile;
use DeBear\Models\SysMon\ChecksLogfileBreakdown;
use DeBear\Models\SysMon\ChecksServer;
use DeBear\Models\SysMon\ChecksUsage;
use DeBear\Models\SysMon\Logs;
use DeBear\Exceptions\SysMon\IncompatibleCheckException;

class CheckTest extends UnitTestCase
{
    /**
     * Test the check grouping
     * @return void
     */
    public function testGroups(): void
    {
        // Headline info.
        $all = CheckGroups::all();
        $this->assertEquals(9, sizeof($all));
        $internal = $all->where('internal', '=', 1);
        $this->assertEquals(1, sizeof($internal));
        $this->assertEquals(6, $internal->first()->group_id);
        $external = $all->where('internal', '=', 0);
        $this->assertEquals(8, sizeof($external));

        // Per-group checks.
        $expected = [
            1 => 3,
            2 => 2,
            3 => 4,
            4 => 5,
            5 => 0,
            6 => 1,
            7 => 7,
            8 => 4,
            9 => 3,
        ];
        foreach ($expected as $group_id => $num_groups) {
            $group = $all->find($group_id);
            $this->assertEquals($group_id, $group->group_id);
            $this->assertEquals($num_groups, sizeof($group->checks));
        }
    }

    /**
     * Test an individual 'usage' check
     * @return void
     */
    public function testUsage(): void
    {
        // Headline info.
        $check = Checks::find(1);
        $this->assertEquals(1, $check->check_id);
        $this->assertEquals(1, $check->groupDetails->group_id);
        $this->assertNull($check->notificationsSnooze);
        $this->assertEquals('61%', $check->formatSummary());

        // URLs.
        $this->assertEquals('/check/disk-usage-1', $check->linkDetail());
        $this->assertEquals('/check/disk-usage-1/logs', $check->linkLogs());

        // Logs.
        $logs = $check->logs;
        $this->assertInstanceOf(Collection::class, $logs);
        $this->assertEquals(91, sizeof($logs));
        $logs_first = $logs->sortBy('log_date')->first();
        $this->assertInstanceOf(Logs::class, $logs_first);
        $this->assertEquals(1, $logs_first->check->check_id);
        $this->assertEquals('2018-09-11 00:00:00', $logs_first->log_date);
        $this->assertEquals('11th Sep 2018', $logs_first->logDateFormat());
        $this->assertEquals('2018-09-11 04:10:02', $logs_first->started);
        $this->assertEquals('okay', $logs_first->status);
        $this->assertStringContainsString('Usage: 2991.5 MiB', $logs_first->info);

        // Details.
        $details = $check->details();
        $this->assertInstanceOf(Collection::class, $details);
        $this->assertEquals(91, sizeof($details));
        // Details Formatted Cols.
        $details_first = $details->first();
        $this->assertInstanceOf(ChecksUsage::class, $details_first);
        $this->assertEquals('2018-12-10', $details_first->checked->toDateString());
        $this->assertEquals('10th Dec 2018', $details_first->formatCol('checked'));
        $this->assertEquals(3066815615, $details_first->total_usage);
        $this->assertEquals('2.856 GiB', $details_first->formatCol('total_usage'));
        $this->assertEquals(1933184385, $details_first->remaining);
        $this->assertEquals('1.800 GiB', $details_first->formatCol('remaining'));
        $this->assertEquals(20058055, $details_first->email_debear);
        $this->assertEquals('19.129 MiB', $details_first->formatCol('email_debear'));
        $this->assertEquals(12345, $details_first->email_padd);
        $this->assertEquals('12.056 KiB', $details_first->formatCol('email_padd'));
        $this->assertEquals(244043776, $details_first->backup);
        $this->assertEquals('232.738 MiB', $details_first->formatCol('backup'));
        $this->assertEquals(14356480, $details_first->logs);
        $this->assertEquals('13.691 MiB', $details_first->formatCol('logs'));
        $this->assertEquals(61267968, $details_first->merges);
        $this->assertEquals('58.430 MiB', $details_first->formatCol('merges'));
        $this->assertEquals(347222016, $details_first->sites_cdn);
        $this->assertEquals('331.137 MiB', $details_first->formatCol('sites_cdn'));
        $this->assertEquals(153604096, $details_first->sites_other);
        $this->assertEquals('146.488 MiB', $details_first->formatCol('sites_other'));
        $this->assertEquals(2050151137, $details_first->db_sports);
        $this->assertEquals('1.909 GiB', $details_first->formatCol('db_sports'));
        $this->assertEquals(1381452, $details_first->db_fantasy);
        $this->assertEquals('1.317 MiB', $details_first->formatCol('db_fantasy'));
        $this->assertEquals(63884626, $details_first->db_other);
        $this->assertEquals('60.925 MiB', $details_first->formatCol('db_other'));
        $this->assertEquals(110833664, $details_first->other);
        $this->assertEquals('105.699 MiB', $details_first->formatCol('other'));

        // Display Columns.
        $exp_keys = [
            'checked',
            'total_usage',
            'remaining',
            // 'email_debear', Removed after move away from juliet.
            // 'email_padd', Removed after move away from juliet.
            'backup',
            'logs',
            'merges',
            'sites_other',
            'sites_cdn',
            'db_sports',
            'db_fantasy',
            'db_other',
            'other',
        ];
        $cols = $check->getColumns();
        $this->assertEquals(sizeof($exp_keys), sizeof($cols));
        $this->assertEquals($exp_keys, array_keys($cols));

        // Highcharts Object.
        $chart = $check->buildHighchartsObject('chart');
        $this->assertEquals('area', $chart['chart']['type']);

        // Details Breakdown (Exception).
        $this->expectException(IncompatibleCheckException::class);
        $check->detailsBreakdown(Carbon::now());
    }

    /**
     * Test an individual 'server' check
     * @return void
     */
    public function testServer(): void
    {
        // Headline info.
        $check = Checks::find(4);
        $this->assertEquals(4, $check->check_id);
        $this->assertEquals(1, $check->groupDetails->group_id);
        $this->assertNull($check->notificationsSnooze);
        $this->assertEquals('Up', $check->formatSummary());

        // URLs.
        $this->assertEquals('/check/data-server-4', $check->linkDetail());
        $this->assertEquals('/check/data-server-4/logs', $check->linkLogs());

        // Logs.
        $logs = $check->logs;
        $this->assertInstanceOf(Collection::class, $logs);
        $this->assertEquals(2176, sizeof($logs));
        $logs_first = $logs->sortBy('log_date')->first();
        $this->assertInstanceOf(Logs::class, $logs_first);
        $this->assertEquals(4, $logs_first->check->check_id);
        $this->assertEquals('2018-09-11 00:10:02', $logs_first->log_date);
        $this->assertEquals('11th Sep 2018, 00:10', $logs_first->logDateFormat());
        $this->assertEquals('2018-09-11 00:10:02', $logs_first->started);
        $this->assertEquals('okay', $logs_first->status);
        $this->assertStringContainsString('Host: data.debear.co.uk;', $logs_first->info);

        // Details.
        $details = $check->details();
        $this->assertInstanceOf(Collection::class, $details);
        $this->assertEquals(2176, sizeof($details));
        // Details Formatted Cols.
        $details_first = $details->first();
        $this->assertInstanceOf(ChecksServer::class, $details_first);
        $this->assertEquals('2018-12-10 14:10:09', $details_first->checked->toDateTimeString());
        $this->assertEquals('10th Dec 2018, 14:10', $details_first->formatCol('checked'));
        $this->assertEquals('2018-12-10', $details_first->checked_date->toDateString());
        $this->assertEquals('14:10:09', $details_first->checked_time);
        $this->assertEquals('94.0.108.86', $details_first->ip);
        $this->assertEquals('94.0.108.86', $details_first->formatCol('ip'));
        $this->assertEquals('up', $details_first->state);
        $this->assertEquals('<span class="icon icon_server_up">Up</span>', $details_first->formatCol('state'));

        // Display Columns.
        $exp_keys = [
            'checked',
            'ip',
            'state',
        ];
        $cols = $check->getColumns();
        $this->assertEquals(sizeof($exp_keys), sizeof($cols));
        $this->assertEquals($exp_keys, array_keys($cols));

        // Highcharts Object.
        $chart = $check->buildHighchartsObject('chart');
        $this->assertEquals('column', $chart['chart']['type']);

        // Details Breakdown (Exception).
        $this->expectException(IncompatibleCheckException::class);
        $check->detailsBreakdown(Carbon::now());
    }

    /**
     * Test an individual 'comms' check
     * @return void
     */
    public function testComms(): void
    {
        // Headline info.
        $check = Checks::find(2);
        $this->assertEquals(2, $check->check_id);
        $this->assertEquals(2, $check->groupDetails->group_id);
        $this->assertNull($check->notificationsSnooze);
        $this->assertEquals('Sent', $check->formatSummary());

        // URLs.
        $this->assertEquals('/check/emails-sent-2', $check->linkDetail());
        $this->assertEquals('/check/emails-sent-2/logs', $check->linkLogs());

        // Logs.
        $logs = $check->logs;
        $this->assertInstanceOf(Collection::class, $logs);
        $this->assertEquals(90, sizeof($logs));
        $logs_first = $logs->sortBy('log_date')->first();
        $this->assertInstanceOf(Logs::class, $logs_first);
        $this->assertEquals(2, $logs_first->check->check_id);
        $this->assertEquals('2018-09-11 00:00:00', $logs_first->log_date);
        $this->assertEquals('11th Sep 2018', $logs_first->logDateFormat());
        $this->assertEquals('2018-09-12 02:10:02', $logs_first->started);
        $this->assertEquals('okay', $logs_first->status);
        $this->assertStringContainsString('Sent: 2;', $logs_first->info);

        // Details.
        $details = $check->details();
        $this->assertInstanceOf(Collection::class, $details);
        $this->assertEquals(90, sizeof($details));
        // Details Formatted Cols.
        $details_first = $details->first();
        $this->assertInstanceOf(ChecksComms::class, $details_first);
        $this->assertEquals('2018-12-09', $details_first->date->toDateString());
        $this->assertEquals('9th Dec 2018', $details_first->formatCol('date'));
        $this->assertEquals(0, $details_first->num_queued);
        $this->assertEquals(0, $details_first->formatCol('num_queued'));
        $this->assertEquals(1, $details_first->num_sent);
        $this->assertEquals(1, $details_first->formatCol('num_sent'));
        $this->assertEquals(0, $details_first->num_failed);
        $this->assertEquals(0, $details_first->formatCol('num_failed'));
        $this->assertEquals(0, $details_first->num_other);
        $this->assertEquals(0, $details_first->formatCol('num_other'));

        // Display Columns.
        $exp_keys = [
            'date' ,
            'num_queued',
            'num_sent',
            'num_failed',
            'num_other',
        ];
        $cols = $check->getColumns();
        $this->assertEquals(sizeof($exp_keys), sizeof($cols));
        $this->assertEquals($exp_keys, array_keys($cols));

        // Highcharts Object.
        $chart = $check->buildHighchartsObject('chart');
        $this->assertEquals('column', $chart['chart']['type']);

        // Details Breakdown (Exception).
        $this->expectException(IncompatibleCheckException::class);
        $check->detailsBreakdown(Carbon::now());
    }

    /**
     * Test an individual 'lockfile' check
     * @return void
     */
    public function testLockfile(): void
    {
        // Headline info.
        $check = Checks::find(6);
        $this->assertEquals(6, $check->check_id);
        $this->assertEquals(4, $check->groupDetails->group_id);
        $this->assertNull($check->notificationsSnooze);
        $this->assertEquals('Valid', $check->formatSummary());

        // URLs.
        $this->assertEquals('/check/nfl-lockfile-6', $check->linkDetail());
        $this->assertEquals('/check/nfl-lockfile-6/logs', $check->linkLogs());

        // Logs.
        $logs = $check->logs;
        $this->assertInstanceOf(Collection::class, $logs);
        $this->assertEquals(91, sizeof($logs));
        $logs_first = $logs->sortBy('log_date')->first();
        $this->assertInstanceOf(Logs::class, $logs_first);
        $this->assertEquals(6, $logs_first->check->check_id);
        $this->assertEquals('2018-09-11 00:00:00', $logs_first->log_date);
        $this->assertEquals('11th Sep 2018', $logs_first->logDateFormat());
        $this->assertEquals('2018-09-11 12:10:06', $logs_first->started);
        $this->assertEquals('okay', $logs_first->status);
        $this->assertStringContainsString('Started: 2018-09-11 09:56:14;', $logs_first->info);

        // Details.
        $details = $check->details();
        $this->assertInstanceOf(Collection::class, $details);
        $this->assertEquals(91, sizeof($details));
        // Details Formatted Cols.
        $details_first = $details->first();
        $this->assertInstanceOf(ChecksLockfile::class, $details_first);
        $this->assertEquals('2018-12-10', $details_first->checked->toDateString());
        $this->assertEquals('10th Dec 2018', $details_first->formatCol('checked'));
        $this->assertEquals('valid', $details_first->state);
        $this->assertEquals('<span class="icon icon_lockfile_valid">Valid</span>', $details_first->formatCol('state'));

        // Display Columns.
        $exp_keys = [
            'checked',
            'state',
        ];
        $cols = $check->getColumns();
        $this->assertEquals(sizeof($exp_keys), sizeof($cols));
        $this->assertEquals($exp_keys, array_keys($cols));

        // Highcharts Object.
        $chart = $check->buildHighchartsObject('chart');
        $this->assertEquals('column', $chart['chart']['type']);

        // Details Breakdown (Exception).
        $this->expectException(IncompatibleCheckException::class);
        $check->detailsBreakdown(Carbon::now());
    }

    /**
     * Test an individual 'logfile' check
     * @return void
     */
    public function testLogfile(): void
    {
        // Headline info.
        $check = Checks::find(12);
        $this->assertEquals(12, $check->check_id);
        $this->assertEquals(3, $check->groupDetails->group_id);
        $this->assertNull($check->notificationsSnooze);
        $this->assertEquals('10 errors', $check->formatSummary());

        // URLs.
        $this->assertEquals('/check/browser-reports-12', $check->linkDetail());
        $this->assertEquals('/check/browser-reports-12/logs', $check->linkLogs());

        // Logs.
        $logs = $check->logs;
        $this->assertInstanceOf(Collection::class, $logs);
        $this->assertEquals(90, sizeof($logs));
        $logs_first = $logs->sortBy('log_date')->first();
        $this->assertInstanceOf(Logs::class, $logs_first);
        $this->assertEquals(12, $logs_first->check->check_id);
        $this->assertEquals('2018-09-11 00:00:00', $logs_first->log_date);
        $this->assertEquals('11th Sep 2018', $logs_first->logDateFormat());
        $this->assertEquals('2018-09-12 00:10:06', $logs_first->started);
        $this->assertEquals('critical', $logs_first->status);
        $this->assertStringContainsString('- php: 4', $logs_first->info);

        // Details.
        $details = $check->details();
        $this->assertInstanceOf(Collection::class, $details);
        $this->assertEquals(90, sizeof($details));
        // Details Formatted Cols.
        $details_first = $details->first();
        $this->assertInstanceOf(ChecksLogfile::class, $details_first);
        $this->assertEquals('2018-12-09 00:00:00', $details_first->checked->toDateTimeString());
        $this->assertEquals('9th Dec 2018', $details_first->formatCol('checked'));
        $this->assertEquals(10, $details_first->errors);
        $this->assertEquals(10, $details_first->formatCol('errors'));
        $this->assertEquals("By site:
- sports: 10\n
By type:
- php: 10\n
Raw List:
- 2018-12-09 21:19:13 - sports - /moto2/race-12 - php - 10\n", $details_first->detail);
        $this->assertStringContainsString("<pre>By site:
- sports: 10\n
By type:
- php: 10\n
Raw List:
- 2018-12-09 21:19:13 - sports - /moto2/race-12 - php - 10</pre>", $details_first->formatCol('detail'));

        // Details Breakdown.
        $breakdown = $check->detailsBreakdown(Carbon::createMidnightDate(2018, 11, 19));
        $this->assertInstanceOf(Collection::class, $breakdown);
        $this->assertEquals(22, sizeof($breakdown));
        $breakdown_day = $breakdown->where('checked', '=', '2018-11-19 00:00:00');
        // CSP.
        $breakdown_csp = $breakdown_day->where('section', '=', 'csp')->first();
        $this->assertInstanceOf(ChecksLogfileBreakdown::class, $breakdown_csp);
        $this->assertEquals('2018-11-19', $breakdown_csp->logfile->checked->toDateString());
        $this->assertEquals('2018-11-19', $breakdown_csp->checked->toDateString());
        $this->assertEquals('19th Nov 2018', $breakdown_csp->formatCol('checked'));
        $this->assertEquals(3, $breakdown_csp->errors);
        $this->assertEquals(3, $breakdown_csp->formatCol('errors'));
        // JavaScript.
        $breakdown_js = $breakdown_day->where('section', '=', 'js')->first();
        $this->assertInstanceOf(ChecksLogfileBreakdown::class, $breakdown_js);
        $this->assertEquals('2018-11-19', $breakdown_js->logfile->checked->toDateString());
        $this->assertEquals('2018-11-19', $breakdown_js->checked->toDateString());
        $this->assertEquals('19th Nov 2018', $breakdown_js->formatCol('checked'));
        $this->assertEquals(3, $breakdown_js->errors);
        $this->assertEquals(3, $breakdown_js->formatCol('errors'));
        // PHP.
        $breakdown_php = $breakdown_day->where('section', '=', 'php')->first();
        $this->assertInstanceOf(ChecksLogfileBreakdown::class, $breakdown_php);
        $this->assertEquals('2018-11-19', $breakdown_php->logfile->checked->toDateString());
        $this->assertEquals('2018-11-19', $breakdown_php->checked->toDateString());
        $this->assertEquals('19th Nov 2018', $breakdown_php->formatCol('checked'));
        $this->assertEquals(8, $breakdown_php->errors);
        $this->assertEquals(8, $breakdown_php->formatCol('errors'));

        // Display Columns.
        $exp_keys = [
            'checked',
            'errors',
            'detail',
        ];
        $cols = $check->getColumns();
        $this->assertEquals(sizeof($exp_keys), sizeof($cols));
        $this->assertEquals($exp_keys, array_keys($cols));

        // Highcharts Object.
        $chart = $check->buildHighchartsObject('chart');
        $this->assertEquals('column', $chart['chart']['type']);
        $this->assertStringContainsString('CSP', $chart['series'][0]['name']);
        $this->assertStringContainsString('JS', $chart['series'][1]['name']);
        $this->assertStringContainsString('PHP', $chart['series'][2]['name']);
    }

    /**
     * Test an individual 'http_request' check
     * @return void
     */
    public function testHTTPRequest(): void
    {
        // Headline info.
        $check = Checks::find(19);
        $this->assertEquals(19, $check->check_id);
        $this->assertEquals(8, $check->groupDetails->group_id);
        $this->assertNull($check->notificationsSnooze);
        $this->assertEquals('1 error', $check->formatSummary());

        // URLs.
        $this->assertEquals('/check/sports-requests-19', $check->linkDetail());
        $this->assertEquals('/check/sports-requests-19/logs', $check->linkLogs());

        // Logs.
        $logs = $check->logs;
        $this->assertInstanceOf(Collection::class, $logs);
        $this->assertEquals(21, sizeof($logs));
        $logs_first = $logs->sortBy('log_date')->first();
        $this->assertInstanceOf(Logs::class, $logs_first);
        $this->assertEquals(19, $logs_first->check->check_id);
        $this->assertEquals('2018-12-09 10:10:06', $logs_first->log_date);
        $this->assertEquals('9th Dec 2018, 10:10', $logs_first->logDateFormat());
        $this->assertEquals('2018-12-09 10:10:05', $logs_first->started);
        $this->assertEquals('critical', $logs_first->status);
        $this->assertStringContainsString('/: 200, 0.750s: okay', $logs_first->info);

        // Details.
        $details = $check->details();
        $this->assertInstanceOf(Collection::class, $details);
        $this->assertEquals(231, sizeof($details));
        // Details Formatted Cols.
        $details_first = $details->first();
        $this->assertInstanceOf(ChecksHTTPRequest::class, $details_first);
        $this->assertEquals('2018-12-10', $details_first->checked->toDateString());
        $this->assertEquals('10th Dec 2018, 06:10', $details_first->formatCol('checked'));
        $this->assertEquals('slow', $details_first->state);
        $this->assertEquals('<span class="icon icon_http_slow">Slow</span>', $details_first->formatCol('state'));

        // Display Columns.
        $exp_keys = [
            'checked',
            'path',
            'status_expected',
            'status_returned',
            'content_length',
            'duration',
            'state',
        ];
        $cols = $check->getColumns();
        $this->assertEquals(sizeof($exp_keys), sizeof($cols));
        $this->assertEquals($exp_keys, array_keys($cols));

        // Highcharts Object.
        $chart = $check->buildHighchartsObject('chart');
        $this->assertEquals('column', $chart['chart']['type']);

        // Details Breakdown (Exception).
        $this->expectException(IncompatibleCheckException::class);
        $check->detailsBreakdown(Carbon::now());
    }

    /**
     * Test an individual 'http_status' check
     * @return void
     */
    public function testHTTPStatus(): void
    {
        // Headline info.
        $check = Checks::find(28);
        $this->assertEquals(28, $check->check_id);
        $this->assertEquals(7, $check->groupDetails->group_id);
        $this->assertNull($check->notificationsSnooze);
        $this->assertEquals('50% OK', $check->formatSummary());

        // URLs.
        $this->assertEquals('/check/my-http-status-28', $check->linkDetail());
        $this->assertEquals('/check/my-http-status-28/logs', $check->linkLogs());

        // Logs.
        $logs = $check->logs;
        $this->assertInstanceOf(Collection::class, $logs);
        $this->assertEquals(27, sizeof($logs));
        $logs_first = $logs->sortBy('log_date')->first();
        $this->assertInstanceOf(Logs::class, $logs_first);
        $this->assertEquals(28, $logs_first->check->check_id);
        $this->assertEquals('2018-11-14 00:00:00', $logs_first->log_date);
        $this->assertEquals('14th Nov 2018', $logs_first->logDateFormat());
        $this->assertEquals('2018-12-10 19:42:10', $logs_first->started);
        $this->assertEquals('okay', $logs_first->status);
        $this->assertStringContainsString('2xx: 2, 5xx: 2, 2xx %age: 50.00%', $logs_first->info);

        // Details.
        $details = $check->details();
        $this->assertInstanceOf(Collection::class, $details);
        $this->assertEquals(27, sizeof($details));
        // Details Formatted Cols.
        $details_first = $details->first();
        $this->assertInstanceOf(ChecksHTTPStatus::class, $details_first);
        $this->assertEquals('2018-12-10', $details_first->date->toDateString());
        $this->assertEquals('10th Dec 2018', $details_first->formatCol('date'));
        $this->assertEquals(4, $details_first->tot_requests);

        // Display Columns.
        $exp_keys = [
            'date',
            'tot_requests',
            'tot_1xx',
            'tot_2xx',
            'num_200',
            'num_201',
            'num_204',
            'tot_3xx',
            'num_301',
            'num_302',
            'num_304',
            'num_307',
            'num_308',
            'tot_4xx',
            'num_400',
            'num_401',
            'num_403',
            'num_404',
            'num_405',
            'tot_5xx',
            'num_500',
            'num_501',
            'tot_other',
        ];
        $cols = $check->getColumns();
        $this->assertEquals(sizeof($exp_keys), sizeof($cols));
        $this->assertEquals($exp_keys, array_keys($cols));

        // Highcharts Object.
        $chart = $check->buildHighchartsObject('chart');
        $this->assertEquals('pie', $chart['chart']['type']);

        // Details Breakdown (Exception).
        $this->expectException(IncompatibleCheckException::class);
        $check->detailsBreakdown(Carbon::now());
    }

    /**
     * Test the various permutations of Checks::formatSummary() due to awkwardness within Feature tests
     * @return void
     */
    public function testFormatSummary(): void
    {
        $check = new Checks();
        // (None).
        $this->assertEquals('<span>Unknown</span>', $check->formatSummary());
        // Usage.
        $check->script = 'usage';
        $check->last_summary = 100;
        $this->assertEquals('100%', $check->formatSummary());
        // Comms.
        $check->script = 'comms';
        $check->last_summary = 99;
        $this->assertEquals('<span>None</span>', $check->formatSummary());
        $check->last_summary = 3;
        $this->assertEquals('Other', $check->formatSummary());
        $check->last_summary = 2;
        $this->assertEquals('Queued', $check->formatSummary());
        $check->last_summary = 1;
        $this->assertEquals('Sent', $check->formatSummary());
        $check->last_summary = 0;
        $this->assertEquals('Failed', $check->formatSummary());
        // Lockfile.
        $check->script = 'lockfile';
        $check->last_summary = 2;
        $this->assertEquals('Missing', $check->formatSummary());
        $check->last_summary = 1;
        $this->assertEquals('Valid', $check->formatSummary());
        $check->last_summary = 0;
        $this->assertEquals('Failed', $check->formatSummary());
        // Server.
        $check->script = 'server';
        $check->last_summary = true;
        $this->assertEquals('Up', $check->formatSummary());
        $check->last_summary = false;
        $this->assertEquals('Down', $check->formatSummary());
        // Logs.
        $check->script = 'logs';
        $check->last_summary = 0;
        $this->assertEquals('0 errors', $check->formatSummary());
        $check->last_summary = 1;
        $this->assertEquals('1 error', $check->formatSummary());
        $check->last_summary = 2;
        $this->assertEquals('2 errors', $check->formatSummary());
        // HTTP Request.
        $check->script = 'http_request';
        $check->last_summary = 0;
        $this->assertEquals('0 errors', $check->formatSummary());
        $check->last_summary = 1;
        $this->assertEquals('1 error', $check->formatSummary());
        $check->last_summary = 2;
        $this->assertEquals('2 errors', $check->formatSummary());
        // HTTP Status.
        $check->script = 'http_status';
        $check->last_summary = 0;
        $this->assertEquals('0% OK', $check->formatSummary());
        $check->last_summary = 50;
        $this->assertEquals('50% OK', $check->formatSummary());
        $check->last_summary = 100;
        $this->assertEquals('100% OK', $check->formatSummary());
    }
}
