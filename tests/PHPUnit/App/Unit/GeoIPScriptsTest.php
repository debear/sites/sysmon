<?php

namespace Tests\PHPUnit\App\Unit;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Config;
use DeBear\Models\Skeleton\Country;

class GeoIPScriptsTest extends UnitTestCase
{
    /**
     * Original configuration before we modified it per-test.
     * @var array
     */
    protected array $config;

    /**
     * Test a lookup in the UK
     * @return void
     */
    public function testUnitedKingdom(): void
    {
        $this->assertWorker('gb');
    }

    /**
     * Test a lookup in France
     * @return void
     */
    public function testFrance(): void
    {
        $this->assertWorker('fr');
    }

    /**
     * Test a lookup in Russia
     * @return void
     */
    public function testRussia(): void
    {
        $this->assertWorker('ru');
    }

    /**
     * Test a lookup in the UAE
     * @return void
     */
    public function testUnitedArabEmirates(): void
    {
        $this->assertWorker('ae');
    }

    /**
     * Test a lookup in Japan
     * @return void
     */
    public function testJapan(): void
    {
        $this->assertWorker('jp');
    }

    /**
     * Test lookups in the United States
     * @return void
     */
    public function testUnitedStates(): void
    {
        $this->assertWorker('us');
    }

    /**
     * Over-loaded worker method for processing an IP
     * @param string $testKey The config key with the test details to run.
     * @return void
     */
    protected function assertWorker(string $testKey): void
    {
        // Get the details out of the tests to run.
        $tests = FrameworkConfig::get("debear.tests.geoip.$testKey");
        if (isset($tests['ip'])) {
            // In some instances, we have multiple tests per key, so ensure all inputs match this mechanism.
            $tests = [$tests];
        }

        // Run the GeoIP calcs and tests.
        $script = __DIR__ . '/../../../../scripts/pre-process/report_uri/_geoip.php';
        foreach ($tests as $test) {
            // Run the command.
            exec("php $script " . escapeshellarg($test['ip']), $output, $ret_code);

            // Return code okay?
            $this->assertEquals(0, $ret_code);

            // Returns a valid string?
            $output = join("\n", $output); // Convert to string.
            $this->assertGreaterThan(0, strlen($output));

            // Can we convert it to JSON?
            $geoip = json_decode($output, true);
            $this->assertIsArray($geoip);

            // Ensure we have City information available, guesstimated from the returned country's timezone.
            if (!isset($geoip['city'])) {
                // @codeCoverageIgnoreStart
                // For simplicity this is done here rather than in the lookup wrapper. As a response is not guaranteed
                // (and not strictly what we are testing), this should not count towards our code coverage analysis.
                $country = Country::find(strtolower($geoip['country']['iso_code']));
                $city = str_replace('_', ' ', preg_replace('@^.+/([^/]+)$@', '$1', $country->default_timezone));
                $geoip['city'] = ['names' => ['en' => $city]];
                // @codeCoverageIgnoreEnd
            }

            // Match the city details.
            $this->assertArrayHasKey('city', $geoip);
            $this->assertArrayHasKey('names', $geoip['city']);
            $this->assertArrayHasKey('en', $geoip['city']['names']);
            if (is_string($test['city']['name'])) {
                $this->assertEquals($geoip['city']['names']['en'], $test['city']['name']);
            } else {
                $this->assertContains($geoip['city']['names']['en'], $test['city']['name']);
            }

            // Match the country code.
            $this->assertArrayHasKey('country', $geoip);
            $this->assertArrayHasKey('iso_code', $geoip['country']);
            $this->assertEquals($test['country']['code'], $geoip['country']['iso_code']);

            // Match the country name.
            $this->assertArrayHasKey('country', $geoip);
            $this->assertArrayHasKey('names', $geoip['country']);
            $this->assertArrayHasKey('en', $geoip['country']['names']);
            $this->assertEquals($test['country']['name'], $geoip['country']['names']['en']);

            // Match the continent code.
            $this->assertArrayHasKey('continent', $geoip);
            $this->assertArrayHasKey('code', $geoip['continent']);
            $this->assertEquals($test['continent']['code'], $geoip['continent']['code']);

            // Match the continent name.
            $this->assertArrayHasKey('continent', $geoip);
            $this->assertArrayHasKey('names', $geoip['continent']);
            $this->assertArrayHasKey('en', $geoip['continent']['names']);
            $this->assertEquals($test['continent']['name'], $geoip['continent']['names']['en']);
        }
    }

    /**
     * Update internal objects between tests
     * @return void
     */
    protected function setUp(): void
    {
        // Load the standard test setup methods.
        parent::setUp();
        // Tweak the config.
        $this->config = FrameworkConfig::get('debear');
        Config::loadSite();
    }

    /**
     * Restore changes made during the setUp phase
     * @return void
     */
    protected function tearDown(): void
    {
        // Restore our configuration changes.
        FrameworkConfig::set(['debear' => $this->config]);
        // Run our standard restoration.
        parent::tearDown();
    }
}
