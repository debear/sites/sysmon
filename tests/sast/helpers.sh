# Additional helpers to the standard methods found in _scripts

# Determine our colour coding escape sequences
[ -t 1 ] && esc_red='\e[31m' || esc_red=
[ -t 1 ] && esc_ylw='\e[33m' || esc_yel=
[ -t 1 ] && esc_blu='\e[36m' || esc_blu=
[ -t 1 ] && esc_cnc='\e[0m'  || esc_cnc=

# Parse the supplied command line arguments
mode=last # By default, process the the last artifacts we have available
pipeline_id=
summary=0
function parse_args() {
    while [[ $# -gt 0 ]]
    do
        if [ ! -z $( echo "$1" | grep -Po '^(list|latest|emulated)$' ) ]
        then
            # Perform the specific action ("list" shows available reports, "latest" download upstream pipeline runs to get the latest artifacts from the source-of-truth)
            [ $mode != 'last' ] && display_warning "Overlapping pipeline arguments supplied: previous mode '${mode}' ignored"
            [ "$1" != 'latest' ] && mode="$1" || mode=process
        elif [ $(echo "$1" | grep -Pc '^[1-9][0-9]{8,}$') -eq 1 ]
        then
            # We have been passed an ID to process
            # Warn, if we have already been supplied an argument, what is going to be over-ridden
            [ $mode != 'last' ] && display_warning "Overlapping pipeline arguments supplied: previous ID '${pipeline_id:-latest}' ignored"
            if [ $(ls -d $dir_runs/20??-??-??_$1 2>/dev/null | wc -l) -eq 0 ]
            then
                # This is not a pipeline we have already processed
                display_error "Argument '$1' is not a pipeline ID we have previously processed" >&2
                exit 1
            fi
            # Looks valid, so proceed
            mode=process
            pipeline_id="$1"
            dir_run=$(ls -d $dir_runs/20??-??-??_$pipeline_id)
            pipeline_date=$(echo "$dir_run" | grep -Po '20[0-9]{2}-[0-9]{2}-[0-9]{2}')
        elif [ "x$1" = 'x--summary' ] && [ $summary -eq 0 ]
        then
            summary=1
        else
            display_warning "Unknown argument '$1' will be ignored"
        fi
        shift
    done
}

declare -A counts
function analyse_report() {
    job_file="$1"
    declare -A job_counts

    # Using JQ, parse for list
    vulnerabilities=$(zcat $job_file | jq -r '.vulnerabilities[] | "\(.severity)"')
    if [ ! -z "$vulnerabilities" ]
    then
        # Update our counters
        job_counts=()
        for by_sev in $(echo "$vulnerabilities" | sort -n | uniq -c | sed -r 's/^\s+([0-9]+)\s+(.+)$/\2;\1/')
        do
            sev=$(echo "$by_sev" | awk -F';' '{ print $1 }')
            job_counts[$sev]=$(echo "$by_sev" | awk -F';' '{ print $2 }')
            counts[$sev]=$(( counts[$sev] + job_counts[$sev] ))
        done

        # Render, if we're not summarising counts
        if [ $summary -eq 0 ]
        then
            display_count_table "${job_counts[Critical]}" "${job_counts[High]}" "${job_counts[Medium]}" "${job_counts[Low]}" "${job_counts[Info]}" "${job_counts[Unknown]}"
            echo
            zcat $job_file | jq -r '.vulnerabilities[] | "\(.severity)#\(.location.file):\(.location.start_line)#\(.name)"' \
                | sed -r -e "s/^(Critical|High)/${esc_red}&${esc_cnc}/g" \
                    -e "s/^(Medium)/${esc_ylw}&${esc_cnc}/g" \
                    -e "s/^(Low|Info|Unknown)/${esc_blu}&${esc_cnc}/g" \
                    -e 's/e\[([0-9]+)m/\x1b[\1m/g' \
                | column --table --separator '#' --output-separator ' | '
        fi
    elif [ $summary -eq 0 ]
    then
        display_info "No vulnerabilities identified"
    fi
}

function summarise_report() {
    [ ! -z "$pipeline_id" ] && m=display_section || m=display_info
    # Summarise total number by category across all jobs
    echo; $m "Overall counts by severity:"
    display_count_table "${counts[Critical]}" "${counts[High]}" "${counts[Medium]}" "${counts[Low]}" "${counts[Info]}" "${counts[Unknown]}"
}

# Render a table of vulnerability counts by severity
function display_count_table() {
    num_critical="$1"
    num_high="$2"
    num_medium="$3"
    num_low="$4"
    num_info="$5"
    num_unknown="$6"

    titles="${esc_red}Critical${esc_cnc},${esc_red}High${esc_cnc},${esc_ylw}Medium${esc_cnc},${esc_blu}Low${esc_cnc},${esc_blu}Info${esc_cnc}"
    values="${esc_red}${num_critical:-0}${esc_cnc},${esc_red}${num_high:-0}${esc_cnc},${esc_ylw}${num_medium:-0}${esc_cnc},${esc_blu}${num_low:-0}${esc_cnc},${esc_blu}${num_info:-0}${esc_cnc}"
    if [ ! -z "$num_unknown" ] && [ $num_unknown -gt 0 ]
    then
        titles="$titles,${esc_blu}Unknown${esc_cnc}"
        values="$values,${esc_blu}${num_unknown:-0}${esc_cnc}"
    fi
    echo -e "$titles\n$values" | column --table --separator ',' --output-separator '  '
}
