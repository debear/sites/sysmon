#
# Base Dump Archive
#
mysqldump debearco_sysmon | gzip >base.sql.gz

#
# Individual Status Components
#
mysqldump --no-create-info --skip-extended-insert --complete-insert debearco_sysmon SYSMON_STATUS_GROUPS | grep -F 'INSERT INTO' | sed -r 's/^INSERT INTO.+VALUES /  /g' | less -S
mysqldump --no-create-info --skip-extended-insert --complete-insert debearco_sysmon SYSMON_STATUS_ITEMS | grep -F 'INSERT INTO' | sed -r 's/^INSERT INTO.+VALUES /  /g' | less -S

mysqldump --no-create-info --skip-extended-insert --complete-insert debearco_sysmon SYSMON_INCIDENTS | grep -F 'INSERT INTO' | sed -r -e 's/^INSERT INTO.+VALUES /  /g' -e 's/;$/,/' | less -S
mysqldump --no-create-info --skip-extended-insert --complete-insert debearco_sysmon SYSMON_INCIDENTS_LEDGER | grep -F 'INSERT INTO' | sed -r -e 's/^INSERT INTO.+VALUES /  /g' -e 's/;$/,/' | less -S

#
# Modified Incidents
#
- SP-1936202; Status: 'warn' to 'critical'
- DBR-1935705; Status: 'warn' to 'critical'; Resolved: NULL; Is Current: 1
