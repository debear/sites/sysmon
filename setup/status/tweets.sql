USE debearco_common;
SET @app := 'sysmon_status';

# Incident Opened
SET @tweet_type := 'incident_raised';
DELETE FROM `COMMS_TWITTER_TEMPLATE` WHERE `app` = @app AND `tweet_type` = @tweet_type;

INSERT INTO `COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@app, @tweet_type, NULL, '&#x1F6A8; New Incident:

{ref_code} &ndash; {group} &raquo; {item} &ndash; {title}

Full details and updates on {domain}.'),
  (@app, @tweet_type, NULL, '&#x1F6A8; Incident Raised:

{ref_code} &ndash; {group} &raquo; {item} &ndash; {title}

See {domain} for details and updates.'),
  (@app, @tweet_type, NULL, '&#x1F6A8; Incident Reported:

{ref_code} &ndash; {group} &raquo; {item} &ndash; {title}

More information on {domain}.'),
  (@app, @tweet_type, NULL, '&#x1F6A8; Incident Identified:

{ref_code} &ndash; {group} &raquo; {item} &ndash; {title}

More details and updates on {domain}.'),
  (@app, @tweet_type, NULL, '&#x1F6A8; Incident Discovered:

{ref_code} &ndash; {group} &raquo; {item} &ndash; {title}

We&#39;ll keep {domain} up-to-date with the latest updates.');

# Incident Closed
SET @tweet_type := 'incident_resolved';
DELETE FROM `COMMS_TWITTER_TEMPLATE` WHERE `app` = @app AND `tweet_type` = @tweet_type;

INSERT INTO `COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@app, @tweet_type, NULL, '&#x1F197; Incident Cleared:

{ref_code} &ndash; {group} &raquo; {item} &ndash; {title}'),
  (@app, @tweet_type, NULL, '&#x1F197; Incident Closed:

{ref_code} &ndash; {group} &raquo; {item} &ndash; {title}'),
  (@app, @tweet_type, NULL, '&#x1F197; Incident Resolved:

{ref_code} &ndash; {group} &raquo; {item} &ndash; {title}'),
  (@app, @tweet_type, NULL, '&#x1F197; Incident Fixed:

{ref_code} &ndash; {group} &raquo; {item} &ndash; {title}'),
  (@app, @tweet_type, NULL, '&#x1F197; Incident Concluded:

{ref_code} &ndash; {group} &raquo; {item} &ndash; {title}');
