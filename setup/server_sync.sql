SET @sync_app_base := 'sysmon';

#
# Latest SysMon data
#
SET @sync_app := CONCAT(@sync_app_base, '_data');

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'System Monitor Data', 'live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    (@sync_app,  1, '__DIR__ SYSMON_CHECKS', 1, 'database'),
    (@sync_app,  2, '__DIR__ SYSMON_CHECKS_GROUPS', 2, 'database'),
    (@sync_app, 19, '__DIR__ SYSMON_CHECKS_ADDITIONAL', 3, 'database'),
    (@sync_app,  3, '__DIR__ SYSMON_LOG', 4, 'database'),
    (@sync_app,  4, '__DIR__ SYSMON_USAGE', 5, 'database'),
    (@sync_app,  5, '__DIR__ SYSMON_COMMS', 6, 'database'),
    (@sync_app,  6, '__DIR__ SYSMON_LOCKFILE', 7, 'database'),
    (@sync_app,  7, '__DIR__ SYSMON_SERVER', 8, 'database'),
    (@sync_app,  8, '__DIR__ SYSMON_LOGFILE', 9, 'database'),
    (@sync_app,  9, '__DIR__ SYSMON_LOGFILE_BREAKDOWN', 10, 'database'),
    (@sync_app, 11, '__DIR__ SYSMON_HTTP_REQUEST', 11, 'database'),
    (@sync_app, 23, '__DIR__ SYSMON_HTTP_STATUS', 12, 'database'),
    (@sync_app, 20, '__DIR__ SYSMON_AGE', 13, 'database'),
    (@sync_app, 10, '__DIR__ SYSMON_NOTIFICATION_SILENT', 14, 'database'),
    (@sync_app, 12, '__DIR__ SYSMON_STATUS_GROUPS', 15, 'database'),
    (@sync_app, 21, '__DIR__ SYSMON_STATUS_GROUPS_METRICS', 16, 'database'),
    (@sync_app, 13, '__DIR__ SYSMON_STATUS_ITEMS', 17, 'database'),
    (@sync_app, 22, '__DIR__ SYSMON_STATUS_ITEMS_METRICS', 18, 'database'),
    (@sync_app, 14, '__DIR__ SYSMON_CHECKS_STATUS_OVERRIDE', 19, 'database'),
    (@sync_app, 15, '__DIR__ SYSMON_INCIDENTS', 20, 'database'),
    (@sync_app, 16, '__DIR__ SYSMON_INCIDENTS_LEDGER', 21, 'database'),
    (@sync_app, 17, '__DIR__ COMMS_TWITTER_TEMPLATE', 22, 'database');
    -- (@sync_app, 18, '__DIR__ COMMS_TWITTER', 31, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    (@sync_app, 1, 'debearco_sysmon', 'SYSMON_CHECKS', ''),
    (@sync_app, 2, 'debearco_sysmon', 'SYSMON_CHECKS_GROUPS', ''),
    (@sync_app, 3, 'debearco_sysmon', 'SYSMON_LOG', ''),
    (@sync_app, 4, 'debearco_sysmon', 'SYSMON_USAGE', ''),
    (@sync_app, 5, 'debearco_sysmon', 'SYSMON_COMMS', ''),
    (@sync_app, 6, 'debearco_sysmon', 'SYSMON_LOCKFILE', ''),
    (@sync_app, 7, 'debearco_sysmon', 'SYSMON_SERVER', ''),
    (@sync_app, 8, 'debearco_sysmon', 'SYSMON_LOGFILE', ''),
    (@sync_app, 9, 'debearco_sysmon', 'SYSMON_LOGFILE_BREAKDOWN', ''),
    (@sync_app, 10, 'debearco_sysmon', 'SYSMON_NOTIFICATION_SILENT', ''),
    (@sync_app, 11, 'debearco_sysmon', 'SYSMON_HTTP_REQUEST', ''),
    (@sync_app, 12, 'debearco_sysmon', 'SYSMON_STATUS_GROUPS', ''),
    (@sync_app, 13, 'debearco_sysmon', 'SYSMON_STATUS_ITEMS', ''),
    (@sync_app, 14, 'debearco_sysmon', 'SYSMON_CHECKS_STATUS_OVERRIDE', ''),
    (@sync_app, 15, 'debearco_sysmon', 'SYSMON_INCIDENTS', ''),
    (@sync_app, 16, 'debearco_sysmon', 'SYSMON_INCIDENTS_LEDGER', ''),
    (@sync_app, 17, 'debearco_common', 'COMMS_TWITTER_TEMPLATE', 'app LIKE ''sysmon%'''),
    -- (@sync_app, 18, 'debearco_common', 'COMMS_TWITTER', 'app LIKE ''sysmon%'''),
    (@sync_app, 19, 'debearco_sysmon', 'SYSMON_CHECKS_ADDITIONAL', ''),
    (@sync_app, 20, 'debearco_sysmon', 'SYSMON_AGE', ''),
    (@sync_app, 21, 'debearco_sysmon', 'SYSMON_STATUS_GROUPS_METRICS', ''),
    (@sync_app, 22, 'debearco_sysmon', 'SYSMON_STATUS_ITEMS_METRICS', ''),
    (@sync_app, 23, 'debearco_sysmon', 'SYSMON_HTTP_STATUS', '');

#
# Latest browser report data
#
SET @sync_app := CONCAT(@sync_app_base, '_browser');

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'System Monitor Browser Reports', 'live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    (@sync_app, 1, '__DIR__ REPORTS_INSTANCE', 1, 'database'),
    (@sync_app, 2, '__DIR__ REPORTS_CSP', 2, 'database'),
    (@sync_app, 3, '__DIR__ REPORTS_JS', 3, 'database'),
    (@sync_app, 4, '__DIR__ REPORTS_PHP', 4, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    (@sync_app, 1, 'debearco_sysmon', 'REPORTS_INSTANCE', ''),
    (@sync_app, 2, 'debearco_sysmon', 'REPORTS_CSP', ''),
    (@sync_app, 3, 'debearco_sysmon', 'REPORTS_JS', ''),
    (@sync_app, 4, 'debearco_sysmon', 'REPORTS_PHP', '');
