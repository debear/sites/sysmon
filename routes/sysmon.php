<?php

/**
 * Routes for the SysMon sub-domain
 */

Route::get('/', 'SysMon\App@index')
    ->middleware('auth:user:admin');

// Details of an individual check.
Route::get('/check/{name}-{id}', 'SysMon\App@check')
    ->where('name', '[a-z\-]+')
    ->where('id', '[0-9]+')
    ->middleware('auth:user:admin');
Route::get('/check/{name}-{id}/page/{page}', 'SysMon\App@check')
    ->where('name', '[a-z\-]+')
    ->where('id', '[0-9]+')
    ->where('page', '[0-9]+')
    ->middleware('auth:user:admin');

// Granular log for an individual check.
Route::get('/check/{name}-{id}/logs', 'SysMon\App@logs')
    ->where('name', '[a-z\-]+')
    ->where('id', '[0-9]+')
    ->middleware('auth:user:admin');
Route::get('/check/{name}-{id}/logs/page/{page}', 'SysMon\App@logs')
    ->where('name', '[a-z\-]+')
    ->where('id', '[0-9]+')
    ->where('page', '[0-9]+')
    ->middleware('auth:user:admin');
