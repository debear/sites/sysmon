<?php

return [
    /*
     * Site name details
     */
    'names' => [
        'site' => 'DeBear System Monitor',
        'section' => 'DeBear System Monitor',
    ],

    /*
     * Site-specific resource additions
     */
    'resources' => [
        'css' => [
            'layout.css',
            'common.css',
        ],
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'navigation' => [
                'enabled' => false,
            ],
            'descrip' => 'The latest status of DeBear.uk',
        ],
        'register' => [
            'enabled' => false,
        ],
        'sitemap' => [
            'enabled' => false,
        ],
    ],

    /*
     * Site revision info
     */
    'version' => [
        'breakdown' => [
            'major' => 2,
            'minor' => 0,
        ],
    ],

    /*
     * Google Analytics
     */
    'analytics' => [
        'urchins' => [
            'tracking' => [ 'UA-48789782-12' ],
        ],
    ],

    /*
     * Site Setup
     */
    'setup' => [
        'chart-points' => 90,
    ],
];
