<?php

/*
 * Secondary Status site config (to be loaded manually)
 */

use DeBear\Helpers\HTTP;

return [
    /*
     * Site name details
     */
    'names' => [
        'site' => 'DeBear.uk System Status',
        'section' => 'DeBear.uk System Status',
    ],

    /*
     * Site-specific resource additions
     */
    'resources' => [
        'css' => [
            'common.css',
        ],
    ],

    /*
     * Date/Time setup
     */
    'datetime' => [
        'timezone_db' => 'UTC',
    ],

    /*
     * Incident configuration
     */
    'incidents' => [
        'recency' => [
            'from' => (!HTTP::isTest() ? '-1 month' : '2018-01-01'), // When testing, a fixed date before the first row.
            'number' => 5,
        ],
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'navigation' => [
                'enabled' => false,
            ],
            'descrip' => 'The site-wide system status of DeBear.uk',
        ],
        'register' => [
            'enabled' => false,
        ],
        'sitemap' => [
            'enabled' => false,
        ],
        'status' => [
            'footer' => false,
        ],
    ],

    /*
     * Site revision info
     */
    'version' => [
        'breakdown' => [
            'major' => 1,
            'minor' => 0,
        ],
    ],

    /*
     * Google Analytics
     */
    'analytics' => [
        'urchins' => [
            'tracking' => [ 'UA-48789782-14' ],
        ],
    ],
];
