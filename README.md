# DeBear System Monitor

An internal site with the SitRep of key processes, with logging, notification and monitoring.

## Status

[![Version: 2.0](https://img.shields.io/badge/Version-2.0-brightgreen.svg)](https://gitlab.com/debear/sysmon/blob/master/CHANGELOG.md)
[![Build: 385](https://img.shields.io/badge/Build-385-yellow.svg)](https://gitlab.com/debear/sysmon/blob/master/CHANGELOG.md)
[![Skeleton: 1432](https://img.shields.io/badge/Skeleton-1432-orange.svg)](https://gitlab.com/debear/skeleton)
[![Coding Style: PSR-12](https://img.shields.io/badge/Coding_Style-PSR--12-lightgrey.svg)](https://github.com/php-fig/fig-standards/blob/master/proposed/extended-coding-style-guide.md)
[![Pipeline Status](https://gitlab.com/debear/sysmon/badges/master/pipeline.svg)](https://gitlab.com/debear/sysmon/commits/master)
[![Coverage Report](https://gitlab.com/debear/sites/sysmon/badges/master/coverage.svg)](https://gitlab.com/debear/sites/sysmon/commits/master)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

## Motivation

A homebrew combination of Nagios, Report URI and Bugsnag. Not intended to be as feature rich as these services, it represents a basic status of various internal services with email notification on errors, 90 day status logging (though older is still archived as standard) and visual representation of key metrics for each service.

## Future Requirements

* Report URI filtering (CSP, CT(??) and PHP)

## Authors

* **Thierry Draper** - [@ThierryDraper](https://gitlab.com/thierrydraper)

## Contribute

Please read [CONTRIBUTING.md](https://gitlab.com/debear/sysmon/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is made available under the terms of the GNU Affero General Public License v3.0. Please see the [LICENSE](https://gitlab.com/debear/sysmon/blob/master/LICENSE) file for details.

GNU AGPLv3 © [Thierry Draper](https://gitlab.com/thierrydraper)
