# DeBear SysMon CI/CD Jobs

stages:
  - setup
  - syntax
  - security
  - standards
  - units
  - deploy

# External templates we use
include:
  - template: Jobs/SAST.gitlab-ci.yml

# Set some ENV variables
variables:
  # Configure MySQL environment variables (https://hub.docker.com/_/mysql/)
  MYSQL_DATABASE: test_debearco
  MYSQL_ROOT_PASSWORD: testingmysqlinci

# Workflow rules for disabling SAST jobs and pushes of tags
workflow:
  rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    variables:
      SAST_DISABLED: true
  - if: $CI_COMMIT_TAG
    when: never
  - when: always

# Our standard rules template for non-deployment jobs - when pushing a branch (not tag), but not the master branch
.testonly_rules:
  rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    when: never
  - if: $CI_COMMIT_BRANCH && $CI_PIPELINE_SOURCE == "push"
  - if: $CI_COMMIT_BRANCH && $CI_PIPELINE_SOURCE == "web"

# Setup composer
setup:
  stage: setup
  rules:
  - !reference [.testonly_rules, rules]
  image: registry.gitlab.com/debear/utils/ci-data/php-fpm:${PHP_VERSION}-latest
  cache:
    key: composer
    paths:
    - vendor/
    policy: pull-push
  before_script:
  # Install/Update the Composer dependencies
  - composer install
  script:
  - tests/setup

# PHP Parallel Lint (https://github.com/JakubOnderka/PHP-Parallel-Lint)
php_linting:
  stage: syntax
  rules:
  - !reference [.testonly_rules, rules]
  needs: ['setup']
  image: registry.gitlab.com/debear/utils/ci-data/php-fpm:${PHP_VERSION}-latest
  cache:
    key: composer
    paths:
    - vendor/
    policy: pull
  script:
  - vendor/bin/parallel-lint --short -e php config controllers exceptions models routes scripts tests/PHPUnit views
  - tests/blade/lint

# CSS (via stylelint) (https://github.com/stylelint/stylelint)
css_linting:
  stage: syntax
  rules:
  - !reference [.testonly_rules, rules]
  needs: ['setup']
  image: registry.gitlab.com/debear/utils/ci-data/npm-css:latest
  script:
  - stylelint --config tests/stylelint/linting.json --ignore-path tests/stylelint/ignore "resources/css/**/*.css"

# JS (https://eslint.org/docs/rules/)
js_linting:
  stage: syntax
  rules:
  - !reference [.testonly_rules, rules]
  needs: ['setup']
  image: pipelinecomponents/eslint
  script:
  - eslint --config tests/eslint/linting.js --ignore-pattern resources/js/merged --ignore-pattern resources/js/highcharts resources/js

# Perl (via 'perl -c' wrapper script)
perl_linting:
  stage: syntax
  rules:
  - !reference [.testonly_rules, rules]
  needs: ['setup']
  image: registry.gitlab.com/debear/utils/ci-data/perl:latest
  script:
  - tests/perl/lint scripts

# MySQL (via mysql import wrapper script)
mysql_linting:
  stage: syntax
  rules:
  - !reference [.testonly_rules, rules]
  needs: ['setup']
  image: registry.gitlab.com/debear/utils/ci-data/php-fpm:${PHP_VERSION}-latest
  services:
  - name: mysql:$MYSQL_VERSION
    command: ['--character-set-server=latin1', '--collation-server=latin1_general_ci', '--skip-character-set-client-handshake', '--disable-log-bin']
  cache:
    key: composer
    paths:
    - vendor/
    policy: pull
  script:
  - . tests/mysql/setenv
  - tests/mysql/setup
  - tests/mysql/schema
  #- tests/mysql/lint
  - tests/mysql/clean

# Composer In-built
# Enlightn Security Checker (https://github.com/enlightn/security-checker)
php_security:
  stage: security
  rules:
  - !reference [.testonly_rules, rules]
  needs: ['php_linting']
  allow_failure: true # These should not necessarily prevent this run from completing, but flags future action will be required
  image: registry.gitlab.com/debear/utils/ci-data/php-fpm:${PHP_VERSION}-latest
  cache:
    key: composer
    paths:
    - vendor/
    policy: pull
  script:
  - composer audit
  - php vendor/bin/security-checker --no-interaction security:check

# NPM In-built
#js_security:
#  stage: security
#  rules:
#  - !reference [.testonly_rules, rules]
#  needs: ['js_linting']
#  allow_failure: true # These should not necessarily prevent this run from completing, but flags future action will be required
#  image: node:current-alpine
#  script:
#  - npm --prefix path/to/dir audit

# GitLab-managed SAST (customisations to the CI template)
sast:
  stage: security
  artifacts:
    paths:
    - gl-sast-report.json

# PHP PSR-12 (via PHP_CodeSniffer) (https://github.com/squizlabs/PHP_CodeSniffer)
# PHP Mess Detector (https://phpmd.org/)
# Phan (https://github.com/phan/phan, via skeleton)
php_standards:
  stage: standards
  rules:
  - !reference [.testonly_rules, rules]
  needs: ['php_security']
  image: registry.gitlab.com/debear/utils/ci-data/php-fpm:${PHP_VERSION}-latest
  cache:
    key: composer
    paths:
    - vendor/
    policy: pull
  script:
  - vendor/bin/phpcs -p --standard=tests/phpcs/ruleset.xml --extensions=php config controllers exceptions models routes scripts tests/PHPUnit
  - vendor/bin/phpcs -p --standard=tests/phpcs/faux_psr-4.xml --extensions=php exceptions models tests/PHPUnit
  - vendor/bin/phpmd config,controllers,exceptions,models,routes,tests/PHPUnit,views text tests/phpmd/ruleset.xml
  - tests/phan/run

# CSS Standards (via stylelint) (https://github.com/stylelint/stylelint)
css_standards:
  stage: standards
  rules:
  - !reference [.testonly_rules, rules]
  needs: ['css_linting']
  image: registry.gitlab.com/debear/utils/ci-data/npm-css:latest
  script:
  - stylelint --config tests/stylelint/standards.json --ignore-path tests/stylelint/ignore "resources/css/**/*.css"
  - prettier --check --config tests/prettier/css_standards.json --ignore-path tests/prettier/css_ignore "resources/css/**/*.css"

# JS Standards (https://eslint.org/docs/rules/)
js_standards:
  stage: standards
  rules:
  - !reference [.testonly_rules, rules]
  needs: ['js_linting'] # Switch to js_security when enabled
  image: pipelinecomponents/eslint
  script:
  - eslint --config tests/eslint/standards.js --ignore-pattern resources/js/merged --ignore-pattern resources/js/highcharts resources/js

# PHP Unit Tests (https://phpunit.de/)
php_unit:
  stage: units
  rules:
  - !reference [.testonly_rules, rules]
  needs: ['php_standards']
  coverage: '/^\s*Lines:\s*(\d+.\d+)\%/'
  image: registry.gitlab.com/debear/utils/ci-data/php-fpm:${PHP_VERSION}-latest
  services:
  - name: mysql:$MYSQL_VERSION
    command: ['--character-set-server=latin1', '--collation-server=latin1_general_ci', '--skip-character-set-client-handshake', '--disable-log-bin']
  cache:
    key: composer
    paths:
    - vendor/
    policy: pull-push
  before_script:
  # Force locale to en_GB
  - export LANG=en_GB.UTF-8
  # Enable PCOV for code coverage
  - echo -e "[pcov]\nextension=\"$(ls /usr/local/lib/php/extensions/no-debug-non-zts-*/pcov.so | head)\"" >/usr/local/etc/php/conf.d/debear-coverage.ini
  # Setup the status subsite alias
  - tests/_scripts/aliases.sh
  script:
  - tests/PHPUnit/setup
  - . tests/mysql/setenv
  - tests/mysql/seed skeleton
  - tests/mysql/seed
  - tests/PHPUnit/run
  - tests/mysql/clean

# Deploy (Master only, and only on success of all tests)
deploy:
  stage: deploy
  environment:
    name: production
    url: https://sysmon.debear.uk
  rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  coverage: '/^\s*Lines:\s*(\d+.\d+)\%/'
  image: registry.gitlab.com/debear/utils/ci-data/php-fpm:${PHP_VERSION}-latest
  cache:
    key: composer
    paths:
    - vendor/
    policy: pull
  before_script:
  - eval $(ssh-agent -s)
  # Configure our SSH keys
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - echo -e "Host deployer\n\tStrictHostKeyChecking no\n\tHostName $DEPLOY_SSH_HOST\n\tPort $DEPLOY_SSH_PORT\n\tUser $DEPLOY_SSH_USER\n" > ~/.ssh/config
  - echo "$DEPLOY_SSH_KEY_PRIVATE" > ~/.ssh/id_rsa
  - chmod 600 ~/.ssh/id_rsa
  # Automate repo determination
  - repo=$(tests/_scripts/git.sh repo)
  - commit=$(tests/_scripts/git.sh commit)
  script:
  - tests/PHPUnit/coverage
  - vendor/bin/envoy run deploy --repo="$repo" --commit="$commit"
