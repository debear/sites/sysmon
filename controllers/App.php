<?php

namespace DeBear\Http\Controllers\SysMon;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Strings;
use DeBear\Models\SysMon\CheckGroups;
use DeBear\Models\SysMon\Checks;

class App extends Controller
{
    /**
     * Homepage list of all checks
     * @return Response The relevant response, which could be a page asking the user to log in
     */
    public function index(): Response
    {
        // Get and display the list.
        Resources::addCSS('app/home.css');
        HTML::noHeaderLink();
        HTML::linkMetaDescription('home');
        $checks = CheckGroups::with([
            'checks',
            'checks.notificationSnooze',
        ])->orderBy('order')->get();
        return response(view('sysmon.app.home', compact('checks')));
    }

    /**
     * The details of an individual check
     * @param string  $name Codified name for the check.
     * @param integer $id   The ID of the check we want to view.
     * @param integer $page Page number of info to list. (Optional).
     * @return Response The relevant response, which could be an HTTP error page
     */
    public function check(string $name, int $id, int $page = -1): Response
    {
        // Validate the check passed in, and get the details.
        $check = Checks::with([
            'logs',
            'logs.check',
        ])->find($id);
        if (!isset($check) || (Strings::codifyURL($check->name) != $name)) {
            return HTTP::sendNotFound();
        }
        // Validate the pagination.
        $perPage = 25;
        $resultsOnly = true;
        if ($page < 0) {
            $page = 1;
            $resultsOnly = false;
        }
        $allDetails = $check->details();
        $totalRows = sizeof($allDetails);
        $details = $allDetails->forPage($page, $perPage);
        if (!sizeof($details)) {
            return HTTP::sendNotFound();
        }

        // We have the potential for a subnav, so setup the base object.
        $subnav = [
            'list' => [],
            'default' => false,
            'tabs-below' => true,
        ];

        // All good, so display the list.
        $charts = [];
        if (!$resultsOnly) {
            HTML::setPageTitle($check->name);
            HTML::setMetaDescription("The detailed history about the {$check->name} "
                . FrameworkConfig::get('debear.names.site') . ' check');
            Resources::addCSS('skel/widgets/scrolltable.css');
            Resources::addCSS('app/check.css');
            Resources::addCSS('app/check/' . $check->script . '.css');
            Resources::addJS('skel/widgets/scrolltable.js');
            Resources::addJS('widgets/pagination.js');
            Resources::addJS('app/check.js');

            // Build the main chart.
            $charts[] = $check->buildHighchartsObject('chart-history');
            $charts[0]['subnav-code'] = 'history';

            // Any secondary charts?
            $extra_charts = $check->buildHighchartsExtraObjects();
            if (sizeof($extra_charts)) {
                // Enable the subnav component.
                $subnav['default'] = 'chart-' . $charts[0]['subnav-code'];
                $subnav['list'][$subnav['default']] = 'Check History';
                // Add to the list.
                foreach ($extra_charts as $chart) {
                    // Interpolate the $dom_id.
                    $dom_id = 'chart-' . $chart['subnav-code'];
                    $chart['chart']['renderTo'] = $dom_id;
                    // Add to the list.
                    $charts[] = $chart;
                    // And also add to the subnav list.
                    $subnav['list'][$dom_id] = $chart['subnav-name'];
                }
            }
        }
        $view = 'sysmon.app.check' . ($resultsOnly ? '.data' : '');
        return response(view($view, compact('check', 'charts', 'subnav', 'details', 'totalRows', 'page', 'perPage')));
    }

    /**
     * View the standardised logs of a particular check
     * @param string  $name Codified name for the check.
     * @param integer $id   The ID of the check we want to view.
     * @param integer $page Page number of info to list. (Optional).
     * @return Response The relevant response, which could be an HTTP error page
     */
    public function logs(string $name, int $id, int $page = -1): Response
    {
        // Validate the check passed in, and get the logs.
        $check = Checks::with([
            'logs',
            'logs.check',
        ])->find($id);
        if (!isset($check) || (Strings::codifyURL($check->name) != $name)) {
            return HTTP::sendNotFound();
        }
        // Validate the pagination.
        $perPage = 50;
        $resultsOnly = true;
        if ($page < 0) {
            $page = 1;
            $resultsOnly = false;
        }
        $allLogs = $check->logs->sortByDesc('log_date');
        $totalRows = sizeof($allLogs);
        $logs = $allLogs->forPage($page, $perPage);
        if (!sizeof($logs)) {
            return HTTP::sendNotFound();
        }

        // All good, so display the list.
        if (!$resultsOnly) {
            Resources::addCSS('app/logs.css');
            Resources::addJS('widgets/pagination.js');
            HTML::setPageTitle([$check->name, 'Logs']);
            HTML::setMetaDescription("The latest logs of the {$check->name} "
                . FrameworkConfig::get('debear.names.site') . ' check');
        }
        $view = 'sysmon.app.logs' . ($resultsOnly ? '.data' : '');
        return response(view($view, compact('check', 'logs', 'totalRows', 'page', 'perPage')));
    }
}
