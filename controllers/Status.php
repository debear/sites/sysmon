<?php

namespace DeBear\Http\Controllers\SysMon;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use DeBear\Http\Controllers\Controller;
use DeBear\Models\SysMon\Incidents;
use DeBear\Models\SysMon\StatusItem;
use DeBear\Models\SysMon\StatusGroup;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Policies;
use DeBear\Repositories\Time;

class Status extends Controller
{
    /**
     * Homepage list of all checks
     * @return Response The current system status information
     */
    public function index(): Response
    {
        Resources::addCSS('status/home.css');
        Resources::addCSS('status/item_icons.css');
        HTML::noHeaderLink();
        HTML::linkMetaDescription('home');

        // Get the current information out.
        $status_groups = StatusGroup::where('active', 1)->orderBy('order')->get();
        $status_items = StatusItem::where([
            ['active', '=', 1],
            ['admin_only', '<=', (int)Policies::match('user:admin')],
        ])->orderBy('order')->get();
        // Get the current incidents.
        $statuses = $status_items->pluck('status_id')->toArray();
        $incidents = Incidents::with(['statusItem', 'statusItem.group', 'ledger'])
            ->where('is_current', 1)
            ->whereIn('status_id', $statuses)
            ->orderBy('started')
            ->get();
        // And all recently resolved incidents too (up to a maximum number).
        $from = App::make(Time::class)->adjustFormatDatabase(FrameworkConfig::get('debear.incidents.recency.from'));
        $recent = Incidents::with(['statusItem', 'statusItem.group', 'ledger'])
            ->whereIn('status_id', $statuses)
            ->where([
                ['is_current', '=', 0],
                ['started', '>=', $from],
            ])
            ->orderByDesc('started')
            ->limit(FrameworkConfig::get('debear.incidents.recency.number'))
            ->get();

        // Setup the page subnav.
        $subnav = null;
        $inc_current = $incidents->count();
        $inc_recent = $recent->count();
        if ($inc_current || $inc_recent) {
            $subnav = [
                'list' => [],
                'default' => $inc_current ? 'current' : 'recent',
            ];
            // Determine the list options.
            if ($inc_current) {
                $subnav['list']['current'] = 'Current Incidents';
            }
            if ($inc_recent) {
                Resources::addJS('status/home-recent.js');
                $subnav['list']['recent'] = 'Recently Resolved';
            }
        }

        // Process.
        return response(view('sysmon.status.home', compact(
            'status_groups',
            'status_items',
            'incidents',
            'recent',
            'subnav'
        )));
    }
}
